package com.emts.chasebid;

/**
 * Created by theone on 2/22/2017.
 */

public interface UserDetailGet {
    public void onUserDetailResponse(boolean isSuccess);
}
