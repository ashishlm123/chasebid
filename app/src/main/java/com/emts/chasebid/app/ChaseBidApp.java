package com.emts.chasebid.app;

import android.app.Application;
import android.content.res.Configuration;
import android.provider.Settings;
import android.support.multidex.MultiDexApplication;
import android.view.ContextThemeWrapper;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerConversionListener;
import com.appsflyer.AppsFlyerLib;
import com.appsflyer.AppsFlyerProperties;
import com.emts.chasebid.helper.LocaleUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by theone on 7/4/2016.
 */
public class ChaseBidApp extends MultiDexApplication {

    //    private RequestQueue requestQueue;
    static ChaseBidApp mInstance;
    PreferenceHelper preferenceHelper;
    private static final String AF_DEV_KEY = "dJ9ded6h33rSzKt4HNEvzP";


    @Override
    public void onCreate() {
        super.onCreate();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        mInstance = this;
        preferenceHelper = PreferenceHelper.getInstance(this);

        LocaleUtils.setLocale(this, preferenceHelper.getString(PreferenceHelper.APP_USER_COUNTRY_CODE, "en"));
//        LocaleUtils.updateConfig(getBaseContext());


        AppsFlyerConversionListener conversionDataListener = new AppsFlyerConversionListener() {
            @Override
            public void onInstallConversionDataLoaded(Map<String, String> map) {

            }

            @Override
            public void onInstallConversionFailure(String s) {

            }

            @Override
            public void onAppOpenAttribution(Map<String, String> map) {

            }

            @Override
            public void onAttributionFailure(String s) {

            }
        };

        AppsFlyerLib.getInstance().init(AF_DEV_KEY, conversionDataListener);

        String senderId = "Your-Sender-ID"; // A.K.A Project Number;
        AppsFlyerLib.getInstance().enableUninstallTracking(senderId);

        AppsFlyerLib.getInstance().startTracking(this);

        String androidId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                Settings.Secure.ANDROID_ID);
        AppsFlyerLib.getInstance().setAndroidIdData(androidId);
        AppsFlyerLib.getInstance().setCollectAndroidID(true);
        AppsFlyerLib.getInstance().setCollectIMEI(true);

        if (preferenceHelper.isLogin()) {
            AppsFlyerLib.getInstance().setCustomerUserId(String.valueOf(preferenceHelper.getUserId()));
            AppsFlyerLib.getInstance().setUserEmails(AppsFlyerProperties.EmailsCryptType.MD5,
                    preferenceHelper.getUserEmail());
        }

    }


    public static synchronized ChaseBidApp getInstance() {
        return mInstance;
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);

//        LocaleUtils.updateConfig(getBaseContext());
        LocaleUtils.setLocale(this, preferenceHelper.getString(PreferenceHelper.APP_USER_COUNTRY_CODE, "en"));
    }

//    public RequestQueue getRequestQueue() {
//        if (requestQueue == null) {
//            requestQueue = Volley.newRequestQueue(getApplicationContext());
//        }
//        return requestQueue;
//    }
//
//    public void cancelPendingRequests(Object tag) {
//        if (requestQueue != null) {
//            requestQueue.cancelAll(tag);
//        }
//    }


    public void afTrackRegisterEvent(String userId, String regMethod) {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.CUSTOMER_USER_ID, userId);
        eventValue.put(AFInAppEventParameterName.REGSITRATION_METHOD, regMethod);
//        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, "1234567");
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.COMPLETE_REGISTRATION, eventValue);
    }

    public void afTrackPurchaseEvent(double revenue, String contentType, String contentId, String currency) {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put(AFInAppEventParameterName.REVENUE, revenue);
        eventValue.put(AFInAppEventParameterName.CONTENT_TYPE, contentType);
        eventValue.put(AFInAppEventParameterName.CONTENT_ID, contentId);
        eventValue.put(AFInAppEventParameterName.CURRENCY, currency);
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), AFInAppEventType.PURCHASE, eventValue);
    }

    public void afTrackBidding(String pId, String bidPrice, String bidDate, String singleOrMultiple) {
        Map<String, Object> eventValue = new HashMap<String, Object>();
        eventValue.put("productId", pId);
        eventValue.put("bidPrice", bidPrice);
        eventValue.put("bidDate", bidDate);
        eventValue.put("singleOrMultiple", singleOrMultiple);
        AppsFlyerLib.getInstance().trackEvent(getApplicationContext(), "eventBidPlaced", eventValue);
    }

}
