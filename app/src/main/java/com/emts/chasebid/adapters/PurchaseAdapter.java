package com.emts.chasebid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.model.Transaction;

import java.util.ArrayList;

/**
 * Created by Srijana on 3/27/2017.
 */

public class PurchaseAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    ArrayList<Transaction> purchaseList;
    private Context context;

    public PurchaseAdapter(ArrayList<Transaction> purchaseList, Context context) {
        this.purchaseList = purchaseList;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_user_purchase, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ViewHolder viewHolder = (ViewHolder) holder;
        Transaction transaction = purchaseList.get(position);
        viewHolder.invoiceId.setText(transaction.getInvoiceId());
        viewHolder.description.setText(transaction.getTransName());
        viewHolder.date.setText(transaction.getDate());
        viewHolder.credit.setText(transaction.getCreditUsed());
        viewHolder.bonusPoint.setText(transaction.getBidPoint());

    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView invoiceId, description, bonusPoint, date, credit;

        public ViewHolder(final View itemView) {
            super(itemView);

            invoiceId = (TextView) itemView.findViewById(R.id.txtinvoice);
            description = (TextView) itemView.findViewById(R.id.purchase_description);
            bonusPoint = (TextView) itemView.findViewById(R.id.purchase_bonus_credit);
            date = (TextView) itemView.findViewById(R.id.purchase_date);
            credit = (TextView) itemView.findViewById(R.id.purchase_credit);
        }
    }

    @Override
    public int getItemCount() {
        return purchaseList.size();

    }
}
