package com.emts.chasebid.adapters.searchCategory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import com.emts.chasebid.R;

/**
 * Created by shreedhar on 7/4/2016.
 */
public class SearchCategoryAdapter extends BaseExpandableListAdapter {

    private Context context;
    private ArrayList<CategoryItem> headerList;
    private ArrayList<List<CategoryItem>> childList;
    public SearchCategoryAdapter(Context context, ArrayList<CategoryItem> headerList, ArrayList<List<CategoryItem>> childList) {
        this.context = context;
        this.headerList = headerList;
        this.childList = childList;
    }

    @Override
    public int getGroupCount() {
        return this.headerList.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.childList.get(groupPosition).size();
    }

    @Override
    public Object getGroup(int groupPosition) {
        return this.headerList.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this.childList.get(groupPosition).get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return (long) headerList.get(groupPosition).getCategoryId();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return (long) childList.get(groupPosition).get(childPosition).getSubCategoryId();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
       CategoryItem headerModel = headerList.get(groupPosition);
        if (convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandablelist_group_sell_module,null);
        }

        TextView lblListHeader = (TextView) convertView.findViewById(R.id.lblListHeader);
        //lblListHeader.setTypeface(null,Typeface.BOLD);
        lblListHeader.setText(headerModel.getTitle());

        return convertView;
    }

    @Override
    public View getChildView(int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final CategoryItem childObj = (CategoryItem) getChild(groupPosition,childPosition);
        final String childName = childObj.getTitle();
        final int childIcon = childObj.getIcon();

        if(convertView == null){
            LayoutInflater inflater = (LayoutInflater) this.context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.expandablelist_child_sell_module,null);

        }
        TextView listChild = (TextView) convertView.findViewById(R.id.lblListItem);
        listChild.setText(childName);

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
