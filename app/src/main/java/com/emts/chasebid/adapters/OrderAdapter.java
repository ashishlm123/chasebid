package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2016-07-20.
 */
public class OrderAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    Context context;
    ArrayList<Product> orderLists;

    public OrderAdapter(Context context, ArrayList<Product> orderLists) {
        this.context = context;
        this.orderLists = orderLists;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_order_list, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Product product = orderLists.get(position);


        viewHolder.oName.setText(product.getpName());
        viewHolder.oDate.setText(product.getFavTime());
        viewHolder.oStatus.setText(product.getStatus());
        viewHolder.oTotal.setText(PreferenceHelper.getInstance(context).getCurrency() + " " + Utils.formatAmount(product.getpPrice()));

        if (!TextUtils.isEmpty(product.getImageUrl())) {
            Picasso.with(context).load(product.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.orderedImage);
        }
    }

    @Override
    public int getItemCount() {
        return orderLists.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView oName, oDate, oStatus, oTotal;
        ImageView orderedImage;


        public ViewHolder(View itemView) {
            super(itemView);

            oName = (TextView) itemView.findViewById(R.id.item_order_name);
            oDate = (TextView) itemView.findViewById(R.id.item_order_date);
            oStatus = (TextView) itemView.findViewById(R.id.item_order_status);
            oTotal = (TextView) itemView.findViewById(R.id.item_order_total);
            orderedImage = (ImageView) itemView.findViewById(R.id.item_order_image);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("product_id", orderLists.get(getLayoutPosition()).getpId());
                    context.startActivity(intent);
                }
            });
        }
    }
}
