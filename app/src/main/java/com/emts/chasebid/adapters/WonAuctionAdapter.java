package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.activity.PaymentConfirmActivity;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.fragment.account.WonAuctions;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.model.WonAuction;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by Srijana on 3/29/2017.
 */

public class WonAuctionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private ArrayList<WonAuction> wList;
    private Context context;
    private PreferenceHelper preferenceHelper;

    public WonAuctionAdapter(ArrayList<WonAuction> wList, Context context) {
        this.wList = wList;
        this.context = context;
        preferenceHelper = PreferenceHelper.getInstance(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_won_auction_list, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        WonAuction wonAuction = wList.get(position);
        viewHolder.pName.setText(wonAuction.getpName());
        viewHolder.pPrice.setText(preferenceHelper.getCurrency() + " " + String.valueOf(wonAuction.getpPrice()));
        if (!TextUtils.isEmpty(wonAuction.getImageUrl())) {
            Picasso.with(context).load(wonAuction.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.pImage);
        }
        viewHolder.pDate.setText(wonAuction.getFavTime());
        if (wList.get(position).getPayment_status().equalsIgnoreCase("Completed")) {
            viewHolder.btnPay.setText(context.getString(R.string.paid));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView pName, pPrice, pDate, btnPay;
        ImageView pImage;

        public ViewHolder(View itemView) {
            super(itemView);

            pName = (TextView) itemView.findViewById(R.id.item_won_name);
            pPrice = (TextView) itemView.findViewById(R.id.item_won_price);
            pImage = (ImageView) itemView.findViewById(R.id.item_won_image);
            btnPay = (TextView) itemView.findViewById(R.id.btn_pay_now);
            pDate = (TextView) itemView.findViewById(R.id.date_time);
            btnPay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (!wList.get(getLayoutPosition()).getPayment_status().equalsIgnoreCase("Completed")) {
                        Intent intent = new Intent(context, PaymentConfirmActivity.class);
                        intent.putExtra("product_id", wList.get(getLayoutPosition()).getpId());
                        intent.putExtra("winner_id", (long) preferenceHelper.getUserId());
                        ((MainActivity) context).startActivityForResult(intent, WonAuctions.PAYMENT_CONFIRM_REQ_CODE);
                    }
                }
            });

            pName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("product_id", wList.get(getLayoutPosition()).getpId());
                    intent.putExtra("is_closed", true);
                    context.startActivity(intent);

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return wList.size();
    }
}
