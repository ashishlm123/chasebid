package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.activity.PaymentConfirmActivity;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.model.Product;
import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

/**
 * Created by User on 2016-07-19.
 */
public class FavListAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<Product> favList;
    private Context context;
    private boolean isFavList;
    public static final int PAYMENT_ACTIVITY_REQUEST_CODE = 578;
    public static final int PRODUCT_DETAIL_REQUEST_CODE = 528;
    private OnRecyclerViewItemClickListener listener;
    private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

    public FavListAdapter(Context context, ArrayList<Product> favList, boolean isFavList) {
        this.context = context;
        this.favList = favList;
        this.isFavList = isFavList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_fav_list, null, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Product product = favList.get(position);
        viewHolder.pName.setText(product.getpName());
        if (!TextUtils.isEmpty(product.getImageUrl())) {
            Picasso.with(context).load(product.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.pImage);
        }
        viewHolder.pAddedDate.setText(product.getFavTime());
        Date startdate = new Date(product.getStartTime());
        Date enddate = new Date(product.getEndTime());

        viewHolder.pStartDate.setText(sdf.format(startdate));
        viewHolder.pEndDate.setText(sdf.format(enddate));
    }

    @Override
    public int getItemCount() {
        return favList.size();
    }

    @Override
    public long getItemId(int position) {
        return favList.get(position).getId();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView pName, pAddedDate, remove, status, buy, expireTime, pStartDate, pEndDate;
        ImageView pImage;

        public ViewHolder(View itemView) {
            super(itemView);

            pName = (TextView) itemView.findViewById(R.id.item_fav_name);
            pAddedDate = (TextView) itemView.findViewById(R.id.item_fav_added_date);
            pImage = (ImageView) itemView.findViewById(R.id.item_fav_image);
            pStartDate = (TextView) itemView.findViewById(R.id.item_fav_start_date);
            pEndDate = (TextView) itemView.findViewById(R.id.item_fav_end_date);
            remove = (TextView) itemView.findViewById(R.id.item_fav_remove);
            status = (TextView) itemView.findViewById(R.id.item_status);
            buy = (TextView) itemView.findViewById(R.id.item_buy);
            expireTime = (TextView) itemView.findViewById(R.id.expire_date);

            buy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, PaymentConfirmActivity.class);
                    intent.putExtra("product_id", favList.get(getLayoutPosition()).getpId());
                    intent.putExtra("winner_id", favList.get(getLayoutPosition()).getWinnerId());
                    ((MainActivity) context).startActivityForResult(intent, PAYMENT_ACTIVITY_REQUEST_CODE);
                }
            });
            if (isFavList) {
                remove.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
//                        removeFromFavTask(favList.get(getLayoutPosition()).getId(), getLayoutPosition());
                        if (listener != null) {
                            listener.onRemoveFav(favList.get(getLayoutPosition()).getpId(), getLayoutPosition());
                        }
                    }
                });
            } else {
                pName.setTextColor(context.getResources().getColor(R.color.transparent_black));
                remove.setTextColor(context.getResources().getColor(R.color.colorAccent));
            }

            if (isFavList) {
                itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        Intent intent = new Intent(context, ProductDetail.class);
                        intent.putExtra("product_id", favList.get(getLayoutPosition()).getpId());
                        ((MainActivity) context).startActivityForResult(intent, PRODUCT_DETAIL_REQUEST_CODE);
                    }
                });
            }
        }
    }


    public interface OnRecyclerViewItemClickListener {
        void onRemoveFav(long id, int position);
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener listener) {
        this.listener = listener;
    }
}
