package com.emts.chasebid.adapters;

import android.animation.Animator;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 2016-06-28.
 */
public class LiveProductAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Product> pList;
    private OnRecyclerViewItemClickListener clickListener;
    private long serverTime;
    private PreferenceHelper helper;
    private boolean fillParent;
    private LinearLayout.LayoutParams lpFill;

    public LiveProductAdapter(Context context, ArrayList<Product> pList) {
        this.context = context;
        this.pList = pList;
        helper = PreferenceHelper.getInstance(context);

        lpFill = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFill.setMargins((int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12),
                (int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12));
    }

    public void setFillParent(boolean fillParent) {
        this.fillParent = fillParent;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        view = LayoutInflater.from(context).inflate(R.layout.item_live_product2, parent, false);
        if (fillParent) {
//            view = LayoutInflater.from(context).inflate(R.layout.item_live_product_fill, parent, false);
            view.setLayoutParams(lpFill);
        } else {
//            view = LayoutInflater.from(context).inflate(R.layout.item_live_product2, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final Product product = pList.get(position);

        Picasso.with(context).load(product.getImageUrl())
                .placeholder(R.drawable.placeholder)
                .error(R.drawable.placeholder)
                .into(viewHolder.pImage);

        viewHolder.pName.setText(product.getpName());
        if (product.getBidFee() > 1) {
            viewHolder.pBidFee.setText(String.valueOf(product.getBidFee()) + " " + context.getString(R.string.title_credit));
        } else {
            viewHolder.pBidFee.setText(String.valueOf(product.getBidFee()) + " " + context.getString(R.string.credits));
        }
        viewHolder.pRetailPrice.setText(PreferenceHelper.getInstance(context).getCurrency() + " " + Utils.formatAmount(product.getpPrice()));

        if (product.isFavourite()) {
            viewHolder.iconFab.setImageResource(R.drawable.ic_favorite_selected);
        } else {
            viewHolder.iconFab.setImageResource(R.drawable.ic_favorite);
        }

        getRemainingTime(product.getEndTime(), viewHolder);
    }

    @Override
    public int getItemCount() {
        return pList.size();
    }

    @Override
    public long getItemId(int position) {
        return pList.get(position).getpId();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pImage, iconFab;
        TextView pName, pRetailPrice, pBidFee;
        TextView btnBid;
        TextView timerDay, timerHours, timerMinutes, timerSeconds;

        public ViewHolder(View itemView) {
            super(itemView);

            pImage = (ImageView) itemView.findViewById(R.id.item_product_image);
            if (fillParent) {
                pImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        (int) Utils.pxFromDp(context, 175)));
            }
            pName = (TextView) itemView.findViewById(R.id.p_name);
            pRetailPrice = (TextView) itemView.findViewById(R.id.item_retail_price);
            pBidFee = (TextView) itemView.findViewById(R.id.item_bid_fee);

            timerDay = (TextView) itemView.findViewById(R.id.timer_day);
            timerHours = (TextView) itemView.findViewById(R.id.timer_hours);
            timerMinutes = (TextView) itemView.findViewById(R.id.timer_minutes);
            timerSeconds = (TextView) itemView.findViewById(R.id.timer_seconds);

            //icons
            iconFab = (ImageView) itemView.findViewById(R.id.icon_fav);
            iconFab.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (helper.isLogin()) {
                        setFavouriteTask(pList.get(getLayoutPosition()).isFavourite(),
                                pList.get(getLayoutPosition()).getpId(), view, getLayoutPosition());
                    } else {
                        AlertUtils.showSnack(context, view, context.getString(R.string.log_to_ad_fav));
                    }
                }
            });

            btnBid = (TextView) itemView.findViewById(R.id.item_btn_bid);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        clickListener.onRecyclerViewItemClicked(view, getLayoutPosition(),
                                LiveProductAdapter.this.getItemId(getLayoutPosition()));
                    }
                }
            });
        }
    }

    private void setFavouriteTask(final boolean favourite, final long productId, final View view, final int position) {
        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        if (helper.isLogin()) {
            postParams.put("user_id", String.valueOf(helper.getUserId()));
            postParams.put("token", String.valueOf(helper.getToken()));
        }
        postParams.put("product_id", String.valueOf(productId));


        vHelper.addVolleyRequestListeners(Api.getInstance().favListUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        animateFav(view, position);
                    } else {
                        AlertUtils.showSnack(context, view, resObj.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("setFavouriteTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                Product product = pList.get(position);
                product.setFavourite(!product.isFavourite());
                if (product.isFavourite()) {
                    ((ImageView) view).setImageResource(R.drawable.ic_favorite);
                    product.setFavourite(false);
                } else {
                    ((ImageView) view).setImageResource(R.drawable.ic_favorite_selected);
                    product.setFavourite(true);
                }
            }
        }, "setFavouriteTask");
    }

    private void animateFav(View view, final int position) {
        final AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator imgScaleDownY = ObjectAnimator.ofFloat(view, "scaleY", 1f, 0f);
        imgScaleDownY.setDuration(300);
        imgScaleDownY.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator imgScaleDownX = ObjectAnimator.ofFloat(view, "scaleX", 1f, 0f);
        imgScaleDownX.setDuration(300);
        imgScaleDownX.setInterpolator(new DecelerateInterpolator());

        ObjectAnimator imgScaleUpY = ObjectAnimator.ofFloat(view, "scaleY", 0f, 1f);
        imgScaleUpY.setDuration(300);
        imgScaleUpY.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator imgScaleUpX = ObjectAnimator.ofFloat(view, "scaleX", 0f, 1f);
        imgScaleUpX.setDuration(300);
        imgScaleUpX.setInterpolator(new DecelerateInterpolator());

        view.setVisibility(View.VISIBLE);
        animatorSet.playTogether(imgScaleDownY, imgScaleDownX);
        if (pList.get(position).isFavourite()) {
            ((ImageView) view).setImageResource(R.drawable.ic_favorite);
            pList.get(position).setFavourite(false);
        } else {
            ((ImageView) view).setImageResource(R.drawable.ic_favorite_selected);
            pList.get(position).setFavourite(true);
        }
        animatorSet.play(imgScaleUpY).with(imgScaleUpX).after(imgScaleDownX);
        animatorSet.start();
        animatorSet.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {

            }

            @Override
            public void onAnimationEnd(Animator animator) {
                notifyItemChanged(position);
            }

            @Override
            public void onAnimationCancel(Animator animator) {

            }

            @Override
            public void onAnimationRepeat(Animator animator) {

            }
        });
    }

    public interface OnRecyclerViewItemClickListener {
        void onRecyclerViewItemClicked(View view, int position, long id);
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }

    //timmer
    private String getRemainingTime(long endDate, ViewHolder viewHolder) {
        if (endDate == 0) {
            return "end date not available";
        }

        if (serverTime < endDate) {
            long elapsedTime = Math.abs(endDate - serverTime);
            return ddHHMMSS(elapsedTime, viewHolder);
        } else {
            return "Auction Closed";
        }
    }

    private static String ddHHMMSS(long elapsedTimes, ViewHolder viewHolder) {
        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTimes);
        elapsedTimes -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTimes);
        elapsedTimes -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTimes);
        elapsedTimes -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTimes);
        String remainingTime = "";
        if (days != 0) {
            remainingTime = days + "d : ";
        }
        if (hours != 0) {
            remainingTime = remainingTime + hours + "h : ";
        }
        if (minute != 0) {
            remainingTime = remainingTime + minute + "m : ";
        }
        if (second != 0)
            remainingTime = remainingTime + second + "s";

        boolean last5minute = false;
        if (days == 0 && hours == 0 && minute < 5) {
            last5minute = true;
        } else {
            last5minute = false;
        }
        if (last5minute) {
            viewHolder.timerDay.setTextColor(Color.RED);
            viewHolder.timerHours.setTextColor(Color.RED);
            viewHolder.timerMinutes.setTextColor(Color.RED);
            viewHolder.timerSeconds.setTextColor(Color.RED);
        } else {
            viewHolder.timerDay.setTextColor(Color.WHITE);
            viewHolder.timerHours.setTextColor(Color.WHITE);
            viewHolder.timerMinutes.setTextColor(Color.WHITE);
            viewHolder.timerSeconds.setTextColor(Color.WHITE);
        }
        viewHolder.timerDay.setText(String.format("%02d", (int) days));
        viewHolder.timerHours.setText(String.format("%02d", (int) hours));
        viewHolder.timerMinutes.setText(String.format("%02d", (int) minute));
        viewHolder.timerSeconds.setText(String.format("%02d", (int) second));

        return remainingTime;
    }

    public void setServerTime(long time) {
        serverTime = time;
    }

}
