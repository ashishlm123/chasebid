package com.emts.chasebid.adapters.searchCategory;

import java.io.Serializable;

/**
 * Created by shreedhar on 7/4/2016.
 */
public class CategoryItem extends IconTitleModel implements Serializable {
    private int categoryId;
    private int subCategoryId;
    private String imageUrl;

    public CategoryItem(String title, int icon) {
        super(title, icon);
    }

    public CategoryItem(String title, String imageUrl) {
        super(title, imageUrl);
    }


    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(int categoryId) {
        this.categoryId = categoryId;
    }

    public int getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(int subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

}
