package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.fragment.MyDialogFragment;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.model.Product;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2016-07-01.
 */
public class OngoingAuctionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<Product> auctionList;
    public static int PRODUCT_DETAIL_REQUEST_CODE_FROM_AUCTION = 206;

    public OngoingAuctionAdapter(Context context, ArrayList<Product> auctionList) {
        this.context = context;
        this.auctionList = auctionList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_ongoing_auction, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Product auction = auctionList.get(position);

        viewHolder.pName.setText(auction.getpName());
        viewHolder.pPrice.setText(PreferenceHelper.getInstance(context).getCurrency() + " "
                + Utils.formatAmount(auction.getpPrice()));
        if (!TextUtils.isEmpty(auction.getImageUrl())) {
            Picasso.with(context).load(auction.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.pImage);
        }
    }

    @Override
    public int getItemCount() {
        return auctionList.size();
    }

    @Override
    public long getItemId(int position) {
        return auctionList.get(position).getId();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView pName, pPrice, viewDetail;
        ImageView pImage;

        public ViewHolder(View itemView) {
            super(itemView);

            pImage = (ImageView) itemView.findViewById(R.id.item_product_image);
            pName = (TextView) itemView.findViewById(R.id.p_name);
            pPrice = (TextView) itemView.findViewById(R.id.p_price);
            viewDetail = (TextView) itemView.findViewById(R.id.view_detail);

            viewDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    FragmentManager fm = ((MainActivity) context).getSupportFragmentManager();
                    MyDialogFragment dialogFragment = new MyDialogFragment();
                    Bundle bundle = new Bundle();
                    bundle.putString("auctionName", auctionList.get(getLayoutPosition()).getpName());
                    bundle.putLong("id", auctionList.get(getLayoutPosition()).getpId());
                    dialogFragment.setArguments(bundle);
                    dialogFragment.show(fm, "Sample Fragment");
                }
            });

            pName.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("product_id", (long) auctionList.get(getLayoutPosition()).getpId());
                    ((MainActivity) context).startActivityForResult(intent, PRODUCT_DETAIL_REQUEST_CODE_FROM_AUCTION);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("product_id", (long) auctionList.get(getLayoutPosition()).getpId());
                    ((MainActivity) context).startActivityForResult(intent, PRODUCT_DETAIL_REQUEST_CODE_FROM_AUCTION);
                }
            });
        }
    }

}
