package com.emts.chasebid.adapters.searchCategory;

import java.io.Serializable;

/**
 * Created by shreedhar on 7/4/2016.
 */
public class IconTitleModel implements Serializable {
    private String title;
    private int drawableIcon;
    private String imageUrl;
    private String id;

 public IconTitleModel(String title, int icon){
     this.title = title;
     this.drawableIcon = icon;
 }
    public IconTitleModel(String title, String imageUrl){
        this.title = title;
        this.imageUrl = imageUrl;
    }

    public String getTitle(){
        return this.title;
    }
    public int getIcon(){
        return  this.drawableIcon;
    }
}

