package com.emts.chasebid.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.model.BuyCredit;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2016-07-01.
 */
public class BidCreditAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<BuyCredit> creditList;
    private OnRecyclerViewItemClickListener clickListener;

    public BidCreditAdapter(Context context, ArrayList<BuyCredit> creditList) {
        this.context = context;
        this.creditList = creditList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_bid_credit, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        BuyCredit credit = creditList.get(position);

        viewHolder.price.setText(PreferenceHelper.getInstance(context).getCurrency() + " " + credit.getCreditPrice());
        viewHolder.normalCredits.setText(credit.getNormalCredit() + "");
        viewHolder.bonusCredits.setText("" + credit.getBonusCredit());

        final String[] paymentGateWay = credit.getPaymentGateway();
        final String[] paymentLogo = credit.getPaymentLogo();

        viewHolder.paymentOptions.setAdapter(new BaseAdapter() {

            @Override
            public int getCount() {
                return paymentGateWay.length;
            }

            @Override
            public Object getItem(int i) {
                return null;
            }

            @Override
            public long getItemId(int i) {
                return i;
            }

            @NonNull
            @Override
            public View getView(int position, View convertView, @NonNull ViewGroup parent) {
                View view = LayoutInflater.from(context).inflate(R.layout.sp_item_payment_options, parent, false);
                TextView paymentName = (TextView) view.findViewById(R.id.payment_method_name);
                ImageView imgPaymentLogo = (ImageView) view.findViewById(R.id.payment_method_logo);
                if (paymentLogo[position].equals("")) {
                    paymentName.setText(paymentGateWay[position]);
                    paymentName.setVisibility(View.VISIBLE);
                    imgPaymentLogo.setVisibility(View.GONE);
                } else {
                    paymentName.setVisibility(View.GONE);
                    Picasso.with(context).load(paymentLogo[position]).into(imgPaymentLogo);
                    imgPaymentLogo.setVisibility(View.VISIBLE);
                }
                return view;
            }
        });
    }

    @Override
    public int getItemCount() {
        return creditList.size();
    }

    @Override
    public long getItemId(int position) {
        return creditList.get(position).getId();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView price, normalCredits, bonusCredits, buy_now;
        EditText couponCode;
        Spinner paymentOptions;

        public ViewHolder(View itemView) {
            super(itemView);

            price = (TextView) itemView.findViewById(R.id.item_credit_price);
            normalCredits = (TextView) itemView.findViewById(R.id.item_normal_credits);
            bonusCredits = (TextView) itemView.findViewById(R.id.item_bonus_credits);
            buy_now = (TextView) itemView.findViewById(R.id.tv_buy_now);

            couponCode = (EditText) itemView.findViewById(R.id.coupon_code);
            paymentOptions = (Spinner) itemView.findViewById(R.id.payment_option);

            buy_now.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (clickListener != null) {
                        final String[] paymentIds = creditList.get(getLayoutPosition()).getPaymentId();
                        clickListener.onRecyclerViewItemClicked(view, getLayoutPosition(),
                                BidCreditAdapter.this.getItemId(getLayoutPosition()), (String) paymentOptions.getSelectedItem(),
                                paymentIds[paymentOptions.getSelectedItemPosition()], couponCode.getText().toString().trim());
                    }
                }
            });
        }
    }

    public interface OnRecyclerViewItemClickListener {
        void onRecyclerViewItemClicked(View view, int position, long id, String paymentMethod,
                                       String paymentMethodId, String couponCode);
    }

    public void setOnRecyclerViewItemClickListener(OnRecyclerViewItemClickListener clickListener) {
        this.clickListener = clickListener;
    }
}
