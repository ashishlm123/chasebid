package com.emts.chasebid.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RadioButton;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.model.Redeem;

import java.util.ArrayList;

/**
 * Created by Srijana on 3/28/2017.
 */

public class RedeemBonusAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    ArrayList<Redeem> rList;

    public RedeemBonusAdapter(Context context, ArrayList<Redeem> rList) {
        this.context = context;
        this.rList = rList;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_redeem_bonus_point, parent, false));

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        Redeem redeem = rList.get(position);
        viewHolder.bonusPoint.setText(redeem.getBonuspoint());
        viewHolder.bid.setText(redeem.getBid());
        if (redeem.isChecked()) {
            viewHolder.choice.setChecked(true);
        } else {
            viewHolder.choice.setChecked(false);
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView bonusPoint, bid;
        RadioButton choice;

        public ViewHolder(final View itemView) {
            super(itemView);

            bonusPoint = (TextView) itemView.findViewById(R.id.bonus_point);
            choice = (RadioButton) itemView.findViewById(R.id.choice);
            bid = (TextView) itemView.findViewById(R.id.bid);
            choice.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                        int position = getLayoutPosition();
                        for (int i = 0; i < rList.size(); i++) {
                            if (i == position) {
                                rList.get(i).setChecked(true);
                            } else {
                                rList.get(i).setChecked(false);
                            }
                        }
                        notifyDataSetChanged();
                    }
                    return true;
                }
            });
        }
    }

    @Override
    public int getItemCount() {
        return rList.size();
    }
}
