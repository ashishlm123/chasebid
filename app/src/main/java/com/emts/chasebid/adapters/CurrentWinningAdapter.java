package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.helper.ImageHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.model.WonAuction;
import com.squareup.picasso.Picasso;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Srijana on 5/5/2017.
 */

public class CurrentWinningAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private ArrayList<WonAuction> cList;
    Context context;
    private long serverTime;
    private boolean fullScreen;
    //    private CardView.LayoutParams lp;
    private LinearLayout.LayoutParams lpFill;

    public CurrentWinningAdapter(ArrayList<WonAuction> cList, Context context) {
        this.cList = cList;
        this.context = context;

        lpFill = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFill.setMargins((int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12),
                (int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12));
    }

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
//        lp = new CardView.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
//        int dp = (int) (context.getResources().getDimension(R.dimen.activity_horizontal_margin));
//        lp.setMargins(dp + 6, dp - 6, dp + 6, dp - 6);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_current_winning, parent, false);
        if (fullScreen) {
//            ((CardView) view).setLayoutParams(lp);
            view.setLayoutParams(lpFill);
        }
        return new ViewHolder(view);

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        WonAuction wonAuction = cList.get(position);

        if (!TextUtils.isEmpty(wonAuction.getImageUrl())) {
            Picasso.with(context).load(wonAuction.getImageUrl()).into(viewHolder.pImage);
        }
        if (!TextUtils.isEmpty(wonAuction.getWinnerUrl())) {
            Picasso.with(context).load(wonAuction.getWinnerUrl()).into(viewHolder.wImage);
        } else {
            Picasso.with(context).load(ImageHelper.getDefaultUserImage(context, wonAuction.getWinnerGender()))
                    .into(viewHolder.wImage);
        }
        viewHolder.pName.setText(wonAuction.getpName());
        viewHolder.wName.setText(wonAuction.getWinnerName());
        viewHolder.address.setText(wonAuction.getAddress());

        getRemainingTime(wonAuction.getEndTime(), viewHolder);
    }

    private String getRemainingTime(long endDate, ViewHolder holder1) {
        if (endDate == 0) {
            return "end date not available";
        }

        if (serverTime < endDate) {
            long elapsedTime = Math.abs(endDate - serverTime);
            return ddHHMMSS(elapsedTime, holder1);
        } else {
            return "Auction Closed";
        }
    }

    private static String ddHHMMSS(long elapsedTimes, ViewHolder viewHolder) {
        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTimes);
        elapsedTimes -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTimes);
        elapsedTimes -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTimes);
        elapsedTimes -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTimes);
        String remainingTime = "";
        if (days != 0) {
            remainingTime = days + "d : ";
        }
        if (hours != 0) {
            remainingTime = remainingTime + hours + "h : ";
        }
        if (minute != 0) {
            remainingTime = remainingTime + minute + "m : ";
        }
        if (second != 0)
            remainingTime = remainingTime + second + "s";

        boolean last5minute = false;
        if (days == 0 && hours == 0 && minute < 5) {
            last5minute = true;
        } else {
            last5minute = false;
        }
        if (last5minute) {
            viewHolder.timerDay.setTextColor(Color.RED);
            viewHolder.timerHours.setTextColor(Color.RED);
            viewHolder.timerMinutes.setTextColor(Color.RED);
            viewHolder.timerSeconds.setTextColor(Color.RED);
        } else {
            viewHolder.timerDay.setTextColor(Color.WHITE);
            viewHolder.timerHours.setTextColor(Color.WHITE);
            viewHolder.timerMinutes.setTextColor(Color.WHITE);
            viewHolder.timerSeconds.setTextColor(Color.WHITE);
        }
        viewHolder.timerDay.setText(String.format("%02d", (int) days));
        viewHolder.timerHours.setText(String.format("%02d", (int) hours));
        viewHolder.timerMinutes.setText(String.format("%02d", (int) minute));
        viewHolder.timerSeconds.setText(String.format("%02d", (int) second));

        return remainingTime;
    }

    public void setServerTime(long time) {
        serverTime = time;
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        TextView pName, wName, address, timerDay, timerHours, timerMinutes, timerSeconds, bidNow;
        ImageView pImage, wImage;

        public ViewHolder(View itemView) {
            super(itemView);

            timerDay = (TextView) itemView.findViewById(R.id.timer_day);
            timerHours = (TextView) itemView.findViewById(R.id.timer_hours);
            timerMinutes = (TextView) itemView.findViewById(R.id.timer_minutes);
            timerSeconds = (TextView) itemView.findViewById(R.id.timer_seconds);
            pName = (TextView) itemView.findViewById(R.id.p_name);
            wName = (TextView) itemView.findViewById(R.id.winner_name);
            address = (TextView) itemView.findViewById(R.id.tv_address);
            wImage = (ImageView) itemView.findViewById(R.id.winner_image);
            pImage = (ImageView) itemView.findViewById(R.id.item_product_image);
            if (fullScreen) {
                pImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        (int) Utils.pxFromDp(context, 175)));
            }
            bidNow = (TextView) itemView.findViewById(R.id.item_btn_bid);
            bidNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("product_id", cList.get(getLayoutPosition()).getpId());
                    intent.putExtra("current", cList.get(getLayoutPosition()));
                    intent.putExtra("profile_dir", profileImageDir);
                    intent.putExtra("serverTime", serverTime);
                    intent.putExtra("is_current", true);
                    context.startActivity(intent);

                }
            });

        }
    }

    @Override
    public int getItemCount() {
        return cList.size();
    }

    private String profileImageDir = "";

    public void setProfileImageDir(String profileImageDir) {
        this.profileImageDir = profileImageDir;
    }
}
