package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.LoginActivity;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.VoteProduct;
import com.squareup.picasso.Picasso;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 3/24/2017.
 */

public class MakeWishAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<VoteProduct> pList;
    PreferenceHelper prefsHelper;

    public MakeWishAdapter(Context context, ArrayList<VoteProduct> pList) {
        this.context = context;
        this.pList = pList;
        prefsHelper = PreferenceHelper.getInstance(context);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ViewHolder(LayoutInflater.from(context).inflate(R.layout.item_make_wish, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        ViewHolder viewHolder = (ViewHolder) holder;
        VoteProduct product = pList.get(position);
        if (!TextUtils.isEmpty(product.getImageUrl())) {
            Picasso.with(context).load(product.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.pImage);
        }

        viewHolder.pName.setText(product.getpName());
        if (product.getpPrice() > 0) {
            viewHolder.pRetailPrice.setTextColor(viewHolder.pTotalVote.getTextColors());
            viewHolder.pRetailPrice.setText(prefsHelper.getCurrency() + " " + String.valueOf(product.getpPrice()));
        } else {
            viewHolder.pRetailPrice.setTextColor(context.getResources().getColor(R.color.red));
            viewHolder.pRetailPrice.setText(R.string.priceless);
        }
        viewHolder.noLike.setText(product.getPosiviteRating() + context.getString(R.string.likes));
        viewHolder.noDisLike.setText(product.getNegativeRating() + context.getString(R.string.dislikes));
        viewHolder.pTotalVote.setText(String.valueOf(Integer.parseInt(product.getNegativeRating()) + Integer.parseInt(product.getPosiviteRating())));

        if (product.getVoteStatus() == VoteProduct.VOTE_STATUS_POSITIVE) {
            viewHolder.pLike.setColorFilter(context.getResources().getColor(R.color.green1));
        } else if (product.getVoteStatus() == VoteProduct.VOTE_STATUS_NEGATIVE) {
            viewHolder.pDisLike.setColorFilter(context.getResources().getColor(R.color.google_icon_bg));
        } else if (product.getVoteStatus() == VoteProduct.VOTE_STATUS_NOT_RATE) {
            viewHolder.pDisLike.setColorFilter(context.getResources().getColor(R.color.white));
        }
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pImage, pLike, pDisLike;
        TextView pName, pRetailPrice, pTotalVote, noLike, noDisLike;


        public ViewHolder(final View itemView) {
            super(itemView);
            pImage = (ImageView) itemView.findViewById(R.id.make_product_image);
            pLike = (ImageView) itemView.findViewById(R.id.img_like);
            pDisLike = (ImageView) itemView.findViewById(R.id.img_dislike);
            pName = (TextView) itemView.findViewById(R.id.m_p_name);
            pRetailPrice = (TextView) itemView.findViewById(R.id.make_retail_price);
            pTotalVote = (TextView) itemView.findViewById(R.id.no_vote);
            noLike = (TextView) itemView.findViewById(R.id.no_like);
            noDisLike = (TextView) itemView.findViewById(R.id.no_dislike);

            pLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (prefsHelper.isLogin()) {
                        HashMap<String, String> postMap = Api.getInstance().getPostParams();
                        postMap.put("user_id", String.valueOf(prefsHelper.getUserId()));
                        postMap.put("product_id", String.valueOf(pList.get(getLayoutPosition()).getpId()));
                        postMap.put("vote_type", "positive");
                        voteTask(postMap, noLike, noDisLike, pTotalVote, pLike, pDisLike);
                    } else {
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                }
            });
            pDisLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (prefsHelper.isLogin()) {
                        HashMap<String, String> postMap = Api.getInstance().getPostParams();
                        postMap.put("user_id", String.valueOf(prefsHelper.getUserId()));
                        postMap.put("product_id", String.valueOf(pList.get(getLayoutPosition()).getpId()));
                        postMap.put("vote_type", "negative");
                        voteTask(postMap, noLike, noDisLike, pTotalVote, pLike, pDisLike);
                    } else {
                        Intent intent = new Intent(context, LoginActivity.class);
                        context.startActivity(intent);
                    }
                }
            });

        }
    }

    private void voteTask(HashMap<String, String> postMap, final TextView like, final TextView disLike, final TextView total,
                          final ImageView likeIcon, final ImageView disLikeIcon) {


        VolleyHelper vHelper = VolleyHelper.getInstance(context);
        vHelper.addVolleyRequestListeners(Api.getInstance().auctionVoteUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {

                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {

                        AlertUtils.showSnack(context, total, resObj.getString("message"));

                        String votePositive = resObj.getString("vote_positive") + " " + context.getString(R.string.likes);
                        like.setText(votePositive);

                        String negativePositive = resObj.getString("vote_negative") + " " + context.getString(R.string.dislikes);
                        disLike.setText(negativePositive);

                        String totalVote = resObj.getString("total_vote");
                        total.setText(totalVote);

                        if (resObj.getString("vote_type").equalsIgnoreCase("negative")) {
                            disLikeIcon.setColorFilter(context.getResources().getColor(R.color.google_icon_bg));
                            likeIcon.setColorFilter(context.getResources().getColor(R.color.white));
                        } else if (resObj.getString("vote_type").equalsIgnoreCase("positive")) {
                            likeIcon.setColorFilter(context.getResources().getColor(R.color.green1));
                            disLikeIcon.setColorFilter(context.getResources().getColor(R.color.white));
                        }

                    } else {

                        AlertUtils.showSnack(context, total, resObj.getString("message"));

                    }

                } catch (Exception e) {

                    Logger.e("voteTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {

            }
        }, "voteTask");
    }


    @Override
    public int getItemCount() {
        return pList.size();
    }
}
