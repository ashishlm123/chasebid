package com.emts.chasebid.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.helper.ImageHelper;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.model.WonAuction;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by User on 2017-03-08.
 */

public class RecentClosedAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private Context context;
    private ArrayList<WonAuction> closedList;
    private PreferenceHelper preferenceHelper;
    boolean fillParent = false;

    private LinearLayout.LayoutParams lpFill;

    public void setFillParent(boolean fillParent) {
        this.fillParent = fillParent;
    }

    public RecentClosedAdapter(Context context, ArrayList<WonAuction> closedList) {
        this.context = context;
        this.closedList = closedList;
        preferenceHelper = PreferenceHelper.getInstance(context);


        lpFill = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lpFill.setMargins((int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12),
                (int) Utils.pxFromDp(context, 28), (int) Utils.pxFromDp(context, 12));
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_recent_closed, parent, false);
        if (fillParent) {
//            view = LayoutInflater.from(context).inflate(R.layout.item_live_product_fill, parent, false);
            view.setLayoutParams(lpFill);
        } else {
//            view = LayoutInflater.from(context).inflate(R.layout.item_live_product2, parent, false);
        }
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        final ViewHolder viewHolder = (ViewHolder) holder;
        final WonAuction product = closedList.get(position);

        if (!TextUtils.isEmpty(product.getImageUrl())) {
            Picasso.with(context).load(product.getImageUrl())
                    .placeholder(R.drawable.placeholder)
                    .error(R.drawable.placeholder)
                    .into(viewHolder.pImage);
        }

        if (!TextUtils.isEmpty(product.getWinnerUrl())) {
            Picasso.with(context).load(product.getWinnerUrl())
                    .into(viewHolder.winnerImage);
        } else {
            Picasso.with(context).load(ImageHelper.getDefaultUserImage(context, product.getWinnerGender()))
                    .into(viewHolder.winnerImage);
        }

        viewHolder.winningBid.setText(preferenceHelper.getCurrency() + " " + product.getWinningBidAmount());
        Double retailPrice = (double) product.getpPrice();
        Double winningBid = Double.parseDouble(product.getWinningBidAmount());
        Double shipping = Double.parseDouble(product.getShippingCost());

        double saving = ((retailPrice - (winningBid + shipping)) / retailPrice) * 100;
        viewHolder.savingPercentage.setText(Utils.formatAmount(saving) + " %");

        viewHolder.winnerName.setText(product.getWinnerName());
        viewHolder.pName.setText(product.getpName());
        viewHolder.retailPrice.setText(context.getString(R.string.retail_value) + " " + PreferenceHelper.getInstance(context).getCurrency() + " " + Utils.formatAmount(product.getpPrice()));
        viewHolder.totalCreditUsed.setText(context.getString(R.string.total_credit_used) + " " + product.getTotalCreditUsed());
    }

    @Override
    public int getItemCount() {
        return closedList.size();
    }

    class ViewHolder extends RecyclerView.ViewHolder {
        ImageView pImage, winnerImage;
        TextView winningBid, savingPercentage, winnerName, pName, retailPrice, totalCreditUsed;

        public ViewHolder(View itemView) {
            super(itemView);

            pImage = (ImageView) itemView.findViewById(R.id.item_product_image);
            if (fillParent) {
                pImage.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                        (int) Utils.pxFromDp(context, 175)));
            }
            winnerImage = (ImageView) itemView.findViewById(R.id.winner_image);

            pName = (TextView) itemView.findViewById(R.id.p_name);
            winningBid = (TextView) itemView.findViewById(R.id.tv_wining_bid);
            savingPercentage = (TextView) itemView.findViewById(R.id.tv_total_saving);
            winnerName = (TextView) itemView.findViewById(R.id.winner_name);
            retailPrice = (TextView) itemView.findViewById(R.id.tv_retail_value);
            totalCreditUsed = (TextView) itemView.findViewById(R.id.tv_total_credit_used);


            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(context, ProductDetail.class);
                    intent.putExtra("won", closedList.get(getLayoutPosition()));
                    intent.putExtra("product", closedList.get(getLayoutPosition()));
                    intent.putExtra("product_id", closedList.get(getLayoutPosition()).getpId());
                    intent.putExtra("is_closed", true);
                    context.startActivity(intent);
                }
            });
        }
    }
}

