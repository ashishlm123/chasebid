package com.emts.chasebid.customviews;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.TextView;

import com.emts.chasebid.R;

import rm.com.clocks.Clock;

/**
 * Created by User on 2016-07-15.
 */
public class ProgressDialog extends Dialog {

    public ProgressDialog(Context context) {
        super(context);
        init();
    }

    public ProgressDialog(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    protected ProgressDialog(Context context, boolean cancelable, OnCancelListener cancelListener) {
        super(context, cancelable, cancelListener);
        init();
    }

    public void init() {
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.custom_progress_dailog);
        setCanceledOnTouchOutside(false);

        Clock clock = (Clock) findViewById(R.id.clocks);
        clock.animateIndeterminate();


    }

    public void setMessage(String message) {
        ((TextView) findViewById(R.id.progredd_msg)).setText(message + "");
    }
}
