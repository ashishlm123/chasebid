package com.emts.chasebid.customviews;

import android.content.Context;
import android.support.annotation.AttrRes;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;

import com.emts.chasebid.R;

import rm.com.clocks.Clock;


/**
 * Created by Srijana on 6/12/2017.
 */

public class CustomProgressBar extends FrameLayout {
    public CustomProgressBar(@NonNull Context context) {
        super(context);
        init(context);
    }

    public CustomProgressBar(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init(context);
    }

    public CustomProgressBar(@NonNull Context context, @Nullable AttributeSet attrs, @AttrRes int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context);
    }

    public void init(Context context) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_progress, null, false);
        Clock clock = (Clock) view.findViewById(R.id.clocks);
        clock.animateIndeterminate();

        addView(view);
    }
}
