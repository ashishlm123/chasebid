package com.emts.chasebid.customviews;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.support.v7.widget.AppCompatTextView;
import android.util.AttributeSet;

import com.emts.chasebid.R;

public class RobotoTextView extends AppCompatTextView {

    public RobotoTextView(Context context) {
        super(context);
        if (isInEditMode()) return;
        parseAttributes(null);
    }

    public RobotoTextView(Context context, AttributeSet attrs) {
        super(context, attrs);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    public RobotoTextView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        if (isInEditMode()) return;
        parseAttributes(attrs);
    }

    private void parseAttributes(AttributeSet attrs) {
        int typeface;
        if (attrs == null) { //Not created from xml
//            typeface = Roboto.ROBOTO_REGULAR;
        } else {
            TypedArray values = getContext().obtainStyledAttributes(attrs, R.styleable.RobotoTextView);
            typeface = values.getInt(R.styleable.RobotoTextView_typeface, -1);
            values.recycle();

            if (typeface != -1) {
                setTypeface(getRoboto(typeface));
            }
        }
    }

    public void setRobotoTypeface(int typeface) {
        setTypeface(getRoboto(typeface));
    }

    private Typeface getRoboto(int typeface) {
        return getRoboto(getContext(), typeface);
    }

    public static Typeface getRoboto(Context context, int typeface) {
        switch (typeface) {
            case Roboto.YANO_REGULAR:
                if (Roboto.yanoRegular == null) {
                    Roboto.yanoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/yanonekaffeesatz_regular.ttf");
                }
                return Roboto.yanoRegular;

            case Roboto.TEKO_REGULAR:
                if (Roboto.tekoRegular == null) {
                    Roboto.tekoRegular = Typeface.createFromAsset(context.getAssets(), "fonts/teko_regular.ttf");
                }
                return Roboto.tekoRegular;

            case Roboto.TEKO_BOLD:
                if (Roboto.tekoBold == null) {
                    Roboto.tekoBold = Typeface.createFromAsset(context.getAssets(), "fonts/teko_bold.ttf");
                }
                return Roboto.tekoBold;

            default:
                return Typeface.DEFAULT;
        }
    }

    public static class Roboto {
        //nexa
        public static final int YANO_REGULAR = 19;
        public static final int TEKO_REGULAR = 20;
        public static final int TEKO_BOLD = 21;

        private static Typeface yanoRegular;
        private static Typeface tekoRegular;
        private static Typeface tekoBold;
    }
}
