package com.emts.chasebid.customviews;

import android.content.Context;
import android.util.AttributeSet;
import android.view.MotionEvent;

import com.daimajia.slider.library.SliderLayout;

/**
 * Created by User on 2017-04-06.
 */

public class CustomSlider extends SliderLayout {

    public CustomSlider(Context context) {
        super(context);
    }

    public CustomSlider(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent e) {
        return false;
    }
}