package com.emts.chasebid.fcm;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;

import com.emts.chasebid.R;
import com.emts.chasebid.activity.SplashActivity;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Date;
import java.util.Map;

/**
 * Created by User on 2016-09-30.
 */
public class MyFirebaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    private static final String NOTI_LOWEST_BID = "lowest_unique_bidder_notification_user";

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    // [START receive_message]
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {
        // [START_EXCLUDE]
        // There are two types of messages data messages and notification messages. Data messages are handled
        // here in onMessageReceived whether the app is in the foreground or background. Data messages are the type
        // traditionally used with GCM. Notification messages are only received here in onMessageReceived when the app
        // is in the foreground. When the app is in the background an automatically generated notification is displayed.
        // When the user taps on the notification they are returned to the app. Messages containing both notification
        // and data payloads are treated as notification messages. The Firebase console always sends notification
        // messages. For more see: https://firebase.google.com/docs/cloud-messaging/concept-options
        // [END_EXCLUDE]

        // Not getting messages here? See why this may be: https://goo.gl/39bRNJ
        Logger.e(TAG, "From: " + remoteMessage.getFrom());

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {
            Logger.e(TAG, "Message data payload: " + remoteMessage.getData());
            sendNotification(remoteMessage.getData());
        }

        // Check if message contains a notification payload.
        if (remoteMessage.getNotification() != null) {
            Logger.e(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        }

        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    private void sendNotification(Map<String, String> data) {

        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);

        try {
            Intent intent = null;
//            String message = data.get("message");
//            JSONObject jsonObject=new JSONObject(message);
            String title = data.get("subject");

            String body = data.get("message");
            String notify_type = data.get("notify_type");
            int userId = 0;
            Logger.e("try", "u r here");
            //check if login to app else open splash

            if (notify_type.equals(NOTI_LOWEST_BID)) {
                if (!preferenceHelper.isLogin()) {
                    //if not login don't show the notification
                    return;
                }
            }
            intent = new Intent(this, SplashActivity.class);


            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
            PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                    PendingIntent.FLAG_ONE_SHOT);

//            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
//            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
//                    .setSmallIcon(R.drawable.ic_noti_icon)
//                    .setContentTitle(title)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(body))
//                    .setContentText(body)
//                    .setAutoCancel(true)
//                    .setSound(defaultSoundUri)
//                    .setContentIntent(pendingIntent);
//            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
//                notificationBuilder.setSmallIcon(R.drawable.ic_noti_icon);
////                notificationBuilder.setColor(getResources().getColor(R.color.gray4));
//            }


            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.BigTextStyle bigTextStyle = new NotificationCompat.BigTextStyle();
            bigTextStyle.setBigContentTitle(title);
            bigTextStyle.bigText(body);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_noti_icon)
//                    .setLargeIcon(icon1)
//                    .setLargeIcon(BitmapFactory.decodeResource(this.getResources(), R.drawable.ic_launcher))
                    .setTicker(title)
//                    .setTicker(message)
//                    .setContent(remoteViews)
                    .setContentTitle(title)
                    .setContentText(body)
                    .setAutoCancel(true)
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(title))
//                    .setStyle(new NotificationCompat.BigTextStyle().bigText(message))
                    .setStyle(bigTextStyle)
//                    .setPriority(Notification.PRIORITY_MAX)
                    .setContentIntent(pendingIntent)
//                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                    .setSound(defaultSoundUri)
                    .setVibrate(new long[]{1, 1, 1})
//                    .setDefaults(Notification.DEFAULT_SOUND)
                    .setDefaults(Notification.DEFAULT_ALL);

//            Notification notify = new Notification();
//            notify.flags = Notification.FLAG_SHOW_LIGHTS | Notification.FLAG_ONLY_ALERT_ONCE;

            NotificationManager notificationManager =
                    (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);


            int m = (int) ((new Date().getTime() / 1000L) % Integer.MAX_VALUE);
            notificationManager.notify(notify_type, m /* ID of notification */, notificationBuilder.build());
//            notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());
//            notificationManager.notify(notiTag, notificationId, notificationBuilder.build());

        } catch (Exception e) {
            Logger.e(TAG + "data2 ex", e.getMessage() + " ");
        }
    }

}