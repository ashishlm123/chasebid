package com.emts.chasebid.helper;

import android.content.Context;
import android.content.Intent;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 2017-01-06.
 */

public class VolleyHelper {
    public static final int STATUS_SITE_OFFLINE = 503;
    //    public static final int STATUS_AUTH_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_SESSION_OUT_ERROR = 407;//Proxy Authentication Required
    public static final int STATUS_AUTH_ERROR = 401;// Unauthorized
    public static final int STATUS_FORBIDDEN_ACCESS = 403;

    private Context context;
    static RequestQueue requestQueue;
    static VolleyHelper volleyHelper;
    static PreferenceHelper preferenceHelper;

    private VolleyHelper(Context context) {
        this.context = context;
    }

    public static VolleyHelper getInstance(Context context) {
        preferenceHelper = PreferenceHelper.getInstance(context);
        if (volleyHelper != null) {
            return volleyHelper;
        } else {
            requestQueue = Volley.newRequestQueue(context);
            volleyHelper = new VolleyHelper(context);
            return volleyHelper;
        }

    }

    public void addVolleyRequestListeners(String url, int reqMethod, HashMap<String, String> postParams,
                                          VolleyHelperInterface callbacks, String requestTag) {
        addRequestToQueue(url, reqMethod, postParams, callbacks, requestTag);
    }

    public interface VolleyHelperInterface {
        void onSuccess(String response);

        void onError(String errorResponse, VolleyError volleyError);
    }

    private void addRequestToQueue(String url, final int reqMethod, final HashMap<String, String> postParams,
                                   final VolleyHelperInterface callbacks, final String requestTag) {
        Logger.e(requestTag + " url", url);

        StringRequest request = new StringRequest(reqMethod, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Logger.e(requestTag + " response", response);
                if (response.toLowerCase().contains(context.getString(R.string.season_expired).toLowerCase())) {
                    //let not clear first time profile page redirect
                    boolean firstTimeProfile = preferenceHelper.getBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true);
                    preferenceHelper.edit().clear().commit();
                    preferenceHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, firstTimeProfile).commit();
                    cancelAllRequests();

                    Intent intent = new Intent(context, MainActivity.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    context.startActivity(intent);

                    AlertUtils.showToast(context.getApplicationContext(), context.getString(R.string.season_expired));
                    return;
                }
                try {
                    if (callbacks != null) {
                        callbacks.onSuccess(response);
                    }
                } catch (Exception e) {
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Logger.e(requestTag + " error", error.getMessage() + "");
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Logger.e(requestTag + " error res", errorObj.toString());
                    if (callbacks != null) {
                        callbacks.onError(errorObj.toString(), error);
                        return;
                    }
                } catch (Exception e) {
                    Logger.e(requestTag + " error ex", e.getMessage() + "");
                }
                try {
                    if (callbacks != null) {
                        callbacks.onError("", error);
                    }
                } catch (Exception e) {
                }
            }
        }) {
            @Override
            protected Map<String, String> getParams() throws AuthFailureError {
                if (reqMethod == Method.POST && postParams != null) {
                    if (preferenceHelper.isLogin()) {
                        postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
                        postParams.put("token", String.valueOf(preferenceHelper.getToken()));
                    }
                    Logger.e(requestTag + " Post Params", postParams.toString());
                    return postParams;
                }
                return super.getParams();
            }

//            @Override
//            public Map<String, String> getHeaders() throws AuthFailureError {
//                HashMap<String, String> params = new HashMap<>();
//                String creds = String.format("%s:%s", "bfs", "KytQ4VJACgh5");
//                String auth = "Basic " + Base64.encodeToString(creds.getBytes(), Base64.NO_WRAP);
//                params.put("Authorization", auth);
//                return params;
//            }
        };
        RetryPolicy retryPolicy = new DefaultRetryPolicy(50000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
        request.setRetryPolicy(retryPolicy);
        request.setTag(requestTag);

        requestQueue.add(request);
    }

    private void cancelAllRequests() {
        requestQueue.cancelAll(new RequestQueue.RequestFilter() {
            @Override
            public boolean apply(Request<?> request) {
                Logger.e("DEBUG", "request running: " + request.getTag().toString());
                return true;
            }
        });
    }

    public void cancelRequest(String tag) {
        requestQueue.cancelAll(tag);
    }
}
