package com.emts.chasebid.helper;

import android.annotation.TargetApi;
import android.app.Application;
import android.content.Context;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Build;
import android.view.ContextThemeWrapper;

import java.util.Locale;

/**
 * Created by User on 2017-04-05.
 */

public class LocaleUtils {
    private static Locale sLocale;

    public static void setLocale(Application app, String lang) {
        if (!"".equals(lang) && !getLanguage(app.getBaseContext()).equals(lang)) {
            setLocale(new Locale(lang));

            if (sLocale != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
//                updateConfig((ContextThemeWrapper) context);
                updateConfig(app.getBaseContext());
            } else {
                updateConfig(app);
            }
        }
    }

    private static void setLocale(Locale locale) {
        sLocale = locale;
        if (sLocale != null) {
            Locale.setDefault(sLocale);
        }
    }

    public static void updateConfig(ContextThemeWrapper wrapper) {
        if (sLocale != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration configuration = getConfiguration(wrapper);
            configuration.setLocale(sLocale);
            wrapper.applyOverrideConfiguration(configuration);
        }
    }

    public static void updateConfig(Context context) {
        if (sLocale != null && Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
            Configuration configuration = getConfiguration(context);
            configuration.setLocale(sLocale);
            context.createConfigurationContext(configuration);
        }
    }

    public static void updateConfig(Application app) {
        if (sLocale != null && Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN_MR1) {
            //Wrapping the configuration to avoid Activity endless loop
            Configuration config = getConfiguration(app.getBaseContext());
            // We must use the now-deprecated config.locale and res.updateConfiguration here,
            // because the replacements aren't available till API level 24 and 17 respectively.
            config.locale = sLocale;
            Resources res = app.getBaseContext().getResources();
            res.updateConfiguration(config, res.getDisplayMetrics());
        }
    }

    private static Configuration getConfiguration(Context context) {
        Configuration config = context.getResources().getConfiguration();
        return new Configuration(config);
    }

    private static String getLanguage(Context context) {
        return getCurrentLocale(context).getLanguage();
    }

    @TargetApi(Build.VERSION_CODES.N)
    private static Locale getCurrentLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            return getConfiguration(context).getLocales().get(0);
        } else {
            //noinspection deprecation
            return getConfiguration(context).locale;
        }
    }
}
