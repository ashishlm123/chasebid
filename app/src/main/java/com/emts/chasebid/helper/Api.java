package com.emts.chasebid.helper;

import android.content.Context;
import android.text.TextUtils;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.chasebid.UserDetailGet;
import com.emts.chasebid.app.ChaseBidApp;

import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by theone on 10/18/2016.
 */

public class Api {

    private static Api api;

    public static Api getInstance() {
//        if (api != null) {
//            return api;
//        } else {
        api = new Api();
        return api;
//        }

    }

    public final String apiKey = "12341234";

//    public String getDefCountryCode = "http://uat.chasebid.com/default_lang";
    public String getDefCountryCode = "http://www.chasebid.com/default_lang";

    private String getIp() {
//        return "http://uat.chasebid.com/" + getCountryCode() + "/api/";
        return "https://www.chasebid.com/" + getCountryCode() + "/api/";
//        return "http://202.166.198.151:8888/chasebid/" + getCountryCode() + "/api/";
//        return "http://nepaimpressions.com/dev/chasebid/" + getCountryCode() + "/api/";
//        return "http://192.168.0.27/chasebid/"+ getCountryCode() +"/api/";
    }

    public String getIpGeneral() {
        return "http://nepaimpressions.com/dev/chasebid/" + getCountryCode() + "/";
    }

    private String getCountryCode() {
        return PreferenceHelper.getInstance(ChaseBidApp.getInstance().getApplicationContext()).getCountryCode();
    }

    public final String buyBidCreditPaymentUrl = getIp() + "auction/buybids";

    public final String countryListUrl = getIp() + "user/country_list";
    public final String indiaStateListApi = getIp() + "user/get_indian_states";

    public final String mobileVerification = getIp() + "user/mobile_verification";

    public final String registerUrl = getIp() + "user/register";
    public final String loginUrl = getIp() + "user/login";
    public final String forgetPasswordUrl = getIp() + "user/forgot";
    public final String fbRegisterUrl = getIp() + "user/facebook_login";
    public final String googleRegisterUrl = getIp() + "user/google_login";
    public final String twitterRegisterUrl = getIp() + "user/twitter_login";

    public final String upadeDeviceTokenUrl = getIp() + "user/update_push_id";


    public final String tvAuctionBannerUrl = getIp() + "auction/get_tv_show_banner";
    public final String bannerUrl = getIp() + "auction/get_home_page_banners";
    public final String getLiveAuction = getIp() + "auction/get_live_auctions";
    public final String getClosedAuctionUrl = getIp() + "auction/get_closed_auctions";
    public final String auctionVoteUrl = getIp() + "auction/vote"; //user_id , product_id , vote_type(positive or negative)
    public final String getmakeYourWishUrl = getIp() + "auction/make_your_wish";
    public final String getFavUrl = getIp() + "user/watchlist";
    public final String favListUrl = getIp() + "auction/add_watch_list";//{user_id}
    public final String getEditProfileUrl = getIp() + "user/update_profile";//{user_id}
    public final String getProfileDataUrl = getIp() + "user/profile";//{user_id}
    public final String onGoingAuctionUrl = getIp() + "user/ongoing_auctions";
    public String bidHistoryApi = getIp() + "user/get_bid_histrory";
    public String purchaseHistoryApi = getIp() + "user/purchase";
    public final String referFriendUrl = getIp() + "user/refer";//{user_id}
    public final String deleteAccountUrl = getIp() + "user/cancel";//{user_id)
    public final String profilePicUrl = getIp() + "user/create_avatar";
    public final String userBonusUrl = getIp() + "user/bonuspackage";//{user_id}

    public final String wonAuctionUrl = getIp() + "auction/wonauctions";//{user_id}
    public String buyBidCreditsUrl = getIp() + "auction/buybids";//{user_id}
    public String checkPromoCodeUrl = getIp() + "auction/checkpromocode";//{user_id}


    public final String productDetailApi = getIp() + "auction/get_product_details";//{product_id}
    public final String closedProductDetailApi = getIp() + "auction/details";//{product_id}
    public final String searchUrl = getIp() + "auction/category_lists";

    public final String singleBidUrl = getIp() + "auction/single_bid";
    public final String multipleBidUrl = getIp() + "auction/multiple_bid";
    public final String orderAuctionUrl = getIp() + "user/buyauctions";//{user_id}
    public final String getCurrentWinningUrl = getIp() + "auction/get_tv_auctions";//{api_key}


    public final String buyNowUrl = getIp() + "auction/buynow";
    public final String paymentConfirmationUrl = getIp() + "auction/wonauctionsconfirm";
    public final String wonProductSuccessUrl = getIp() + "my-account/purchase/success";
    public final String wonProductCancelUrl = getIp() + "my-account/purchase/cancel";


    public final String contactUsUrl = getIp() + "cms/contact_us";
    public final String faqUrl = getIp() + "cms/faq";
    public final String cmsUrl = getIp() + "cms";//{user_id}


    private final String userAccountDataUrl = getIp() + "user/get_user_info";//{user_id}

    public HashMap<String, String> getPostParams() {
        HashMap<String, String> postParams = new HashMap<>();
        postParams.put("api_key", apiKey);
        return postParams;
    }

    public final String siteSettingUrl = getIp() + "user/get_site_settings";
    public final String getDefaultLangUrl = getIp() + "user/get_default_language";

    //CCPayments
    public final String getCCPaymentInfoUrl = getIp() + "auction/ccavenue_required_info";
    public final String ccPaymentUrl = getIp() + "auction/ccavenue";
//    public final String ccCancelUrl = getIp() + "ccavenue_res/ccavenue_cancel";
//    public final String ccSuccessOrErrorUrl = getIp() + "ccavenue_res/ccavenue_ipn";

    //Paytm payments
//    public final String getPaytmInfoUrl = getIp() + "auction/paytm_required_info";
    public final String paytmBuyBidCredits = getIp() + "auction/paytm";
    public final String payTmCallBackUrl = getIp() + "paytm_res/paytm_ipn";

    public void getUserDetailTask(Context context, final UserDetailGet interfaceCalback) {
        final PreferenceHelper prefsHelper = PreferenceHelper.getInstance(context);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        VolleyHelper vHelper = VolleyHelper.getInstance(ChaseBidApp.getInstance().getApplicationContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().userAccountDataUrl, Request.Method.POST, postParams,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resJson = new JSONObject(response);
                            if (resJson.getBoolean("status")) {
                                JSONObject userObj = resJson.getJSONObject("profile");
                                prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                                String userName = userObj.getString("user_name");
                                if (TextUtils.isEmpty(userName.trim())) {
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("first_name")).commit();
                                } else {
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userName).commit();
                                }
                                prefsHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                                prefsHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, userObj.getString("balance")).commit();
                                prefsHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(userObj.getString("id"))).commit();
                                prefsHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, userObj.getString("bonus_points")).commit();
                                prefsHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, userObj.getString("country_short_code")).commit();
                                if (!TextUtils.isEmpty(userObj.getString("image_available"))) {
                                    prefsHelper.edit().putString(PreferenceHelper.APP_USER_PROFILE_URL, userObj.getString("image")).commit();
                                }
                                prefsHelper.edit().putString(PreferenceHelper.APP_USER_GENDER, userObj.getString("gender")).commit();
//                                prefsHelper.edit().putString(PreferenceHelper.NODE_SEVER, userObj.getString("node_server")).commit();
//                                prefsHelper.edit().putString(PreferenceHelper.NODE_PORT, userObj.getString("node_port")).commit();
                                if (interfaceCalback != null) {
                                    interfaceCalback.onUserDetailResponse(true);
                                }
                            } else {
                                if (interfaceCalback != null) {
                                    interfaceCalback.onUserDetailResponse(false);
                                }
                            }
                        } catch (Exception e) {
                            Logger.e("getUserDetailTask res ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {

                        if (interfaceCalback != null) {
                            interfaceCalback.onUserDetailResponse(false);
                        }
                    }
                }, "getUserDetailTask");
    }
}