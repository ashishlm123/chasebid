package com.emts.chasebid.helper;

import android.util.Log;

/**
 * Created by theone on 2016-03-04.
 */
public class Logger {

    public static void e(String tag, String message) {
        Log.e(tag, message);
    }

    public static void printLongLog(String tag, String message) {
        int maxLogSize = 1000;
        for (int i = 0; i <= message.length() / maxLogSize; i++) {
            int start = i * maxLogSize;
            int end = (i + 1) * maxLogSize;
            end = end > message.length() ? message.length() : end;
            e(tag, message.substring(start, end));
        }
    }
}
