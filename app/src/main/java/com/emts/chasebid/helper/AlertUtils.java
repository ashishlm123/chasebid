package com.emts.chasebid.helper;


import android.content.Context;
import android.graphics.Color;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;
import android.widget.Toast;

import com.emts.chasebid.R;
import com.emts.chasebid.customviews.ProgressDialog;

/**
 * Created by User on 2016-10-19.
 */

public class AlertUtils {

    public static void showAlertMessage(Context context, String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        if (!TextUtils.isEmpty(title)) {
            builder.setTitle(title);
        }
        builder.setMessage(Html.fromHtml(message));
        builder.show();
    }

    public static void showSnack(Context context, View view, String message) {
        Snackbar snackbar = Snackbar.make(view, message, Snackbar.LENGTH_LONG);
        View snackBarView = snackbar.getView();
        TextView tv = (TextView) snackBarView.findViewById(android.support.design.R.id.snackbar_text);
        tv.setTextColor(Color.WHITE);
        tv.setGravity(Gravity.CENTER);
        snackBarView.setBackgroundColor(ContextCompat.getColor(context,
                R.color.colorAccent));
        snackbar.show();
    }

    public static void showToast(Context context, String message) {
        try {
            Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Logger.e("alertUtils showToast ex", e.getMessage() + "");
        }
    }

//    public static void showErrorAlert(Context context, String title, String message, boolean isError) {
//        final AlertDialog.Builder alertDailog = new AlertDialog.Builder(context);
//        final View view = LayoutInflater.from(context).inflate(R.layout.alert_warningmesg, null);
//        alertDailog.setView(view);
//        final TextView heading = (TextView) view.findViewById(R.id.alert_title);
//        heading.setText(title);
//        final TextView alertMesg = (TextView) view.findViewById(R.id.warn_mesg);
//        alertMesg.setText(Html.fromHtml(message));
//        final TextView close = (TextView) view.findViewById(R.id.txt_close);
//
//
//        if (isError) {
//            heading.setBackgroundColor(context.getResources().getColor(R.color.colorPrimary));
//            heading.setTextColor(context.getResources().getColor(R.color.appWhite));
//            close.setTextColor(context.getResources().getColor(R.color.colorPrimary));
//        } else {
//            heading.setBackgroundColor(context.getResources().getColor(R.color.gray3_5));
//            heading.setTextColor(context.getResources().getColor(R.color.appBlack));
//            close.setTextColor(context.getResources().getColor(R.color.appBlack));
//        }
//        final Dialog dialog = alertDailog.show();
//        close.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                dialog.dismiss();
//            }
//        });
//
//    }

    public static ProgressDialog showProgressDialog(Context context, String message) {
        ProgressDialog pDialog = new ProgressDialog(context);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage(message);
        pDialog.show();
        return pDialog;
    }

    public static void hideInputMethod(Context context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);

    }
}
