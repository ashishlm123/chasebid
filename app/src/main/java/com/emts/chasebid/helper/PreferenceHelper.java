package com.emts.chasebid.helper;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;

import java.util.Map;
import java.util.Set;

/**
 * Created by User on 2016-03-04.
 */
public class PreferenceHelper implements SharedPreferences {
    public static final String APP_SHARED_PREFS = "chasebid_prefs";

    public static final String GCM_TOKEN = "gcmToken";
    public static final String IS_LOGIN = "is_login";

    //tokens
    public static final String TOKEN = "app_user_api_key";

    public static final String COUNTRY_LAST_UPDATE = "country_last_update";

    //user detail
    public static final String APP_USER_ID = "app_user_id";
    public static final String APP_USER_EMAIL = "app_user_email";
    public static final String APP_USER_NAME = "app_user_name";
    public static final String APP_USER_PROFILE_URL = "app_user_profile";
    public static final String APP_USER_GENDER = "app_user_gender";

    public static final String App_USER_BID_CREDITS = "app_user_bid_credits";
    public static final String App_USER_BONUS_POINTS = "app_user_bonus_points";
    public static final String APP_USER_COUNTRY_CODE = "app_user_country_code";

    //site settings
    public static final String SITE_NAME = "site_name";
    public static final String SITE_CONTACT_EMAIL = "site_email";
    public static final String SITE_STATUS = "site_status";
    public static final String SITE_CURR_SIGN = "site_curr_sign";
    public static final String SITE_CURR_CODE = "site_curr_code";
    public static final String FB_APP_ID = "fb_app_id";
    public static final String FB_URL = "fb_url";
    public static final String TWITTER_APP_KEY = "twitter_app_key";
    public static final String TWITTER_APP_SECRET = "twitter_app_secret";
    public static final String NODE_SEVER = "node_server";
    public static final String NODE_PORT = "node_port";
    public static final String FIRST_TIME_PROFILE = "first_time_profile";
    public static final String FIRST_TIME_LOGIN = "first_time_login";

    private SharedPreferences prefs;
    private static PreferenceHelper helper;

    public static PreferenceHelper getInstance(Context context) {
        if (helper != null) {
            return helper;
        } else {
            helper = new PreferenceHelper(context);
            return helper;
        }

    }

    private PreferenceHelper(Context context) {
        prefs = context.getSharedPreferences(APP_SHARED_PREFS, Context.MODE_PRIVATE);
    }

    public SharedPreferences getPrefs() {
        return prefs;
    }

    @Override
    public Map<String, ?> getAll() {
        return prefs.getAll();
    }

    @Override
    public String getString(String s, String s2) {
        return prefs.getString(s, s2);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public Set<String> getStringSet(String s, Set<String> strings) {
        return prefs.getStringSet(s, strings);
    }

    @Override
    public int getInt(String s, int i) {
        return prefs.getInt(s, i);
    }

    @Override
    public long getLong(String s, long l) {
        return prefs.getLong(s, l);
    }

    @Override
    public float getFloat(String s, float v) {
        return prefs.getFloat(s, v);
    }

    @Override
    public boolean getBoolean(String s, boolean b) {
        return prefs.getBoolean(s, b);
    }

    @Override
    public boolean contains(String s) {
        return prefs.contains(s);
    }

    @Override
    public Editor edit() {
        return prefs.edit();
    }

    @Override
    public void registerOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    @Override
    public void unregisterOnSharedPreferenceChangeListener(OnSharedPreferenceChangeListener onSharedPreferenceChangeListener) {

    }

    public boolean isLogin() {
        return prefs.getBoolean(IS_LOGIN, false);
    }

    public int getUserId() {
        return prefs.getInt(APP_USER_ID, -1);
    }

    public String getUserEmail() {
        return prefs.getString(APP_USER_EMAIL, "");
    }

    public String getToken() {
        return prefs.getString(TOKEN, "k14eAjXdnUNNWOovQFnTtqtMrwos2Xe4");
    }

    public String getCurrency() {
        return prefs.getString(SITE_CURR_SIGN, "Rs. ");
    }

    public String getCountryCode() {
        return prefs.getString(APP_USER_COUNTRY_CODE, "en");
    }


}
