package com.emts.chasebid.helper;

/**
 * Created by User on 2016-07-07.
 */
public class Config {
    public static final String SMS_ORIGIN = "todo";
    public static final String OTP_DELIMITER = ":";
    public static final String IMAGE_DIRECTORY_NAME = "ChaseBid";

    public static final int PAYMENT_GATE_PAYPAL_ID = 1;
    public static final int PAYMENT_GATE_CCAVENUE_ID = 3;
    public static final int PAYMENT_GATE_PAYTM_ID = 4;

    public static int ONGOING_AUCTION = 1;
    public static int LIMIT = 10;

}
