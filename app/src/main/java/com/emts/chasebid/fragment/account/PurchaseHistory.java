package com.emts.chasebid.fragment.account;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.PurchaseAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Transaction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 3/27/2017.
 */

public class PurchaseHistory extends Fragment {
    RecyclerView listRecycleView;
    ArrayList<Transaction> pList = new ArrayList<>();
    LinearLayoutManager layoutManager;
    PurchaseAdapter adapter;
    PreferenceHelper preferenceHelper;

    FrameLayout progressBar;
    //    ProgressBar progressBar;
    TextView errorText;
    int limit = 6;
    int offset = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_user_purchase, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listRecycleView = (RecyclerView) view.findViewById(R.id.holder_purchase);
        layoutManager = new LinearLayoutManager(getActivity());
        listRecycleView.setLayoutManager(layoutManager);
        listRecycleView.setNestedScrollingEnabled(false);
        listRecycleView.setItemAnimator(null);

        adapter = new PurchaseAdapter(pList, getContext());
        listRecycleView.setAdapter(adapter);

//        progressBar = (ProgressBar) view.findViewById(R.id.progress_purchase);
        progressBar = (FrameLayout) view.findViewById(R.id.progress_purchase);
        errorText = (TextView) view.findViewById(R.id.error__purchse);
        preferenceHelper = PreferenceHelper.getInstance(getContext());

        if (NetworkUtils.isInNetwork(getActivity())) {
            PurchaseListingTask(limit, offset);
        } else {
            progressBar.setVisibility(View.GONE);
            listRecycleView.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }


    }

    private void PurchaseListingTask(int limit, final int offset) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));
        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().purchaseHistoryApi, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
//                pList.clear();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray pArray = resObj.getJSONArray("get_trans");
                        if (pArray.length() > 0) {
                            pList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                Transaction transaction = new Transaction();
                                transaction.setInvoiceId(eachObj.getString("invoice_id"));
                                transaction.setDate(eachObj.getString("transaction_date"));
                                String bonusPoints = eachObj.getString("bonus_points");
                                if (bonusPoints.equals("") || bonusPoints.equalsIgnoreCase("null")) {
                                    transaction.setBidPoint("---");
                                } else {
                                    transaction.setBidPoint(bonusPoints);
                                }
                                transaction.setAuctionId(eachObj.getString("auc_id"));
                                transaction.setTransName(eachObj.getString("transaction_name"));
//                                if (eachObj.getString("credit_used").equalsIgnoreCase("null")) {
//                                    transaction.setCreditUsed("---");
//                                } else {
//                                    transaction.setCreditUsed(eachObj.getString("credit_get"));
//                                }
                                String creditGet = eachObj.getString("credit_get");
                                if (creditGet.equals("") || creditGet.equalsIgnoreCase("null")) {
                                    transaction.setCreditUsed("---");
                                } else {
                                    transaction.setCreditUsed(creditGet);
                                }
                                pList.add(transaction);
                            }

                            listRecycleView.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                            errorText.setText(getString(R.string.no_product));
                        }

                    } else {
                        listRecycleView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e(" PurchaseListingTask json ex", e.getMessage() + "");
                } catch (Exception e) {
                    Logger.e(" PurchaseListingTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    if (offset == 0) {
                        errorText.setText(errorObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        listRecycleView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    Logger.e("PurchaseListingTask onError Exception", e.getMessage() + "");
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }

            }
        }, " PurchaseListingTask");
    }

}
