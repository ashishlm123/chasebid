package com.emts.chasebid.fragment;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.activity.MoreClosedAuctions;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.activity.SearchProductListingActivity;
import com.emts.chasebid.adapters.LiveProductAdapter;
import com.emts.chasebid.adapters.RecentClosedAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;
import com.emts.chasebid.model.WonAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 2016-06-28.
 */
public class Home extends Fragment {
    RecyclerView pListRecyclerView, recyclerClosed;//, recyclerWinning;
    ArrayList<Product> pList = new ArrayList<>();
    ArrayList<WonAuction> closedList = new ArrayList<>();
    //    ArrayList<WonAuction> tvItemList = new ArrayList<>();
    ArrayList<String> bannerImageClickUrl = new ArrayList<>();

    //    ProgressBar progressBar, progressClosed, progressWinning;
    FrameLayout progressBar, progressClosed;//, progressWinning;
    TextView errorText, errorClosed;//, errorWinning, bidNWin;
    LiveProductAdapter adapter;
    RecentClosedAdapter adapterClosed;
//    CurrentWinningAdapter adapterCurrent;

    Handler mainHandler;//, currentHandler;
    LinearLayoutManager layoutManager;//, currentLayoutManager;
    ScheduledExecutorService service;//, curService;
    public static final int PRODUCT_DETAIL_REQUEST_CODE = 497;
    PreferenceHelper helper;
    int limit = Config.LIMIT;
    int offset = 0;
//    io.socket.client.Socket socketJs;
//    boolean connectionConfirm;

    long serverTime;//, curServiceTime;
//    String profileImageDir = "";

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = PreferenceHelper.getInstance(getActivity());
        serverTime = new Date().getTime();
//        curServiceTime = new Date().getTime();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TextView tvMoreLiveAuction = (TextView) view.findViewById(R.id.more_live_auctions);
        tvMoreLiveAuction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), SearchProductListingActivity.class);
                intent.putExtra("moreLive", true);
                startActivity(intent);
            }
        });
        pListRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_live_auctions);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        pListRecyclerView.setLayoutManager(layoutManager);
        pListRecyclerView.setNestedScrollingEnabled(false);
        pListRecyclerView.setItemAnimator(null);

        progressBar = (FrameLayout) view.findViewById(R.id.live_auction_progress);
//        progressBar = (ProgressBar) view.findViewById(R.id.live_auction_progress);
        errorText = (TextView) view.findViewById(R.id.error_tv_live_auction);

        adapter = new LiveProductAdapter(getActivity(), pList);
        pListRecyclerView.setAdapter(adapter);

        adapter.setOnRecyclerViewItemClickListener(new LiveProductAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClicked(View view, int position, long id) {
                Intent intent = new Intent(getActivity(), ProductDetail.class);
                intent.putExtra("product_id", id);
                intent.putExtra("serverTime", serverTime);
                intent.putExtra("position", position);
                intent.putExtra("product", pList.get(position));
                getActivity().startActivityForResult(intent, PRODUCT_DETAIL_REQUEST_CODE);
            }
        });

        TextView moreClosed = (TextView) view.findViewById(R.id.more_recent_closed);
        moreClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MoreClosedAuctions.class);
                startActivity(intent);
            }
        });
        recyclerClosed = (RecyclerView) view.findViewById(R.id.recycler_recent_closed);
        LinearLayoutManager closedLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerClosed.setLayoutManager(closedLayoutManager);
        recyclerClosed.setNestedScrollingEnabled(false);
        recyclerClosed.setItemAnimator(null);

        progressClosed = (FrameLayout) view.findViewById(R.id.closed_progress);
//        progressClosed = (ProgressBar) view.findViewById(R.id.closed_progress);
        errorClosed = (TextView) view.findViewById(R.id.error_tv_closed);

        adapterClosed = new RecentClosedAdapter(getActivity(), closedList);
        recyclerClosed.setAdapter(adapterClosed);

//        errorWinning = (TextView) view.findViewById(R.id.error_current_bid);
//        progressWinning = (FrameLayout) view.findViewById(R.id.current_wining_progress);
////        progressWinning = (ProgressBar) view.findViewById(R.id.current_wining_progress);
//        recyclerWinning = (RecyclerView) view.findViewById(R.id.recycler_cur_winning);
//        adapterCurrent = new CurrentWinningAdapter(tvItemList, getActivity());
//        recyclerWinning.setAdapter(adapterCurrent);
//        currentLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        recyclerWinning.setLayoutManager(currentLayoutManager);
//        recyclerWinning.setItemAnimator(null);
//        bidNWin = (TextView) view.findViewById(R.id.bid_win);

        if (NetworkUtils.isInNetwork(getActivity())) {
            PListingTask(limit, offset);
        } else {
            progressBar.setVisibility(View.GONE);
            pListRecyclerView.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            recentClosedTask(limit, offset);
        } else {
            progressClosed.setVisibility(View.GONE);
            errorClosed.setVisibility(View.VISIBLE);
            recyclerClosed.setVisibility(View.GONE);
            errorClosed.setText(getString(R.string.error_no_internet));
        }

//        if (NetworkUtils.isInNetwork(getActivity())) {
//            currentWinningTask();
//        } else {
//            progressWinning.setVisibility(View.GONE);
//            errorWinning.setVisibility(View.VISIBLE);
//            recyclerWinning.setVisibility(View.GONE);
//            errorWinning.setText(getString(R.string.error_no_internet));
//        }

        mainHandler = new Handler(Looper.getMainLooper());
        service = Executors.newSingleThreadScheduledExecutor();

//        currentHandler = new Handler(Looper.getMainLooper());
//        curService = Executors.newSingleThreadScheduledExecutor();


        if (NetworkUtils.isInNetwork(getActivity())) {
            showBannerImageTask(view);
        }
    }

//    private void connectToServerJs() throws URISyntaxException {
//        if (socketJs != null) {
//            if (socketJs.connected()) {
//                socketJs.off();
//                socketJs.disconnect();
//            }
//        }
//        String nodeServerHostUrl = helper.getString(PreferenceHelper.NODE_SEVER, "http://www.chasebid.com");
////        String nodeServerHostUrl = "http://202.166.198.151";
//        String nodePortNo = helper.getString(PreferenceHelper.NODE_PORT, "3000");
////        String nodePortNo = "3030";
////        socketJs = IO.socket(Api.nodeServer);
//        socketJs = IO.socket(nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        Logger.e("nodeJs url", nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        socketJs.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def connect call", "Object : ");
//
//                if (socketJs.connected()) {
//                    Logger.e("nodeJs connected", socketJs.toString() + "");
//                    socketJs.off();
//
////                    for (int i = 0; i < tvItemList.size(); i++) {
////                        emitConnectAndJoinAuctionRoom(tvItemList.get(i).getpId());
////                    }
//
//                    listenForAuctionWinner();
//                } else {
//                    try {
//                        connectToServerJs();
//
//                    } catch (URISyntaxException e) {
////                        e.printStackTrace();
//                    }
//                }
//
//            }
//
//        }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def. disconnect", "Object " + args.length);
//                connectionConfirm = false;
//                socketJs.off();
//            }
//        });
//        socketJs.connect();
//    }
//
//    //    START NODE JS LISTENERS
//    public void emitConnectAndJoinAuctionRoom(long hostId) {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs emit for connected and join auction room", "emitter for socket connected and join to auction room");
//            //for auction_room
//            try {
//                JSONObject roomObj = new JSONObject();
//                roomObj.put("aid", hostId);
//                Logger.e("nodeJs auction_room emitter data", roomObj.toString() + " **");
//
//                socketJs.emit("auction_room", roomObj, new Ack() {
//                    @Override
//                    public void call(Object... args) {
//                        Logger.e("nodeJs auction_room emit ack", "Ack received for auction_room :" +
//                                (args.length > 0 ? args[0].toString() : "0"));
//                    }
//                });
//            } catch (JSONException e) {
//                Logger.e("nodeJs ex15", e.getMessage() + "");
//            }
//        }
//    }
//
//    public void listenForAuctionWinner() {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs listener for bidding_success", "listener set for winner");
//
//            socketJs.off("bidding_success");
//            socketJs.on("bidding_success", new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs response for listener -->", "bidding_success : " + args.length);
//
//                    if (args.length > 0) {
//                        Logger.printLongLog("nodeJs bidding_success", args[0].toString() + " **");
//                        try {
//                            JSONObject bidPlacedObj = new JSONObject(args[0].toString());
//                            int aId = bidPlacedObj.getInt("auc_id");
//                            final String name = bidPlacedObj.getString("name");
//                            final String address = bidPlacedObj.getString("address");
//                            final String imageUrl = bidPlacedObj.getString("image");
//                            String message = bidPlacedObj.getString("message");
//
//                            final int position = getProductPosition(aId);
//                            Logger.e("nodeJs auction_item_finished pos", "Update list position : " + position);
//
//                            try {
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if (position >= 0) {
//                                            tvItemList.get(position).setWinnerName(name);
//                                            tvItemList.get(position).setAddress(address);
//                                            tvItemList.get(position).setWinnerUrl(profileImageDir + imageUrl);
//
//                                            adapterCurrent.notifyItemChanged(position);
//                                            Logger.e("nodeJs notify auction_item_finished",
//                                                    "Product sold pos:" + position);
//                                        } else {
//                                            currentWinningTask();
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Logger.e("nodeJs el18672", "ex :" + e.getMessage());
//                            }
//
//                        } catch (JSONException e) {
//                            Logger.e("nodeJs itemFinished listen ex", e.getMessage() + "");
//                        }
//                    }
//                }
//            });
//        } else {
//            Logger.e("nodeJs lbl 1", "not connected !!!");
//        }
//    }
//
//    private int getProductPosition(int aId) {
//        //NOTE pId = -1 if only host needs to be notify
//        for (int i = 0; i < tvItemList.size(); i++) {
//            if (tvItemList.get(i).getpId() == (aId)) {
//                return i;
//            }
//        }
//        return -1;
//    }
//
//    private void currentWinningTask() {
//        errorWinning.setVisibility(View.GONE);
//        progressWinning.setVisibility(View.VISIBLE);
//        recyclerWinning.setVisibility(View.GONE);
//        HashMap<String, String> postMap = Api.getInstance().getPostParams();
//
//        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
//        vHelper.addVolleyRequestListeners(Api.getInstance().getCurrentWinningUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
////                pList.clear();
//                try {
//                    JSONObject resObj = new JSONObject(response);
//                    if (resObj.getBoolean("status")) {
//                        profileImageDir = resObj.getString("profile_path");
//
//                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//                        Date serverDate = sdf.parse(resObj.getString("server_time"));
//                        curServiceTime = serverDate.getTime();
//
//                        JSONArray pArray = resObj.getJSONArray("tv_live_auctions");
//                        if (pArray.length() > 0) {
//                            for (int i = 0; i < pArray.length(); i++) {
//                                JSONObject eachObj = pArray.getJSONObject(i);
//
//                                WonAuction current = new WonAuction();
//                                current.setpId(Long.parseLong(eachObj.getString("product_id")));
//                                current.setId(Integer.parseInt(eachObj.getString("id")));
//
//                                Date endTime = sdf.parse(eachObj.getString("end_date"));
//                                current.setEndTime(endTime.getTime());
//
//                                Date startTime = sdf.parse(eachObj.getString("start_date"));
//                                current.setStartTime(startTime.getTime());
//
//                                if (!TextUtils.isEmpty(eachObj.getString("image1"))) {
//                                    current.setImageUrl(eachObj.getString("image1"));
//                                }
//                                current.setpPrice(Double.parseDouble(eachObj.getString("price")));
//                                current.setpName(eachObj.getString("name"));
//
//                                try {
//                                    JSONObject winnerData = eachObj.getJSONObject("winner_data");
//                                    current.setWinnerName(winnerData.getString("first_name") + " " + winnerData.getString("last_name"));
////                                    current.setAddress(winnerData.getString("address") + "," + winnerData.getString("city") + "," + winnerData.getString("country"));
//                                    current.setAddress(winnerData.getString("city"));
//                                    if (!TextUtils.isEmpty(winnerData.getString("image"))) {
//                                        current.setWinnerUrl(profileImageDir + winnerData.getString("image"));
//                                    } else {
//                                        current.setWinnerUrl(resObj.getString("default_path"));
//                                    }
//                                    current.setWinnerId(Long.parseLong(winnerData.getString("user_id")));
//                                    current.setWinningBidAmount(winnerData.getString("userbid_bid_amt"));
//                                } catch (JSONException e) {
//                                    Logger.e("currentWinningTask", e.getMessage());
//                                    current.setWinnerUrl(resObj.getString("default_path"));
//                                    current.setWinnerName("N/A");
//                                    current.setAddress("N/A");
//                                }
//                                tvItemList.add(current);
//                            }
//                            recyclerWinning.setVisibility(View.VISIBLE);
//                            adapterCurrent.setServerTime(curServiceTime);
//                            adapterCurrent.setProfileImageDir(profileImageDir);
//                            if (curService != null && !curService.isShutdown() && !curService.isTerminated()) {
//                                curService.scheduleAtFixedRate(current,
//                                        0, 1000, TimeUnit.MILLISECONDS);
//                            }
//                            try {
//                                connectToServerJs();
//                            } catch (URISyntaxException e) {
//                                Logger.e("nodejs server url ex", e.getMessage() + "");
//                            }
//                            adapterCurrent.notifyDataSetChanged();
//                        } else {
//                            errorWinning.setVisibility(View.VISIBLE);
//                            recyclerWinning.setVisibility(View.GONE);
//                            errorWinning.setText(getString(R.string.no_live_auc));
//                        }
//
//                    } else {
//                        recyclerWinning.setVisibility(View.GONE);
////                        errorWinning.setText(resObj.getString("message"));
//                        errorWinning.setText(getString(R.string.no_live_auc));
//                        errorWinning.setVisibility(View.VISIBLE);
//                    }
//
//                } catch (JSONException e) {
//                    Logger.e("currentWinningTask json ex", e.getMessage() + "");
//                } catch (ParseException e) {
//                    Logger.e("currentWinningTask ParseException ex", e.getMessage() + "");
//                }
//                progressWinning.setVisibility(View.GONE);
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                progressWinning.setVisibility(View.GONE);
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    errorWinning.setText(errorObj.getString("message"));
//                    errorWinning.setVisibility(View.VISIBLE);
//                    recyclerWinning.setVisibility(View.GONE);
//
//                } catch (Exception e) {
//                    try {
//                        if (error instanceof NetworkError) {
//                            errorWinning.setText(getActivity().getString(R.string.error_no_internet));
//                        } else if (error instanceof ServerError) {
//                            errorWinning.setText(getActivity().getString(R.string.error_server));
//                        } else if (error instanceof AuthFailureError) {
//                            errorWinning.setText(getActivity().getString(R.string.error_authFailureError));
//                        } else if (error instanceof ParseError) {
//                            errorWinning.setText(getActivity().getString(R.string.error_parse_error));
//                        } else if (error instanceof TimeoutError) {
//                            errorWinning.setText(getActivity().getString(R.string.error_time_out));
//                        }
//                        if (offset == 0) {
//                            errorWinning.setVisibility(View.VISIBLE);
//                            recyclerWinning.setVisibility(View.GONE);
//                        }
//                    } catch (Exception ee) {
//                    }
//                }
//            }
//        }, "currentWinningTask");
//    }

    private void showBannerImageTask(final View view) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().bannerUrl, Request.Method.POST, postMap,
                new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                JSONArray bannerArray = resObj.getJSONArray("banners");
                                ArrayList<String> images = new ArrayList<>();
                                bannerImageClickUrl.clear();
                                for (int i = 0; i < bannerArray.length(); i++) {
                                    JSONObject eachObj = bannerArray.getJSONObject(i);
                                    images.add(eachObj.getString("banner"));
                                    bannerImageClickUrl.add(eachObj.getString("url"));
                                }
                                setBannerImage(view, images);
                            }
                        } catch (JSONException e) {
                            Logger.e("showBannerImageTaskJson ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                    }
                }, "showBannerImageTask");
    }

    private void setBannerImage(View view, final ArrayList<String> images) {
        final SliderLayout imageSlider = (SliderLayout) view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = (PagerIndicator) view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (int i = 0; i < images.size(); i++) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(images.get(i))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Bundle b = slider.getBundle();
                            String pId = getProductIdFromUrl(b.getString("click_url"));
                            if (pId.equals("") || pId.equalsIgnoreCase(null)) {
                                return;
                            }
                            Logger.e("productId", pId);

                            try {
                                Long.parseLong(pId);

                                Intent intent = new Intent(getActivity(), ProductDetail.class);
                                intent.putExtra("product_id", Long.parseLong(pId));
                                getActivity().startActivityForResult(intent, Home.PRODUCT_DETAIL_REQUEST_CODE);
                            } catch (Exception e) {

                            }
                        }
                    });

            //add your extra information
            Bundle bundle = new Bundle();
            bundle.putString("click_url", bannerImageClickUrl.get(i));
            bundle.putString("position", String.valueOf(i));
            defaultSlider.bundle(bundle);
//            defaultSlider.getBundle()
//                    .putString("extra", images.get(i));

            imageSlider.addSlider(defaultSlider);

//            try {
//                Class<?> c = pagerIndicator.getClass();
//                Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                f.setAccessible(true);
//                f.setInt(pagerIndicator, -1);
//                f.setAccessible(false);
//            } catch (Exception e) {
//                Logger.e("reflection ex 123", e.getMessage() + " ");
//            }
        }

        if (images.size() > 1) {
//            imageSlider.setCurrentPosition(0); //workaround for auto slide to second
//            imageSlider.setDuration(40000);
//            imageSlider.setCustomAnimation(new DescriptionAnimation());
//            imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);

//            try {
//                Class<?> c = pagerIndicator.getClass();
//                Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                f.setAccessible(true);
//                f.setInt(pagerIndicator, -1);
//                f.setAccessible(false);
//            } catch (Exception e) {
//                Logger.e("reflection ex 12", e.getMessage() + " ");
//            }
//            imageSlider.setCustomIndicator(pagerIndicator);
//            imageSlider.setCurrentPosition(0, false);
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
//                        try {
//                            Class<?> c = pagerIndicator.getClass();
//                            Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                            f.setAccessible(true);
//                            f.setInt(pagerIndicator, -1);
//                            f.setAccessible(false);
//                        } catch (Exception e) {
//                            Logger.e("reflection ex 1", e.getMessage() + " ");
//                        }

                        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        imageSlider.setCustomIndicator(pagerIndicator);
//                        imageSlider.setCurrentPosition(0, false);
//                        imageSlider.setCurrentPosition(0, false); //this hangs and gives ANR
                        //so instead of going to first go to last and move to next
                        imageSlider.setCurrentPosition(images.size() - 1, false);
                        imageSlider.moveNextPosition();
                        pagerIndicator.onPageSelected(0);

                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }

    private String getProductIdFromUrl(String url) {
        try {
            return url.substring(url.lastIndexOf("-") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    public void PListingTask(final int limit, final int offset) {
        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        pListRecyclerView.setVisibility(View.GONE);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offset));
        postMap.put("limit", String.valueOf(limit));
        if (helper.isLogin()) {
            postMap.put("user_id", String.valueOf(helper.getUserId()));
            postMap.put("token", helper.getToken());
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().getLiveAuction, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
//                pList.clear();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(resObj.getString("server_time"));
                        serverTime = serverDate.getTime();

                        JSONArray pArray = resObj.getJSONArray("live_auctions");
                        if (pArray.length() > 0) {
                            pList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                Product product = new Product();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

                                product.setFavourite(eachObj.getBoolean("watch"));

                                pList.add(product);
                            }
                            pListRecyclerView.setVisibility(View.VISIBLE);
                            adapter.setServerTime(serverTime);
                            if (service != null && !service.isShutdown() && !service.isTerminated()) {
                                service.scheduleAtFixedRate(runnable,
                                        0, 1000, TimeUnit.MILLISECONDS);
                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            pListRecyclerView.setVisibility(View.GONE);
                            errorText.setText(getString(R.string.no_live_auc));
                        }

                    } else {
                        pListRecyclerView.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("PListingTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("PListingTask ParseException ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorText.setText(errorObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        pListRecyclerView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            pListRecyclerView.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }
            }
        }, "PListingTask");

    }

    public void recentClosedTask(final int limit, final int offset) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offset));
        postMap.put("limit", String.valueOf(limit));
        if (helper.isLogin()) {
            postMap.put("user_id", String.valueOf(helper.getUserId()));
            postMap.put("token", helper.getToken());
        }
        errorClosed.setVisibility(View.GONE);
        progressClosed.setVisibility(View.VISIBLE);
        recyclerClosed.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().getClosedAuctionUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressClosed.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        JSONArray pArray = resObj.getJSONArray("closed_auctions");
                        if (pArray.length() > 0) {
                            closedList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                WonAuction product = new WonAuction();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

//                                if (TextUtils.isEmpty(eachObj.getString("current_winner_name").trim())) {
                                    product.setWinnerName(eachObj.getString("first_name") + " " + eachObj.getString("last_name"));
//                                } else {
//                                    product.setWinnerName(eachObj.getString("current_winner_name"));
//                                }
                                product.setWinnerUrl(eachObj.getString("image"));
                                product.setWinnerGender(eachObj.getString("gender"));
                                product.setWinningBidAmount(eachObj.getString("current_winner_amount"));
                                product.setTotalCreditUsed(eachObj.getString("total_bids"));

                                closedList.add(product);
                            }
                            recyclerClosed.setVisibility(View.VISIBLE);
                            adapterClosed.notifyDataSetChanged();
                        } else {
                            errorClosed.setVisibility(View.VISIBLE);
                            recyclerClosed.setVisibility(View.GONE);
                            errorClosed.setText(R.string.no_closed_auc);
                        }
                    } else {
                        recyclerClosed.setVisibility(View.GONE);
                        progressClosed.setVisibility(View.GONE);
                        errorClosed.setText(resObj.getString("message"));
                        errorClosed.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("recentClosedTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("recentClosedTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorClosed.setText(errorObj.getString("message"));
                        errorClosed.setVisibility(View.VISIBLE);
                        recyclerClosed.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorClosed.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorClosed.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorClosed.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorClosed.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorClosed.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorClosed.setVisibility(View.VISIBLE);
                            recyclerClosed.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }
                progressClosed.setVisibility(View.GONE);
            }
        }, "recentClosedTask");

    }

//    @Override
//    public void setUserVisibleHint(boolean isVisibleToUser) {
//        super.setUserVisibleHint(isVisibleToUser);
//        if (isVisibleToUser) {
//            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
//        } else {
//            getActivity().setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
//        }
//    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            final int firstVisible = layoutManager.findFirstVisibleItemPosition();
            final int lastVisible = layoutManager.findLastVisibleItemPosition();

            mainHandler.post(new Runnable() {
                @Override
                public void run() {
//                    for (int j = 0; j < pList.size(); j++) {
////                        long startTime = pList.get(j).getStartTime();
////                        startTime = startTime - 1000;
////                        pList.get(j).setStartTime(startTime);
//                        long endTime = pList.get(j).getEndTime();
//                        endTime = endTime - 1000;
//                        pList.get(j).setEndTime(endTime);
//                    }
                    adapter.setServerTime(serverTime);
                    serverTime = serverTime + 1000;

                    for (int i = firstVisible; i <= lastVisible; i++) {
                        try {
                            adapter.notifyItemChanged(i);
                        } catch (Exception e) {
                        }
                    }
                }
            });
        }
    };
//
//    Runnable current = new Runnable() {
//        @Override
//        public void run() {
//            final int firstVisible = currentLayoutManager.findFirstVisibleItemPosition();
//            final int lastVisible = currentLayoutManager.findLastVisibleItemPosition();
//
//            currentHandler.post(new Runnable() {
//                @Override
//                public void run() {
////                    for (int j = 0; j < pList.size(); j++) {
//////                        long startTime = pList.get(j).getStartTime();
//////                        startTime = startTime - 1000;
//////                        pList.get(j).setStartTime(startTime);
////                        long endTime = pList.get(j).getEndTime();
////                        endTime = endTime - 1000;
////                        pList.get(j).setEndTime(endTime);
////                    }
//
//                    adapterCurrent.setServerTime(curServiceTime);
//                    curServiceTime = curServiceTime + 1000;
//
//                    for (int i = firstVisible; i <= lastVisible; i++) {
//                        try {
//                            adapterCurrent.notifyItemChanged(i);
//                        } catch (Exception e) {
//                        }
//                    }
//                }
//            });
//
//        }
//    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == PRODUCT_DETAIL_REQUEST_CODE) {
            int position = data.getIntExtra("position", -1);
            if (data.hasExtra("product") && position != -1) {
                Product product = (Product) data.getSerializableExtra("product");
                pList.get(position).setFavourite(product.isFavourite());
                adapter.notifyItemChanged(position);
            }
        }
        if (resultCode == Activity.RESULT_OK && requestCode == MainActivity.START_ACCOUNT_ACTIVITY) {
            if (data.hasExtra("removedIdList")) {
                ArrayList<Long> removedFav = (ArrayList<Long>) data.getSerializableExtra("removedIdList");
                for (int i = 0; i < pList.size(); i++) {
                    long pId = pList.get(i).getpId();
                    if (removedFav.contains(pId)) {
                        pList.get(i).setFavourite(false);
                    }
                }
            }
            if (data.hasExtra("isFav")) {
                for (int i = 0; i < pList.size(); i++) {
                    long pId = pList.get(i).getpId();
                    if (data.getLongExtra("pId", -1) == pId) {
                        pList.get(i).setFavourite(data.getBooleanExtra("isFav", false));
                    }
                }
            }
        }
        adapter.notifyDataSetChanged();
    }


    @Override
    public void onDestroy() {
        service.shutdown();
        VolleyHelper.getInstance(getActivity()).cancelRequest("PListingTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("showBannerImageTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("recentClosedTask");
        VolleyHelper.getInstance(getActivity()).cancelRequest("currentWinningTask");
        super.onDestroy();
    }

    @Override
    public void onPause() {
//        mainHandler.removeCallbacks(runnable);
//        service.shutdown();
//        VolleyHelper.getInstance(getActivity()).cancelRequest("PListingTask");
//        VolleyHelper.getInstance(getActivity()).cancelRequest("showBannerImageTask");
//        VolleyHelper.getInstance(getActivity()).cancelRequest("recentClosedTask");
//        VolleyHelper.getInstance(getActivity()).cancelRequest("currentWinningTask");
        super.onPause();
    }

    @Override
    public void onResume() {
        super.onResume();

//        mainHandler = new Handler(Looper.getMainLooper());
//        service = Executors.newSingleThreadScheduledExecutor();
//        service.scheduleAtFixedRate(runnable,
//                0, 1000, TimeUnit.MILLISECONDS);
    }

    private void openLink(String link) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        startActivity(i);
    }

}