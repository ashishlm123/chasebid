package com.emts.chasebid.fragment;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.Html;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 2016-06-27.
 */
public class ContactUs extends Fragment {
    EditText etName, etEmail, etPhone, etMessage;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_contact_us, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        etName = (EditText) view.findViewById(R.id.et_contact_name);
        etEmail = (EditText) view.findViewById(R.id.et_contact_email);
        etPhone = (EditText) view.findViewById(R.id.et_contact_mobile);
        etMessage = (EditText) view.findViewById(R.id.et_contact_message);

        //error tv
        final TextView tvErrorName = (TextView) view.findViewById(R.id.error_name);
        final TextView tvErrorEmail = (TextView) view.findViewById(R.id.error_email);
        final TextView tvErrorPhone = (TextView) view.findViewById(R.id.error_phone);
        final TextView tvErrorMessage = (TextView) view.findViewById(R.id.error_message);

        final TextView btnSubmit = (TextView) view.findViewById(R.id.btn_submit);
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(!NetworkUtils.isInNetwork(getActivity())){
                    AlertUtils.showSnack(getActivity(),btnSubmit,getString(R.string.error_no_internet));
                    return;
                }
                boolean valid = true;

                //show error
                //name username
                if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                    tvErrorName.setText(getString(R.string.empty_error_msg));
                    tvErrorName.setVisibility(View.VISIBLE);
                    valid = false;
                } else {
                    if (etName.getText().toString().trim().length() > 1) {
                        tvErrorName.setVisibility(View.GONE);
                    } else {
                        tvErrorName.setText(getString(R.string.error_name_length));
                        tvErrorName.setVisibility(View.VISIBLE);
                        valid = false;
                    }
                }
                //email
                if (TextUtils.isEmpty(etEmail.getText().toString().trim())) {
                    tvErrorEmail.setText(getString(R.string.empty_error_msg));
                    tvErrorEmail.setVisibility(View.VISIBLE);
                    valid = false;
                } else {
                    if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                        tvErrorEmail.setVisibility(View.GONE);
                    } else {
                        tvErrorEmail.setText(getString(R.string.enter_email));
                        tvErrorEmail.setVisibility(View.VISIBLE);
                        valid = false;
                    }
                }
                //phone
                if (TextUtils.isEmpty(etPhone.getText().toString().trim())) {
                    tvErrorPhone.setText(getString(R.string.empty_error_msg));
                    tvErrorPhone.setVisibility(View.VISIBLE);
                    valid = false;
                } else {
                    tvErrorPhone.setVisibility(View.GONE);
                }
                //message
                if (TextUtils.isEmpty(etMessage.getText().toString().trim())) {
                    tvErrorMessage.setText(getString(R.string.empty_error_msg));
                    tvErrorMessage.setVisibility(View.VISIBLE);
                    valid = false;
                } else {
                    tvErrorMessage.setVisibility(View.GONE);
                }

                if (valid) {
                    view.clearAnimation();

                    if (NetworkUtils.isInNetwork(getActivity())) {
                        contactUsTask(etName.getText().toString().trim(), etEmail.getText().toString().trim(),
                                etPhone.getText().toString().trim(), etMessage.getText().toString().trim());
                    } else {
                     AlertUtils.showSnack(getActivity(),btnSubmit,getString(R.string.error_no_internet));
                    }

                } else {
                    view.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
                    view.setAnimation(anim);
                    Toast.makeText(getActivity(), getString(R.string.form_validate_message), Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void contactUsTask(final String username, final String email, final String phone, final String message) {
         final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getString(R.string.please_wait));
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();


        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> map = Api.getInstance().getPostParams();
        map.put("fname", username);
        map.put("email", email);
        map.put("mobile", phone);
        map.put("message", message);

        vHelper.addVolleyRequestListeners(Api.getInstance().contactUsUrl, Request.Method.POST, map, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        showAlert(String.valueOf(Html.fromHtml(res.getString("message"))), true);
                    } else {
                        showAlert(String.valueOf(Html.fromHtml(res.getString("message"))), false);
                    }
                } catch (JSONException e) {
                    Logger.e("contactUsTask json ex", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showAlert(String.valueOf(Html.fromHtml(errorObj.getString("message"))), false);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        showAlert(getString(R.string.error_no_internet), false);
                    } else if (error instanceof ServerError) {
                        showAlert(getString(R.string.error_server), false);
                    } else if (error instanceof AuthFailureError) {
                        showAlert(getString(R.string.error_authFailureError), false);
                    } else if (error instanceof ParseError) {
                        showAlert(getString(R.string.error_parse_error), false);
                    } else if (error instanceof TimeoutError) {
                        showAlert(getString(R.string.error_time_out), false);
                    }
                }
            }
        }, "contactUsTask");

    }

    private void showAlert(String message, final boolean exit) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
//        builder.setTitle("Alert");
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (exit) {
                    etName.setText("");
                    etEmail.setText("");
                    etPhone.setText("");
                    etMessage.setText("");
                }
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }
}
