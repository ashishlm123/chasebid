package com.emts.chasebid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.util.HashMap;

public class AboutApp extends Fragment {
//    ProgressBar progressBar;
    FrameLayout progressBar;
    TextView tvHelp, tvError;
    LinearLayout faqLayout;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_about_app, container, false);
        faqLayout = (LinearLayout) view.findViewById(R.id.faq_layout);

        tvHelp = (TextView) view.findViewById(R.id.tv_help);
        tvError = (TextView) view.findViewById(R.id.tv_error);
        progressBar = (FrameLayout) view.findViewById(R.id.progress);
//        progressBar = (ProgressBar) view.findViewById(R.id.progress);

        Bundle bundle = getArguments();
        if (bundle == null) {
            TextView error = new TextView(getActivity());
            error.setText(R.string.no_argument_attach);
            return error;
        }
        boolean privacy = bundle.getBoolean("privacy", false);
        boolean faq = bundle.getBoolean("faq", false);
        boolean howItWorks = bundle.getBoolean("works", false);
        boolean aboutUs = bundle.getBoolean("aboutUs", false);
        boolean terms_and_condition = bundle.getBoolean("tnc", false);

        if (privacy) {
            faqLayout.setVisibility(View.VISIBLE);
            tvHelp.setVisibility(View.GONE);
            String url = Api.getInstance().cmsUrl;
            String slug = "privacy-policy";
            if (NetworkUtils.isInNetwork(getActivity())) {
                howItWorkTask(url, slug);
            } else {
                tvError.setText(getString(R.string.error_no_internet));
                tvError.setVisibility(View.VISIBLE);
                faqLayout.setVisibility(View.GONE);
            }

        } else if (howItWorks) {
            faqLayout.setVisibility(View.GONE);
            tvHelp.setVisibility(View.VISIBLE);
            String url = Api.getInstance().cmsUrl;
            String slug = "how-it-works";
            if (NetworkUtils.isInNetwork(getActivity())) {
                howItWorkTask(url, slug);
            } else {
                tvError.setText(getString(R.string.error_no_internet));
                tvError.setVisibility(View.VISIBLE);
                tvHelp.setVisibility(View.GONE);
            }

        } else if (aboutUs) {
            faqLayout.setVisibility(View.GONE);
            tvHelp.setVisibility(View.VISIBLE);
            String url = Api.getInstance().cmsUrl;
            String slug = "about-us";
            if (NetworkUtils.isInNetwork(getActivity())) {
                howItWorkTask(url, slug);
            } else {
                tvError.setText(getString(R.string.error_no_internet));
                tvError.setVisibility(View.VISIBLE);
                tvHelp.setVisibility(View.GONE);
            }

        } else if (terms_and_condition) {
            faqLayout.setVisibility(View.GONE);
            tvHelp.setVisibility(View.VISIBLE);
            String url = Api.getInstance().cmsUrl;
            String slug = "terms-and-conditions";

            if (NetworkUtils.isInNetwork(getActivity())) {
                howItWorkTask(url, slug);
            } else {
                tvError.setText(getString(R.string.error_no_internet));
                tvError.setVisibility(View.VISIBLE);
                tvHelp.setVisibility(View.GONE);
            }

        } else if (faq) {
            faqLayout.setVisibility(View.VISIBLE);
            tvHelp.setVisibility(View.GONE);
            String url = Api.getInstance().cmsUrl;
            if (NetworkUtils.isInNetwork(getActivity())) {
                faqTask(url);
            } else {
                tvError.setText(getString(R.string.error_no_internet));
                tvError.setVisibility(View.VISIBLE);
                faqLayout.setVisibility(View.GONE);
            }
        }

        return view;
    }

    private void howItWorkTask(String url, String slug) {
        progressBar.setVisibility(View.VISIBLE);
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("cms_slug", slug);

        vHelper.addVolleyRequestListeners(url, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
//                        try {
                            JSONObject eachObj = res.getJSONObject("cms");
//                            tvHelp.setText(Html.fromHtml(URLDecoder.decode(eachObj.getString("content"), "UTF-8")));
                            tvHelp.setText(Html.fromHtml(eachObj.getString("content")));
                            tvHelp.setVisibility(View.VISIBLE);
                            tvError.setVisibility(View.GONE);
//                        } catch (UnsupportedEncodingException e) {
//                            Logger.e(": UnsupportedEncodingException", e.getMessage() + "*");
//                        }

                    } else {
                        tvError.setText(res.getString("message"));
                        tvHelp.setVisibility(View.GONE);
                        tvError.setVisibility(View.VISIBLE);
                        //  Toast.makeText(getActivity(), res.getString("message"), Toast.LENGTH_SHORT).show();
                    }
                } catch (JSONException e) {
                    Logger.e("howItWorkTaskJson ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    tvError.setText(errorObj.getString("message"));
                    tvError.setVisibility(View.VISIBLE);
                    tvHelp.setVisibility(View.GONE);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        tvError.setText(getActivity().getString(R.string.error_no_internet));
                    } else if (error instanceof ServerError) {
                        tvError.setText(getActivity().getString(R.string.error_server));
                    } else if (error instanceof AuthFailureError) {
                        tvError.setText(getActivity().getString(R.string.error_authFailureError));
                    } else if (error instanceof ParseError) {
                        tvError.setText(getActivity().getString(R.string.error_parse_error));
                    } else if (error instanceof TimeoutError) {
                        tvError.setText(getActivity().getString(R.string.error_time_out));
                    }
                    tvError.setVisibility(View.VISIBLE);
                    tvHelp.setVisibility(View.GONE);
                }
            }
        }, "howItWorksTask");


    }

    private void faqTask(String url) {

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().faqUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                Logger.printLongLog("response", response);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray categoryList = res.getJSONArray("help_cat");
                        for (int i = 0; i < categoryList.length(); i++) {
                            JSONObject category = categoryList.getJSONObject(i);
                            addCategoryView(category.getString("help_category_name"));
                            JSONArray data = category.getJSONArray("cat_data");
                            for (int j = 0; j < data.length(); j++) {
                                JSONObject qsnAns = data.getJSONObject(j);
                                addQsnAnsView(qsnAns.getString("title"), qsnAns.getString("description"));
                            }
                        }
                        tvError.setVisibility(View.GONE);
                        faqLayout.setVisibility(View.VISIBLE);

                    } else {
                        tvError.setText(res.getString("message"));
                        faqLayout.setVisibility(View.GONE);
                        tvError.setVisibility(View.VISIBLE);
                    }
                } catch (JSONException e) {
                    Logger.e("faqTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    tvError.setText(errorObj.getString("message"));
                    tvError.setVisibility(View.VISIBLE);
                    faqLayout.setVisibility(View.GONE);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        tvError.setText(getActivity().getString(R.string.error_no_internet));
                    } else if (error instanceof ServerError) {
                        tvError.setText(getActivity().getString(R.string.error_server));
                    } else if (error instanceof AuthFailureError) {
                        tvError.setText(getActivity().getString(R.string.error_authFailureError));
                    } else if (error instanceof ParseError) {
                        tvError.setText(getActivity().getString(R.string.error_parse_error));
                    } else if (error instanceof TimeoutError) {
                        tvError.setText(getActivity().getString(R.string.error_time_out));
                    }
                    tvError.setVisibility(View.VISIBLE);
                    faqLayout.setVisibility(View.GONE);
                }
            }
        }, "faqTask");

        progressBar.setVisibility(View.VISIBLE);
    }


    private void addQsnAnsView(String question, String answer) {

        View v;
        v = LayoutInflater.from(getActivity()).inflate(R.layout.inflated_faq_qsn_answer_layout, null);
        final TextView qsn = (TextView) v.findViewById(R.id.faq_qsn);
        final TextView ans = (TextView) v.findViewById(R.id.faq_answer);
        qsn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (ans.getVisibility() == View.VISIBLE) {
                    ans.setVisibility(View.GONE);
                    qsn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_expand, 0, 0, 0);
                } else {
                    ans.setVisibility(View.VISIBLE);
                    qsn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.ic_down, 0, 0, 0);
                }
            }
        });
        qsn.setText(question);
        ans.setText(Html.fromHtml(answer));
        faqLayout.addView(v);
    }

    private void addCategoryView(String cat_name) {
        View v;
        v = LayoutInflater.from(getActivity()).inflate(R.layout.inflated_faq_title_layout, null, false);
        TextView category = (TextView) v.findViewById(R.id.category_name);
        category.setText(cat_name);
        faqLayout.addView(v);
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("howItWorksTask");
        VolleyHelper.getInstance(getContext()).cancelRequest("faqTask");
        super.onPause();
    }

}
