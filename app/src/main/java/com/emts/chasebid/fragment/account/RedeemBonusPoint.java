package com.emts.chasebid.fragment.account;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.adapters.RedeemBonusAdapter;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Redeem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import rm.com.clocks.Clock;

/**
 * Created by Srijana on 3/28/2017.
 */

public class RedeemBonusPoint extends Fragment {
    ArrayList<Redeem> rList;

    RecyclerView listRecycleView;
    LinearLayoutManager layoutManager;
    RedeemBonusAdapter adapter;
    LinearLayout reedemHolder;
    PreferenceHelper preferenceHelper;

//    ProgressBar progressBar;
    TextView errorText, bonus, btnRedeem;
    FrameLayout progressBar;
//    Clock clock;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_redeem_bonus, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        preferenceHelper = PreferenceHelper.getInstance(getContext());

//        progressBar = (ProgressBar) view.findViewById(R.id.redeem_progress);
        progressBar = (FrameLayout) view.findViewById(R.id.custom_progress);
        errorText = (TextView) view.findViewById(R.id.error_redeem);
        listRecycleView = (RecyclerView) view.findViewById(R.id.redeem_list);
        listRecycleView.setNestedScrollingEnabled(false);
        reedemHolder = (LinearLayout) view.findViewById(R.id.reedem_layout);
        bonus = (TextView) view.findViewById(R.id.bonusvalue);
        btnRedeem = (TextView) view.findViewById(R.id.btn_redeem);
        btnRedeem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    boolean isSthChecked = false;
                    for (int i = 0; i < rList.size(); i++) {
                        if (rList.get(i).isChecked()) {
                            isSthChecked = true;
                        }
                    }
                    if (isSthChecked) {
                        placeRedeemTask();
                    } else {
                        AlertUtils.showSnack(getActivity(), view, getString(R.string.choose_bonus_redeem));
                    }
                } else {
                    AlertUtils.showSnack(getActivity(), errorText, getString(R.string.error_no_internet));
                }
            }
        });

        layoutManager = new LinearLayoutManager(getActivity());
        listRecycleView.setLayoutManager(layoutManager);

        rList = new ArrayList<>();

        adapter = new RedeemBonusAdapter(getContext(), rList);
        listRecycleView.setAdapter(adapter);


        if (NetworkUtils.isInNetwork(getActivity())) {
            redeemListTask();
        } else {
            progressBar.setVisibility(View.GONE);
            listRecycleView.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void placeRedeemTask() {
        final ProgressDialog pDailog = AlertUtils.showProgressDialog(getContext(), getString(R.string.please_wait));
        pDailog.show();
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));

        for (int i = 0; i < rList.size(); i++) {
            if (rList.get(i).isChecked()) {
                postMap.put("package", rList.get(i).getId());
            }
        }

        errorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().userBonusUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDailog.dismiss();

//                pList.clear();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        ((MainActivity) getActivity()).getUserData();
                        ((MainActivity) getActivity()).openPurchaseHistory();


                        AlertUtils.showToast(getActivity(), resObj.getString("message"));

                    } else {
                        AlertUtils.showAlertMessage(getActivity(), getString(R.string.title_error_mesg), resObj.getString("message"));
                        errorText.setVisibility(View.GONE);
                    }

                } catch (JSONException e) {
                    Logger.e("placeRedeemTaskjson ex", e.getMessage() + "");
                } catch (Exception e) {
                    Logger.e("placeRedeemTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDailog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
//                    if (offset == 0) {
                    AlertUtils.showAlertMessage(getActivity(), getString(R.string.title_error_mesg), errorObj.getString("message"));
                    errorText.setVisibility(View.GONE);
//                    }
                } catch (Exception e) {
                    Logger.e("placeRedeemTask onError Exception", e.getMessage() + "");
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
//                        if (offset == 0) {
                        errorText.setVisibility(View.VISIBLE);
                        listRecycleView.setVisibility(View.GONE);
//                        }
                    } catch (Exception ee) {
                    }
                }
            }
        }, " placeRedeemTask");

    }

    private void redeemListTask() {

        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        postMap.put("token", preferenceHelper.getToken());

        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
        reedemHolder.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().userBonusUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                reedemHolder.setVisibility(View.VISIBLE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {

                        bonus.setText(resObj.getString("user_total_bonus") + " points");

                        JSONArray pArray = resObj.getJSONArray("bonus_packages");
                        if (pArray.length() > 0) {
                            rList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                Redeem redeem = new Redeem();
                                redeem.setBonuspoint(eachObj.getString("bonus_points"));
                                redeem.setBid(eachObj.getString("credits"));
                                redeem.setId(eachObj.getString("id"));
                                rList.add(redeem);
                            }
                            listRecycleView.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                            errorText.setText(getString(R.string.no_product));
                        }

                    } else {
                        reedemHolder.setVisibility(View.GONE);
                        listRecycleView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("  redeemListTask json ex", e.getMessage() + "");
                } catch (Exception e) {
                    Logger.e(" redeemListTask ParseException ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
//                    if (offset == 0) {
                    errorText.setText(errorObj.getString("message"));
                    errorText.setVisibility(View.VISIBLE);
                    listRecycleView.setVisibility(View.GONE);
//                    }
                } catch (Exception e) {
                    Logger.e(" redeemListTask onError Exception", e.getMessage() + "");
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
//                        if (offset == 0) {
                        errorText.setVisibility(View.VISIBLE);
                        listRecycleView.setVisibility(View.GONE);
//                        }
                    } catch (Exception ee) {
                    }
                }

            }
        }, "  redeemListTask");

    }


}
