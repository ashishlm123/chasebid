package com.emts.chasebid.fragment.account;


import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class ReferFriend extends android.support.v4.app.Fragment {
    boolean isValid = true;
    EditText fName, fEmail;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_refer_friend, container, false);

        TextView btnReferFriend = (TextView) view.findViewById(R.id.btn_refer_frnd);
        btnReferFriend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                isValid = true;
                fName = (EditText) view.findViewById(R.id.et_friend_name);
                fEmail = (EditText) view.findViewById(R.id.et_friend_email);
                TextView error_name = (TextView) view.findViewById(R.id.error_name);
                TextView error_email = (TextView) view.findViewById(R.id.error_email);

                if (TextUtils.isEmpty(fName.getText().toString().trim())) {
                    error_name.setText(getString(R.string.empty_error_msg));
                    error_name.setVisibility(View.VISIBLE);
                    isValid = false;
                } else {
                    error_name.setVisibility(View.GONE);
                }

                if (TextUtils.isEmpty(fEmail.getText().toString().trim())) {
                    isValid = false;
                    error_email.setText(getString(R.string.empty_error_msg));
                    error_email.setVisibility(View.VISIBLE);
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(fEmail.getText().toString().trim()).matches()) {
                        isValid = false;
                        error_email.setText(getString(R.string.enter_valid_email));
                        error_email.setVisibility(View.VISIBLE);
                    } else {
                        error_email.setVisibility(View.GONE);
                    }
                }
                if (isValid) {
                    v.clearAnimation();

                    if (NetworkUtils.isInNetwork(getActivity())) {
                        referFriendTask(fName.getText().toString().trim(), fEmail.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getActivity(), error_email, getString(R.string.error_no_internet));
                    }
                } else {
                    v.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
                    v.setAnimation(anim);
                }
            }
        });

        return view;
    }

    private void referFriendTask(final String name, final String email) {
        final ProgressDialog pDialog = new ProgressDialog(getActivity());
        pDialog.setMessage(getString(R.string.sending));
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        final PreferenceHelper prefsHelper = PreferenceHelper.getInstance(getActivity());

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> map = Api.getInstance().getPostParams();
        map.put("user_id", String.valueOf(prefsHelper.getUserId()));
        map.put("name[1]", name);
        map.put("email[1]", email);
        map.put("token", prefsHelper.getToken());

        vHelper.addVolleyRequestListeners(Api.getInstance().referFriendUrl, Request.Method.POST, map, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        showAlert(res.getString("message"), true);
                    } else {
                        showAlert(res.getString("message"), false);
                    }
                } catch (JSONException e) {
                    Logger.e(" referFriend Json exception ", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();

                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showAlert(errorObj.getString("message"), false);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        showAlert(getString(R.string.error_no_internet), false);
                    } else if (error instanceof ServerError) {
                        showAlert(getString(R.string.error_server), false);
                    } else if (error instanceof AuthFailureError) {
                        showAlert(getString(R.string.error_authFailureError), false);
                    } else if (error instanceof ParseError) {
                        showAlert(getString(R.string.error_parse_error), false);
                    } else if (error instanceof TimeoutError) {
                        showAlert(getString(R.string.error_time_out), false);
                    }
                }
            }
        }, "referFriend");

    }

    private void showAlert(String message, final boolean exit) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle("Alert");
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (exit) {
                    fName.setText("");
                    fEmail.setText("");
                }
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("referFriendTask");
        super.onPause();
    }
}

