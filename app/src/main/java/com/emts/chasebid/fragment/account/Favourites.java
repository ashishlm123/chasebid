package com.emts.chasebid.fragment.account;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.FavListAdapter;
import com.emts.chasebid.customviews.EndlessRecyclerViewScrollListener;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class Favourites extends android.support.v4.app.Fragment {
    RecyclerView favList;
    ArrayList<Product> favLists = new ArrayList<>();
    SwipeRefreshLayout listRefresh;
    ArrayList<Long> favRemovedId = new ArrayList<>();
    ProgressBar progressInfinite;
    //    ProgressBar progressBar, progressInfinite;
    FrameLayout progressBar;
    TextView errorText;
    FavListAdapter adapter;
    int limit = Config.LIMIT;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        favList = (RecyclerView) view.findViewById(R.id.list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        favList.setLayoutManager(layoutManager);

        progressBar = (FrameLayout) view.findViewById(R.id.progress);
//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressInfinite = (ProgressBar) view.findViewById(R.id.progress_infinite);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);

        adapter = new FavListAdapter(getActivity(), favLists, true);
        favList.setAdapter(adapter);
        // favList.setVisibility(View.VISIBLE);

        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    offset = 0;
                    favListingTask(offset);
                } else {
                    listRefresh.setRefreshing(false);
                    favList.setVisibility(View.GONE);
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }


            }
        });

        adapter.setOnRecyclerViewItemClickListener(new FavListAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onRemoveFav(long id, int position) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    removeFromFavTask(id, position);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                }

            }
        });
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (favLists.size() >= limit) {
                    favListingTask(offset);
                    Logger.e("loadmore", "loadmore");
                }
            }
        };
        favList.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            favListingTask(offset);
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            favList.setVisibility(View.GONE);
        }

    }

    public void favListingTask(final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }
        final PreferenceHelper helper = PreferenceHelper.getInstance(getActivity());
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", String.valueOf(helper.getUserId()));
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offsetValue));


        vHelper.addVolleyRequestListeners(Api.getInstance().getFavUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray pArray = resObj.getJSONArray("watchlist");
                        if (pArray.length() > 0) {
                            if (offset == 0) {
                                favLists.clear();
                            }
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                Product product = new Product();
                                product.setpId(Long.parseLong(eachObj.getString("auction_id")));
                                product.setId(Integer.parseInt(eachObj.getString("id")));
                                product.setWinnerId(Long.parseLong(eachObj.getString("user_id")));
                                product.setpName(eachObj.getString("name"));
//                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setFavTime(eachObj.getString("watch_date"));
                                product.setImageUrl(eachObj.getString("image1"));
                                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date starttime = null;
                                try {
                                    starttime = sdf2.parse(eachObj.getString("start_date"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                product.setStartTime(starttime.getTime());
                                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date endtime = null;
                                try {
                                    endtime = sdf2.parse(eachObj.getString("end_date"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                product.setEndTime(endtime.getTime());

                                favLists.add(product);
                            }
                            offset = offset + limit;
                            adapter.notifyDataSetChanged();
                            favList.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        } else {
                            if (offset == 0) {
                                errorText.setVisibility(View.VISIBLE);
                                favList.setVisibility(View.GONE);
                                errorText.setText(R.string.no_prod_in_fav);
                                errorText.setVisibility(View.VISIBLE);
                            } else {
                                favList.setVisibility(View.VISIBLE);
                                AlertUtils.showSnack(getActivity(), errorText, getString(R.string.no_more_fav));
                            }
//                            infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                        }

                    } else {
                        if (offset == 0) {
                            favList.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            errorText.setText(resObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                            progressInfinite.setVisibility(View.GONE);
                        } else {
                            favList.setVisibility(View.GONE);
                            AlertUtils.showSnack(getActivity(), errorText, resObj.getString("message"));
                            progressInfinite.setVisibility(View.GONE);
                        }
                    }

                } catch (JSONException e) {
                    Logger.e("json ex", e.getMessage() + "");
                }

                progressBar.setVisibility(View.GONE);
                progressInfinite.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);

            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("onError Exception", e.getMessage() + "");

                }
                try {
                    if (error instanceof NetworkError) {
                        errorMessage = getActivity().getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMessage = getActivity().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMessage = getActivity().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMessage = getActivity().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMessage = getActivity().getString(R.string.error_time_out);
                    }
                } catch (Exception ee) {
                    Logger.e("favListingTask error catch", ee.getMessage() + "");
                }
                if (offset == 0) {
                    errorText.setText(errorMessage);
                    errorText.setVisibility(View.VISIBLE);
                    favList.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(getActivity(), errorText, errorMessage);
                }
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("favListingTask  exception", e.getMessage());
                }
                listRefresh.setRefreshing(false);
                favList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }, "favListingTask");

        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
    }

    private void removeFromFavTask(final long productId, final int position) {
        final PreferenceHelper helper = PreferenceHelper.getInstance(getActivity());

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> map = Api.getInstance().getPostParams();
        map.put("user_id", String.valueOf(helper.getUserId()));
        map.put("product_id", String.valueOf(productId));

        vHelper.addVolleyRequestListeners(Api.getInstance().favListUrl, Request.Method.POST, map, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        favLists.remove(position);
                        adapter.notifyItemRemoved(position);

                        // while removing favourite to update in live auction
                        favRemovedId.add(productId);
                        Intent intent = new Intent();
                        intent.putExtra("removedIdList", favRemovedId);
                        getActivity().setResult(Activity.RESULT_OK, intent);

                        if (favLists.size() == 0) {
                            errorText.setText(getString(R.string.no_prod_in_fav));
                            favList.setVisibility(View.GONE);
                            errorText.setVisibility(View.VISIBLE);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("removeFromFavTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ServerError) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof AuthFailureError) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof ParseError) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                        } else if (error instanceof TimeoutError) {
                            Toast.makeText(getActivity(), getActivity().getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ee) {
                        Logger.e("orderListTask error catch", e.getMessage() + "");
                    }
                }

            }
        }, "orderListTask");

    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("removeFromFavTask");
        VolleyHelper.getInstance(getContext()).cancelRequest("favListingTask");
        super.onPause();
        offset = 0;
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        offset = 0;
    }

    @Override
    public void onDetach() {
        VolleyHelper.getInstance(getContext()).cancelRequest("removeFromFavTask");
        VolleyHelper.getInstance(getContext()).cancelRequest("favListingTask");
        super.onDetach();
        offset = 0;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == FavListAdapter.PRODUCT_DETAIL_REQUEST_CODE) {
            favRemovedId.add(data.getLongExtra("pId", -1));
            Intent intent = new Intent();
            intent.putExtra("removedIdList", favRemovedId);
            getActivity().setResult(Activity.RESULT_OK, intent);
            favListingTask(offset);
        }
    }



}
