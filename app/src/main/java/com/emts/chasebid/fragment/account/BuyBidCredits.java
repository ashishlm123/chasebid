package com.emts.chasebid.fragment.account;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.PaymentActivityAll;
import com.emts.chasebid.adapters.BidCreditAdapter;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.BuyCredit;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class BuyBidCredits extends android.support.v4.app.Fragment {
    RecyclerView buyList;
    FrameLayout progressBar;
    //    ProgressBar progressBar;
    SwipeRefreshLayout listRefresh;
    TextView errorText;
    final ArrayList<BuyCredit> creditList = new ArrayList<>();
    BidCreditAdapter adapter;
    PreferenceHelper prefsHelper;

    String ccSuccessUrlInfo, ccCancelUrlInfo;

    String trackPrice, trackId;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefsHelper = PreferenceHelper.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        buyList = (RecyclerView) view.findViewById(R.id.list_recycler);
        buyList.setLayoutManager(new LinearLayoutManager(getActivity()));

        progressBar = (FrameLayout) view.findViewById(R.id.progress);
//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);

        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    getBuyBidCreditsTask();
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    listRefresh.setRefreshing(false);
                    buyList.setVisibility(View.GONE);
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });

        adapter = new BidCreditAdapter(getActivity(), creditList);
        buyList.setAdapter(adapter);

        adapter.setOnRecyclerViewItemClickListener(new BidCreditAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClicked(View view, int position, long id,
                                                  String paymentMethod, String paymentId, String couponCode) {
                if (paymentId.equalsIgnoreCase("-1")) {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.choose_payment_to_continue));
                    return;
                }
                if (paymentId.equals(String.valueOf(Config.PAYMENT_GATE_CCAVENUE_ID))) {
                    //get cc payment info
                    getCCPaymentInfoTask(paymentId, couponCode, position);
                } else if (paymentId.equals(String.valueOf(Config.PAYMENT_GATE_PAYPAL_ID))) {
                    String postParams = "api_key=" + Api.getInstance().apiKey + "&token=" + prefsHelper.getToken()
                            + "&user_id=" + prefsHelper.getUserId()
                            + "&transaction_type=purchase_credit&package=" + creditList.get(position).getPackageId()
                            + "&payment_type=" + paymentId + "&voucher=" + couponCode;

                    if (TextUtils.isEmpty(couponCode)) {
                        Intent intent = new Intent(getActivity(), PaymentActivityAll.class);
                        intent.putExtra("price", creditList.get(position).getCreditPrice());
                        intent.putExtra("id", creditList.get(position).getPackageId());
//                        intent.putExtra("currency", infoObj.getString("currency"));

                        intent.putExtra("post_params", postParams);
                        intent.putExtra("is_buy_credit", true);
                        intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYPAL_ID);
                        startActivity(intent);
                    } else {
                        trackId = creditList.get(position).getPackageId();
                        trackPrice = creditList.get(position).getCreditPrice();
                        checkPromoCodeTask(couponCode, postParams);
                    }
                } else if (paymentId.equals(String.valueOf(Config.PAYMENT_GATE_PAYTM_ID))) {
//                    getPaytmPaymentInfoTask(paymentId, couponCode, position);

                    String postParams = "api_key=" + Api.getInstance().apiKey + "&token=" + prefsHelper.getToken()
                            + "&user_id=" + prefsHelper.getUserId()
                            + "&transaction_type=purchase_credit&package=" + creditList.get(position).getPackageId()
                            + "&payment_type=" + paymentId + "&voucher=" + couponCode;

                    if (TextUtils.isEmpty(couponCode)) {
                        Intent intent = new Intent(getActivity(), PaymentActivityAll.class);
                        intent.putExtra("price", creditList.get(position).getCreditPrice());
                        intent.putExtra("id", creditList.get(position).getPackageId());
//                        intent.putExtra("currency", infoObj.getString("currency"));

                        intent.putExtra("post_params", postParams);
                        intent.putExtra("is_buy_credit", true);
                        intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYTM_ID);
                        startActivity(intent);
                    } else {
                        trackId = creditList.get(position).getPackageId();
                        trackPrice = creditList.get(position).getCreditPrice();
                        checkPromoCodeTask(couponCode, postParams);
                    }
                }
            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            getBuyBidCreditsTask();
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            buyList.setVisibility(View.GONE);
        }
    }

//    private void getPaytmPaymentInfoTask(final String paymentId, final String couponCode, final int position)  {
//        final Dialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.get_cc_info));
//
//        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
//        HashMap<String, String> postparams = Api.getInstance().getPostParams();
//        postparams.put("token", prefsHelper.getToken());
//        postparams.put("user_id", String.valueOf(prefsHelper.getUserId()));
//
//        vHelper.addVolleyRequestListeners(Api.getInstance().getPaytmInfoUrl, Request.Method.POST, postparams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                pDialog.dismiss();
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//                        //cc payment
//                        String postParams = "api_key=" + Api.getInstance().apiKey + "&token=" + prefsHelper.getToken()
//                                + "&user_id=" + prefsHelper.getUserId()
//                                + "&transaction_type=purchase_credit'" +
//                                "&package=" + creditList.get(position).getPackageId()
//                                + "&amount=" + creditList.get(position).getCreditPrice()
//                                + "&payment_type=" + paymentId + "&voucher=" + couponCode;
//                        //insert ccPaymentInfo from response to post
//                        JSONObject infoObj = res.getJSONObject("data");
//                        postParams = postParams + "&tid=" + infoObj.getString("tid") +
//                                "&merchant_id=" + infoObj.getString("merchant_id") +
//                                "&currency=" + infoObj.getString("currency") +
//                                "&redirect_url=" + infoObj.getString("redirect_url") +
//                                "&cancel_url=" + infoObj.getString("cancel_url") +
//                                "&order_id=" + infoObj.getString("order_id");
//
//                        ccSuccessUrlInfo = infoObj.getString("redirect_url");
//                        ccCancelUrlInfo = infoObj.getString("cancel_url");
//
//                        if (TextUtils.isEmpty(couponCode)) {
//                            Intent intent = new Intent(getActivity(), PaymentActivityAll.class);
//                            intent.putExtra("post_params", postParams);
//                            intent.putExtra("is_buy_credit", true);
//                            intent.putExtra("success_url", infoObj.getString("redirect_url"));
//                            intent.putExtra("cancel_url", infoObj.getString("cancel_url"));
//                            intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYTM_ID);
//                            startActivity(intent);
//                        } else {
//    trackId = creditList.get(position).getPackageId();
//    trackPrice = creditList.get(position).getCreditPrice();
//                            checkPromoCodeTask(couponCode, postParams);
//                        }
//                    } else {
//                        AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"));
//                    }
//                } catch (JSONException e) {
//                    Logger.e(" getPaytmPaymentInfoTask Json exception ", e.getMessage());
//                    AlertUtils.showAlertMessage(getActivity(), "Error", "Cannot get required payment information");
//                }
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError error) {
//                String errorMsg = getString(R.string.unexpected_error);
//                try {
//                    JSONObject errorObj = new JSONObject(errorResponse);
//                    errorMsg = errorObj.getString("message");
//                } catch (Exception e) {
//                    if (error instanceof NetworkError) {
//                        errorMsg = getActivity().getString(R.string.error_no_internet);
//                    } else if (error instanceof ServerError) {
//                        errorMsg = getActivity().getString(R.string.error_server);
//                    } else if (error instanceof AuthFailureError) {
//                        errorMsg = getActivity().getString(R.string.error_authFailureError);
//                    } else if (error instanceof ParseError) {
//                        errorMsg = getActivity().getString(R.string.error_parse_error);
//                    } else if (error instanceof TimeoutError) {
//                        errorMsg = getActivity().getString(R.string.error_time_out);
//                    }
//                }
//                AlertUtils.showAlertMessage(getActivity(), "Error !!!", errorMsg);
//            }
//        }, "getPaytmPaymentInfoTask");
//    }

    private void getCCPaymentInfoTask(final String paymentId, final String couponCode, final int position) {
        final Dialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.get_cc_info));

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postparams = Api.getInstance().getPostParams();
        postparams.put("token", prefsHelper.getToken());
        postparams.put("user_id", String.valueOf(prefsHelper.getUserId()));

        vHelper.addVolleyRequestListeners(Api.getInstance().getCCPaymentInfoUrl, Request.Method.POST, postparams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        //insert ccPaymentInfo from response to post
                        JSONObject infoObj = res.getJSONObject("data");
                        //cc payment
                        String postParams = "api_key=" + Api.getInstance().apiKey + "&token=" + prefsHelper.getToken()
                                + "&user_id=" + prefsHelper.getUserId()
                                + "&transaction_type=purchase_credit" +
                                "&package=" + creditList.get(position).getPackageId()
                                + "&amount=" + creditList.get(position).getCreditPrice()
                                + "&payment_type=" + paymentId + "&voucher=" + couponCode;
                        postParams = postParams + "&tid=" + infoObj.getString("tid") +
                                "&merchant_id=" + infoObj.getString("merchant_id") +
                                "&currency=" + infoObj.getString("currency") +
                                "&redirect_url=" + infoObj.getString("redirect_url") +
                                "&cancel_url=" + infoObj.getString("cancel_url") +
                                "&order_id=" + infoObj.getString("order_id");

                        ccSuccessUrlInfo = infoObj.getString("redirect_url");
                        ccCancelUrlInfo = infoObj.getString("cancel_url");

                        if (TextUtils.isEmpty(couponCode)) {
                            Intent intent = new Intent(getActivity(), PaymentActivityAll.class);
                            intent.putExtra("price", creditList.get(position).getCreditPrice());
                            intent.putExtra("id", creditList.get(position).getPackageId());
                            intent.putExtra("currency", infoObj.getString("currency"));

                            intent.putExtra("post_params", postParams);
                            intent.putExtra("is_buy_credit", true);
                            intent.putExtra("success_url", infoObj.getString("redirect_url"));
                            intent.putExtra("cancel_url", infoObj.getString("cancel_url"));
                            intent.putExtra("payment_type", Config.PAYMENT_GATE_CCAVENUE_ID);
                            startActivity(intent);
                        } else {
                            trackId = creditList.get(position).getPackageId();
                            trackPrice = creditList.get(position).getCreditPrice();
                            checkPromoCodeTask(couponCode, postParams);
                        }
                    } else {
                        AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e(" getCCPaymentInfoTask Json exception ", e.getMessage());
                    AlertUtils.showAlertMessage(getActivity(), "Error", "Cannot get required payment information");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMsg = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        errorMsg = getActivity().getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMsg = getActivity().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMsg = getActivity().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMsg = getActivity().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMsg = getActivity().getString(R.string.error_time_out);
                    }
                }
                AlertUtils.showAlertMessage(getActivity(), "Error !!!", errorMsg);
            }
        }, "getCCPaymentInfoTask");
    }

    private void checkPromoCodeTask(String voucher, final String postParams) {
        final Dialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.verify_coupon));

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        final HashMap<String, String> postparams = Api.getInstance().getPostParams();
        postparams.put("token", prefsHelper.getToken());
        postparams.put("user_id", String.valueOf(prefsHelper.getUserId()));
        postparams.put("voucher", voucher);

        vHelper.addVolleyRequestListeners(Api.getInstance().checkPromoCodeUrl, Request.Method.POST,
                postparams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                AlertDialog.Builder alertDialog = new AlertDialog.Builder(getContext());
                                alertDialog.setTitle("Alert message");
                                alertDialog.setMessage(res.getString("message"));
                                alertDialog.setCancelable(false);
                                alertDialog.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();

                                        String paypalId = "&payment_type=" + Config.PAYMENT_GATE_PAYPAL_ID;
                                        String ccAvenueId = "&payment_type=" + Config.PAYMENT_GATE_CCAVENUE_ID;
                                        String payTm = "&payment_type=" + Config.PAYMENT_GATE_CCAVENUE_ID;

                                        Intent intent = new Intent(getActivity(), PaymentActivityAll.class);
                                        intent.putExtra("price", trackPrice);
                                        intent.putExtra("id", trackId);
//                                      intent.putExtra("currency", );
                                        intent.putExtra("post_params", postParams);
                                        intent.putExtra("is_buy_credit", true);
                                        if (postParams.contains(ccAvenueId)) {
                                            intent.putExtra("success_url", ccSuccessUrlInfo);
                                            intent.putExtra("cancel_url", ccCancelUrlInfo);
                                            intent.putExtra("payment_type", Config.PAYMENT_GATE_CCAVENUE_ID);
                                        } else if (postParams.contains(paypalId)) {
                                            intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYPAL_ID);
                                        } else if (postParams.contains(payTm)) {
                                            intent.putExtra("success_url", ccSuccessUrlInfo);
                                            intent.putExtra("cancel_url", ccCancelUrlInfo);
                                            intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYTM_ID);
                                        }
                                        startActivity(intent);
                                    }
                                });
                                alertDialog.show();


                            } else {
                                AlertUtils.showAlertMessage(getActivity(), "Error", res.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e(" checkPromoCodeTask Json exception ", e.getMessage());
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        String errorMsg = getString(R.string.unexpected_error);
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorMsg = errorObj.getString("message");
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                errorMsg = getActivity().getString(R.string.error_no_internet);
                            } else if (error instanceof ServerError) {
                                errorMsg = getActivity().getString(R.string.error_server);
                            } else if (error instanceof AuthFailureError) {
                                errorMsg = getActivity().getString(R.string.error_authFailureError);
                            } else if (error instanceof ParseError) {
                                errorMsg = getActivity().getString(R.string.error_parse_error);
                            } else if (error instanceof TimeoutError) {
                                errorMsg = getActivity().getString(R.string.error_time_out);
                            }
                        }
                        AlertUtils.showAlertMessage(getActivity(), "Error !!!", errorMsg);
                    }
                }, "checkPromoCodeTask");


    }

    private void getBuyBidCreditsTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("token", prefsHelper.getToken());
        postParams.put("user_id", String.valueOf(prefsHelper.getUserId()));

        vHelper.addVolleyRequestListeners(Api.getInstance().buyBidCreditsUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                creditList.clear();

                                JSONArray credits = res.getJSONArray("bid_packages");
                                if (credits.length() > 0) {
                                    for (int i = 0; i < credits.length(); i++) {
                                        JSONObject eachBid = credits.getJSONObject(i);

                                        BuyCredit credit = new BuyCredit();
                                        credit.setPackageId(eachBid.getString("id"));
                                        credit.setCreditPrice(eachBid.getString("amount"));
                                        credit.setPackageName(eachBid.getString("name"));
                                        credit.setNormalCredit(Integer.parseInt(eachBid.getString("credits")));
                                        credit.setBonusCredit(Integer.parseInt(eachBid.getString("bonus_points")));

                                        //payment list
                                        JSONArray payments = res.getJSONArray("payment_lists");
                                        String[] paymentId = new String[payments.length() + 1];
                                        String[] paymentLogo = new String[payments.length() + 1];
                                        String[] paymentGateWay = new String[payments.length() + 1];

                                        //insert choose payment method
                                        paymentId[0] = "-1";
                                        paymentLogo[0] = "";
                                        paymentGateWay[0] = getString(R.string.choose_payment_gateway);

                                        for (int j = 0; j < payments.length(); j++) {
                                            JSONObject eachPayment = payments.getJSONObject(j);

                                            paymentId[j + 1] = eachPayment.getString("id");
                                            paymentLogo[j + 1] = eachPayment.getString("payment_logo");
                                            paymentGateWay[j + 1] = eachPayment.getString("payment_gateway");
                                        }
                                        credit.setPaymentId(paymentId);
                                        credit.setPaymentLogo(paymentLogo);
                                        credit.setPaymentGateway(paymentGateWay);

                                        creditList.add(credit);
                                    }

                                    adapter.notifyDataSetChanged();
                                    buyList.setVisibility(View.VISIBLE);
                                    errorText.setVisibility(View.GONE);
                                } else {
                                    errorText.setText(R.string.no_buy_credits);
                                    errorText.setVisibility(View.VISIBLE);
                                    buyList.setVisibility(View.GONE);
                                }
                            } else {
                                errorText.setText(res.getString("message"));
                                errorText.setVisibility(View.VISIBLE);
                            }
                        } catch (JSONException e) {
                            Logger.e(" BuyBidCredits Json exception ", e.getMessage());
                        }
                        progressBar.setVisibility(View.GONE);
                        listRefresh.setRefreshing(false);
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            errorText.setText(errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                errorText.setText(getActivity().getString(R.string.error_no_internet));
                            } else if (error instanceof ServerError) {
                                errorText.setText(getActivity().getString(R.string.error_server));
                            } else if (error instanceof AuthFailureError) {
                                errorText.setText(getActivity().getString(R.string.error_authFailureError));
                            } else if (error instanceof ParseError) {
                                errorText.setText(getActivity().getString(R.string.error_parse_error));
                            } else if (error instanceof TimeoutError) {
                                errorText.setText(getActivity().getString(R.string.error_time_out));
                            }
                            buyList.setVisibility(View.GONE);
                        }
                        progressBar.setVisibility(View.GONE);
                        errorText.setVisibility(View.VISIBLE);
                        listRefresh.setRefreshing(false);
                    }
                }, "getBuyBidCreditsTask");

    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("getBuyBidCreditsTask");
        super.onPause();
    }
}
