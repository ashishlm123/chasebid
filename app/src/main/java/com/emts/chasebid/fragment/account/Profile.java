package com.emts.chasebid.fragment.account;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.IdRes;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v7.app.AlertDialog;
import android.text.Editable;
import android.text.Html;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.activity.MyCropActivity;
import com.emts.chasebid.activity.RegisterActivity;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.ImageHelper;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;

import static com.emts.chasebid.activity.RegisterActivity.DATEPICKER_TAG;

/**
 * Created by User on 2016-07-01.
 */
public class Profile extends Fragment {
    boolean isValid = true;
    EditText firstName, lastName, verifyCode, email, address1, address2, city, postalCode,
            mobileNo, oldPassword, newPassword, confirmPassword;
    TextView date_of_birth, userName;
    LinearLayout holderVerify;
    String userMobile = "";
    boolean phoneChanged;
    TextView btnVerify;
    Spinner country, spState;
    ArrayList<String> countries = new ArrayList<>();
    ArrayList<Integer> countryIdList = new ArrayList<>();
    ArrayList<String> countryCodeList = new ArrayList<>();
    LinearLayout holderStates;
    ArrayList<String> indiaStateArray = new ArrayList<>();
    String userState = "";
    boolean isCountry = false;
    RadioGroup rgGender;
    ImageView profileImage;
    PreferenceHelper preferenceHelper;
    int index = -1;
    public static Uri fileUri;
    public static int RESULT_LOAD_IMAGE = 123;
    public static final int MEDIA_TYPE_IMAGE = 1;
    public static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 9873;

    public static final int REQ_PERMISSIONS_CAMERA = 953;
    public static final int REQ_PERMISSIONS_GALLERY = 956;

    public static final int REQ_CUSTOM_CROP_ACT = 355;

    boolean noProfilePic = false;
    boolean firstTimeUpload = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        date_of_birth = (TextView) view.findViewById(R.id.et_dob);
        date_of_birth.setOnClickListener(dob_touchListener);

        rgGender = (RadioGroup) view.findViewById(R.id.rg_gender);
        RadioButton male = (RadioButton) view.findViewById(R.id.rb_male);
        RadioButton female = (RadioButton) view.findViewById(R.id.rb_female);

        Typeface font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/yanonekaffeesatz_regular.ttf");
        male.setTypeface(font);
        female.setTypeface(font);

        firstName = (EditText) view.findViewById(R.id.et_f_name);
        userName = (TextView) view.findViewById(R.id.user_name);
        lastName = (EditText) view.findViewById(R.id.et_l_name);
        oldPassword = (EditText) view.findViewById(R.id.et_old_password);
        newPassword = (EditText) view.findViewById(R.id.et_new_password);
        confirmPassword = (EditText) view.findViewById(R.id.et_confirm_new_password);
        mobileNo = (EditText) view.findViewById(R.id.et_contact_mobile);
        mobileNo.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (charSequence.toString().trim().equals(userMobile)) {
                    phoneChanged = false;
                    holderVerify.setVisibility(View.GONE);
                    btnVerify.setVisibility(View.GONE);
                } else {
                    phoneChanged = true;
                    holderVerify.setVisibility(View.VISIBLE);
                    btnVerify.setVisibility(View.VISIBLE);
                    isProfileChanged = true;
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
        btnVerify = (TextView) view.findViewById(R.id.verify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    if (Patterns.PHONE.matcher(mobileNo.getText().toString().trim()).matches() &&
                            mobileNo.getText().toString().trim().length() > 9 &&
                            mobileNo.getText().toString().trim().length() < 15) {
                        mobileVerifyTask(mobileNo.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getActivity(), view, getString(R.string.error_mobile_length));
                    }
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });
        holderVerify = (LinearLayout) view.findViewById(R.id.holder_verify_code);
        verifyCode = (EditText) view.findViewById(R.id.et_verification_code);
        email = (EditText) view.findViewById(R.id.et_email);
        address1 = (EditText) view.findViewById(R.id.et_address1);
        address2 = (EditText) view.findViewById(R.id.et_address2);
        city = (EditText) view.findViewById(R.id.et_city);
        postalCode = (EditText) view.findViewById(R.id.et_postal_code);
        country = (Spinner) view.findViewById(R.id.sp_country);
        profileImage = (ImageView) view.findViewById(R.id.user_profile_image);
        profileImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showChoosePictureAlert();
            }
        });

        preferenceHelper = PreferenceHelper.getInstance(getActivity());

        File countryList = new File(getActivity().getFilesDir() + "/" + "countrylist.txt");
        if (countryList.exists()) {
            isCountry = true;

            String response = readFromFile(getActivity());
            try {
                JSONObject jsonObject = new JSONObject(response);
                JSONArray countryArray = jsonObject.getJSONArray("countries");
                for (int i = 0; i < countryArray.length(); i++) {
                    JSONObject eachObj = countryArray.getJSONObject(i);
                    String countryName = eachObj.getString("country");
                    Integer countryId = Integer.valueOf(eachObj.getString("id"));
                    String countryCode = eachObj.getString("short_code");
                    countries.add(countryName);
                    countryIdList.add(countryId);
                    countryCodeList.add(countryCode);
                }
                setSpinnerAdapter();
            } catch (JSONException e) {
                Logger.e("country json parse ex", e.getMessage() + "");
            }

        } else {
            if (NetworkUtils.isInNetwork(getContext())) {
                getCountryTask();
            }
        }

        if (NetworkUtils.isInNetwork(getActivity())) {
            if (isCountry) {
                getProfileDataTask();
            }
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
        }
        spState = (Spinner) view.findViewById(R.id.sp_state);
        holderStates = (LinearLayout) view.findViewById(R.id.holder_state);
        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                if (countries.get(i).equalsIgnoreCase("india")) {
                    if (indiaStateArray.size() < 1) {
                        getStatesTask();
                    }
                } else {
                    holderStates.setVisibility(View.GONE);
                }
                if (isCountryReady) {
//                    isProfileChanged = true;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        TextView btn_update_info = (TextView) view.findViewById(R.id.btn_update);
        btn_update_info.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!isProfileChanged) {
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.change_profile_to_update));
                    return;
                }

                isValid = true;

                int selectedId = rgGender.getCheckedRadioButtonId();
                String gender_m_f = "";
                if (selectedId == R.id.rb_male) {
                    gender_m_f = "M";
                } else if (selectedId == R.id.rb_female) {
                    gender_m_f = "F";
                }
                if (TextUtils.isEmpty(gender_m_f)) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_choose_gender), getString(R.string.choose_gender));
                } else {
                    hideErrorView((TextView) view.findViewById(R.id.error_choose_gender));
                }

                if (TextUtils.isEmpty(firstName.getText().toString().trim()) && TextUtils.isEmpty(lastName.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_name), getString(R.string.empty_both));
                } else {
                    if (TextUtils.isEmpty(lastName.getText().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_name), getString(R.string.empty_error_msg));
                    } else if (TextUtils.isEmpty(firstName.getText().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_name), getString(R.string.empty_error_msg));
                    } else {
                        hideErrorView((TextView) view.findViewById(R.id.error_name));
                    }
                }
                if (TextUtils.isEmpty(mobileNo.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_mobile), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) view.findViewById(R.id.error_mobile));
                    if (phoneChanged) {
                        if (TextUtils.isEmpty(verifyCode.getText().toString().trim())) {
                            isValid = false;
                            showErrorView((TextView) view.findViewById(R.id.error_verification_code),
                                    getString(R.string.mobile_no_changed));
                        } else {
                            hideErrorView((TextView) view.findViewById(R.id.error_mobile));
                        }
                    }
                }

                if (TextUtils.isEmpty(email.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_email), getString(R.string.empty_error_msg));
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_email), getString(R.string.incorrect_email));
                    } else {
                        hideErrorView((TextView) view.findViewById(R.id.error_email));
                    }
                }

                if (TextUtils.isEmpty(address1.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_address1), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) view.findViewById(R.id.error_address1));
                }
                if (TextUtils.isEmpty(city.getText().toString().trim()) && TextUtils.isEmpty(postalCode.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_citypostal), getString(R.string.empty_both));
                } else {
                    if (TextUtils.isEmpty(postalCode.getText().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_citypostal), getString(R.string.error_postal_code));
                    } else if (TextUtils.isEmpty(city.getText().toString().trim())) {
                        showErrorView((TextView) view.findViewById(R.id.error_citypostal), getString(R.string.error_city_req));
                    } else {
                        hideErrorView((TextView) view.findViewById(R.id.error_citypostal));
                    }
                }
                if (country.getSelectedItem() != null) {
                    if (TextUtils.isEmpty(country.getSelectedItem().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_country), getString(R.string.empty_error_msg));
                    } else {
                        if (country.getSelectedItem().toString().trim().equals("india")) {
                            //state is compulsory
                            if (spState.getAdapter() != null) {
                                if (spState.getSelectedItemPosition() == -1) {
                                    isValid = false;
                                    showErrorView((TextView) view.findViewById(R.id.error_state), getString(R.string.empty_error_msg));
                                } else {
                                    hideErrorView((TextView) view.findViewById(R.id.error_state));
                                }
                            } else {
                                isValid = false;
                                getStatesTask();
                                showErrorView((TextView) view.findViewById(R.id.error_state), getString(R.string.empty_error_msg));
                                return;
                            }
                        } else {
                            hideErrorView((TextView) view.findViewById(R.id.error_country));
                        }
                    }
                } else {
                    isValid = false;
                    AlertUtils.showSnack(getActivity(), view, getString(R.string.reload_country));
                    return;
                }
                if (TextUtils.isEmpty(date_of_birth.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) view.findViewById(R.id.error_dob), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) view.findViewById(R.id.error_dob));
                }

                String oldPass = oldPassword.getText().toString().trim();
                String newPass = newPassword.getText().toString().trim();
                String conPass = confirmPassword.getText().toString().trim();


                if (TextUtils.isEmpty(oldPass) && TextUtils.isEmpty(conPass) &&
                        TextUtils.isEmpty(newPass)) {
                    hideErrorView((TextView) view.findViewById(R.id.error_old_password));
                    hideErrorView((TextView) view.findViewById(R.id.error_new_pass));
                    hideErrorView((TextView) view.findViewById(R.id.error_c_password));

                } else {
                    if (TextUtils.isEmpty(oldPass)) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_old_password), getString(R.string.empty_error_msg));
                    } else {
                        hideErrorView((TextView) view.findViewById(R.id.error_old_password));
                    }
                    if (TextUtils.isEmpty(newPass)) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_new_pass), getString(R.string.empty_error_msg));
                    } else {
                        if (newPass.length() > 5) {
                            if (newPass.equals(conPass)) {
                                hideErrorView((TextView) view.findViewById(R.id.error_new_pass));
                            } else {
                                isValid = false;
                                showErrorView((TextView) view.findViewById(R.id.error_new_pass), getString(R.string.new_con_pass_same));

                            }

                        } else {
                            isValid = false;
                            showErrorView((TextView) view.findViewById(R.id.error_new_pass), getString(R.string.error_password_length));
                        }

                    }
                    if (TextUtils.isEmpty(conPass)) {
                        isValid = false;
                        showErrorView((TextView) view.findViewById(R.id.error_c_password), getString(R.string.empty_error_msg));
                    } else {
                        hideErrorView((TextView) view.findViewById(R.id.error_c_password));
                    }

                }

                if (isValid) {
                    v.clearAnimation();
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        String stateSel = "";
                        if (country.getSelectedItem().toString().trim().equalsIgnoreCase("india")) {
                            stateSel = spState.getSelectedItem().toString().trim();
                        }

                        updateProfileTask(gender_m_f, firstName.getText().toString().trim(), lastName.getText().toString().trim(),
                                mobileNo.getText().toString().trim(), verifyCode.getText().toString().trim(),
                                email.getText().toString().trim(), date_of_birth.getText().toString().trim(),
                                address1.getText().toString().trim(), address2.getText().toString().trim(),
                                city.getText().toString().trim(), postalCode.getText().toString().trim(),
                                String.valueOf(countryIdList.get(country.getSelectedItemPosition())),
                                stateSel,
                                oldPassword.getText().toString().trim(),
                                newPassword.getText().toString().trim(),
                                confirmPassword.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getActivity(), btnVerify, getString(R.string.error_no_internet));
                    }

                } else {
                    v.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(getActivity(), R.anim.shake_animation);
                    v.setAnimation(anim);
                    Toast.makeText(getActivity(), getString(R.string.form_validate_message), Toast.LENGTH_SHORT).show();
                }
            }
        });

        if (preferenceHelper.getBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true)) {
            firstTimeUpload = true;

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage(R.string.add_picture);
            builder.setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                    showChoosePictureAlert();
                }
            });
            builder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();

                    reRoutToHomePage();
                }
            });
            builder.show();

            preferenceHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, false).commit();
        }
    }

    private void reRoutToHomePage() {
        try {
            ((MainActivity) getActivity()).openHomeFragment();
        } catch (Exception e) {

        }
    }

    private void showChoosePictureAlert() {
        final CharSequence[] options = {getString(R.string.take_photo), getString(R.string.choose_from_gallery),
                getString(R.string.alert_cancel)};
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.add_photo);
        builder.setItems(options, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {
                if (options[item].equals(getString(R.string.take_photo))) {
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQ_PERMISSIONS_CAMERA);

                    } else {
                        takePhoto();
                    }

                } else if (options[item].equals(getString(R.string.choose_from_gallery))) {

                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            != PackageManager.PERMISSION_GRANTED) {

                        ActivityCompat.requestPermissions(getActivity(),
                                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE},
                                REQ_PERMISSIONS_GALLERY);

                    } else {
                        chooseFrmGallery();
                    }

                } else if (options[item].equals(getString(R.string.alert_cancel))) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    String year, month, day;
    com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener dateSetListener = new com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
            Profile.this.year = String.valueOf(year);
            Profile.this.month = String.format("%02d", month + 1);
            Profile.this.day = String.format("%02d", day);
            date_of_birth.setText(year + "-" + (Profile.this.month) + "-" + Profile.this.day);
            isProfileChanged = true;
        }
    };

    private void mobileVerifyTask(final String mobileNo) {
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("api_key", Api.getInstance().apiKey);
        postMap.put("mobile", mobileNo);
        Logger.e("post mobile", mobileNo);

        final com.emts.chasebid.customviews.ProgressDialog progressDialog = AlertUtils.showProgressDialog(getActivity(), "Verifying ...");
        progressDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().mobileVerification, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        AlertUtils.showAlertMessage(getActivity(), getString(R.string.success), res.getString("message"));
                    } else {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setTitle(getString(R.string.title_error));
                        builder.setMessage(res.getString("message"));
                        builder.show();

                    }
                } catch (JSONException e) {
                    Logger.e("mobileVerifyTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                    builder.setTitle(getString(R.string.title_error));
                    builder.setMessage(errorObj.getString("message"));
                    builder.show();

                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getActivity(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getActivity(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getActivity(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "mobileVerifyTask");
    }

    private void getProfileDataTask() {
        final com.emts.chasebid.customviews.ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.loading));
        pDialog.setMessage(getString(R.string.loading));
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        postParams.put("token", PreferenceHelper.getInstance(getContext()).getToken());

        vHelper.addVolleyRequestListeners(Api.getInstance().getProfileDataUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();

                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONObject profileData = res.getJSONObject("profile");

                        if (profileData.getString("user_name").equalsIgnoreCase(" ")) {
                            userName.setText(profileData.getString("first_name"));
                        } else {
                            userName.setText(profileData.getString("user_name"));
                        }

                        String gender = profileData.getString("gender");
                        if (gender.equalsIgnoreCase("M")) {
                            rgGender.check(R.id.rb_male);
                        } else if (gender.equalsIgnoreCase("F")) {
                            rgGender.check(R.id.rb_female);
                        }

                        firstName.setText(profileData.getString("first_name"));
                        lastName.setText(profileData.getString("last_name"));
                        userMobile = profileData.getString("mobile");
                        mobileNo.setText(userMobile);
                        email.setText(profileData.getString("email"));
                        PreferenceHelper.getInstance(getActivity()).edit().putString(PreferenceHelper.APP_USER_EMAIL, profileData.getString("email")).apply();
                        address1.setText(profileData.getString("address"));
                        int position = countryIdList.indexOf(Integer.parseInt(profileData.getString("country")));
                        if (position > 0 && position < countryIdList.size()) {
                            country.setSelection(position);
                        }

                        if (!TextUtils.isEmpty(profileData.getString("image"))) {
                            String imageUrl = res.getString("profile_dir") + profileData.getString("image");
                            Picasso.with(getContext()).load(imageUrl)
                                    .into(profileImage);
                        } else {
                            noProfilePic = true;
                            Picasso.with(getContext()).load(ImageHelper.getDefaultUserImage(getContext(),
                                    gender)).into(profileImage);
                        }

                        if (profileData.getString("address2").equalsIgnoreCase("null")) {
                            address2.setText("");
                        } else {

                            address2.setText(profileData.getString("address2"));
                        }


                        city.setText(profileData.getString("city"));
                        postalCode.setText(profileData.getString("post_code"));
                        index = countryCodeList.indexOf(profileData.getString("country"));
                        if (index != -1) {
                            try {
                                country.setSelection(index);
                            } catch (Exception e) {
                            }
                        }
                        userState = profileData.getString("state");
                        String month = changeDates(profileData.getString("dob_month"));
                        date_of_birth.setText(profileData.getString("dob_year") + "-" + month + "-" + profileData.getString("dob_day"));
                        Profile.this.year = profileData.getString("dob_year");
                        Profile.this.month = month;
                        Profile.this.day = profileData.getString("dob_day");

                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, profileData.getString("email")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_NAME, profileData.getString("user_name")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.TOKEN, profileData.getString("token")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, profileData.getString("balance")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, profileData.getString("bonus_points")).commit();
                        preferenceHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(profileData.getString("id"))).commit();
//                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, profileData.getString("country_shotd_code")).commit();

                        setChangeListener();
                    } else {
                        AlertUtils.showAlertMessage(getActivity(), getString(R.string.title_error), res.getString("message"));
                    }
                } catch (JSONException e) {

                    Logger.e("Json exception ", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showAlert(errorObj.getString("message"), false);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        showAlert(getString(R.string.error_no_internet), false);
                    } else if (error instanceof ServerError) {
                        showAlert(getString(R.string.error_server), false);
                    } else if (error instanceof AuthFailureError) {
                        showAlert(getString(R.string.error_authFailureError), false);
                    } else if (error instanceof ParseError) {
                        showAlert(getString(R.string.error_parse_error), false);
                    } else if (error instanceof TimeoutError) {
                        showAlert(getString(R.string.error_time_out), false);
                    }
                }
            }
        }, "FetchProfileData");

    }

    private void updateProfileTask(final String gender, final String firstName, final String lastName,
                                   final String mobNo, final String verifyCode, final String email,
                                   final String dob, final String address, final String address2,
                                   final String city, final String postalCode, final String countryName,
                                   final String state,
                                   final String oldPassword, final String newPassword, final String conPassword) {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.updating));
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> map = Api.getInstance().getPostParams();
        map.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        map.put("gender", gender);
        map.put("fname", firstName);
        map.put("lname", lastName);
        map.put("dobday", Profile.this.day);
        map.put("dobmonth", Profile.this.month);
        map.put("dobyear", Profile.this.year);
        map.put("email", email);
        map.put("address", address);
        map.put("address2", address2);
        map.put("city", city);
        map.put("state", state);
        map.put("zip", postalCode);
        map.put("mobile", mobNo);
        if (!TextUtils.isEmpty(oldPassword) && !TextUtils.isEmpty(newPassword) && !TextUtils.isEmpty(conPassword)) {
            map.put("old_password", oldPassword);
            map.put("new_password", newPassword);
            map.put("re_password", conPassword);
            Logger.e("postparam", oldPassword + " " + newPassword + " " + conPassword);
        }
        if (phoneChanged) {
            map.put("verification_code", verifyCode);
        }
        map.put("country", countryName);

        vHelper.addVolleyRequestListeners(Api.getInstance().getEditProfileUrl, Request.Method.POST, map, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();

                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        showAlert(res.getString("message"), true);

                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, email).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, countryCodeList.get(country.getSelectedItemPosition())).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_GENDER, gender).commit();
                        if (noProfilePic) {
                            Picasso.with(getContext()).load(ImageHelper.getDefaultUserImage(getContext(),
                                    gender)).into(profileImage);
                        }

                        Logger.e("countrycode", countryCodeList.get(country.getSelectedItemPosition()));

                        try {
                            ((MainActivity) getActivity()).setHeaderData();
                        } catch (Exception e) {
                        }
                        isProfileChanged = false;
                    } else {
                        showAlert(res.getString("message"), false);
                    }
                } catch (JSONException e) {
                    Logger.e(" profileUpdate Json exception ", e.getMessage());
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showAlert(errorObj.getString("message"), false);
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getActivity(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getActivity(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getActivity(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getActivity(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "profileUpdate");


    }

    public void chooseFrmGallery() {
        Intent i = new Intent(
                Intent.ACTION_PICK,
                android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);

        getActivity().startActivityForResult(i, RESULT_LOAD_IMAGE);
    }

    public void takePhoto() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        fileUri = getOutputMediaFileUri(MEDIA_TYPE_IMAGE);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, fileUri);

        // start the image capture Intent
        getActivity().startActivityForResult(intent, CAMERA_CAPTURE_IMAGE_REQUEST_CODE);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == CAMERA_CAPTURE_IMAGE_REQUEST_CODE) {
            Logger.e("imagePath 1", requestCode + "");

//            Bitmap bm = ImageHelper.decodeImageFileWithCompression(fileUri.getPath(),
//                    (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f));
//            profileImage.setImageBitmap(bm);
//
//            if (NetworkUtils.isInNetwork(getContext())) {
//                uploadImageTask(fileUri.getPath());
//            } else {
//                Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_LONG).show();
//            }

//            CropImage.activity(fileUri)
//                    .setGuidelines(CropImageView.Guidelines.ON)
//                    .setFixAspectRatio(true)
//                    .setAspectRatio(1, 1)
//                    .start(getActivity());
            Intent intent = new Intent(getActivity(), MyCropActivity.class);
            intent.putExtra("cropPath", fileUri.getPath());
            getActivity().startActivityForResult(intent, REQ_CUSTOM_CROP_ACT);


        } else if (requestCode == RESULT_LOAD_IMAGE) {
            Uri selectedImageUri = data.getData();
            String[] projection = {MediaStore.MediaColumns.DATA};
            CursorLoader cursorLoader = new CursorLoader(getContext(), selectedImageUri, projection, null, null, null);
            Cursor cursor = cursorLoader.loadInBackground();
            int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
            cursor.moveToFirst();
            String selectedImagePath = cursor.getString(column_index);
            Logger.e("imagePath 2", selectedImagePath);

//            Bitmap bm = ImageHelper.decodeImageFileWithCompression(selectedImagePath,
//                    (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f));
//            profileImage.setImageBitmap(bm);

//            if (NetworkUtils.isInNetwork(getContext())) {
//                uploadImageTask(selectedImagePath);
//            } else {
//                Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_LONG).show();
//            }

//            CropImage.activity(selectedImageUri)
//                    .setGuidelines(CropImageView.Guidelines.ON)
//                    .setFixAspectRatio(true)
//                    .setAspectRatio(1, 1)
//                    .start(getActivity());

            Intent intent = new Intent(getActivity(), MyCropActivity.class);
            intent.putExtra("cropPath", selectedImagePath);
            getActivity().startActivityForResult(intent, REQ_CUSTOM_CROP_ACT);

        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {

            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == Activity.RESULT_OK) {
                Uri resultUri = result.getUri();
                Logger.e("Cropped image path", resultUri + "");
//                // File file0 = new File(resultUri.getPath());
//                Bitmap bm = resizeHeightWidth(resultUri.getPath());
//                File file = new File(resultUri.getPath());
//                //compressing the resized image
//                FileOutputStream out = null;
//                try {
//                    out = new FileOutputStream(file);
//                    bm.compress(Bitmap.CompressFormat.JPEG, 70, out);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                } finally {
//                    try {
//                        if (out != null) {
//                            out.close();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
//                }

                correctOrientation(resultUri);

                Bitmap bm = ImageHelper.decodeImageFileWithCompression(resultUri.getPath(),
                        (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f));
                profileImage.setImageBitmap(bm);

                if (NetworkUtils.isInNetwork(getContext())) {
                    uploadImageTask(resultUri.getPath());
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_LONG).show();
                }


            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
                Exception error = result.getError();
            }
        } else if (requestCode == REQ_CUSTOM_CROP_ACT) {

            String cropPath = data.getStringExtra("outputPath");
            correctOrientation(Uri.fromFile(new File(cropPath)));

            Bitmap bm = ImageHelper.decodeImageFileWithCompression(cropPath,
                    (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f));
            profileImage.setImageBitmap(bm);

            if (NetworkUtils.isInNetwork(getContext())) {
                uploadImageTask(cropPath);
            } else {
                Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_LONG).show();
            }

        }
    }

    private void correctOrientation(Uri fileUri) {
        BitmapFactory.Options bounds = new BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(fileUri.getPath(), bounds);

        BitmapFactory.Options opts = new BitmapFactory.Options();
        Bitmap bm = BitmapFactory.decodeFile(fileUri.getPath(), opts);

        int rotationAngle = getCameraPhotoOrientation(getActivity(), fileUri, fileUri.getPath());

        Matrix matrix = new Matrix();
        matrix.postRotate(rotationAngle, (float) bm.getWidth() / 2, (float) bm.getHeight() / 2);
        Bitmap rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);
        FileOutputStream fos = null;
        try {
            fos = new FileOutputStream(fileUri.getPath());
            rotatedBitmap.compress(Bitmap.CompressFormat.JPEG, 100, fos);
            fos.flush();
            fos.close();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public static int getCameraPhotoOrientation(Context context, Uri imageUri, String imagePath) {
        int rotate = 0;
        try {
            context.getContentResolver().notifyChange(imageUri, null);
            File imageFile = new File(imagePath);
            ExifInterface exif = new ExifInterface(imageFile.getAbsolutePath());
            int orientation = exif.getAttributeInt(
                    ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            switch (orientation) {
                case ExifInterface.ORIENTATION_NORMAL:
                    rotate = 0;
                case ExifInterface.ORIENTATION_ROTATE_270:
                    rotate = 270;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_180:
                    rotate = 180;
                    break;
                case ExifInterface.ORIENTATION_ROTATE_90:
                    rotate = 90;
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return rotate;
    }

    private void uploadImageTask(String imagePath) {
        final com.emts.chasebid.customviews.ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.updating_imaging));
        pDialog.show();

        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(imagePath,
                (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f)));
        postParams.put("profile_picture", encodedImage);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().profilePicUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        if (!TextUtils.isEmpty(resObj.getString("image"))) {
                            preferenceHelper.edit().putString(PreferenceHelper.APP_USER_PROFILE_URL,
                                    resObj.getString("image")).apply();
                            noProfilePic = false;
                            try {
                                ((MainActivity) getActivity()).setHeaderData();
                            } catch (Exception e) {
                            }
                        }

                        AlertUtils.showSnack(getActivity(), firstName, resObj.getString("message"));

                        if (firstTimeUpload) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    reRoutToHomePage();
                                }
                            }, 2000);
                            firstTimeUpload = false;
                        }
                    }

                } catch (JSONException e) {
                    Logger.e("uploadImage resObj ex", e.getMessage() + "");
                }
                pDialog.dismiss();
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errorMessage = "Unexpected error !!!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    errorMessage = getString(R.string.unexpected_error);
                    Logger.e("uploadImage error ex", e.getMessage() + "");
                }
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        }, "uploadImageTask");
    }

    private void uploadImageTaskNew(String imagePath) {
        final com.emts.chasebid.customviews.ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.updating_imaging));
        pDialog.show();

        HashMap<String, String> postParams = Api.getInstance().getPostParams();
//        String encodedImage = ImageHelper.encodeTobase64VII(ImageHelper.decodeImageFileWithCompression(imagePath,
//                (int) Utils.pxFromDp(getActivity(), 85f), (int) Utils.pxFromDp(getActivity(), 85f)));
//        postParams.put("profile_picture", encodedImage);

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().profilePicUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        if (!TextUtils.isEmpty(resObj.getString("image"))) {
                            preferenceHelper.edit().putString(PreferenceHelper.APP_USER_PROFILE_URL,
                                    resObj.getString("image")).apply();
                            noProfilePic = false;
                            try {
                                ((MainActivity) getActivity()).setHeaderData();
                            } catch (Exception e) {
                            }
                        }

                        AlertUtils.showSnack(getActivity(), firstName, resObj.getString("message"));

                        if (firstTimeUpload) {
                            new Handler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    reRoutToHomePage();
                                }
                            }, 2000);
                            firstTimeUpload = false;
                        }
                    }

                } catch (JSONException e) {
                    Logger.e("uploadImage resObj ex", e.getMessage() + "");
                }
                pDialog.dismiss();
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
                String errorMessage = "Unexpected error !!!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    errorMessage = getString(R.string.unexpected_error);
                    Logger.e("uploadImage error ex", e.getMessage() + "");
                }
                Toast.makeText(getActivity(), errorMessage, Toast.LENGTH_SHORT).show();
            }
        }, "uploadImageTask");
    }

    public Uri getOutputMediaFileUri(int i) {
        return Uri.fromFile(getOutputMediaFile());
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

    private void setSpinnerAdapter() {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, countries);

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        country.setAdapter(dataAdapter);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                isCountryReady = true;
            }
        }, 2000);
    }

    View.OnClickListener dob_touchListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showDatePickerDialog();
        }
    };

    public void showErrorView(TextView errorView, String message) {
        errorView.setText(message);
        errorView.setVisibility(View.VISIBLE);
    }

    public void hideErrorView(TextView errorView) {
        errorView.setVisibility(View.GONE);
    }

    private void showDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();


        final com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog = com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(dateSetListener, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);

        datePickerDialog.setVibrate(true);
        // datePickerDialog.setYearRange(2015, 2016);
        datePickerDialog.setCloseOnSingleTapDay(false);
        if (!datePickerDialog.isVisible()) {
            datePickerDialog.show(getActivity().getSupportFragmentManager(), DATEPICKER_TAG);
        }

    }

    private void showAlert(String message, final boolean exit) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        //builder.setTitle("Alert");
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (exit) {
                    // getActivity().finish();
                }
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    private String readFromFile(Context context) {
        String ret = "";

        try {
            InputStream inputStream = context.openFileInput("countrylist.txt");

            if (inputStream != null) {
                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
                String receiveString = "";
                StringBuilder stringBuilder = new StringBuilder();

                while ((receiveString = bufferedReader.readLine()) != null) {
                    stringBuilder.append(receiveString);
                }
                inputStream.close();
                ret = stringBuilder.toString();
            }
        } catch (FileNotFoundException e) {
            Log.e("login activity", "File not found: " + e.toString());
        } catch (IOException e) {
            Log.e("login activity", "Can not read file: " + e.toString());
        }

        return ret;
    }

    private void getCountryTask() {

        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.loading));
        pDialog.setMessage(getString(R.string.loading));
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().countryListUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    Date date2 = null;
                    if (res.getBoolean("status")) {
//                        new InsertCountryAsync().execute(res.getJSONArray("message").toString());
                        JSONArray countryArray = res.getJSONArray("countries");
                        for (int i = 0; i < countryArray.length(); i++) {
                            JSONObject countryObj = countryArray.getJSONObject(i);


                            String countryName = countryObj.getString("country");
                            Integer countryId = Integer.valueOf(countryObj.getString("id"));
                            String countryCode = countryObj.getString("short_code");
                            countries.add(countryName);
                            countryIdList.add(countryId);
                            countryCodeList.add(countryCode);


                            String lastUpdate = countryObj.getString("last_update");
                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
                            Date date1 = formatter.parse(lastUpdate);
                            String preLastUpdate = preferenceHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");


                            if (TextUtils.isEmpty(preLastUpdate)) {
                                preferenceHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastUpdate).commit();
                            } else {
                                date2 = formatter.parse(preLastUpdate);
                                if (date2.after(date1)) {
                                    preferenceHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastUpdate).commit();
                                }
                            }

                        }
                        setSpinnerAdapter();
                        writeToFile(response, getContext());
                        getProfileDataTask();

                    }

                } catch (Exception e) {//
                    Logger.e("getCountryTask  Ex", e.getMessage() + " ");
                }

            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {

                pDialog.dismiss();
            }
        }, "getCountryTask");
    }

    private void writeToFile(String response, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("countrylist.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(response);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }

    private void getStatesTask() {
        final com.emts.chasebid.customviews.ProgressDialog pDialog = new com.emts.chasebid.customviews.ProgressDialog(getActivity());
        pDialog.setMessage(getString(R.string.please_wait));
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().indiaStateListApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray dataArray = res.getJSONArray("data");
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject eachState = dataArray.getJSONObject(i);
                                    indiaStateArray.add(eachState.getString("city_state"));
                                }
                            } else {

                            }

                            setStateSpinnerAdapter();

                        } catch (Exception e) {
                            Logger.e("getStatesTask  Ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            final android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(getActivity());
                            builder.setTitle(R.string.title_error_mesg);
                            builder.setMessage(Html.fromHtml(getString(R.string.reload_state)));
                            builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getStatesTask();
                                }
                            });
                            builder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                }
                            });
                            builder.show();


                        } catch (Exception e) {

                        }
                        pDialog.dismiss();
                    }
                }, "getStatesTask");
    }

    private void setStateSpinnerAdapter() {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, indiaStateArray) {
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spState.setAdapter(dataAdapter);
        int stateIndex = indiaStateArray.indexOf(userState);
        try {
            spState.setSelection(stateIndex);
        } catch (Exception e) {
        }
        holderStates.setVisibility(View.VISIBLE);

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                checkChangeListener(spState);
            }
        }, 2000);
    }

    public String changeDates(String monthString) {
        String monthsInt = "01";
        if (monthString.equalsIgnoreCase("Jan")) {
            monthsInt = "01";
        }
        if (monthString.equalsIgnoreCase("Feb")) {
            monthsInt = "02";
        }
        if (monthString.equalsIgnoreCase("Mar")) {
            monthsInt = "03";
        }
        if (monthString.equalsIgnoreCase("Apr")) {
            monthsInt = "04";
        }
        if (monthString.equalsIgnoreCase("May")) {
            monthsInt = "05";
        }
        if (monthString.equalsIgnoreCase("Jun")) {
            monthsInt = "06";
        }
        if (monthString.equalsIgnoreCase("Jul")) {
            monthsInt = "07";
        }
        if (monthString.equalsIgnoreCase("Aug")) {
            monthsInt = "08";
        }
        if (monthString.equalsIgnoreCase("Sep")) {
            monthsInt = "09";
        }
        if (monthString.equalsIgnoreCase("Oct")) {
            monthsInt = "10";
        }
        if (monthString.equalsIgnoreCase("Nov")) {
            monthsInt = "11";
        }
        if (monthString.equalsIgnoreCase("Dec")) {
            monthsInt = "12";
        }
        return monthsInt;
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("getCountryTasks");
        VolleyHelper.getInstance(getContext()).cancelRequest("ProfileUpdateTask");
        VolleyHelper.getInstance(getContext()).cancelRequest("FetchProfileDataTask");
        super.onPause();
    }


    boolean isProfileChanged = false;
    boolean isCountryReady = false;

    public void checkChangeListener(final EditText editText) {
        editText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                isProfileChanged = true;
            }

            @Override
            public void afterTextChanged(Editable editable) {
            }
        });
    }

    public void checkChangeListener(final Spinner spinner) {
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                isProfileChanged = true;
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
    }

    public void checkChangeListener(final RadioGroup rg) {
        rg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, @IdRes int i) {
                isProfileChanged = true;
            }
        });
    }

    public void setChangeListener() {
        checkChangeListener(rgGender);
        checkChangeListener(firstName);
        checkChangeListener(lastName);
        checkChangeListener(oldPassword);
        checkChangeListener(newPassword);
        checkChangeListener(confirmPassword);
        checkChangeListener(email);
        checkChangeListener(address1);
        checkChangeListener(address2);
        checkChangeListener(city);
        checkChangeListener(postalCode);
    }

}
