package com.emts.chasebid.fragment.account;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.OrderAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class OrderedAuctions extends android.support.v4.app.Fragment {
    RecyclerView orderList;
    ArrayList<Product> orderLists = new ArrayList<>();
    SwipeRefreshLayout listRefresh;
    FrameLayout progressBar;
//    ProgressBar progressBar;
    TextView errorText;
    OrderAdapter adapter;
    int limit = Config.LIMIT;
    int offset = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        orderList = (RecyclerView) view.findViewById(R.id.list_recycler);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        orderList.setLayoutManager(layoutManager);

        progressBar = (FrameLayout) view.findViewById(R.id.progress);
//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);

        adapter = new OrderAdapter(getActivity(), orderLists);
        orderList.setAdapter(adapter);
        orderList.setVisibility(View.VISIBLE);

        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    offset = 0;
                    orderListTask(offset);
                } else {
                    listRefresh.setRefreshing(false);
                    orderList.setVisibility(View.GONE);
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }


            }
        });

        if (NetworkUtils.isInNetwork(getActivity())) {
            orderListTask(offset);
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            orderList.setVisibility(View.GONE);
        }
    }


    public void orderListTask(final int offset) {
        if (offset == 0) {
            orderList.setVisibility(View.GONE);
        }
        progressBar.setVisibility(View.VISIBLE);

        final PreferenceHelper helper = PreferenceHelper.getInstance(getActivity());

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", String.valueOf(helper.getUserId()));
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().orderAuctionUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        orderLists.clear();
                        JSONArray pArray = resObj.getJSONArray("buy_auc");
                        if (pArray.length() > 0) {
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                Product product = new Product();
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("amount")));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setId(Integer.parseInt(eachObj.getString("id")));
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setStatus(eachObj.getString("transaction_status"));
                                product.setFavTime(eachObj.getString("transaction_date"));
                                orderLists.add(product);
                            }
                            orderList.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                        } else {
                            errorText.setText(R.string.no_order_auc);
                            errorText.setVisibility(View.VISIBLE);
                        }
                        progressBar.setVisibility(View.GONE);
                        listRefresh.setRefreshing(false);
                        adapter.notifyDataSetChanged();

                    } else {
                        orderList.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("orderListTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                listRefresh.setRefreshing(false);
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorText.setText(errorObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        orderList.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            orderList.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                        Logger.e("orderListTask error catch", e.getMessage() + "");
                    }
                }
            }
        }, "orderListTask");


//        if (offset == 0) {
//        errorText.setVisibility(View.GONE);
//        progressBar.setVisibility(View.VISIBLE);
//        } else if (offset != 0 && !refresh) {
//            progressInfinite.setVisibility(View.VISIBLE);
//        }
//        refresh = false;
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("orderListTask");
        super.onPause();
    }
}
