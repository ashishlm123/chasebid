package com.emts.chasebid.fragment.account;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.OngoingAuctionAdapter;
import com.emts.chasebid.customviews.EndlessRecyclerViewScrollListener;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class OnGoingAuction extends android.support.v4.app.Fragment {
    RecyclerView onGoingList;
    OngoingAuctionAdapter adapter;
    ProgressBar progressInfinite;
//    ProgressBar progressBar, progressInfinite;
    FrameLayout progressBar;
    TextView errorText;
    SwipeRefreshLayout listRefresh;
    ArrayList<Product> aList;
    int limit = Config.LIMIT;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        aList = new ArrayList<>();

        onGoingList = (RecyclerView) view.findViewById(R.id.list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        onGoingList.setLayoutManager(layoutManager);
        adapter = new OngoingAuctionAdapter(getContext(), aList);
        onGoingList.setAdapter(adapter);


//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar = (FrameLayout) view.findViewById(R.id.progress);
        progressInfinite = (ProgressBar) view.findViewById(R.id.progress_infinite);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);

        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    offset = 0;
                    onGoingAuctionTask(offset);
                } else {
                    listRefresh.setRefreshing(false);
                    onGoingList.setVisibility(View.GONE);
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (aList.size() >= limit) {
                    onGoingAuctionTask(offset);
                    Logger.e("loadmore", "loadmore");
                }
            }
        };
        onGoingList.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            onGoingAuctionTask(offset);
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            onGoingList.setVisibility(View.GONE);
        }


    }

    private void onGoingAuctionTask(final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", String.valueOf(PreferenceHelper.getInstance(getActivity()).getUserId()));
        postParams.put("token", PreferenceHelper.getInstance(getActivity()).getToken());
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offsetValue));

        vHelper.addVolleyRequestListeners(Api.getInstance().onGoingAuctionUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        JSONArray OngoingAuctions = res.getJSONArray("ongoing_auc");
                        if (OngoingAuctions.length() > 0) {
                            if (offset == 0) {
                                aList.clear();
                            }
                            for (int i = 0; i < OngoingAuctions.length(); i++) {
                                JSONObject eachObj = OngoingAuctions.getJSONObject(i);
                                Product product = new Product();
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setId(Integer.parseInt(eachObj.getString("id")));
                                product.setpId(Integer.parseInt(eachObj.getString("product_id")));
                                product.setNoOfBids(Integer.parseInt(eachObj.getString("no_bids")));

                                SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                                Date endTime = null;
                                try {
                                    endTime = sdf2.parse(eachObj.getString("end_date"));
                                } catch (ParseException e) {
                                    e.printStackTrace();
                                }
                                product.setFavTime(String.valueOf(endTime.getTime()));
                                aList.add(product);
                            }

                            offset = offset + limit;
                            adapter.notifyDataSetChanged();
                            onGoingList.setVisibility(View.VISIBLE);
                            errorText.setVisibility(View.GONE);
                            Logger.e("offset", String.valueOf(offset));
                            Logger.e("size", String.valueOf(aList.size()));
                        } else {
                            if (offset == 0) {
                                errorText.setVisibility(View.VISIBLE);
                                onGoingList.setVisibility(View.GONE);
                                errorText.setText("No product in Ongoing auction");
                                errorText.setVisibility(View.VISIBLE);
                            } else {
                                onGoingList.setVisibility(View.VISIBLE);
                                AlertUtils.showSnack(getActivity(), errorText, getString(R.string.no_product));
                            }
//                            infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                        }
                    } else {
                        if (offset == 0) {
                            onGoingList.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            errorText.setText(res.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            onGoingList.setVisibility(View.GONE);
                            AlertUtils.showSnack(getActivity(), errorText, res.getString("message"));
                            progressInfinite.setVisibility(View.GONE);
                        }

                    }
                } catch (JSONException e) {
                    Logger.e(" onGoingAuction Json exception ", e.getMessage());
                }
                progressBar.setVisibility(View.GONE);
                progressInfinite.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        errorMessage = getActivity().getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMessage = getActivity().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMessage = getActivity().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMessage = getActivity().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMessage = getActivity().getString(R.string.error_time_out);
                    }
                }
                if (offset == 0) {
                    errorText.setText(errorMessage);
                    errorText.setVisibility(View.VISIBLE);
                    onGoingList.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(getActivity(), errorText, errorMessage);
                }
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("favListingTask  exception", e.getMessage());
                }
                listRefresh.setRefreshing(false);
                onGoingList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }, "onGoingAuctionTask");

        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("onGoingAuctionTask");
        offset = 0;
        super.onPause();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        offset = 0;
    }

    @Override
    public void onDetach() {
        VolleyHelper.getInstance(getContext()).cancelRequest("onGoingAuctionTask");
        super.onDetach();
        offset = 0;
    }
}
