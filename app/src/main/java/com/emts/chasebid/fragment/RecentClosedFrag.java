package com.emts.chasebid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.RecentClosedAdapter;
import com.emts.chasebid.customviews.EndlessRecyclerViewScrollListener;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.WonAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by User on 2017-03-30.
 */

public class RecentClosedFrag extends Fragment {
    ArrayList<WonAuction> closedList = new ArrayList<>();

    RecyclerView recyclerClosed;
    SwipeRefreshLayout listRefresh;
    ProgressBar progressInfinite;
    //    ProgressBar progressClosed, progressInfinite;
    FrameLayout progressClosed;
    TextView errorClosed;
    RecentClosedAdapter adapterClosed;

    PreferenceHelper helper;
    int limit = Config.LIMIT;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = PreferenceHelper.getInstance(getActivity());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        errorClosed = (TextView) view.findViewById(R.id.tv_error_message);
        progressClosed = (FrameLayout) view.findViewById(R.id.progress);
//        progressClosed = (ProgressBar) view.findViewById(R.id.progress);
        progressInfinite = (ProgressBar) view.findViewById(R.id.progress_infinite);
        recyclerClosed = (RecyclerView) view.findViewById(R.id.list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerClosed.setLayoutManager(layoutManager);

        adapterClosed = new RecentClosedAdapter(getActivity(), closedList);
        adapterClosed.setFillParent(true);
        recyclerClosed.setAdapter(adapterClosed);

        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    offset = 0;
                    recentClosedTask(offset);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    listRefresh.setRefreshing(false);
                }

            }
        });
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (closedList.size() >= limit) {
                    recentClosedTask(offset);
                    Logger.e("loadmore", "loadmore");
                }
            }
        };
        recyclerClosed.addOnScrollListener(infiniteScrollListener);
        if (NetworkUtils.isInNetwork(getActivity())) {
            recentClosedTask(offset);
        } else {
            errorClosed.setText(getString(R.string.error_no_internet));
            errorClosed.setVisibility(View.VISIBLE);
            recyclerClosed.setVisibility(View.GONE);
        }
    }

    public void recentClosedTask(final int offsetValue) {
        if (offset == 0) {
            recyclerClosed.setVisibility(View.GONE);
            progressClosed.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offsetValue));
        postMap.put("limit", String.valueOf(limit));
        if (helper.isLogin()) {
            postMap.put("user_id", String.valueOf(helper.getUserId()));
            postMap.put("token", helper.getToken());
        }
        errorClosed.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().getClosedAuctionUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressClosed.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        JSONArray pArray = resObj.getJSONArray("closed_auctions");
                        if (pArray.length() > 0) {

                            if (offset == 0) {
                                closedList.clear();
                            }
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                WonAuction product = new WonAuction();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setDesc(eachObj.getString("description"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

//                                if (TextUtils.isEmpty(eachObj.getString("current_winner_name").trim())) {
                                    product.setWinnerName(eachObj.getString("first_name") + " " + eachObj.getString("last_name"));
//                                } else {
//                                    product.setWinnerName(eachObj.getString("current_winner_name"));
//                                }
                                product.setWinnerUrl(eachObj.getString("image"));
                                product.setWinnerGender(eachObj.getString("gender"));
                                product.setWinningBidAmount(eachObj.getString("current_winner_amount"));
                                product.setTotalCreditUsed(eachObj.getString("total_bids"));

                                closedList.add(product);
                            }
                            offset = offset + limit;
                            recyclerClosed.setVisibility(View.VISIBLE);
                            adapterClosed.notifyDataSetChanged();
                            errorClosed.setVisibility(View.GONE);
                            Logger.e("offset", String.valueOf(offset));
                            Logger.e("size", String.valueOf(closedList.size()));
                        } else {
                            if (offset == 0) {
                                errorClosed.setVisibility(View.VISIBLE);
                                recyclerClosed.setVisibility(View.GONE);
                                errorClosed.setText(getString(R.string.no_closed_auc));

                            } else {
                                recyclerClosed.setVisibility(View.VISIBLE);
                                AlertUtils.showSnack(getActivity(), errorClosed, getString(R.string.no_more_closed_auc));
                            }

                        }
                    } else {


                        if (offset == 0) {

                            progressClosed.setVisibility(View.GONE);
                            errorClosed.setText(resObj.getString("message"));
                            errorClosed.setVisibility(View.VISIBLE);
                        } else {
                            recyclerClosed.setVisibility(View.GONE);
                            AlertUtils.showSnack(getActivity(), errorClosed, resObj.getString("message"));
                            progressInfinite.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("recentClosedTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("recentClosedTask ParseException ex", e.getMessage() + "");
                }
                progressClosed.setVisibility(View.GONE);
                progressInfinite.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("onError Exception", e.getMessage() + "");

                }
                try {
                    if (error instanceof NetworkError) {
                        errorMessage = getActivity().getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMessage = getActivity().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMessage = getActivity().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMessage = getActivity().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMessage = getActivity().getString(R.string.error_time_out);
                    }
                } catch (Exception ee) {
                    Logger.e("favListingTask error catch", ee.getMessage() + "");
                }
                if (offset == 0) {
                    errorClosed.setText(errorMessage);
                    errorClosed.setVisibility(View.VISIBLE);
                    recyclerClosed.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(getActivity(), errorClosed, errorMessage);
                }
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("recentClosedTask exception", e.getMessage());
                }
                listRefresh.setRefreshing(false);
                recyclerClosed.setVisibility(View.GONE);
                errorClosed.setVisibility(View.GONE);
            }
        }, "recentClosedTask");

    }


    @Override
    public void onDestroy() {
        VolleyHelper.getInstance(getActivity()).cancelRequest("recentClosedTask");
        super.onDestroy();
    }
}
