package com.emts.chasebid.fragment;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MoreClosedAuctions;
import com.emts.chasebid.activity.ProductDetail;
import com.emts.chasebid.adapters.CurrentWinningAdapter;
import com.emts.chasebid.adapters.RecentClosedAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.WonAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by User on 2017-06-07.
 */

public class TvShowFragment extends Fragment {
    //    SwipeRefreshLayout swipeLayout;
    RecyclerView listRecycleView;
    ArrayList<WonAuction> tvItemList = new ArrayList<>();
    GridLayoutManager layoutManager;
    CurrentWinningAdapter adapter;
    PreferenceHelper preferenceHelper;

    FrameLayout progressBar;
    //    ProgressBar progressBar;
    TextView errorText;
    int limit = 6;
    int offset = 0;

    LinearLayout holderRecentClosed;
    FrameLayout progressClosed;
    TextView errorClosed;
    RecyclerView recyclerClosed;
    RecentClosedAdapter adapterClosed;
    ArrayList<WonAuction> closedList = new ArrayList<>();

    String profileImageDir;
    long curServiceTime;
    Handler currentHandler;
    ScheduledExecutorService curService;

//    io.socket.client.Socket socketJs;


    ArrayList<String> bannerImageClickUrl = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_tv_show, container, false);
    }

    @Override
    public void onViewCreated(final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

//        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
//        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
//            @Override
//            public void onRefresh() {
//
//                if (NetworkUtils.isInNetwork(getActivity())) {
//                    currentWinningTask();
//                } else {
//                    AlertUtils.showSnack(getActivity(), view, getString(R.string.error_no_internet));
//                }
//                swipeLayout.setRefreshing(false);
//            }
//        });
        listRecycleView = (RecyclerView) view.findViewById(R.id.list_recycler);
        layoutManager = new GridLayoutManager(getActivity(), 1);
        listRecycleView.setLayoutManager(layoutManager);
        listRecycleView.setNestedScrollingEnabled(false);
        listRecycleView.setItemAnimator(null);

        preferenceHelper = PreferenceHelper.getInstance(getContext());

//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar = (FrameLayout) view.findViewById(R.id.progress);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);
        adapter = new CurrentWinningAdapter(tvItemList, getActivity());
        adapter.setFullScreen(true);
        listRecycleView.setAdapter(adapter);

        currentHandler = new Handler(Looper.getMainLooper());
        curService = Executors.newSingleThreadScheduledExecutor();

        if (NetworkUtils.isInNetwork(getActivity())) {
            currentWinningTask();
        } else {
            progressBar.setVisibility(View.GONE);
            listRecycleView.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }


        TextView moreClosed = (TextView) view.findViewById(R.id.more_recent_closed);
        moreClosed.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), MoreClosedAuctions.class);
                startActivity(intent);
            }
        });

        holderRecentClosed = (LinearLayout) view.findViewById(R.id.holder_recently_closed);
        recyclerClosed = (RecyclerView) view.findViewById(R.id.recycler_recent_closed);
        LinearLayoutManager closedLayoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        recyclerClosed.setLayoutManager(closedLayoutManager);
        recyclerClosed.setNestedScrollingEnabled(false);
        recyclerClosed.setItemAnimator(null);

        progressClosed = (FrameLayout) view.findViewById(R.id.closed_progress);
        errorClosed = (TextView) view.findViewById(R.id.error_tv_closed);

        adapterClosed = new RecentClosedAdapter(getActivity(), closedList);
        recyclerClosed.setAdapter(adapterClosed);


        if (NetworkUtils.isInNetwork(getActivity())) {
            showBannerImageTask(view);
        }
    }

    private void showBannerImageTask(final View view) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().tvAuctionBannerUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray bannerArray = resObj.getJSONArray("banners");
                        ArrayList<String> images = new ArrayList<>();
                        for (int i = 0; i < bannerArray.length(); i++) {
                            JSONObject eachObj = bannerArray.getJSONObject(i);
                            images.add(eachObj.getString("banner"));
                            bannerImageClickUrl.add(eachObj.getString("url"));
                        }
                        setBannerImage(view, images);
                    }
                } catch (JSONException e) {
                    Logger.e("showBannerImageTaskJson ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
            }
        }, "showBannerImageTask");
    }

    private void setBannerImage(View view, final ArrayList<String> images) {
        final SliderLayout imageSlider = (SliderLayout) view.findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = (PagerIndicator) view.findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (int i = 0; i < images.size(); i++) {
            DefaultSliderView defaultSlider = new DefaultSliderView(getActivity());
            // initialize a SliderLayout
            defaultSlider
                    .image(images.get(i))
                    .setScaleType(BaseSliderView.ScaleType.Fit)
                    .setOnSliderClickListener(new BaseSliderView.OnSliderClickListener() {
                        @Override
                        public void onSliderClick(BaseSliderView slider) {
                            Bundle b = slider.getBundle();
                            String pId = getProductIdFromUrl(b.getString("click_url"));
                            if (pId.equals("") || pId.equalsIgnoreCase(null)) {
                                return;
                            }
                            Logger.e("productId", pId);

                            try {
                                Long.parseLong(pId);

                                Intent intent = new Intent(getActivity(), ProductDetail.class);
                                intent.putExtra("product_id", Long.parseLong(pId));
                                getActivity().startActivityForResult(intent, Home.PRODUCT_DETAIL_REQUEST_CODE);
                            } catch (Exception e) {

                            }
                        }
                    });

            //add your extra information
            Bundle bundle = new Bundle();
            bundle.putString("click_url", bannerImageClickUrl.get(i));
            bundle.putString("position", String.valueOf(i));
            defaultSlider.bundle(bundle);
//            defaultSlider.getBundle()
//                    .putString("extra", images.get(i));

            imageSlider.addSlider(defaultSlider);

//            try {
//                Class<?> c = pagerIndicator.getClass();
//                Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                f.setAccessible(true);
//                f.setInt(pagerIndicator, -1);
//                f.setAccessible(false);
//            } catch (Exception e) {
//                Logger.e("reflection ex 123", e.getMessage() + " ");
//            }
        }

        if (images.size() > 1) {
//            imageSlider.setCurrentPosition(0); //workaround for auto slide to second
//            imageSlider.setDuration(40000);
//            imageSlider.setCustomAnimation(new DescriptionAnimation());
//            imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);

//            try {
//                Class<?> c = pagerIndicator.getClass();
//                Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                f.setAccessible(true);
//                f.setInt(pagerIndicator, -1);
//                f.setAccessible(false);
//            } catch (Exception e) {
//                Logger.e("reflection ex 12", e.getMessage() + " ");
//            }
//            imageSlider.setCustomIndicator(pagerIndicator);
//            imageSlider.setCurrentPosition(0, false);
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
//                        try {
//                            Class<?> c = pagerIndicator.getClass();
//                            Field f = c.getDeclaredField("mPreviousSelectedPosition");
//                            f.setAccessible(true);
//                            f.setInt(pagerIndicator, -1);
//                            f.setAccessible(false);
//                        } catch (Exception e) {
//                            Logger.e("reflection ex 1", e.getMessage() + " ");
//                        }

                        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        imageSlider.setCustomIndicator(pagerIndicator);
//                        imageSlider.setCurrentPosition(0, false);
//                        imageSlider.setCurrentPosition(0, false); //this hangs and gives ANR
                        //so instead of going to first go to last and move to next
                        imageSlider.setCurrentPosition(images.size() - 1, false);
                        imageSlider.moveNextPosition();
                        pagerIndicator.onPageSelected(0);

                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }

    private String getProductIdFromUrl(String url) {
        try {
            return url.substring(url.lastIndexOf("-") + 1);
        } catch (Exception e) {
            return "";
        }
    }

    private void openLink(String link) {
        Intent i = new Intent(Intent.ACTION_VIEW);
        i.setData(Uri.parse(link));
        startActivity(i);
    }

    public void showForClosedDetail() {
        if (NetworkUtils.isInNetwork(getActivity())) {
            recentClosedTask(limit, offset);
        } else {
            progressClosed.setVisibility(View.GONE);
            errorClosed.setVisibility(View.VISIBLE);
            recyclerClosed.setVisibility(View.GONE);
            errorClosed.setText(getString(R.string.error_no_internet));
        }
    }

    public void recentClosedTask(final int limit, final int offset) {
        holderRecentClosed.setVisibility(View.GONE);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offset));
        postMap.put("limit", String.valueOf(limit));
        if (preferenceHelper.isLogin()) {
            postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postMap.put("token", preferenceHelper.getToken());
        }
        errorClosed.setVisibility(View.GONE);
        progressClosed.setVisibility(View.VISIBLE);
        recyclerClosed.setVisibility(View.GONE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().getClosedAuctionUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressClosed.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        JSONArray pArray = resObj.getJSONArray("closed_auctions");
                        if (pArray.length() > 0) {
                            closedList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                WonAuction product = new WonAuction();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

//                                if (TextUtils.isEmpty(eachObj.getString("current_winner_name").trim())) {
                                    product.setWinnerName(eachObj.getString("first_name") + " " + eachObj.getString("last_name"));
//                                } else {
//                                    product.setWinnerName(eachObj.getString("current_winner_name"));
//                                }
                                product.setWinnerUrl(eachObj.getString("image"));
                                product.setWinnerGender(eachObj.getString("gender"));
                                product.setWinningBidAmount(eachObj.getString("current_winner_amount"));
                                product.setTotalCreditUsed(eachObj.getString("total_bids"));

                                closedList.add(product);
                            }
                            recyclerClosed.setVisibility(View.VISIBLE);
                            adapterClosed.notifyDataSetChanged();
                        } else {
                            errorClosed.setVisibility(View.VISIBLE);
                            recyclerClosed.setVisibility(View.GONE);
                            errorClosed.setText(R.string.no_closed_auc);
                        }
                    } else {
                        recyclerClosed.setVisibility(View.GONE);
                        progressClosed.setVisibility(View.GONE);
                        errorClosed.setText(resObj.getString("message"));
                        errorClosed.setVisibility(View.VISIBLE);
                    }
                    holderRecentClosed.setVisibility(View.VISIBLE);
                } catch (JSONException e) {
                    Logger.e("recentClosedTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("recentClosedTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorClosed.setText(errorObj.getString("message"));
                        errorClosed.setVisibility(View.VISIBLE);
                        recyclerClosed.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorClosed.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorClosed.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorClosed.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorClosed.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorClosed.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorClosed.setVisibility(View.VISIBLE);
                            recyclerClosed.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }
                progressClosed.setVisibility(View.GONE);
                holderRecentClosed.setVisibility(View.VISIBLE);
            }
        }, "recentClosedTask");

    }

    private void currentWinningTask() {
        progressBar.setVisibility(View.VISIBLE);
        errorText.setVisibility(View.GONE);
        listRecycleView.setVisibility(View.GONE);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();

        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().getCurrentWinningUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    tvItemList.clear();
                    if (resObj.getBoolean("status")) {
                        profileImageDir = resObj.getString("profile_path");

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(resObj.getString("server_time"));
                        curServiceTime = serverDate.getTime();

                        JSONArray pArray = resObj.getJSONArray("tv_live_auctions");
                        if (pArray.length() > 0) {
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                WonAuction current = new WonAuction();
                                current.setpId(Long.parseLong(eachObj.getString("product_id")));
                                current.setId(Integer.parseInt(eachObj.getString("id")));

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                current.setEndTime(endTime.getTime());

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                current.setStartTime(startTime.getTime());

                                if (!TextUtils.isEmpty(eachObj.getString("image1"))) {
                                    current.setImageUrl(eachObj.getString("image1"));
                                }
                                current.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                current.setpName(eachObj.getString("name"));

                                try {
                                    JSONObject winnerData = eachObj.getJSONObject("winner_data");
                                    current.setWinnerName(winnerData.getString("first_name") + " " + winnerData.getString("last_name"));
//                                    current.setAddress(winnerData.getString("address") + "," + winnerData.getString("city") + "," + winnerData.getString("country"));
                                    current.setAddress(winnerData.getString("city"));
                                    if (!TextUtils.isEmpty(winnerData.getString("image"))) {
                                        current.setWinnerUrl(profileImageDir + winnerData.getString("image"));
                                    }
//                                    else {
//                                        current.setWinnerUrl(resObj.getString("default_path"));
//                                    }
                                    current.setWinnerId(Long.parseLong(winnerData.getString("user_id")));
                                    current.setWinningBidAmount(winnerData.getString("userbid_bid_amt"));
                                    current.setWinnerGender(winnerData.getString("gender"));
                                } catch (JSONException e) {
                                    Logger.e("currentWinningTask", e.getMessage());
                                    current.setWinnerUrl(resObj.getString("default_path"));
                                    current.setWinnerName("N/A");
                                    current.setAddress("N/A");
                                }
                                tvItemList.add(current);
                            }
                            listRecycleView.setVisibility(View.VISIBLE);
                            adapter.setServerTime(curServiceTime);
                            adapter.setProfileImageDir(profileImageDir);
                            if (curService != null && !curService.isShutdown() && !curService.isTerminated()) {
                                curService.scheduleAtFixedRate(currentRunnable,
                                        0, 1000, TimeUnit.MILLISECONDS);
                            }
//                            try {
//                                connectToServerJs();
//                            } catch (URISyntaxException e) {
//                                Logger.e("nodejs server url ex", e.getMessage() + "");
//                            }
                            adapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                            errorText.setText(getString(R.string.no_tv_show_auc));
                        }

                    } else {
                        listRecycleView.setVisibility(View.GONE);
                        errorText.setText(getString(R.string.no_live_auc));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("currentWinningTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("currentWinningTask ParseException ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
//                swipeLayout.setRefreshing(false);

                showForClosedDetail();
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
//                swipeLayout.setRefreshing(false);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorText.setText(errorObj.getString("message"));
                    errorText.setVisibility(View.VISIBLE);
                    listRecycleView.setVisibility(View.GONE);

                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }

                showForClosedDetail();
            }
        }, "currentWinningTask");
    }
//
//    private void connectToServerJs() throws URISyntaxException {
//        if (socketJs != null) {
//            if (socketJs.connected()) {
//                socketJs.off();
//                socketJs.disconnect();
//            }
//        }
//        String nodeServerHostUrl = preferenceHelper.getString(PreferenceHelper.NODE_SEVER, "http://www.chasebid.com");
//        String nodePortNo = preferenceHelper.getString(PreferenceHelper.NODE_PORT, "8000");
//
//        socketJs = IO.socket(nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        Logger.e("nodeJs url", nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        socketJs.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def connect call", "Objent : ");
//
//                if (socketJs.connected()) {
//                    Logger.e("nodeJs connected", socketJs.toString() + "");
//                    socketJs.off();
//
//                    for (int i = 0; i < tvItemList.size(); i++) {
//                        emitConnectAndJoinAuctionRoom(tvItemList.get(i).getpId());
//                    }
//
//                    listenForAuctionWinner();
//                } else {
//                    try {
//                        connectToServerJs();
//
//                    } catch (URISyntaxException e) {
////                        e.printStackTrace();
//                    }
//                }
//
//            }
//
//        }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def. disconnect", "Object " + args.length);
//                socketJs.off();
//            }
//        });
//        socketJs.connect();
//    }
//
//    //    START NODE JS LISTENERS
//    public void emitConnectAndJoinAuctionRoom(long hostId) {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs emit for connected and join auction room", "emitter for socket connected and join to auction room");
//            //for auction_room
//            try {
//                JSONObject roomObj = new JSONObject();
//                roomObj.put("aid", hostId);
//                Logger.e("nodeJs auction_room emitter data", roomObj.toString() + " **");
//
//                socketJs.emit("auction_room", roomObj, new Ack() {
//                    @Override
//                    public void call(Object... args) {
//                        Logger.e("nodeJs auction_room emit ack", "Ack received for auction_room :" +
//                                (args.length > 0 ? args[0].toString() : "0"));
//                    }
//                });
//            } catch (JSONException e) {
//                Logger.e("nodeJs ex15", e.getMessage() + "");
//            }
//        }
//    }
//
//    public void listenForAuctionWinner() {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs listener for bidding_success", "listener set for winner");
//
//            socketJs.off("bidding_success");
//            socketJs.on("bidding_success", new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs response for listener -->", "bidding_success : " + args.length);
//
//                    if (args.length > 0) {
//                        Logger.printLongLog("nodeJs bidding_success", args[0].toString() + " **");
//                        try {
//                            JSONObject bidPlacedObj = new JSONObject(args[0].toString());
//                            int aId = bidPlacedObj.getInt("auc_id");
//                            final String name = bidPlacedObj.getString("name");
//                            final String address = bidPlacedObj.getString("address");
//                            final String imageUrl = bidPlacedObj.getString("image");
//                            String message = bidPlacedObj.getString("message");
//                            final String gender = bidPlacedObj.optString("gender");
//
//                            final int position = getProductPosition(aId);
//                            Logger.e("nodeJs auction_item_finished pos", "Update list position : " + position);
//
//                            try {
//                                getActivity().runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        if (position >= 0) {
//                                            tvItemList.get(position).setWinnerName(name);
//                                            tvItemList.get(position).setAddress(address);
//                                            if (!TextUtils.isEmpty(imageUrl)) {
//                                                tvItemList.get(position).setWinnerUrl(profileImageDir + imageUrl);
//                                            }
//                                            tvItemList.get(position).setWinnerGender(gender);
//
//                                            adapter.notifyItemChanged(position);
//                                            Logger.e("nodeJs notify auction_item_finished",
//                                                    "Product sold pos:" + position);
//                                        } else {
//                                            currentWinningTask();
//                                        }
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Logger.e("nodeJs el18672", "ex :" + e.getMessage());
//                            }
//
//                        } catch (JSONException e) {
//                            Logger.e("nodeJs itemFinished listen ex", e.getMessage() + "");
//                        }
//                    }
//                }
//            });
//        } else {
//            Logger.e("nodeJs lbl 1", "not connected !!!");
//        }
//    }
//
//    private int getProductPosition(int aId) {
//        //NOTE pId = -1 if only host needs to be notify
//        for (int i = 0; i < tvItemList.size(); i++) {
//            if (tvItemList.get(i).getpId() == (aId)) {
//                return i;
//            }
//        }
//        return -1;
//    }

    Runnable currentRunnable = new Runnable() {
        @Override
        public void run() {
            final int firstVisible = layoutManager.findFirstVisibleItemPosition();
            final int lastVisible = layoutManager.findLastVisibleItemPosition();

            currentHandler.post(new Runnable() {
                @Override
                public void run() {
//                    for (int j = 0; j < pList.size(); j++) {
////                        long startTime = pList.get(j).getStartTime();
////                        startTime = startTime - 1000;
////                        pList.get(j).setStartTime(startTime);
//                        long endTime = pList.get(j).getEndTime();
//                        endTime = endTime - 1000;
//                        pList.get(j).setEndTime(endTime);
//                    }
                    curServiceTime = curServiceTime + 1000;
                    adapter.setServerTime(curServiceTime);

                    for (int i = firstVisible; i <= lastVisible; i++) {
                        try {
                            adapter.notifyItemChanged(i);
                        } catch (Exception e) {
                        }
                    }
                }
            });

        }
    };

}
