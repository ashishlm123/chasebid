package com.emts.chasebid.fragment;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.MakeWishAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;
import com.emts.chasebid.model.VoteProduct;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by Srijana on 3/24/2017.
 */

public class MakeWish extends Fragment {

    RecyclerView listRecycleView;
    ArrayList<VoteProduct> pList = new ArrayList<>();
    GridLayoutManager layoutManager;
    MakeWishAdapter adapter;
    PreferenceHelper preferenceHelper;


//    ProgressBar progressBar;
    FrameLayout progressBar;
    TextView errorText;
    int limit = 6;
    int offset = 0;

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_make_wish, container, false);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        listRecycleView = (RecyclerView) view.findViewById(R.id.recycler_product);
        layoutManager = new GridLayoutManager(getActivity(), 2);
        listRecycleView.setLayoutManager(layoutManager);
        listRecycleView.setNestedScrollingEnabled(false);
        listRecycleView.setItemAnimator(null);

        preferenceHelper = PreferenceHelper.getInstance(getContext());

//        progressBar = (ProgressBar) view.findViewById(R.id.make_vot_progress);
        progressBar = (FrameLayout) view.findViewById(R.id.make_vot_progress);
        errorText = (TextView) view.findViewById(R.id.error_tv_make_vote);
        adapter = new MakeWishAdapter(getActivity(), pList);
        listRecycleView.setAdapter(adapter);

        if (NetworkUtils.isInNetwork(getActivity())) {
            makeWishListingTask(limit, offset);
        } else {
            progressBar.setVisibility(View.GONE);
            listRecycleView.setVisibility(View.GONE);
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
        }
    }

    private void makeWishListingTask(int limit, final int offset) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        if (preferenceHelper.isLogin()) {
            postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postMap.put("token", preferenceHelper.getToken());
        }
        postMap.put("limit", String.valueOf(limit));
        postMap.put("offset", String.valueOf(offset));
        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        vHelper.addVolleyRequestListeners(Api.getInstance().getmakeYourWishUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
//                pList.clear();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray pArray = resObj.getJSONArray("vote_auc");
                        if (pArray.length() > 0) {
                            pList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                VoteProduct product = new VoteProduct();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setPosiviteRating(eachObj.getString("positive_rating"));
                                product.setNegativeRating(eachObj.getString("negative_rating"));
                                try {
                                    JSONObject voteStatus = eachObj.getJSONObject("vote_status");
                                    if (voteStatus.getString("positive_rating").equalsIgnoreCase("1")) {
                                        product.setVoteStatus(VoteProduct.VOTE_STATUS_POSITIVE);
                                    } else if (voteStatus.getString("negative_rating").equalsIgnoreCase("1")) {
                                        product.setVoteStatus(VoteProduct.VOTE_STATUS_NEGATIVE);
                                    }
                                }catch (JSONException e){
                                    product.setVoteStatus(VoteProduct.VOTE_STATUS_NOT_RATE);
                                }
                                pList.add(product);
                            }
                            listRecycleView.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                            errorText.setText(getString(R.string.no_product));
                        }

                    } else {
                        listRecycleView.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("makeWishListingTask json ex", e.getMessage() + "");
                } catch (Exception e) {
                    Logger.e("makeWishListingTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorText.setText(errorObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        listRecycleView.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(getActivity().getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(getActivity().getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(getActivity().getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(getActivity().getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(getActivity().getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            listRecycleView.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }

            }


        }, "makeWishListingTask");
    }
}
