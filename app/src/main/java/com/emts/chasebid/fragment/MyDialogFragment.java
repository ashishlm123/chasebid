package com.emts.chasebid.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.RetryPolicy;
import com.android.volley.VolleyError;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import com.emts.chasebid.R;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;

/**
 * Created by shreedhar on 7/20/2016.
 */
public class MyDialogFragment extends DialogFragment {
    ProgressBar progressBidHistory;
    LinearLayout holderBidHistory;
    PreferenceHelper preferenceHelper;
    long productId = -1;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.my_dialog_layout, container, false);
        setStyle(DialogFragment.STYLE_NO_TITLE, R.style.MyDialog);
        progressBidHistory = (ProgressBar) view.findViewById(R.id.progress_bid_history);
        holderBidHistory = (LinearLayout) view.findViewById(R.id.holder_bid_history);
        preferenceHelper = PreferenceHelper.getInstance(getContext());
        getDialog().getWindow().requestFeature(Window.FEATURE_NO_TITLE);

        Bundle bundle = getArguments();
        if (bundle != null) {
            productId = bundle.getLong("id");


        }
        if (NetworkUtils.isInNetwork(getActivity())) {
            getBidHistoryTask(productId, String.valueOf(preferenceHelper.getUserId()), preferenceHelper.getToken());
        } else {
            Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void getBidHistoryTask(final long productId, final String userId, final String token) {
        progressBidHistory.setVisibility(View.VISIBLE);
        holderBidHistory.removeAllViews();

        String url = Api.getInstance().bidHistoryApi;

        VolleyHelper vHelper =VolleyHelper.getInstance(getContext());
        HashMap<String, String> map = Api.getInstance().getPostParams();
        map.put("product_id", String.valueOf(productId));
        map.put("user_id", userId);
        map.put("token", token);

        vHelper.addVolleyRequestListeners(url, Request.Method.POST, map, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) { progressBidHistory.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        //render history
                        JSONArray historyArray = resObj.getJSONArray("bid_history");
                        if (historyArray.length() > 0) {
                            for (int i = 0; i < historyArray.length(); i++) {
                                JSONObject eachHistory = historyArray.getJSONObject(i);

                                LayoutInflater inflater = LayoutInflater.from(getContext());
                                View view = inflater.inflate(R.layout.dialog_dynamic_bid_history, null, false);
                                TextView sN = (TextView) view.findViewById(R.id.sn);
                                TextView bidDate = (TextView) view.findViewById(R.id.bid_date);
                                TextView bidAmount = (TextView) view.findViewById(R.id.bid_amount);
                                TextView bidStatus = (TextView) view.findViewById(R.id.bid_status);

                                sN.setText(i + 1 + "");

                                bidDate.setText(eachHistory.getString("bid_date"));
                                bidAmount.setText(PreferenceHelper.getInstance(getActivity()).getCurrency() + eachHistory.getString("userbid_bid_amt"));
                                String statusMsg = eachHistory.getString("bidstatus");
                                bidStatus.setText(statusMsg);
                                if (statusMsg.trim().equals("Lowest Unique Bid")) {
                                    bidDate.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    bidAmount.setTextColor(getResources().getColor(R.color.colorPrimary));
                                    bidStatus.setTextColor(getResources().getColor(R.color.colorPrimary));
                                }

                                holderBidHistory.addView(view);

                            }
                        } else {
                            showBidHistoryError(resObj.getString("message"));
                        }
                    } else {
                        showBidHistoryError(resObj.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("json ex", e.getMessage() + "");
                }


            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBidHistory.setVisibility(View.GONE);

                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showBidHistoryError(errorObj.getString("message"));
                } catch (Exception e) {
                    showBidHistoryError(getString(R.string.error_loading_bid_history));
                }
            }
        }, "getBidHistoryTask");

    }

    public void showBidHistoryError(String message) {
        TextView tvNoBid = new TextView(getContext());
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        tvNoBid.setLayoutParams(lp);
        tvNoBid.setPadding(0, 30, 0, 30);
        tvNoBid.setText(message);
        tvNoBid.setGravity(Gravity.CENTER);
        tvNoBid.setTextColor(getResources().getColor(R.color.purple));

        holderBidHistory.addView(tvNoBid);
    }
}
