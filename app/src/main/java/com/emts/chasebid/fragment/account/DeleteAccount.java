package com.emts.chasebid.fragment.account;


import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.TextView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.activity.MainActivity;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by Srijana on 3/29/2017.
 */

public class DeleteAccount extends android.support.v4.app.Fragment {
    PreferenceHelper prefHelper;
    CheckBox termNCondition;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_delete_account, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prefHelper = PreferenceHelper.getInstance(getContext());

        termNCondition = (CheckBox) view.findViewById(R.id.term_n_condn);
        final TextView errorText = (TextView) view.findViewById(R.id.error_del);

        TextView btnCancel = (TextView) view.findViewById(R.id.btn_cancel);

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (termNCondition.isChecked()) {
                    if (NetworkUtils.isInNetwork(getActivity())) {
                        deleteAccountTask();
                    }else{
                        AlertUtils.showSnack(getActivity(),errorText,getString(R.string.error_no_internet));
                    }
                    errorText.setVisibility(View.GONE);
                } else {
                    errorText.setText(R.string.check_tnc);
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void deleteAccountTask() {
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(getActivity(), getString(R.string.please_wait));
        pDialog.show();
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_id", String.valueOf(prefHelper.getUserId()));
        postMap.put("token", prefHelper.getToken());
        postMap.put("t_c", "yes");


        VolleyHelper vHelper = VolleyHelper.getInstance(getActivity());
        vHelper.addVolleyRequestListeners(Api.getInstance().deleteAccountUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
//                pList.clear();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(resObj.getString("message"));
                        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                prefHelper.edit().clear().commit();
                                Intent intent = new Intent(getContext(), MainActivity.class);
                                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(intent);
                            }
                        });
                        builder.setCancelable(false);
                        builder.show();

                    } else {
                        AlertUtils.showAlertMessage(getActivity(), "Error Message", resObj.getString("message"));

                    }

                } catch (JSONException e) {
                    Logger.e("deleteAccountTask json ex", e.getMessage() + "");
                } catch (Exception e) {
                    Logger.e("deleteAccountTaskParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
//                    if (offset == 0) {
                    AlertUtils.showAlertMessage(getActivity(), "Error Message", errorObj.getString("message"));
//                    }
                } catch (Exception e) {
                    Logger.e("deleteAccountTask onError Exception", e.getMessage() + "");

//                        }

                }

            }


        }, "deleteAccountTask");


    }
}
