package com.emts.chasebid.fragment.account;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.WonAuctionAdapter;
import com.emts.chasebid.customviews.EndlessRecyclerViewScrollListener;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.WonAuction;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by User on 2016-07-01.
 */
public class WonAuctions extends android.support.v4.app.Fragment {
    public static final int PAYMENT_CONFIRM_REQ_CODE = 3243;

    RecyclerView wonList;
    ArrayList<WonAuction> wonLists = new ArrayList<>();
    SwipeRefreshLayout listRefresh;
    ProgressBar  progressInfinite;
//    ProgressBar progressBar, progressInfinite;
    FrameLayout progressBar;
    TextView errorText;
    WonAuctionAdapter adapter;
    PreferenceHelper prefsHelper;
    int offset = 0;
    int limit = Config.LIMIT;
    EndlessRecyclerViewScrollListener infiniteScrollListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_product_listing, container, false);
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        prefsHelper = PreferenceHelper.getInstance(getActivity());
        wonList = (RecyclerView) view.findViewById(R.id.list_recycler);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        wonList.setLayoutManager(layoutManager);

//        progressBar = (ProgressBar) view.findViewById(R.id.progress);
        progressBar = (FrameLayout) view.findViewById(R.id.progress);
        progressInfinite = (ProgressBar) view.findViewById(R.id.progress_infinite);
        errorText = (TextView) view.findViewById(R.id.tv_error_message);

        adapter = new WonAuctionAdapter(wonLists, getActivity());
        wonList.setAdapter(adapter);


        listRefresh = (SwipeRefreshLayout) view.findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(getActivity(), 30),
                (int) Utils.pxFromDp(getActivity(), 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getActivity())) {
                    offset = 0;
                    wonListingTask(offset);
                } else {
                    Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    listRefresh.setRefreshing(false);
                    wonList.setVisibility(View.GONE);
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        });
        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (wonLists.size() >= limit) {
                    wonListingTask(offset);
                    Logger.e("loadmore", "loadmore");
                }
            }
        };
        wonList.addOnScrollListener(infiniteScrollListener);

        if (NetworkUtils.isInNetwork(getActivity())) {
            wonListingTask(offset);
        } else {
            errorText.setText(getString(R.string.error_no_internet));
            errorText.setVisibility(View.VISIBLE);
            wonList.setVisibility(View.VISIBLE);
        }
    }

    public void wonListingTask(final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }
        final PreferenceHelper helper = PreferenceHelper.getInstance(getActivity());

        VolleyHelper vHelper = VolleyHelper.getInstance(getContext());
        final HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("token", helper.getToken());
        postParams.put("user_id", String.valueOf(helper.getUserId()));
        postParams.put("offset", String.valueOf(offsetValue));
        postParams.put("limit", String.valueOf(limit));

        vHelper.addVolleyRequestListeners(Api.getInstance().wonAuctionUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        JSONArray pArray = resObj.getJSONArray("won_auc");
                        if (pArray.length() == 0) {
                            errorText.setText("No product in won auction");
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            errorText.setVisibility(View.GONE);
                            if (pArray.length() > 0) {
                                if (offset == 0) {
                                    wonLists.clear();
                                }
                                for (int i = 0; i < pArray.length(); i++) {
                                    JSONObject eachObj = pArray.getJSONObject(i);
                                    WonAuction wonAuction = new WonAuction();
                                    wonAuction.setpId(Long.parseLong(eachObj.getString("product_id")));
                                    wonAuction.setId(Integer.valueOf(eachObj.getString("id")));
                                    wonAuction.setpName(eachObj.getString("name"));
                                    wonAuction.setPayment_status(eachObj.getString("payment_status"));
                                    wonAuction.setShippingStatus(eachObj.getString("shipping_status"));
                                    wonAuction.setpPrice(Double.parseDouble(eachObj.getString("won_amt")));
                                    wonAuction.setFavTime(eachObj.getString("end_date"));
                                    wonAuction.setImageUrl(eachObj.getString("image1"));

                                    wonLists.add(wonAuction);
                                }
                                offset = offset + limit;
                                wonList.setVisibility(View.VISIBLE);
                                adapter.notifyDataSetChanged();
                                errorText.setVisibility(View.GONE);
                                Logger.e("offset", String.valueOf(offset));
                                Logger.e("size", String.valueOf(wonLists.size()));
                            } else {
                                if (offset == 0) {
                                    errorText.setVisibility(View.VISIBLE);
                                    wonList.setVisibility(View.GONE);
                                    errorText.setText(resObj.getString("message"));
                                    errorText.setVisibility(View.VISIBLE);
                                } else {
                                    wonList.setVisibility(View.VISIBLE);
                                    AlertUtils.showSnack(getActivity(), errorText, getString(R.string.no_more_won_auc));
                                }

                            }
                        }


                    } else {
                        if (offset == 0) {
                            wonList.setVisibility(View.GONE);
                            progressBar.setVisibility(View.GONE);
                            errorText.setText(resObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            wonList.setVisibility(View.GONE);
                            AlertUtils.showSnack(getActivity(), errorText, resObj.getString("message"));
                            progressInfinite.setVisibility(View.GONE);
                        }
                    }


                } catch (JSONException e) {
                    Logger.e("wonAuctions json ex", e.getMessage() + "");
                }
                progressBar.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
                progressInfinite.setVisibility(View.GONE);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (errorObj.has("message"))
                        errorMessage = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("onError Exception", e.getMessage() + "");

                }
                try {
                    if (error instanceof NetworkError) {
                        errorMessage = getActivity().getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMessage = getActivity().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMessage = getActivity().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMessage = getActivity().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMessage = getActivity().getString(R.string.error_time_out);
                    }
                } catch (Exception ee) {
                    Logger.e("favListingTask error catch", ee.getMessage() + "");
                }
                if (offset == 0) {
                    errorText.setText(errorMessage);
                    errorText.setVisibility(View.VISIBLE);
                    wonList.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(getActivity(), errorText, errorMessage);
                }
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("favListingTask  exception", e.getMessage());
                }
                listRefresh.setRefreshing(false);
                wonList.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }, "wonListingTask");


        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);
    }

    @Override
    public void onPause() {
        VolleyHelper.getInstance(getContext()).cancelRequest("wonListingTask");
        super.onPause();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == Activity.RESULT_OK && requestCode == PAYMENT_CONFIRM_REQ_CODE) {
            if (NetworkUtils.isInNetwork(getActivity())) {
                wonList.setVisibility(View.GONE);
                errorText.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                //  UserAccountDataTask();
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        wonListingTask(offset);
                    }
                }, 0);// timer set due to delay in update in database
            } else {
                Toast.makeText(getActivity(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
            }
        }
    }
}

