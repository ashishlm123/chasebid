package com.emts.chasebid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.fragment.AboutApp;

/**
 * Created by Shreedhar'pc on 3/15/2016.
 */
public class TermAndConditionActivity extends AppCompatActivity {
    Toolbar toolbar;
    TextView accept, discard;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_term_and_condition);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(getString(R.string.title_term_condn));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        accept = (TextView) findViewById(R.id.btn_accept);
        accept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Accept", true);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        discard = (TextView) findViewById(R.id.btn_discard);
        discard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent returnIntent = new Intent();
                returnIntent.putExtra("Accept", false);
                setResult(Activity.RESULT_OK, returnIntent);
                finish();
            }
        });

        Bundle bundle = new Bundle();
        bundle.putBoolean("tnc", true);
        Fragment tncFrag = new AboutApp();
        tncFrag.setArguments(bundle);
        getSupportFragmentManager().beginTransaction().replace(R.id.tnc_container, tncFrag, "AboutApp").commit();


    }

}
