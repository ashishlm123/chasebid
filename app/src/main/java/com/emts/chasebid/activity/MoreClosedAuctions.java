package com.emts.chasebid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.GravityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.fragment.account.Profile;
import com.emts.chasebid.helper.PreferenceHelper;
import com.squareup.picasso.Picasso;

/**
 * Created by User on 2017-03-30.
 */

public class MoreClosedAuctions extends AppCompatActivity {
    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_more_closed);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_recent_closed);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);

        if (preferenceHelper.isLogin()) {
            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            holderUserPoints.setVisibility(View.GONE);
        }
    }
}
