package com.emts.chasebid.activity;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.firebase.iid.FirebaseInstanceId;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterAuthToken;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.identity.TwitterAuthClient;
import com.twitter.sdk.android.core.identity.TwitterLoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import io.fabric.sdk.android.Fabric;

public class LoginActivity extends AppCompatActivity implements GoogleApiClient.OnConnectionFailedListener {
    CallbackManager callbackManager;
    PreferenceHelper prefsHelper;
    GoogleApiClient mGoogleApiClient;
    private static final int GOOGLE_SIGN_IN = 121;
    TextView errorName, errorPassword;
    public static final String DATEPICKER_TAG = "datepicker";
    Calendar calendar;
    boolean isFb = false;
    boolean isTwitter = false;
    TwitterLoginButton twitterButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        String fBId = prefsHelper.getString(PreferenceHelper.FB_APP_ID, getString(R.string.facebook_app_id));
        FacebookSdk.setApplicationId(fBId);

        //twitter
        String twitterConsumerKey = prefsHelper.getString(PreferenceHelper.TWITTER_APP_KEY, getString(R.string.twitter_consumer_key));
        String twitterConsumerSecret = prefsHelper.getString(PreferenceHelper.TWITTER_APP_SECRET, getString(R.string.twitter_consumer_secret));

        Logger.e("con", prefsHelper.getString(PreferenceHelper.TWITTER_APP_KEY, getString(R.string.twitter_consumer_key)));
//        String twitterConsumerKey =  getString(R.string.twitter_consumer_key);
//        String twitterConsumerSecret = getString(R.string.twitter_consumer_secret);

        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                twitterConsumerKey, twitterConsumerSecret);
        Fabric.with(this, new Twitter(authConfig));

        // Configure sign-in to request the user's ID, email address, and basic
        // profile. ID and basic profile are included in DEFAULT_SIGN_IN.
        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        // Build a GoogleApiClient with access to the Google Sign-In API and the
        // options specified by gso.
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this)
                    .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                    .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                    .build();
        }

        setContentView(R.layout.activity_login);

        calendar = Calendar.getInstance();

        //sign up google
        RelativeLayout signUpGoogle = (RelativeLayout) findViewById(R.id.btn_registergoogle);
        signUpGoogle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                    googleSignIn();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, twitterButton, getString(R.string.error_no_internet));
                }
            }
        });

        final RelativeLayout signuptwitter = (RelativeLayout) findViewById(R.id.btn_registertwitter);
        twitterButton = (TwitterLoginButton) findViewById(R.id.twitter_login);
        signuptwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (NetworkUtils.isInNetwork(LoginActivity.this)) {
                    twitterButton.performClick();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, twitterButton, getString(R.string.error_no_internet));
                }
            }
        });

        twitterButton.setCallback(new Callback<TwitterSession>() {
            @Override
            public void success(Result<TwitterSession> result) {
                final TwitterSession session = Twitter.getSessionManager().getActiveSession();
                TwitterAuthToken authToken = session.getAuthToken();
                String token = authToken.token;
                String secret = authToken.secret;
                String name = session.getUserName();
                final String birthday = "";

//for email from twitter
                final TwitterAuthClient authClient = new TwitterAuthClient();
                authClient.requestEmail(session, new Callback<String>() {
                    @Override
                    public void success(Result<String> result) {
                        if (TextUtils.isEmpty(result.data)) {
                            isFb = false;
                            isTwitter = true;
                            showAlertTask(String.valueOf(session.getUserId()), "", session.getUserName(), isFb, isTwitter);
                        } else {
                            registerWithTwitter(result.data, session.getUserId(), session.getUserName(), birthday, false);
                            CookieSyncManager.createInstance(LoginActivity.this);
                            CookieManager cookieManager = CookieManager.getInstance();
                            cookieManager.removeSessionCookie();
                            Twitter.getSessionManager().clearActiveSession();
                            Twitter.logOut();
                        }

                    }

                    @Override
                    public void failure(TwitterException exception) {
                        isFb = false;
                        isTwitter = true;
                        showAlertTask(String.valueOf(session.getUserId()), "", session.getUserName(), isFb, isTwitter);
                        CookieSyncManager.createInstance(LoginActivity.this);
                        CookieManager cookieManager = CookieManager.getInstance();
                        cookieManager.removeSessionCookie();
                        Twitter.getSessionManager().clearActiveSession();
                        Twitter.logOut();
                        Logger.e("exception", String.valueOf(exception) + "hhh");
                    }
                });
            }

            @Override
            public void failure(TwitterException exception) {
                // Do something on failure
            }
        });


        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
        CollapsingToolbarLayout collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.toolbar_layout);
        Typeface tf = Typeface.createFromAsset(getAssets(), "fonts/teko_bold.ttf");
        collapsingToolbarLayout.setCollapsedTitleTypeface(tf);
        collapsingToolbarLayout.setExpandedTitleTypeface(tf);

//        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        LinearLayout llSignUp = (LinearLayout) findViewById(R.id.ll_signup);
        llSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
                startActivity(intent);
            }
        });

        TextView btnLogin = (TextView) findViewById(R.id.btn_login);
        RelativeLayout fb_register = (RelativeLayout) findViewById(R.id.btn_registerfbs);
        assert fb_register != null;
        fb_register.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    fbTask();
                } else {
                    AlertUtils.showSnack(LoginActivity.this, toolbar, getString(R.string.error_no_internet));
                }

            }
        });

        final EditText userName = (EditText) findViewById(R.id.et_login_username);
        userName.setText(PreferenceHelper.getInstance(getApplicationContext()).getString(PreferenceHelper.APP_USER_NAME, ""));
        final EditText password = (EditText) findViewById(R.id.et_login_password);

        errorName = (TextView) findViewById(R.id.validate_username);
        errorPassword = (TextView) findViewById(R.id.validate_password);

        assert btnLogin != null;
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(userName.getText().toString().trim()) ||
                        TextUtils.isEmpty(password.getText().toString().trim())) {
                    view.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake_animation);
                    view.setAnimation(anim);
                    errorPassword.setText(R.string.enter_username_pass);
                    errorPassword.setVisibility(View.VISIBLE);
                } else {
                    errorPassword.setVisibility(View.GONE);
                    view.clearAnimation();
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
                        loginTask(userName.getText().toString().trim(), password.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(LoginActivity.this, errorName, getString(R.string.error_no_internet));
                    }
                }
            }
        });

    }

    private void registerWithTwitter(final String email, final long userId, final String userName,
                                     String birthday, final boolean isRegister) {
        String year = "";
        String month = "";
        String day = "";

        if (!birthday.equalsIgnoreCase("")) {
            String bDay = birthday;
            String[] parts = bDay.split("-");
            year = parts[0];
            month = parts[1];
            day = parts[2];
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(LoginActivity.this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("id", String.valueOf(userId));
        postParams.put("email", email);
        postParams.put("day", day);
        postParams.put("month", month);
        postParams.put("year", year);
        postParams.put("name", userName);
        postParams.put("push_id", "");


        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, getString(R.string.logging_in));
        pDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().twitterRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        if (res.getBoolean("required_register")) {
                            JSONObject userObj = res.getJSONObject("user_data");
                            prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("first_name")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, userObj.getString("bonus_points")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, userObj.getString("balance")).commit();
                            prefsHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(userObj.getString("id"))).commit();
//                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, res.getString("country_short_code")).commit();

                            if (isRegister) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true).commit();
                                ChaseBidApp.getInstance().afTrackRegisterEvent(userObj.optString("id"), "twitter");
                            }

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }

                    } else {
                        if (res.has("required_register")) {
                            if (res.getBoolean("required_register")) {
                                isFb = false;
                                isTwitter = true;
                                showAlertTask(String.valueOf(userId), email, userName, isFb, isTwitter);
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                            }
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }

                    }
                } catch (JSONException e) {
                    Logger.e("registerWithTwitter json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();

                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "registerWithTwitter");


    }

    @Override
    public void onStart() {
        super.onStart();
        if (!mGoogleApiClient.isConnected()) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
    }

    public void forgetPassword(View view) {
        startActivity(new Intent(getApplicationContext(), ForgetPassword.class));
    }

    private void loginTask(final String userName, final String password) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_name", userName);
        postMap.put("password", password);
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postMap.put("push_id", pushToken);
        }

//
        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, getString(R.string.logging_in));
        pDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().loginUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        JSONObject userObj = res.getJSONObject("details");
                        prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("user_name")).commit();
                        prefsHelper.edit().putString(PreferenceHelper.TOKEN, res.getString("token")).commit();
                        prefsHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, userObj.getString("balance")).commit();
                        prefsHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, userObj.getString("bonus_points")).commit();
                        prefsHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(userObj.getString("id"))).commit();
                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, res.getString("country_short_code")).commit();

                        prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, false).commit();

                        Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                    } else {
                        AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.loging_error), res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("loginTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.loging_error), errorObj.getString("message"));
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "loginTask");

    }

    private void fbTask() {
        callbackManager = CallbackManager.Factory.create();
        LoginManager.getInstance().logOut();

        LoginManager.getInstance().logInWithReadPermissions(LoginActivity.this, (Arrays.asList("email",
                "public_profile")));
        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Logger.e("facebook login response", loginResult.toString() + " &&\n "
                                + loginResult.getAccessToken() + "\n" + loginResult.getRecentlyGrantedPermissions()
                                + "\n" + loginResult.getRecentlyDeniedPermissions() + "\n");

                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(
                                            JSONObject object,
                                            GraphResponse response) {
//                                        Logger.e("fbTask res", object + " ");
//                                        Logger.e("graph response error", response.getError().getErrorMessage() + "\n"
//                                                + response.getRequest() + "\n" + response.getJSONObject());
                                        // Application code
                                        try {
                                            String id = object.optString("id");
                                            String name = object.optString("name");
                                            String first_name = object.optString("first_name");
                                            String last_name = object.optString("last_name");
//                                            String city = object.getString("hometown");
//                                            String location = object.getJSONObject("location").getString("name");
                                            String age = object.optString("age_range");
                                            String picture = object.optString("picture");
                                            String email = object.optString("email"); //Get null value here
                                            String gender = object.optString("gender");


                                            Logger.e("Facebook Data", id + "\n" + name + "\n" + first_name
                                                    + "\n" + last_name + "\n" + email + "\n" + age + "\n" + picture);
                                            LoginManager.getInstance().logOut();

                                            String birthday = "";

                                            if (TextUtils.isEmpty(email)) {
                                                isFb = true;
                                                isTwitter = false;
                                                showAlertTask(id, "", name, isFb, isTwitter);
                                            } else {
                                                registerWithFbTask(id, name, email, birthday, false);
                                            }
                                        } catch (Exception e) {
                                            LoginManager.getInstance().logOut();
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,first_name,last_name,email,gender," +
                                "age_range");
                        request.setParameters(parameters);
                        request.executeAsync();
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Logger.e("cancell", "called");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Logger.e("error", exception.getMessage() + "\n" + exception.getCause());
                    }
                });

    }

    private void registerWithFbTask(final String fbId, final String fbFirst_name, final String fbEmail,
                                    final String fbBirthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("id", fbId);
        postParams.put("email", fbEmail);
        postParams.put("birthday", fbBirthday);
        postParams.put("first_name", fbFirst_name);
        postParams.put("name", fbFirst_name);
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postParams.put("push_id", pushToken);
        }


        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, getString(R.string.logging_in));
        pDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().fbRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        if (res.getBoolean("required_register")) {
                            JSONObject userObj = res.getJSONObject("user_data");
                            prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("first_name")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, userObj.getString("bonus_points")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, userObj.getString("balance")).commit();
                            prefsHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(userObj.getString("id"))).commit();
//                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, res.getString("country_short_code")).commit();

                            if (isRegister) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true).commit();
                                ChaseBidApp.getInstance().afTrackRegisterEvent(userObj.optString("id"), "facebook");
                            }

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }
                    } else {

                        if (res.has("required_register")) {
                            if (res.getBoolean("required_register")) {
                                isFb = true;
                                isTwitter = false;
                                showAlertTask(fbId, fbEmail, fbFirst_name, isFb, isTwitter);
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                            }
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }

                    }
                } catch (JSONException e) {
                    Logger.e("loginTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();

                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "registerWithFbTask");

    }

    // [START signOut]
    private void signOut() {
        if (mGoogleApiClient.isConnected()) {
            Auth.GoogleSignInApi.signOut(mGoogleApiClient).setResultCallback(
                    new ResultCallback<Status>() {
                        @Override
                        public void onResult(@NonNull Status status) {
                            // [START_EXCLUDE]
                            Logger.e("google signOut", status.getStatus().getStatusMessage() + " ** "
                                    + status.isSuccess());
                            // [END_EXCLUDE]
                        }
                    });
        }
    }


    private void googleSignIn() {
        Intent intent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
        startActivityForResult(intent, GOOGLE_SIGN_IN);
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Logger.e("google sign in conn failed", connectionResult.getErrorMessage() + "");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == GOOGLE_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            handleSignInResult(result);
        }


        twitterButton.onActivityResult(requestCode, resultCode, data);
    }

    private void handleSignInResult(GoogleSignInResult result) {
        Logger.e("handelSignInResult", "handleSignInResult:" + result.isSuccess());
        if (result.isSuccess()) {
            // Signed in successfully, show authenticated UI.
            GoogleSignInAccount acct = result.getSignInAccount();

            String userName = acct.getDisplayName();
            String id = acct.getId();
            String f_name = acct.getGivenName();
            String email = acct.getEmail();
            String birthday = "";


            if (TextUtils.isEmpty(email)) {
                isFb = false;
                isTwitter = false;
                showAlertTask(id, "", userName, isFb, isTwitter);
            } else {
                registerWithGoogleTask(id, f_name, email, birthday, false);
            }

//            Uri photoUri = acct.getPhotoUrl();
//            if (photoUri != null) {
//                intent.putExtra("google_pp", acct.getPhotoUrl().toString());
//            }


            signOut();
//            startActivity(intent);


        } else {
            // Signed out, show unauthenticated UI.
            signOut();
        }

    }

    private void registerWithGoogleTask(final String id, final String userName, final String email,
                                        String birthday, final boolean isRegister) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("id", id);
        postParams.put("email", email);
        postParams.put("birthday", birthday);
        postParams.put("first_name", userName);
        postParams.put("name", userName);
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postParams.put("push_id", pushToken);
        }

        final ProgressDialog pDialog = AlertUtils.showProgressDialog(LoginActivity.this, getString(R.string.logging_in));
        pDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().googleRegisterUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        if (res.getBoolean("required_register")) {

                            JSONObject userObj = res.getJSONObject("user_data");
                            prefsHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, userObj.getString("email")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.APP_USER_NAME, userObj.getString("first_name")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, userObj.getString("bonus_points")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.TOKEN, userObj.getString("token")).commit();
                            prefsHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, userObj.getString("balance")).commit();
                            prefsHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(userObj.getString("id"))).commit();
//                        prefsHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, res.getString("country_short_code")).commit();

                            if (isRegister) {
                                prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true).commit();
                                ChaseBidApp.getInstance().afTrackRegisterEvent(userObj.optString("id"), "google");
                            }

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(intent);
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }
                    } else {
                        if (res.has("required_register")) {
                            if (res.getBoolean("required_register")) {
                                isFb = false;
                                isTwitter = false;
                                showAlertTask(id, email, userName, isFb, isTwitter);
                            } else {
                                AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                            }
                        } else {
                            AlertUtils.showAlertMessage(LoginActivity.this, getString(R.string.title_error), res.getString("message"));
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("registerWithGoogleTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();

                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    Toast.makeText(getApplicationContext(), errorObj.getString("message"), Toast.LENGTH_LONG).show();
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "registerWithGoogleTask");

    }

    private void showAlertTask(final String id, final String email, final String name, final boolean isfb, final boolean isTwitter) {
        final android.support.v7.app.AlertDialog.Builder alertDailog = new android.support.v7.app.AlertDialog.Builder(LoginActivity.this);
        final View view = LayoutInflater.from(LoginActivity.this).inflate(R.layout.alert_social_login, null);
        alertDailog.setView(view);
        alertDailog.show();

        final EditText alertEmail = (EditText) view.findViewById(R.id.alert_email);
        Logger.e("emal", email + "f");
        if (!TextUtils.isEmpty(email)) {
            alertEmail.setText(email);
            alertEmail.setEnabled(false);
        } else {
            alertEmail.setEnabled(true);
        }
        final EditText alertBirthday = (EditText) view.findViewById(R.id.alert_birthday);

        final com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener dateSetListener = new com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, month);
                calendar.set(Calendar.DAY_OF_MONTH, day);
                alertBirthday.setText(year + "-" + (month + 1) + "-" + day);

            }
        };
        alertBirthday.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    final com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog =
                            com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(dateSetListener,
                                    calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
//                                1997, 00, 01, true);

                    datePickerDialog.setVibrate(true);
                    datePickerDialog.setYearRange(1902, 1997);
                    datePickerDialog.setCloseOnSingleTapDay(false);
                    if (!datePickerDialog.isVisible()) {
                        datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
                    }
                }

                return true;
            }
        });

        TextView ok = (TextView) view.findViewById(R.id.txt_ok);
        ok.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Calendar requiredDate = Calendar.getInstance();
                requiredDate.setTime(new Date());
                requiredDate.add(Calendar.YEAR, -18);
                if (!Patterns.EMAIL_ADDRESS.matcher(alertEmail.getText().toString().trim()).matches() &&
                        TextUtils.isEmpty(alertEmail.getText().toString().trim())) {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.enter_valid_email));

                } else if (calendar.after(requiredDate)) {
                    AlertUtils.showSnack(LoginActivity.this, view, getString(R.string.age_limit));
                } else {
                    if (isTwitter) {
                        registerWithTwitter(alertEmail.getText().toString().trim(), Long.parseLong(id), name, alertBirthday.getText().toString().trim(), true);
                    } else {
                        if (isfb) {
                            registerWithFbTask(id, name, alertEmail.getText().toString().trim(), alertBirthday.getText().toString().trim(), true);
                        } else {
                            registerWithGoogleTask(id, name, alertEmail.getText().toString().trim(), alertBirthday.getText().toString().trim(), true);
                        }
                    }
                }
            }
        });

    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("registerWithTwitter");
        VolleyHelper.getInstance(this).cancelRequest("loginTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithFbTask");
        VolleyHelper.getInstance(this).cancelRequest("registerWithGoogleTask");
        super.onDestroy();
        mGoogleApiClient.stopAutoManage(LoginActivity.this);
        mGoogleApiClient.disconnect();
    }


}
