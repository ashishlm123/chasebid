//package com.emts.chasebid.activity;
//
//
//import android.content.DialogInterface;
//import android.content.Intent;
//import android.graphics.Bitmap;
//import android.os.Bundle;
//import android.os.Handler;
//import android.support.annotation.Nullable;
//import android.support.v7.app.AlertDialog;
//import android.support.v7.app.AppCompatActivity;
//import android.support.v7.widget.Toolbar;
//import android.view.View;
//import android.webkit.CookieManager;
//import android.webkit.CookieSyncManager;
//import android.webkit.JavascriptInterface;
//import android.webkit.WebSettings;
//import android.webkit.WebView;
//import android.webkit.WebViewClient;
//import android.widget.TextView;
//
//import com.emts.chasebid.R;
//import com.emts.chasebid.customviews.ProgressDialog;
//import com.emts.chasebid.helper.Api;
//import com.emts.chasebid.helper.Logger;
//import com.emts.chasebid.helper.PreferenceHelper;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
///**
// * Created by theone on 7/12/2016.
// */
//public class PayPalPaymetActivity extends AppCompatActivity {
//    WebView webView;
//    ProgressDialog progressDialog;
//
//    String successUri = Api.getInstance().wonProductSuccessUrl;
//    String cancelUri = Api.getInstance().wonProductCancelUrl;
//    boolean isBuyBidCredit = false;
//    boolean buyNow = false;
//
//    PreferenceHelper prefsHelper;
//
//    @Override
//    protected void onCreate(@Nullable Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_payment_paypal);
//        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
//        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_payment);
//        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                onBackPressed();
//            }
//        });
//
//        progressDialog = new ProgressDialog(PayPalPaymetActivity.this);
//        progressDialog.setCancelable(true);
//        progressDialog.setCanceledOnTouchOutside(false);
//        progressDialog.setMessage(getString(R.string.loading));
//        progressDialog.show();
//
//        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
//
//        webView = (WebView) findViewById(R.id.payment_webview);
//
//        WebSettings webSettings = webView.getSettings();
//        webSettings.setLoadsImagesAutomatically(true);
//        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
//        webSettings.setSupportZoom(true);
//        webSettings.setBuiltInZoomControls(true);
//        webSettings.setDisplayZoomControls(false);
//        webSettings.setLoadWithOverviewMode(true);
//        webSettings.setUseWideViewPort(true);
//        webSettings.setJavaScriptEnabled(true);
//
//        // Register a new JavaScript interface called HTMLOUT /
//        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");
//
//        webView.setWebViewClient(new WebViewClientPaymentGateway());
//
//        CookieSyncManager.createInstance(PayPalPaymetActivity.this);
//        CookieSyncManager.getInstance().sync();
//
//        CookieManager.getInstance().removeAllCookie();
//        webView.clearCache(true);
//
//        String postParams = "";
//        Intent intent = getIntent();
//        if (intent != null) {
//            postParams = intent.getStringExtra("post_params");
//            isBuyBidCredit = intent.getBooleanExtra("is_buy_credit", false);
//            buyNow=intent.getBooleanExtra("is_buy_now",false);
//        }
//        Logger.e("Post Params:", postParams);
//
//        String url = "";
//        if (isBuyBidCredit) {
//            url = Api.getInstance().buyBidCreditPaymentUrl;
//            webView.postUrl(url, postParams.getBytes());
//        } else if (buyNow) {
//            url = Api.getInstance().buyNowUrl;
//            webView.postUrl(url, postParams.getBytes());
//        } else {
//            url = Api.getInstance().paymentConfirmationUrl;
//            webView.postUrl(url, postParams.getBytes());
//        }
//    }
//
//    private void removeSessionCookies() {
//        CookieManager.getInstance().removeSessionCookie();
//        CookieSyncManager.getInstance().sync();
//    }
//
//    private class WebViewClientPaymentGateway
//            extends WebViewClient {
//        WebViewClientPaymentGateway() {
//        }
//
//        public void onPageFinished(WebView paramWebView, String paramString) {
//            progressDialog.dismiss();
//
//            // This call inject JavaScript into the page which just finished loading.
//            if (isBuyBidCredit && paramString.contains(Api.getInstance().getIpGeneral())) {
//                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//            }
//        }
//
//        public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap) {
//            Logger.e("onPageStarted", paramString);
//            if (paramString.contains("")) {
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        try {
//                            progressDialog.dismiss();
//                        } catch (Exception e) {
//                        }
//                    }
//                }, 4000);
//
//            }
//            if (paramString.contains(successUri)) {
//                progressDialog.dismiss();
//                webView.stopLoading();
//                new Handler().postDelayed(new Runnable() {
//                    @Override
//                    public void run() {
//                        Api.getInstance().getUserDetailTask(PayPalPaymetActivity.this, null);
//                    }
//                }, 0); // due to the delay  in update
//
//                showAlert(getString(R.string.payment_sucess), true);
//            } else if (paramString.contains(cancelUri)) {
//                progressDialog.dismiss();
//                webView.stopLoading();
////                String errorOnly = paramString;
////                errorOnly = errorOnly.substring(errorOnly.lastIndexOf("=") + 1);
////                try {
////                    String result = java.net.URLDecoder.decode(errorOnly, "UTF-8");
////                    showAlert(result, false);
////                } catch (UnsupportedEncodingException e) {
////                    showAlert("Payment Cancelled", false);
////                }
//                showAlert(getString(R.string.payment_cancel), false);
//                webView.setVisibility(View.GONE);
//            }
//            super.onPageStarted(paramWebView, paramString, paramBitmap);
//        }
//
//        public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2) {
//            Logger.e("Error", "Any Error?");
//        }
//    }
//
//    @Override
//    protected void onPause() {
//        CookieSyncManager.getInstance().stopSync();
//        super.onPause();
//    }
//
//    protected void onResume() {
//        CookieSyncManager.getInstance().startSync();
//        super.onResume();
//    }
//
//    private void showAlert(String message, final boolean ok) {
//        AlertDialog.Builder builder = new AlertDialog.Builder(PayPalPaymetActivity.this);
//        builder.setCancelable(false);
//        builder.setMessage(message);
//        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
//            @Override
//            public void onClick(DialogInterface dialogInterface, int i) {
//                dialogInterface.dismiss();
//                if (ok) {
//                    Intent returnIntent = new Intent();
//                    returnIntent.putExtra("paid", true);
//                    setResult(RESULT_OK, returnIntent);
//                }
//                finish();
//            }
//        });
//        builder.show();
//    }
//
//    class MyJavaScriptInterface {
//        @SuppressWarnings("unused")
//        @JavascriptInterface
//        public void processHTML(String html) {
//            // process the html as needed by the app
//            Logger.e("full html content", html + "");
//
//            String jsonOnly = "{" + html.substring(html.indexOf(">{") + 2, html.indexOf("}</pre>")) + "}";
//            Logger.e("json only", jsonOnly);
//
//            try {
//                final JSONObject resObj = new JSONObject(jsonOnly);
//                if (!resObj.getBoolean("status")) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            try {
//                                webView.setVisibility(View.GONE);
//                                AlertDialog.Builder builder = new AlertDialog.Builder(PayPalPaymetActivity.this);
//                                builder.setCancelable(false);
//                                builder.setMessage(resObj.getString("message"));
//                                builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
//                                    @Override
//                                    public void onClick(DialogInterface dialogInterface, int i) {
//                                        dialogInterface.dismiss();
//                                        onBackPressed();
//
//                                    }
//                                });
//                                builder.show();
//                            } catch (Exception e) {
//                                Logger.e("ex label 23432", e.getMessage() + "");
//                            }
//                        }
//                    });
//                }
//            } catch (JSONException ex) {
//                Logger.e("error parsing html", ex.getMessage() + "");
//            }
//        }
//    }
//}
