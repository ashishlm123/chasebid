package com.emts.chasebid.activity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.widget.ImageView;

import com.android.volley.Request;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;

import rm.com.clocks.Clock;

/**
 * Created by User on 2016-08-17.
 */
public class SplashActivity extends AppCompatActivity {
    PreferenceHelper prefHelper;
    Clock clock;
    int count = 0;
    boolean hasCountryCode = false;
    int countryCodeRetry = 0;
    private final int COUNTRY_CODE_RETRY_COUNT = 2;
    private final int SPLASH_TIMER = 3;//seconds

//    private InterstitialAd mInterstitialAd;


    Handler handler;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            handler.postDelayed(runnable, 1000);
            count++;

            if (count > SPLASH_TIMER && hasCountryCode) {
//                showAd();
                startActivity(new Intent(getApplicationContext(), MainActivity.class));
//            overridePendingTransition(R.anim.in_from_right, R.anim.fade_out);
                SplashActivity.this.finish();
                handler.removeCallbacks(runnable);
            } else {
                if (countryCodeRetry > COUNTRY_CODE_RETRY_COUNT && count > SPLASH_TIMER) {
                    showAlert(false);
                }
            }
        }
    };

    private void showAlert(final boolean loadOtherApis) {
        final AlertDialog.Builder builder = new AlertDialog.Builder(SplashActivity.this);
        builder.setTitle(getString(R.string.title_error));
        builder.setMessage(R.string.cant_get_required_info);
        builder.setPositiveButton(R.string.try_again, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                count = 0;
                countryCodeRetry = 0;
                hasCountryCode = false;
                if (NetworkUtils.isInNetwork(SplashActivity.this)) {
                    getCountryCodeTask();
                    if (loadOtherApis) {
                        siteSettingTask();
//                        getCountryTask();
                        if (prefHelper.isLogin()) {
                            Api.getInstance().getUserDetailTask(SplashActivity.this, null);
                        }
                    }
                } else {
                    builder.show();
                }
            }
        });
        builder.setNegativeButton(getString(R.string.alert_cancel), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                onBackPressed();
            }
        });
        builder.show();
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prefHelper = PreferenceHelper.getInstance(this);


//        if (TextUtils.isEmpty(prefHelper.getString(PreferenceHelper.APP_USER_COUNTRY_CODE, ""))) {
        if (NetworkUtils.isInNetwork(this)) {
            getCountryCodeTask();
        } else {
            showAlert(true);
        }
//        } else {
//            hasCountryCode = true;
//        }

        if (NetworkUtils.isInNetwork(this)) {
            siteSettingTask();
            getCountryTask();
            if (prefHelper.isLogin()) {
                Api.getInstance().getUserDetailTask(SplashActivity.this, null);
            }
        }
        setContentView(R.layout.activity_splash);

        handler = new Handler();
        handler.post(runnable);

        final ImageView imgView = (ImageView) findViewById(R.id.activity_splash);
//        Animation animation = AnimationUtils.loadAnimation(this, R.anim.fade_out);
//        animation.setDuration(2400);
//        animation.setStartOffset(1000);
//        animation.setFillAfter(true);
//        imgView.startAnimation(animation);

//        AnimatorSet set = (AnimatorSet) AnimatorInflater
//                .loadAnimator(this, R.animator.splash_animator);
//        set.setTarget(imgView);
//        set.start();

//        final float[] from = new float[3], to = new float[3];
//
//        Color.colorToHSV(getResources().getColor(R.color.purple), from);   // from white
//        Color.colorToHSV(getResources().getColor(R.color.blue_light), to);     // to red
//
//        ValueAnimator anim = ValueAnimator.ofFloat(0, 1);   // animate from 0 to 1
//        anim.setDuration(2400);                              // for 300 ms
//
//        final float[] hsv = new float[3];                  // transition color
//        anim.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
//            @Override
//            public void onAnimationUpdate(ValueAnimator animation) {
//                // Transition along each axis of HSV (hue, saturation, value)
//                hsv[0] = from[0] + (to[0] - from[0]) * animation.getAnimatedFraction();
//                hsv[1] = from[1] + (to[1] - from[1]) * animation.getAnimatedFraction();
//                hsv[2] = from[2] + (to[2] - from[2]) * animation.getAnimatedFraction();
//
//                imgView.setBackgroundColor(Color.HSVToColor(hsv));
//            }
//        });
//        anim.start();

//        anim = (AnimationDrawable) imgView.getBackground();
//        anim.setEnterFadeDuration(1200);
//        anim.setExitFadeDuration(2000);

        clock = (Clock) findViewById(R.id.clocks);
        clock.animateIndeterminate();


        Logger.e("pushId", "" + FirebaseInstanceId.getInstance().getToken());
    }

    private void getCountryCodeTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().getDefCountryCode, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                hasCountryCode = true;
                                prefHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, resObj.getString("language_info")).commit();
                            } else {
                                showAlert(false);
                                countryCodeRetry++;
                                if (countryCodeRetry < COUNTRY_CODE_RETRY_COUNT) {
                                    getCountryCodeTask();
                                }
                            }
                        } catch (Exception e) {
                            Logger.e("getCountryCodeTask Parse Ex", e.getMessage() + " ");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        showAlert(false);
                        countryCodeRetry++;
                        if (countryCodeRetry < COUNTRY_CODE_RETRY_COUNT) {
                            getCountryCodeTask();
                        }
                    }
                }, "getCountryCodeTask");

    }

//    AnimationDrawable anim;
//    // Starting animation:- start the animation on onResume.
//    @Override
//    protected void onResume() {
//        super.onResume();
//        if (anim != null && !anim.isRunning())
//            anim.start();
//    }
//
//    // Stopping animation:- stop the animation on onPause.
//    @Override
//    protected void onPause() {
//        super.onPause();
//        if (anim != null && anim.isRunning())
//            anim.stop();
//    }

    private void siteSettingTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        vHelper.addVolleyRequestListeners(Api.getInstance().siteSettingUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);

                    if (resObj.getBoolean("status")) {
                        JSONArray site = resObj.getJSONArray("site_info");
                        if (site.length() > 0) {
                            for (int i = 0; i < site.length(); i++) {
                                JSONObject ssObj = site.getJSONObject(i);
                                prefHelper.edit().putString(PreferenceHelper.SITE_NAME, ssObj.getString("site_name")).apply();
                                prefHelper.edit().putString(PreferenceHelper.SITE_CONTACT_EMAIL, ssObj.getString("contact_email")).apply();
                                prefHelper.edit().putString(PreferenceHelper.SITE_STATUS, ssObj.getString("site_status")).apply();
                                prefHelper.edit().putString(PreferenceHelper.FB_APP_ID, ssObj.getString("facebook_app_id")).apply();
                                prefHelper.edit().putString(PreferenceHelper.FB_URL, ssObj.getString("facebook_url")).apply();
                                prefHelper.edit().putString(PreferenceHelper.TWITTER_APP_KEY, ssObj.getString("twitter_app_key")).apply();
                                prefHelper.edit().putString(PreferenceHelper.TWITTER_APP_SECRET, ssObj.getString("twitter_app_secret")).apply();
                                prefHelper.edit().putString(PreferenceHelper.NODE_SEVER, ssObj.getString("node_server")).apply();
                                prefHelper.edit().putString(PreferenceHelper.NODE_PORT, ssObj.getString("node_port")).apply();

                            }
                        }
                    }
                } catch (Exception e) {
                    Logger.e("siteSettingTask Parse Ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
            }
        }, "siteSettingTask");
    }

    private void getCountryTask() {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        String lastUpdate = prefHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");
        postParams.put("last_updated_date", lastUpdate);

        vHelper.addVolleyRequestListeners(Api.getInstance().countryListUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    Date date2 = null;
                    if (res.getBoolean("status")) {
                        JSONArray countryArray = res.getJSONArray("countries");

                        for (int i = 0; i < countryArray.length(); i++) {
                            JSONObject countryName = countryArray.getJSONObject(i);

                            if (countryName.getString("default_country").equalsIgnoreCase("Yes")) {
                                prefHelper.edit().putString(PreferenceHelper.SITE_CURR_CODE, countryName.getString("currency_code")).commit();
                                prefHelper.edit().putString(PreferenceHelper.SITE_CURR_SIGN, countryName.getString("currency_sign")).commit();
                                prefHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, countryName.getString("short_code")).commit();
                            }
                            String lastUpdate = countryName.getString("last_update");

                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
                            Date date1 = formatter.parse(lastUpdate);
                            String prefLastUpdate = prefHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");

                            if (TextUtils.isEmpty(prefLastUpdate)) {
                                prefHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastUpdate).commit();
                            } else {
                                date2 = formatter.parse(prefLastUpdate);
                                if (date2.after(date1)) {
                                    prefHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastUpdate).commit();
                                }
                            }
                        }
                        writeToFile(response, SplashActivity.this);
                    }

                } catch (Exception e) {
                    Logger.e("getCountryTask  Ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError volleyError) {
            }
        }, "getCountryTask");
    }

    private void writeToFile(String response, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("countrylist.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(response);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }
    }


    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("getCountryTask");
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        handler.removeCallbacks(runnable);
        super.onBackPressed();
    }

//    public void showAd() {
//        //init ad mob
////        MobileAds.initialize(this, "ca-app-pub-5437340406861574/9734976061");//clients
//        MobileAds.initialize(this, "ca-app-pub-3940256099942544/1033173712");//test
//
//        mInterstitialAd = new InterstitialAd(this);
////        mInterstitialAd.setAdUnitId("ca-app-pub-5437340406861574/9734976061");//client ad mob id
//        mInterstitialAd.setAdUnitId("ca-app-pub-3940256099942544/1033173712");//test ad mob id
//        mInterstitialAd.loadAd(new AdRequest.Builder().build());
//
////        if (mInterstitialAd.isLoaded()) {
////            mInterstitialAd.show();
////        } else {
////            Log.d("TAG", "The interstitial wasn't loaded yet.");
////        }
//        mInterstitialAd.setAdListener(new AdListener() {
//            @Override
//            public void onAdLoaded() {
//                // Code to be executed when an ad finishes loading.
//                Logger.e("Ads", "onAdLoaded");
//                mInterstitialAd.show();
//            }
//
//            @Override
//            public void onAdFailedToLoad(int errorCode) {
//                // Code to be executed when an ad request fails.
//                Logger.e("Ads", "onAdFailedToLoad");
//            }
//
//            @Override
//            public void onAdOpened() {
//                // Code to be executed when the ad is displayed.
//                Logger.e("Ads", "onAdOpened");
//            }
//
//            @Override
//            public void onAdLeftApplication() {
//                // Code to be executed when the user has left the app.
//                Logger.e("Ads", "onAdLeftApplication");
//            }
//
//            @Override
//            public void onAdClosed() {
//                // Code to be executed when when the interstitial ad is closed.
//                Logger.e("Ads", "onAdClosed");
//            }
//        });
//
//    }

}

