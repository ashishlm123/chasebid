package com.emts.chasebid.activity;

import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

/**
 * Created by User on 2017-08-28.
 */

public class MyCropActivity extends AppCompatActivity implements CropImageView.OnSetImageUriCompleteListener,
        CropImageView.OnSaveCroppedImageCompleteListener {
    CropImageView mCropImageView;
    Uri outputUri;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                Logger.e("ex 1093", e.getMessage() + "");
            }
        }

        setContentView(R.layout.activity_crop);

        mCropImageView = (CropImageView) findViewById(R.id.cropImageView);
        mCropImageView.setFixedAspectRatio(true);
        mCropImageView.setAspectRatio(1, 1);
        mCropImageView.setGuidelines(CropImageView.Guidelines.ON);

        mCropImageView.setImageUriAsync(Uri.fromFile(new File(getIntent().getStringExtra("cropPath"))));

        TextView cancel = (TextView) findViewById(R.id.btn_cancel);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setResult(RESULT_CANCELED);
                onBackPressed();
            }
        });
        ImageView imgRotate = (ImageView) findViewById(R.id.rotate);
        imgRotate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mCropImageView.rotateImage(90);
            }
        });
        TextView done = (TextView) findViewById(R.id.btn_done);
        done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                outputUri = getOutputMediaFileUri(1);
                mCropImageView.saveCroppedImageAsync(outputUri);
            }
        });
    }

    @Override
    public void onSaveCroppedImageComplete(CropImageView view, Uri uri, Exception error) {
        Intent intent = new Intent();
        intent.putExtra("outputPath", uri.getPath());
        setResult(RESULT_OK, intent);
        onBackPressed();
    }

    @Override
    public void onSetImageUriComplete(CropImageView view, Uri uri, Exception error) {

    }

    @Override
    protected void onStart() {
        super.onStart();
        mCropImageView.setOnSetImageUriCompleteListener(this);
        mCropImageView.setOnSaveCroppedImageCompleteListener(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mCropImageView.setOnSetImageUriCompleteListener(null);
        mCropImageView.setOnSaveCroppedImageCompleteListener(null);
    }


    public Uri getOutputMediaFileUri(int i) {
        return Uri.fromFile(getOutputMediaFile());
    }

    /**
     * returning image / video
     */
    private static File getOutputMediaFile() {
        // External sdcard location
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                Config.IMAGE_DIRECTORY_NAME);

        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Logger.e("getOutputMediaFile", "Oops! Failed create "
                        + Config.IMAGE_DIRECTORY_NAME + " directory");
                return null;
            }
        }

        // Create a media file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",
                Locale.getDefault()).format(new Date());
        File mediaFile;
        mediaFile = new File(mediaStorageDir.getPath() + File.separator
                + "IMG_" + timeStamp + ".jpg");

        return mediaFile;
    }

}
