package com.emts.chasebid.activity;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Patterns;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

/**
 * Created by User on 2016-07-04.
 */
public class ForgetPassword extends AppCompatActivity {
    EditText etEmail;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_forget_password);

        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_forget_password);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        TextView btnSend = (TextView) findViewById(R.id.btn_send);
        assert btnSend != null;
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                etEmail = (EditText) findViewById(R.id.et_email);
                if (NetworkUtils.isInNetwork(getApplicationContext())) {

                    if (!etEmail.getText().toString().trim().isEmpty()) {

                        if (Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString().trim()).matches()) {
                            forgotPasswordTask(etEmail.getText().toString().trim());
                        } else {
                            AlertUtils.showSnack(getApplicationContext(), toolbar, getString(R.string.invalid_email_address));
                        }

                    } else {
                        AlertUtils.showSnack(getApplicationContext(), toolbar, getString(R.string.enter_email));
                    }

                } else {
                    AlertUtils.showSnack(getApplicationContext(), toolbar, getString(R.string.error_no_internet));

                }
            }
        });
    }

    private void forgotPasswordTask(final String email) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("email", email);

//        final ProgressDialog pDialog = AlertUtils.showProgressDialog(ForgetPassword.this, getString(R.string.sending_email));
        final ProgressDialog pDialog = new ProgressDialog(ForgetPassword.this);
        pDialog.setMessage(getString(R.string.sending_email));
        pDialog.show();
        vHelper.addVolleyRequestListeners(Api.getInstance().forgetPasswordUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                etEmail.setText("");
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        showAlert(res.getString("message"));
                    } else {
                        showAlert(res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("forgetPassword json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    showAlert(errorObj.getString("message"));
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "forgotPasswordTask");
    }

    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(ForgetPassword.this);
        builder.setMessage(message);
        builder.setPositiveButton(R.string.alert_ok, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("forgotPasswordTask");
        super.onDestroy();
    }

}
