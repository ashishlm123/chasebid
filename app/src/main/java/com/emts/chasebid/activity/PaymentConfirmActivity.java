package com.emts.chasebid.activity;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.BuyCredit;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

public class PaymentConfirmActivity extends AppCompatActivity {
    public static final int PAYPAL_PAYMENT_REQUEST_CODE = 345;
    PreferenceHelper preferenceHelper;

    TextView pName, pDate, winningBid, shippingCost, totalSum, payNow;
    EditText etName, etAddress, etAddress2, etCity, etZipCode, etMobile;
    Spinner paymentMethod;
    ImageView pImage;
    Toolbar toolbar;
    LinearLayout paymentLayout;
    long productId = -1;
    boolean isValid = false;
    long winnerId = -1;
    BuyCredit credit = new BuyCredit();
    double wonAmount = 0;
    double shipCost = 0;
    String aucWinId = "";
    String creditUsed;

    boolean buyNow = false;

    //bill
    Spinner spShipCountry, spBillCountry;
    EditText etBillEmail, etBillName, etBillAddress, etBillAddress2, etBillCity, etBillZipCode, etBillMobile;

    ArrayList<String> countryName = new ArrayList<>();
    ArrayList<String> countryId = new ArrayList<>();

    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_confirm_payment);

        preferenceHelper = PreferenceHelper.getInstance(PaymentConfirmActivity.this);
        Intent intent = getIntent();

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) (toolbar.findViewById(R.id.custom_toolbar_title))).setText(R.string.title_check_out);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        if (preferenceHelper.isLogin()) {
            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            holderUserPoints.setVisibility(View.GONE);
        }

        paymentLayout = (LinearLayout) findViewById(R.id.payment_layout);
        paymentMethod = (Spinner) findViewById(R.id.payment_option);
        pImage = (ImageView) findViewById(R.id.item_product_image);
        pName = (TextView) findViewById(R.id.p_name);
        pDate = (TextView) findViewById(R.id.p_date);
        winningBid = (TextView) findViewById(R.id.winning_bid);
        shippingCost = (TextView) findViewById(R.id.shipping_cost);
        totalSum = (TextView) findViewById(R.id.total_sum);
        payNow = (TextView) findViewById(R.id.tv_pay_now);

        etName = (EditText) findViewById(R.id.et_username);
        etAddress = (EditText) findViewById(R.id.et_address);
        etAddress2 = (EditText) findViewById(R.id.et_address2);
        etCity = (EditText) findViewById(R.id.et_city);
        etZipCode = (EditText) findViewById(R.id.et_zip_code);
        etMobile = (EditText) findViewById(R.id.et_mobile);
        spShipCountry = (Spinner) findViewById(R.id.sp_country);

        //billing
        etBillEmail = (EditText) findViewById(R.id.et_email);
        etBillName = (EditText) findViewById(R.id.et_bill_name);
        etBillAddress = (EditText) findViewById(R.id.et_bill_address);
        etBillAddress2 = (EditText) findViewById(R.id.et_bill_address2);
        etBillCity = (EditText) findViewById(R.id.et_bill_city);
        etBillZipCode = (EditText) findViewById(R.id.et_bill_zip_code);
        etBillMobile = (EditText) findViewById(R.id.et_bill_mobile);
        spBillCountry = (Spinner) findViewById(R.id.sp_bill_country);

        productId = -1;
        if (intent != null) {
            productId = intent.getLongExtra("product_id", -1);
            winnerId = intent.getLongExtra("winner_id", -1);

            buyNow = intent.getBooleanExtra("buyNow", false);
        }

        if (NetworkUtils.isInNetwork(getApplicationContext())) {

            if (buyNow) {
                buyNowConfirmTask(productId);
            } else {
                paymentConfirmTask(productId);
            }
        } else {
            AlertUtils.showSnack(PaymentConfirmActivity.this, toolbar, getString(R.string.error_no_internet));
        }

        payNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isValid = true;

                if (TextUtils.isEmpty(etName.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_username), getString(R.string.empty_error_msg));
                } else {
                    if (etName.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_username), getString(R.string.errror_name_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_username));
                    }
                }
                if (TextUtils.isEmpty(etAddress.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_address), getString(R.string.empty_error_msg));
                } else {
                    if (etAddress.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_address), getString(R.string.error_address_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_address));
                    }
                }
                if (spShipCountry.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_country), getString(R.string.error_country));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_country));
                }
                if (TextUtils.isEmpty(etCity.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_city), getString(R.string.empty_error_msg));
                } else {
                    if (etCity.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_city), getString(R.string.error_city_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_city));
                    }
                }
                if (TextUtils.isEmpty(etZipCode.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_zip_code), getString(R.string.empty_error_msg));
                } else {
                    if (etZipCode.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_zip_code), getString(R.string.error_zip_code_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_zip_code));
                    }
                }
                if (TextUtils.isEmpty(etMobile.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_mobile), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_mobile));
                }

//                //AGAIN FOR BILLING
                if (TextUtils.isEmpty(etBillEmail.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_email), getString(R.string.empty_error_msg));
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(etBillEmail.getText().toString().trim()).matches()) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_email), getString(R.string.enter_email));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_email));
                    }
                }
                if (TextUtils.isEmpty(etBillName.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_name), getString(R.string.empty_error_msg));
                } else {
                    if (etBillName.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_bill_name), getString(R.string.error_name_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_bill_name));
                    }
                }
                if (TextUtils.isEmpty(etBillAddress.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_address), getString(R.string.empty_error_msg));
                } else {
                    if (etBillAddress.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_bill_address), getString(R.string.error_address_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_bill_address));
                    }
                }
                if (spBillCountry.getSelectedItemPosition() == AdapterView.INVALID_POSITION) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_country), getString(R.string.error_country));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_bill_country));
                }
                if (TextUtils.isEmpty(etBillCity.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_city), getString(R.string.empty_error_msg));
                } else {
                    if (etBillCity.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_bill_city), getString(R.string.error_city_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_bill_city));
                    }
                }
                if (TextUtils.isEmpty(etBillZipCode.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_zip_code), getString(R.string.empty_error_msg));
                } else {
                    if (etBillZipCode.getText().toString().trim().length() < 2) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_bill_zip_code), getString(R.string.error_zip_code_length));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_bill_zip_code));
                    }
                }
                if (TextUtils.isEmpty(etBillMobile.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_bill_mobile), getString(R.string.empty_error_msg));
                } else {

                    hideErrorView((TextView) findViewById(R.id.error_bill_mobile));
                }

                if (isValid) {
                    view.clearAnimation();

                    String transType = "buy_auction";
                    if (buyNow) {
                        transType = "buy_auction";
                    } else {
                        transType = "pay_for_won_auction";
                    }

                    final String[] paymentId = credit.getPaymentId();
                    String pId = paymentId[paymentMethod.getSelectedItemPosition()];
                    if (pId.equals(String.valueOf(Config.PAYMENT_GATE_CCAVENUE_ID))) {
                        //get cc payment info
                        getCCPaymentInfoTask(pId);
                    } else if (pId.equals(String.valueOf(Config.PAYMENT_GATE_PAYPAL_ID))) {
                        String postParams = "payment_type=" + pId +
                                "&transaction_type=" + transType +
                                "&ship_name=" + etName.getText().toString() +
                                "&ship_country=" + spShipCountry.getSelectedItem() +
                                "&ship_address=" + etAddress.getText().toString().trim() +
                                "&ship_address2=" + etAddress2.getText().toString().trim() +
                                "&ship_city=" + etCity.getText().toString().trim() +
                                "&ship_post_code=" + etZipCode.getText().toString().trim() +
                                "&ship_phone=" + etMobile.getText().toString().trim() +
                                "&email=" + etBillEmail.getText().toString().trim() +
                                "&name=" + etBillName.getText().toString().trim() +
                                "&auc_name=" + pName.getText().toString().trim() +
                                "&country=" + spBillCountry.getSelectedItem() +
                                "&address=" + etBillAddress.getText().toString().trim() +
                                "&address2=" + etBillAddress2.getText().toString().trim() +
                                "&city=" + etBillCity.getText().toString().trim() +
                                "&post_code=" + etBillZipCode.getText().toString().trim() +
                                "&phone=" + etBillMobile.getText().toString().trim() +
                                "&auc_win_id=" + aucWinId +
                                "&amount=" + wonAmount +
                                "&ship_cost=" + shipCost +
                                "&product_id=" + productId +
                                "&api_key=" + Api.getInstance().apiKey + "&token=" + preferenceHelper.getToken() +
                                "&user_id=" + preferenceHelper.getUserId();
                        if (buyNow) {
                            postParams = postParams + "&credit_used=" + creditUsed;
                        }

                        Intent intent = new Intent(PaymentConfirmActivity.this, PaymentActivityAll.class);
                        intent.putExtra("price", String.valueOf(wonAmount + shipCost));
                        intent.putExtra("id", String.valueOf(productId));
//                        intent.putExtra("currency", );

                        intent.putExtra("post_params", postParams);
                        intent.putExtra("is_buy_now", buyNow);
                        intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYPAL_ID);
                        startActivityForResult(intent, PAYPAL_PAYMENT_REQUEST_CODE);
                    } else if (pId.equals(String.valueOf(Config.PAYMENT_GATE_PAYTM_ID))) {

                        String postParams = "payment_type=" + pId +
                                "&transaction_type=" + transType +
                                "&ship_name=" + etName.getText().toString() +
                                "&ship_country=" + spShipCountry.getSelectedItem() +
                                "&ship_address=" + etAddress.getText().toString().trim() +
                                "&ship_address2=" + etAddress2.getText().toString().trim() +
                                "&ship_city=" + etCity.getText().toString().trim() +
                                "&ship_post_code=" + etZipCode.getText().toString().trim() +
                                "&ship_phone=" + etMobile.getText().toString().trim() +
                                "&email=" + etBillEmail.getText().toString().trim() +
                                "&name=" + etBillName.getText().toString().trim() +
                                "&auc_name=" + pName.getText().toString().trim() +
                                "&country=" + spBillCountry.getSelectedItem() +
                                "&address=" + etBillAddress.getText().toString().trim() +
                                "&address2=" + etBillAddress2.getText().toString().trim() +
                                "&city=" + etBillCity.getText().toString().trim() +
                                "&post_code=" + etBillZipCode.getText().toString().trim() +
                                "&phone=" + etBillMobile.getText().toString().trim() +
                                "&auc_win_id=" + aucWinId +
                                "&amount=" + wonAmount +
                                "&ship_cost=" + shipCost +
                                "&product_id=" + productId +
                                "&api_key=" + Api.getInstance().apiKey + "&token=" + preferenceHelper.getToken() +
                                "&user_id=" + preferenceHelper.getUserId();
                        if (buyNow) {
                            postParams = postParams + "&credit_used=" + creditUsed;
                        }

                        Intent intent = new Intent(PaymentConfirmActivity.this, PaymentActivityAll.class);
                        intent.putExtra("price", String.valueOf(wonAmount + shipCost));
                        intent.putExtra("id", String.valueOf(productId));
//                        intent.putExtra("currency", );
                        intent.putExtra("post_params", postParams);
                        intent.putExtra("is_buy_now", buyNow);
                        intent.putExtra("payment_type", Config.PAYMENT_GATE_PAYTM_ID);
                        startActivityForResult(intent, PAYPAL_PAYMENT_REQUEST_CODE);
                    }
                } else {
                    view.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(PaymentConfirmActivity.this, R.anim.shake_animation);
                    view.setAnimation(anim);
                    Toast.makeText(PaymentConfirmActivity.this, R.string.form_validate_message, Toast.LENGTH_SHORT).show();
                }
            }
        });

    }

    private void getCCPaymentInfoTask(final String paymentId) {
        final Dialog pDialog = AlertUtils.showProgressDialog(PaymentConfirmActivity.this, getString(R.string.get_cc_info));

        VolleyHelper vHelper = VolleyHelper.getInstance(PaymentConfirmActivity.this);
        HashMap<String, String> postparams = Api.getInstance().getPostParams();
        postparams.put("token", preferenceHelper.getToken());
        postparams.put("user_id", String.valueOf(preferenceHelper.getUserId()));

        vHelper.addVolleyRequestListeners(Api.getInstance().getCCPaymentInfoUrl, Request.Method.POST, postparams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        String transType = "buy_auction";
                        if (buyNow) {
                            transType = "buy_auction";
                        } else {
                            transType = "pay_for_won_auction";
                        }
                        //cc payment
                        String postParams = "payment_type=" + paymentId +
                                "&transaction_type=" + transType +
                                "&ship_name=" + etName.getText().toString() +
                                "&ship_country=" + spShipCountry.getSelectedItem() +
                                "&ship_address=" + etAddress.getText().toString().trim() +
                                "&ship_address2=" + etAddress2.getText().toString().trim() +
                                "&ship_city=" + etCity.getText().toString().trim() +
                                "&ship_post_code=" + etZipCode.getText().toString().trim() +
                                "&ship_phone=" + etMobile.getText().toString().trim() +
                                "&email=" + etBillEmail.getText().toString().trim() +
                                "&name=" + etBillName.getText().toString().trim() +
                                "&auc_name=" + pName.getText().toString().trim() +
                                "&country=" + spBillCountry.getSelectedItem() +
                                "&address=" + etBillAddress.getText().toString().trim() +
                                "&address2=" + etBillAddress2.getText().toString().trim() +
                                "&city=" + etBillCity.getText().toString().trim() +
                                "&post_code=" + etBillZipCode.getText().toString().trim() +
                                "&phone=" + etBillMobile.getText().toString().trim() +
                                "&auc_win_id=" + aucWinId +
                                "&amount=" + wonAmount +
                                "&ship_cost=" + shipCost +
                                "&product_id=" + productId +
                                "&api_key=" + Api.getInstance().apiKey + "&token=" + preferenceHelper.getToken() +
                                "&user_id=" + preferenceHelper.getUserId();

                        //insert ccPaymentInfo from response to post
                        JSONObject infoObj = res.getJSONObject("data");
                        postParams = postParams + "&tid=" + infoObj.getString("tid") +
                                "&merchant_id=" + infoObj.getString("merchant_id") +
                                "&currency=" + infoObj.getString("currency") +
                                "&redirect_url=" + infoObj.getString("redirect_url") +
                                "&cancel_url=" + infoObj.getString("cancel_url") +
                                "&order_id=" + infoObj.getString("order_id");

//                        String ccSuccessUrlInfo = infoObj.getString("redirect_url");
//                        String ccCancelUrlInfo = infoObj.getString("cancel_url");

                        Intent intent = new Intent(PaymentConfirmActivity.this, PaymentActivityAll.class);
                        intent.putExtra("price", String.valueOf(wonAmount + shipCost));
                        intent.putExtra("id", String.valueOf(productId));
//                        intent.putExtra("currency", );

                        intent.putExtra("post_params", postParams);
                        intent.putExtra("is_buy_credit", true);
                        intent.putExtra("success_url", infoObj.getString("redirect_url"));
                        intent.putExtra("cancel_url", infoObj.getString("cancel_url"));
                        intent.putExtra("payment_type", Config.PAYMENT_GATE_CCAVENUE_ID);
                        startActivityForResult(intent, PAYPAL_PAYMENT_REQUEST_CODE);
                    } else {
                        AlertUtils.showAlertMessage(PaymentConfirmActivity.this, "Error", res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e(" getCCPaymentInfoTask Json exception ", e.getMessage());
                    AlertUtils.showAlertMessage(PaymentConfirmActivity.this, "Error", "Cannot get required payment information");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMsg = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        errorMsg = PaymentConfirmActivity.this.getString(R.string.error_no_internet);
                    } else if (error instanceof ServerError) {
                        errorMsg = PaymentConfirmActivity.this.getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMsg = PaymentConfirmActivity.this.getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMsg = PaymentConfirmActivity.this.getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMsg = PaymentConfirmActivity.this.getString(R.string.error_time_out);
                    }
                }
                AlertUtils.showAlertMessage(PaymentConfirmActivity.this, "Error !!!", errorMsg);
            }
        }, "getCCPaymentInfoTask");
    }

    private void buyNowConfirmTask(long productId) {

        final ProgressDialog progressDialog = new ProgressDialog(PaymentConfirmActivity.this);
        progressDialog.setMessage(getString(R.string.payment_processing));
        progressDialog.show();
        final TextView tv_error = (TextView) findViewById(R.id.tv_error);
        assert tv_error != null;

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", preferenceHelper.getUserId() + "");
        postParams.put("token", preferenceHelper.getToken());
        postParams.put("product_id", productId + "");

        vHelper.addVolleyRequestListeners(Api.getInstance().buyNowUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        if (!isValid) {

                            creditUsed = String.valueOf(Integer.valueOf(res.getString("total_bids_placed")) * Integer.valueOf(res.getString("reward_time")));
                            //product data
                            JSONObject pObj = res.getJSONObject("auc_lists");
                            String productImg = res.getString("img_dir") + pObj.getString("image1");
                            if (!TextUtils.isEmpty(productImg)) {
                                Picasso.with(PaymentConfirmActivity.this)
                                        .load(productImg)
                                        .into(pImage);
                            }
                            pName.setText(pObj.getString("name"));
                            pDate.setText(pObj.getString("end_date"));
                            wonAmount = Double.parseDouble(pObj.getString("price"));

                            winningBid.setText(getString(R.string.price) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", wonAmount));

                            shipCost = Double.parseDouble(pObj.getString("shipping_cost"));
                            shippingCost.setText(getString(R.string.shipping_cost) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", shipCost));
                            double totalCost = wonAmount + shipCost;

                            totalSum.setText(getString(R.string.total_cost) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", totalCost));
//                            aucWinId = pObj.getString("auc_win_id");
                            wonAmount = totalCost;

                            //user data
                            //shipping data
                            JSONObject userData = res.getJSONObject("profile");
                            etName.setText(userData.getString("first_name") + " " + userData.getString("last_name"));
                            etAddress.setText(userData.getString("address"));
                            etAddress2.setText(userData.getString("address2"));
                            etCity.setText(userData.getString("city"));
                            etZipCode.setText(userData.getString("post_code"));
                            etMobile.setText(userData.getString("mobile"));
                            //billing data
                            etBillEmail.setText(userData.getString("email"));
                            etBillName.setText(userData.getString("first_name") + " " + userData.getString("last_name"));
                            etBillAddress.setText(userData.getString("address"));
                            etBillAddress2.setText(userData.getString("address2"));
                            etBillCity.setText(userData.getString("city"));
                            etBillZipCode.setText(userData.getString("post_code"));
                            etBillMobile.setText(userData.getString("mobile"));

                            //payment list
                            JSONArray payments = res.getJSONArray("payment_lists");
                            String[] paymentId = new String[payments.length()];
                            String[] paymentLogo = new String[payments.length()];
                            String[] paymentGateWay = new String[payments.length()];
                            for (int j = 0; j < payments.length(); j++) {
                                JSONObject eachPayment = payments.getJSONObject(j);

                                paymentId[j] = eachPayment.getString("id");
                                paymentLogo[j] = eachPayment.getString("payment_logo");
                                paymentGateWay[j] = eachPayment.getString("payment_gateway");
                            }
                            credit.setPaymentId(paymentId);
                            credit.setPaymentLogo(paymentLogo);
                            credit.setPaymentGateway(paymentGateWay);
                            setPaymentMethod();

                            setCountrySpinner(res.getJSONArray("countries"));

                            tv_error.setVisibility(View.GONE);
                            paymentLayout.setVisibility(View.VISIBLE);

                        } else {
                            showAlert(res.getString("message"));
                        }
                    } else {
                        if (!isValid) {
                            if (buyNow) {
                                Intent intent = new Intent();
                                intent.putExtra("buy_now_error", true);
                                intent.putExtra("buy_now_error_msg", res.getString("message"));
                                setResult(RESULT_OK, intent);
                            } else {
                                tv_error.setText(res.getString("message"));
                                tv_error.setVisibility(View.VISIBLE);
                            }
                        } else {
                            showAlert(res.getString("message"));
                            tv_error.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("buyNowConfirmTask Json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (!isValid) {
                        if (buyNow) {
                            Intent intent = new Intent();
                            intent.putExtra("buy_now_error", true);
                            intent.putExtra("buy_now_error_msg", errorObj.getString("message"));
                            setResult(RESULT_OK, intent);
                            finish();
                        } else {
                            tv_error.setText(errorObj.getString("message"));
                            tv_error.setVisibility(View.VISIBLE);
                        }
                    } else {
                        showAlert(errorObj.getString("message"));
                        tv_error.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_time_out));
                        }
                        if (!isValid) {
                            tv_error.setVisibility(View.VISIBLE);
                        } else {
                            showAlert(tv_error.getText().toString().trim());
                            tv_error.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception ee) {
                        Logger.e("buyNowConfirmTask error catch", ee.getMessage() + "");
                    }
                }
            }
        }, "buyNowConfirmTask");

    }

    private void paymentConfirmTask(final long id) {
        final ProgressDialog progressDialog = new ProgressDialog(PaymentConfirmActivity.this);
        progressDialog.setMessage(getString(R.string.payment_processing));
        progressDialog.show();
        final TextView tv_error = (TextView) findViewById(R.id.tv_error);
        assert tv_error != null;

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("user_id", preferenceHelper.getUserId() + "");
        postParams.put("token", preferenceHelper.getToken());
        postParams.put("product_id", productId + "");

        vHelper.addVolleyRequestListeners(Api.getInstance().paymentConfirmationUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        if (!isValid) {
                            //product data
                            JSONObject pObj = res.getJSONObject("auc_lists");
                            String productImg = res.getString("img_dir") + pObj.getString("image1");
                            if (!TextUtils.isEmpty(productImg)) {
                                Picasso.with(PaymentConfirmActivity.this)
                                        .load(productImg)
                                        .into(pImage);
                            }
                            pName.setText(pObj.getString("name"));
                            pDate.setText(pObj.getString("end_date"));

                            wonAmount = Double.parseDouble(pObj.getString("won_amt"));
                            winningBid.setText(getString(R.string.won_amount) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", wonAmount));

                            shipCost = Double.parseDouble(pObj.getString("shipping_cost"));
                            shippingCost.setText(getString(R.string.shipping_cost) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", shipCost));

                            double totalCost = wonAmount + shipCost;
                            totalSum.setText(getString(R.string.total_cost) + preferenceHelper.getCurrency() + " "
                                    + String.format("%.2f", totalCost));
                            aucWinId = pObj.getString("auc_win_id");

                            //user data
                            //shipping data
                            JSONObject userData = res.getJSONObject("profile");
                            etName.setText(userData.getString("first_name") + " " + userData.getString("last_name"));
                            etAddress.setText(userData.getString("address"));
                            etAddress2.setText(userData.getString("address2"));
                            etCity.setText(userData.getString("city"));
                            etZipCode.setText(userData.getString("post_code"));
                            etMobile.setText(userData.getString("mobile"));
                            //billing data
                            etBillEmail.setText(userData.getString("email"));
                            etBillName.setText(userData.getString("first_name") + " " + userData.getString("last_name"));
                            etBillAddress.setText(userData.getString("address"));
                            etBillAddress2.setText(userData.getString("address2"));
                            etBillCity.setText(userData.getString("city"));
                            etBillZipCode.setText(userData.getString("post_code"));
                            etBillMobile.setText(userData.getString("mobile"));

                            //payment list
                            JSONArray payments = res.getJSONArray("payment_lists");
                            String[] paymentId = new String[payments.length()];
                            String[] paymentLogo = new String[payments.length()];
                            String[] paymentGateWay = new String[payments.length()];
                            for (int j = 0; j < payments.length(); j++) {
                                JSONObject eachPayment = payments.getJSONObject(j);

                                paymentId[j] = eachPayment.getString("id");
                                paymentLogo[j] = eachPayment.getString("payment_logo");
                                paymentGateWay[j] = eachPayment.getString("payment_gateway");
                            }
                            credit.setPaymentId(paymentId);
                            credit.setPaymentLogo(paymentLogo);
                            credit.setPaymentGateway(paymentGateWay);
                            setPaymentMethod();

                            setCountrySpinner(res.getJSONArray("countries"));

                            tv_error.setVisibility(View.GONE);
                            paymentLayout.setVisibility(View.VISIBLE);

                        } else {
                            showAlert(res.getString("message"));
                        }
                    } else {
                        if (!isValid) {
                            tv_error.setText(res.getString("message"));
                            tv_error.setVisibility(View.VISIBLE);
                        } else {
                            showAlert(res.getString("message"));
                            tv_error.setVisibility(View.GONE);
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("paymentConfirmTask Json ex", e.getMessage() + "");
                }
                progressDialog.dismiss();
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (!isValid) {
                        tv_error.setText(errorObj.getString("message"));
                        tv_error.setVisibility(View.VISIBLE);
                    } else {
                        showAlert(errorObj.getString("message"));
                        tv_error.setVisibility(View.GONE);
                    }

                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            tv_error.setText(PaymentConfirmActivity.this.getString(R.string.error_time_out));
                        }
                        if (!isValid) {
                            tv_error.setVisibility(View.VISIBLE);
                        } else {
                            showAlert(tv_error.getText().toString().trim());
                            tv_error.setVisibility(View.VISIBLE);
                        }

                    } catch (Exception ee) {
                        Logger.e("paymentConfirmTask error catch", ee.getMessage() + "");
                    }
                }
            }
        }, "paymentConfirmTask");
    }

    private void setCountrySpinner(JSONArray countries) {
        try {
            JSONObject res = new JSONObject();
            res.put("status", true);
            res.put("message", "Country List get success");
            res.put("countries", countries);
            Date date2 = null;
            if (res.getBoolean("status")) {
                JSONArray countryarray = res.getJSONArray("countries");

                for (int i = 0; i < countryarray.length(); i++) {
                    JSONObject countryname = countryarray.getJSONObject(i);
                    countryName.add(countryname.getString("country"));
                    countryId.add(countryname.getString("id"));

                    if (countryname.getString("default_country").equalsIgnoreCase("Yes")) {
                        preferenceHelper.edit().putString(PreferenceHelper.SITE_CURR_CODE, countryname.getString("currency_code")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.SITE_CURR_SIGN, countryname.getString("currency_sign")).commit();
                        preferenceHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, countryname.getString("short_code")).commit();
                    }

                    String lastupdate = countryname.getString("last_update");

                    SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
                    Date date1 = formatter.parse(lastupdate);
                    String preflastUpdate = preferenceHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");

                    if (TextUtils.isEmpty(preflastUpdate)) {
                        preferenceHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastupdate).commit();
                    } else {
                        date2 = formatter.parse(preflastUpdate);
                        if (date2.after(date1)) {
                            preferenceHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastupdate).commit();
                        }
                    }
                }
                writeToFile(res.toString(), PaymentConfirmActivity.this);
            }

        } catch (Exception e) {//
            Logger.e("getCountryTask  Ex", e.getMessage() + " ");
        }

        ArrayAdapter<String> countryAdapter = new ArrayAdapter<>(PaymentConfirmActivity.this,
                android.R.layout.simple_spinner_dropdown_item, countryName);
        spShipCountry.setAdapter(countryAdapter);
        spBillCountry.setAdapter(countryAdapter);
    }

    private void setPaymentMethod() {
        final String[] paymentGateWay = credit.getPaymentGateway();
        final String[] paymentLogo = credit.getPaymentLogo();
        paymentMethod.setAdapter(new ArrayAdapter<String>(this,
                R.layout.sp_item_payment_options, R.id.payment_method_name, paymentGateWay) {

            @Override
            public View getView(int position, View convertView, ViewGroup parent) {
                View view = LayoutInflater.from(PaymentConfirmActivity.this).inflate(R.layout.sp_item_payment_options, parent, false);
                ImageView logo = (ImageView) view.findViewById(R.id.payment_method_logo);
                logo.setVisibility(View.VISIBLE);
                TextView paymentName = (TextView) view.findViewById(R.id.payment_method_name);

                paymentName.setText(paymentGateWay[position]);
                Picasso.with(PaymentConfirmActivity.this).load(paymentLogo[position]).into(logo);
                return view;
            }

            @Override
            public View getDropDownView(int position, View convertView, ViewGroup parent) {
                View view = LayoutInflater.from(PaymentConfirmActivity.this).inflate(R.layout.sp_item_payment_options, parent, false);
                ImageView logo = (ImageView) view.findViewById(R.id.payment_method_logo);
                logo.setVisibility(View.VISIBLE);
                TextView paymentName = (TextView) view.findViewById(R.id.payment_method_name);

                paymentName.setText(paymentGateWay[position]);
                Picasso.with(PaymentConfirmActivity.this).load(paymentLogo[position]).into(logo);
                return view;
            }
        });
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("paymentConfirmTask");
        VolleyHelper.getInstance(this).cancelRequest("buyNowConfirmTask");
        super.onDestroy();
    }

    public void showErrorView(TextView errorView, String message) {
        errorView.setText(message);
        errorView.setVisibility(View.VISIBLE);
    }

    public void hideErrorView(TextView errorView) {
        errorView.setVisibility(View.GONE);
    }

    private void showAlert(String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentConfirmActivity.this);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        builder.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Logger.e("payment confirm result", requestCode + " " + resultCode + " " + data);
        if (resultCode == Activity.RESULT_OK && requestCode == PAYPAL_PAYMENT_REQUEST_CODE) {
            Logger.e("payment confirm result", "go back " + data.getBooleanExtra("paid", false));
            if (data.getBooleanExtra("paid", false)) {
                setResult(RESULT_OK, data);
            }
            PaymentConfirmActivity.this.finish();
        }
    }


    private void writeToFile(String response, Context context) {
        try {
            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("countrylist.txt", Context.MODE_PRIVATE));
            outputStreamWriter.write(response);
            outputStreamWriter.close();
        } catch (IOException e) {
            Log.e("Exception", "File write failed: " + e.toString());
        }

    }
}
