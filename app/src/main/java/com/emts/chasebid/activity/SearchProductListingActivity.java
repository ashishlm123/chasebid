package com.emts.chasebid.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.Nullable;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.LiveProductAdapter;
import com.emts.chasebid.customviews.EndlessRecyclerViewScrollListener;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

/**
 * Created by shreedhar on 7/11/2016.
 */
public class SearchProductListingActivity extends AppCompatActivity {
    public static final int SEARCH_lISTING_ACTIVITY_REQUEST_CODE = 457;

    PreferenceHelper helper;
    ArrayList<Product> pList = new ArrayList<>();

    RecyclerView pListRecyclerView;
    ProgressBar  progressInfinite;
//    ProgressBar progressBar;
    FrameLayout progressBar;
    TextView errorText;
    SwipeRefreshLayout listRefresh;
    LiveProductAdapter adapter;
    LinearLayoutManager layoutManager;

    long serverTime;
    Handler mainHandler;
    ScheduledExecutorService service;

    int limit = Config.LIMIT;
    int offset = 0;
    EndlessRecyclerViewScrollListener infiniteScrollListener;
    boolean fromSearch, fromCategory, fromMoreLive;
    int catId;

    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;


    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_results);

        helper = PreferenceHelper.getInstance(this);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        TextView toolbarTitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_title);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        if (helper.isLogin()) {
            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + helper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + helper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            holderUserPoints.setVisibility(View.GONE);
        }

        pListRecyclerView = (RecyclerView) findViewById(R.id.search_list_recycler);
        layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        pListRecyclerView.setLayoutManager(layoutManager);

        pListRecyclerView.setItemAnimator(null);

//        progressBar = (ProgressBar) findViewById(R.id.progress);
        progressBar = (FrameLayout) findViewById(R.id.progress);
        progressInfinite = (ProgressBar) findViewById(R.id.progress_infinite);
        errorText = (TextView) findViewById(R.id.tv_error_message);

        adapter = new LiveProductAdapter(this, pList);
        adapter.setFillParent(true);
        pListRecyclerView.setAdapter(adapter);
        pListRecyclerView.setVisibility(View.VISIBLE);

        infiniteScrollListener = new EndlessRecyclerViewScrollListener(layoutManager) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                if (pList.size() >= limit) {
                    if (fromCategory) {
                        searchCategoryTask(catId, offset);
                    } else if (fromMoreLive) {
                        PListingTask(offset);
                    }
                }
            }
        };
        pListRecyclerView.addOnScrollListener(infiniteScrollListener);


        adapter.setOnRecyclerViewItemClickListener(new LiveProductAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClicked(View view, int position, long id) {
                Intent intent = new Intent(SearchProductListingActivity.this, ProductDetail.class);
                intent.putExtra("product_id", id);
                intent.putExtra("serverTime", serverTime);
                intent.putExtra("position", position);
                intent.putExtra("product", pList.get(position));
                startActivityForResult(intent, SEARCH_lISTING_ACTIVITY_REQUEST_CODE);
            }
        });

        listRefresh = (SwipeRefreshLayout) findViewById(R.id.swipe_list);
        listRefresh.setColorSchemeResources(android.R.color.holo_green_dark,
                android.R.color.holo_green_light,
                android.R.color.holo_orange_light,
                android.R.color.holo_red_light);
        listRefresh.setProgressViewOffset(false, (int) Utils.pxFromDp(SearchProductListingActivity.this, 30),
                (int) Utils.pxFromDp(SearchProductListingActivity.this, 50));
        listRefresh.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    offset = 0;
                    pList.clear();
                    if (fromCategory) {
                        searchCategoryTask(catId, offset);
                    } else if (fromMoreLive) {
                        PListingTask(offset);
                    }
                } else {
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                    listRefresh.setRefreshing(false);
                }
            }
        });

        serverTime = new Date().getTime();
        mainHandler = new Handler(Looper.getMainLooper());
        service = Executors.newSingleThreadScheduledExecutor();
        service.scheduleAtFixedRate(runnable,
                0, 1000, TimeUnit.MILLISECONDS);

        Intent intent = getIntent();
        if (intent != null) {
            if (intent.getBooleanExtra("fromSearchBar", false)) {
                fromSearch = true;
                String searchKeyWord = intent.getStringExtra("searchKeyWord");
                toolbarTitle.setText(searchKeyWord);
            } else if (intent.getBooleanExtra("fromCategory", false)) {
                fromCategory = true;
                String catName = intent.getStringExtra("childName");
                toolbarTitle.setText(catName);
                int childId = intent.getIntExtra("childId", -1);
                catId = intent.getIntExtra("ParentId", -1);

                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    searchCategoryTask(catId, offset);
                } else {
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }
            } else if (intent.getBooleanExtra("moreLive", false)) {
                fromMoreLive = true;
                toolbarTitle.setText(getString(R.string.title_live_auc));

                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    PListingTask(offset);
                } else {
                    errorText.setText(getString(R.string.error_no_internet));
                    errorText.setVisibility(View.VISIBLE);
                }
            }
        }
    }

    private void searchCategoryTask(int catId, final int offsetValue) {
        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("category_id", catId + "");
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offsetValue));
        if (helper.isLogin()) {
            postParams.put("user_id", String.valueOf(helper.getUserId()));
            postParams.put("token", helper.getToken());
        }

        vHelper.addVolleyRequestListeners(Api.getInstance().searchUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                Logger.printLongLog("response", response);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(res.getString("server_time"));
                        serverTime = serverDate.getTime();

                        JSONArray pArray = res.getJSONArray("search_data");
                        if (pArray.length() > 0) {
                            if (offset == 0) {
                                pList.clear();
                            }
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                Product product = new Product();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

                                product.setFavourite(eachObj.getBoolean("watch"));
                                product.setMinBid(Float.parseFloat(eachObj.getString("min_bids_value")));

                                pList.add(product);
                            }
                            Logger.e("size", String.valueOf(pList.size()));
                            Date serverTime = sdf.parse(res.getString("server_time"));
                            SearchProductListingActivity.this.serverTime = serverTime.getTime();
                            adapter.setServerTime(SearchProductListingActivity.this.serverTime);

                            offset = offset + limit;
                            pListRecyclerView.setVisibility(View.VISIBLE);
                            adapter.notifyDataSetChanged();
                        } else {
                            if (offset == 0) {
                                errorText.setVisibility(View.VISIBLE);
                                pListRecyclerView.setVisibility(View.GONE);
                                errorText.setText(R.string.no_live_auc);
                            } else {
                                pListRecyclerView.setVisibility(View.VISIBLE);
                                AlertUtils.showSnack(SearchProductListingActivity.this, errorText, getString(R.string.no_more_prod));
                            }
                        }
                    } else {
                        if (offset == 0) {
                            pListRecyclerView.setVisibility(View.GONE);
                            errorText.setText(res.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            pListRecyclerView.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(SearchProductListingActivity.this, errorText, res.getString("message"));
                        }
                    }
                } catch (JSONException e) {
                    Logger.e("searchCategoryTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("searchCategoryTask ex", e.getMessage() + " 000");
                }

                progressInfinite.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage = errorObj.getString("message");

                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        errorMessage = getApplicationContext().getString(R.string.error_no_internet);

                    } else if (error instanceof ServerError) {
                        errorMessage = getApplicationContext().getString(R.string.error_server);
                    } else if (error instanceof AuthFailureError) {
                        errorMessage = getApplicationContext().getString(R.string.error_authFailureError);
                    } else if (error instanceof ParseError) {
                        errorMessage = getApplicationContext().getString(R.string.error_parse_error);
                    } else if (error instanceof TimeoutError) {
                        errorMessage = getApplicationContext().getString(R.string.error_time_out);
                    }
                }
                if (offset == 0) {
                    errorText.setText(errorMessage);
                    errorText.setVisibility(View.VISIBLE);
                    pListRecyclerView.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(SearchProductListingActivity.this, errorText, errorMessage);
                }
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("searchFromCategoryTask exception", e.getMessage());
                }
                listRefresh.setRefreshing(false);
                pListRecyclerView.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
            }
        }, "searchCategoryTask");
    }

    public void PListingTask(final int offsetValue) {

        if (offset == 0) {
            progressBar.setVisibility(View.VISIBLE);
        } else {
            progressInfinite.setVisibility(View.VISIBLE);
        }

        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offsetValue));
        postMap.put("limit", String.valueOf(limit));
        if (helper.isLogin()) {
            postMap.put("user_id", String.valueOf(helper.getUserId()));
            postMap.put("token", helper.getToken());

        }
        errorText.setVisibility(View.GONE);
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        vHelper.addVolleyRequestListeners(Api.getInstance().getLiveAuction, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(resObj.getString("server_time"));
                        serverTime = serverDate.getTime();

                        JSONArray pArray = resObj.getJSONArray("live_auctions");
                        if (pArray.length() > 0) {
                            if (offset == 0) {
                                pList.clear();
                            }
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);
                                Product product = new Product();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

                                product.setFavourite(eachObj.getBoolean("watch"));
                                product.setMinBid(Float.parseFloat(eachObj.getString("min_bids_value")));

                                pList.add(product);
                            }
                            offset = offset + limit;
                            pListRecyclerView.setVisibility(View.VISIBLE);
                            adapter.setServerTime(serverTime);
                            adapter.notifyDataSetChanged();
                            Logger.e("offset", String.valueOf(offset));
                        } else {
                            if (offset == 0) {
                                errorText.setVisibility(View.VISIBLE);
                                pListRecyclerView.setVisibility(View.GONE);
                                errorText.setText(R.string.no_live_auc);
                            } else {
                                pListRecyclerView.setVisibility(View.VISIBLE);
                                AlertUtils.showSnack(SearchProductListingActivity.this, errorText, getString(R.string.no_more_prod));
                            }
//                            infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                        }
                    } else {
                        if (offset == 0) {
                            pListRecyclerView.setVisibility(View.GONE);
                            errorText.setText(resObj.getString("message"));
                            errorText.setVisibility(View.VISIBLE);
                        } else {
                            pListRecyclerView.setVisibility(View.VISIBLE);
                            AlertUtils.showSnack(SearchProductListingActivity.this, errorText, resObj.getString("message"));
                        }
                    }

                } catch (JSONException e) {
                    Logger.e("PListingTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("PListingTask ParseException ex", e.getMessage() + "");
                }

                progressInfinite.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMessage = getString(R.string.unexpected_error);
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    errorMessage = errorObj.getString("message");

                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorMessage = getApplicationContext().getString(R.string.error_no_internet);
                        } else if (error instanceof ServerError) {
                            errorMessage = getApplicationContext().getString(R.string.error_server);
                        } else if (error instanceof AuthFailureError) {
                            errorMessage = getApplicationContext().getString(R.string.error_authFailureError);
                        } else if (error instanceof ParseError) {
                            errorMessage = getApplicationContext().getString(R.string.error_parse_error);
                        } else if (error instanceof TimeoutError) {
                            errorMessage = getApplicationContext().getString(R.string.error_time_out);
                        }
                    } catch (Exception ee) {
                    }
                }

                if (offset == 0) {
                    errorText.setText(errorMessage);
                    errorText.setVisibility(View.VISIBLE);
                    pListRecyclerView.setVisibility(View.GONE);
                } else {
                    AlertUtils.showSnack(SearchProductListingActivity.this, errorText, errorMessage);
                }

                progressInfinite.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                listRefresh.setRefreshing(false);
                try {
                    infiniteScrollListener.previousTotalItemCount = infiniteScrollListener.previousTotalItemCount - 1;
                } catch (Exception e) {
                    Logger.e("searchFromCategoryTask exception", e.getMessage());
                }
            }
        }, "PListingTask");

    }

    @Override
    public void onResume() {
        super.onResume();
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            final int firstVisible = layoutManager.findFirstVisibleItemPosition();
            final int lastVisible = layoutManager.findLastVisibleItemPosition();

            mainHandler.post(new Runnable() {
                @Override
                public void run() {
                    serverTime = serverTime + 1000;
                    adapter.setServerTime(serverTime);

                    for (int i = firstVisible; i <= lastVisible; i++) {
                        try {
                            adapter.notifyItemChanged(i);
                        } catch (Exception e) {
                        }
                    }
                }
            });
        }
    };


    @Override
    public void onBackPressed() {
        service.shutdown();
        VolleyHelper.getInstance(this).cancelRequest("searchCategoryTask");
        super.onBackPressed();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        service.shutdown();
        VolleyHelper.getInstance(this).cancelRequest("searchCategoryTask");
        VolleyHelper.getInstance(this).cancelRequest("PListingTask");
        super.onDestroy();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == SEARCH_lISTING_ACTIVITY_REQUEST_CODE) {

            if (data.hasExtra("product")) {
                int position = data.getIntExtra("position", -1);
                Product product = (Product) data.getSerializableExtra("product");
                pList.get(position).setFavourite(product.isFavourite());
            }
            adapter.notifyDataSetChanged();
        }
    }

}
