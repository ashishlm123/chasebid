package com.emts.chasebid.activity;


import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.JavascriptInterface;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by theone on 7/12/2016.
 */
public class PaymentActivityAll extends AppCompatActivity {
    WebView webView;
    ProgressDialog progressDialog;

    String successUri;
    String cancelUri;
    boolean isBuyBidCredit = false;
    boolean buyNow = false;

    PreferenceHelper prefsHelper;
    int paymentGatewayType;
    ProgressBar loadingProgress;

    String trackPrice = "0", trackId = "0", trackCurrency = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_payment_paypal);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_payment);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        loadingProgress = (ProgressBar) findViewById(R.id.page_loading);

        progressDialog = new ProgressDialog(PaymentActivityAll.this);
        progressDialog.setCancelable(true);
        progressDialog.setCanceledOnTouchOutside(false);
        progressDialog.setMessage(getString(R.string.loading));
        progressDialog.show();

        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());

        webView = (WebView) findViewById(R.id.payment_webview);

        WebSettings webSettings = webView.getSettings();
        webSettings.setLoadsImagesAutomatically(true);
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setSupportZoom(true);
        webSettings.setBuiltInZoomControls(true);
        webSettings.setDisplayZoomControls(false);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setUseWideViewPort(true);
        webSettings.setJavaScriptEnabled(true);

        // Register a new JavaScript interface called HTMLOUT /
        webView.addJavascriptInterface(new MyJavaScriptInterface(), "HTMLOUT");

        webView.setWebViewClient(new WebViewClientPaymentGateway());
        webView.setWebChromeClient(new WebChromeClient());

        CookieSyncManager.createInstance(PaymentActivityAll.this);
        CookieSyncManager.getInstance().sync();

        CookieManager.getInstance().removeAllCookie();
        webView.clearCache(true);

        String postParams = "";
        Intent intent = getIntent();
        if (intent == null) {
            AlertUtils.showToast(this, "Give me intent");
            return;
        }
        trackPrice = intent.getStringExtra("price");
        trackId = intent.getStringExtra("id");
        trackCurrency = intent.getStringExtra("currency");

        postParams = intent.getStringExtra("post_params");
        isBuyBidCredit = intent.getBooleanExtra("is_buy_credit", false);
        buyNow = intent.getBooleanExtra("is_buy_now", false);
        Logger.e("Post Params:", postParams);
        paymentGatewayType = intent.getIntExtra("payment_type", 0);
        if (paymentGatewayType < 1) {
            AlertUtils.showToast(this, "Give me payment type");
            return;
        }

        String url = "";
        if (paymentGatewayType == Config.PAYMENT_GATE_CCAVENUE_ID) {        //cc avenue payment gateway
            successUri = intent.getStringExtra("success_url");
            cancelUri = intent.getStringExtra("cancel_url");
            if (isBuyBidCredit) {
                url = Api.getInstance().ccPaymentUrl;
            } else if (buyNow) {
                url = Api.getInstance().buyNowUrl;
            } else {
                url = Api.getInstance().paymentConfirmationUrl;
            }
            webView.postUrl(url, postParams.getBytes());
        } else if (paymentGatewayType == Config.PAYMENT_GATE_PAYPAL_ID) {   //paypal payment gateway
            successUri = Api.getInstance().wonProductSuccessUrl;
            cancelUri = Api.getInstance().wonProductCancelUrl;
            if (isBuyBidCredit) {
                url = Api.getInstance().buyBidCreditPaymentUrl;
            } else if (buyNow) {
                url = Api.getInstance().buyNowUrl;
            } else {
                url = Api.getInstance().paymentConfirmationUrl;
            }
            webView.postUrl(url, postParams.getBytes());
        } else if (paymentGatewayType == Config.PAYMENT_GATE_PAYTM_ID) {    //paytm payment gateway
            successUri = Api.getInstance().payTmCallBackUrl;
            cancelUri = "********this is not used***************";
            if (isBuyBidCredit) {
                url = Api.getInstance().paytmBuyBidCredits;
            } else if (buyNow) {
                url = Api.getInstance().buyNowUrl;
            } else {
                url = Api.getInstance().paymentConfirmationUrl;
            }
            webView.postUrl(url, postParams.getBytes());
        }
    }

    @Override
    public void onBackPressed() {
        if (webView != null) {
            if (webView.canGoBack()) {
                webView.goBack();
            } else {
                super.onBackPressed();
            }
        } else {
            super.onBackPressed();
        }
    }

    private void removeSessionCookies() {
        CookieManager.getInstance().removeSessionCookie();
        CookieSyncManager.getInstance().sync();
    }

    private class WebViewClientPaymentGateway
            extends WebViewClient {
        WebViewClientPaymentGateway() {
        }

        public void onPageFinished(WebView paramWebView, String paramString) {
            Logger.e("onPageFinished", paramString + "");
            progressDialog.dismiss();
            if (loadingProgress.getVisibility() == View.VISIBLE) {
                loadingProgress.setVisibility(View.GONE);
            }

            // This call inject JavaScript into the page which just finished loading.
//            if (isBuyBidCredit && paramString.contains(Api.getInstance().getIpGeneral())) {
//                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
//                Logger.e("");
//            }
            if (paymentGatewayType == Config.PAYMENT_GATE_CCAVENUE_ID && paramString.contains(successUri)) {
                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                Logger.e("inject js CCAVENEU", "js injected success for ccaveneu");
            }
            if (paymentGatewayType == Config.PAYMENT_GATE_PAYTM_ID && paramString.contains(successUri)) {
                webView.loadUrl("javascript:window.HTMLOUT.processHTML('<html>'+document.getElementsByTagName('html')[0].innerHTML+'</html>');");
                Logger.e("inject js PAYTM", "js injected success for paytm");
            }
        }

        public void onPageStarted(WebView paramWebView, String paramString, Bitmap paramBitmap) {
            Logger.e("onPageStarted", paramString);
            if (loadingProgress.getVisibility() == View.GONE) {
                loadingProgress.setVisibility(View.VISIBLE);
            }
            if (paramString.contains("")) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        try {
                            progressDialog.dismiss();
                        } catch (Exception e) {
                        }
                    }
                }, 4000);

            }
            if (paramString.contains(successUri)) {
                progressDialog.dismiss();
                webView.setVisibility(View.GONE);
            } else if (paramString.contains(cancelUri)) {
                progressDialog.dismiss();
                webView.setVisibility(View.GONE);
                showAlert(getString(R.string.payment_cancel), false);
            }
            super.onPageStarted(paramWebView, paramString, paramBitmap);
        }

        public void onReceivedError(WebView paramWebView, int paramInt, String paramString1, String paramString2) {
            Logger.e("Error", "Any Error?");
        }
    }

    @Override
    protected void onPause() {
        CookieSyncManager.getInstance().stopSync();
        super.onPause();
    }

    protected void onResume() {
        CookieSyncManager.getInstance().startSync();
        super.onResume();
    }

    private void showAlert(String message, final boolean ok) {
        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivityAll.this);
        builder.setCancelable(false);
        builder.setMessage(message);
        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
                if (ok) {
                    Intent returnIntent = new Intent();
                    returnIntent.putExtra("paid", true);
                    setResult(RESULT_OK, returnIntent);
                }
                finish();
            }
        });
        builder.show();
    }

    private class MyJavaScriptInterface {
        @SuppressWarnings("unused")
        @JavascriptInterface
        public void processHTML(String html) {
            // process the html as needed by the app
            Logger.e("full html content", html + "");

            if (paymentGatewayType == Config.PAYMENT_GATE_CCAVENUE_ID ||
                    paymentGatewayType == Config.PAYMENT_GATE_PAYTM_ID) {
                String jsonOnly = html.substring(html.indexOf("<body>") + 6, html.indexOf("</body>"));
                Logger.e("json only", jsonOnly);

                try {
                    final JSONObject resObj = new JSONObject(jsonOnly);
                    parseSuccessCancelJson(resObj);
                } catch (JSONException ex) {
                    Logger.e("json only ex", ex.getMessage() + "");
                }

            } else {
                //backup one
                String jsonOnly = html.substring(html.indexOf("{"), html.lastIndexOf("}"));
                Logger.e("json only for catch one", jsonOnly);

                try {
                    final JSONObject resObj = new JSONObject(jsonOnly);
                    parseSuccessCancelJson(resObj);
                } catch (JSONException ex) {
                    Logger.e("json only ex 2 time", ex.getMessage() + "");
                }
            }
        }
    }

    boolean success = false;
    boolean isFailed = false;

    public void parseSuccessCancelJson(final JSONObject resObj) {
        try {
            if (paymentGatewayType == Config.PAYMENT_GATE_CCAVENUE_ID) {
                //there is seperate cancel url so check for success or failed
                success = resObj.getBoolean("status");
            } else if (paymentGatewayType == Config.PAYMENT_GATE_PAYTM_ID) {
                success = resObj.getBoolean("status") && resObj.getString("data").equalsIgnoreCase("Success");
                isFailed = !resObj.getBoolean("status") && resObj.getString("data").equalsIgnoreCase("fail");
            }

//                final boolean success = resObj.getString("data").equalsIgnoreCase("Success");
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    try {
                        webView.setVisibility(View.GONE);
                        AlertDialog.Builder builder = new AlertDialog.Builder(PaymentActivityAll.this);
                        builder.setCancelable(false);
                        if (isFailed) {
                            builder.setMessage("Payment Failed\nRedirect to ChaseBid App??");
                        } else {
                            if (success) {
                                builder.setMessage("Payment Successful\nRedirect to ChaseBid App??");
                            } else {
                                builder.setMessage("Payment Failed\nRedirect to ChaseBid App??");
                            }
                        }
                        builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (success) {
                                    Intent intent = new Intent();
                                    intent.putExtra("paid", true);
                                    setResult(RESULT_OK, intent);
                                    Logger.e("set result", "payment success set result ok");

                                    new Handler().postDelayed(new Runnable() {
                                        @Override
                                        public void run() {
                                            Api.getInstance().getUserDetailTask(PaymentActivityAll.this, null);
                                            try {
                                                MainActivity act = new MainActivity();
                                                act.runMarketingScript("<script src=\"https://www.s2d6.com/js/globalpixel.js?x=sp&a=2021&h=70888&o=payment-" +
                                                        prefsHelper.getString(PreferenceHelper.APP_USER_EMAIL, "N/A")
                                                        + "&g=payment&s=0.00&q=1\"></script>");
                                            } catch (Exception e) {
                                            }
                                        }
                                    }, 500);
                                }
                                PaymentActivityAll.this.finish();
                            }
                        });
                        builder.show();

//                        //appflyer tracker
                        if (success) {
                            if (isBuyBidCredit) {
                                ChaseBidApp.getInstance().afTrackPurchaseEvent(Double.parseDouble(trackPrice),
                                        "buy_bid_credits", trackId, trackCurrency);
                            } else if (buyNow) {
                                ChaseBidApp.getInstance().afTrackPurchaseEvent(Double.parseDouble(trackPrice),
                                        "buy_now", trackId, trackCurrency);
                            } else {
                                ChaseBidApp.getInstance().afTrackPurchaseEvent(Double.parseDouble(trackPrice),
                                        "won_product_payment", trackId, trackCurrency);
                            }
                        }
                    } catch (Exception e) {
                        Logger.e("ex label 23432", e.getMessage() + "");
                    }
                }
            });
        } catch (JSONException e) {
            Logger.e("json parse ex", e.getMessage() + " ");
        }
    }
}
