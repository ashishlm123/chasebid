package com.emts.chasebid.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceRequest;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.emts.chasebid.R;
import com.emts.chasebid.UserDetailGet;
import com.emts.chasebid.adapters.FavListAdapter;
import com.emts.chasebid.adapters.OngoingAuctionAdapter;
import com.emts.chasebid.fragment.AboutApp;
import com.emts.chasebid.fragment.ContactUs;
import com.emts.chasebid.fragment.Home;
import com.emts.chasebid.fragment.MakeWish;
import com.emts.chasebid.fragment.RecentClosedFrag;
import com.emts.chasebid.fragment.TvShowFragment;
import com.emts.chasebid.fragment.account.BuyBidCredits;
import com.emts.chasebid.fragment.account.DeleteAccount;
import com.emts.chasebid.fragment.account.Favourites;
import com.emts.chasebid.fragment.account.OnGoingAuction;
import com.emts.chasebid.fragment.account.OrderedAuctions;
import com.emts.chasebid.fragment.account.Profile;
import com.emts.chasebid.fragment.account.PurchaseHistory;
import com.emts.chasebid.fragment.account.RedeemBonusPoint;
import com.emts.chasebid.fragment.account.ReferFriend;
import com.emts.chasebid.fragment.account.WonAuctions;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.ImageHelper;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;

import java.lang.reflect.Method;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, FragmentManager.OnBackStackChangedListener {
    DrawerLayout drawer;
    FragmentManager fm;
    Toolbar toolbar;
    PreferenceHelper prefsHelper;
    TextView toolbarTitle;
    public static final int START_ACCOUNT_ACTIVITY = 567;
    ImageView profileImage;
    NavigationView navigationView;
    boolean isLogin;
    View views;
    MenuItem mPreviousMenuItem;
    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;

    public static boolean isProfileLoaded = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                Logger.e("ex 1093", e.getMessage() + "");
            }
        }
        prefsHelper = PreferenceHelper.getInstance(getApplicationContext());
        if (prefsHelper.getBoolean(PreferenceHelper.FIRST_TIME_LOGIN, true) && !prefsHelper.isLogin()) {
            startActivity(new Intent(getApplicationContext(), LoginActivity.class));
            prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_LOGIN, false).commit();
        }

        setContentView(R.layout.activity_main);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbarTitle = (TextView) toolbar.findViewById(R.id.custom_toolbar_title);
        toolbar.inflateMenu(R.menu.main);
        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                if (item.getItemId() == R.id.menu_search) {
                    startActivity(new Intent(getApplicationContext(), SearchActivity.class));
                }
                return false;
            }
        });
        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        fm = getSupportFragmentManager();

        if (prefsHelper.isLogin()) {
            isLogin = true;
        }

        drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                AlertUtils.hideInputMethod(MainActivity.this, toolbar);
                super.onDrawerOpened(drawerView);
            }

        };
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = (NavigationView) findViewById(R.id.nav_view);
        if (prefsHelper.isLogin()) {
            navigationView.inflateMenu(R.menu.activity_main_drawer_logged);
        } else {
            navigationView.inflateMenu(R.menu.activity_main_drawer);
        }

        views = navigationView.getHeaderView(0);

        setHeaderData();

        views.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //checkLogin
                if (!isLogin) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                } else {
                    fm.beginTransaction().replace(R.id.content_frame, new Profile(), " Profile")
                            .addToBackStack(null)
                            .commit();
                    drawer.closeDrawer(GravityCompat.START);
                    setTitle();
                }
            }
        });
        navigationView.setNavigationItemSelectedListener(this);

        if (savedInstanceState == null) {
            if (prefsHelper.getBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true) && prefsHelper.isLogin()) {
                fm.beginTransaction().add(R.id.content_frame, new Profile(), " Profile")
                        .commit();
                //put to prefs in profile page after showing first time upload image alert
            } else {
                setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
                fm.beginTransaction().add(R.id.content_frame, new Home(), " Home")
                        .commit();
            }
        }
        setTitle();

        //for marketing script
        if (!TextUtils.isEmpty(getIntent().getStringExtra("marketingScript"))) {
            Logger.e("marketing Script ", getIntent().getStringExtra("marketingScript") + "");
            runMarketingScript(getIntent().getStringExtra("marketingScript"));
        }

    }

    public void setTitle() {
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof Home) {
                    toolbarTitle.setText(R.string.title_home);
                } else if (fragment instanceof Profile) {
                    toolbarTitle.setText(R.string.title_edit_profile);
                } else if (fragment instanceof BuyBidCredits) {
                    toolbarTitle.setText(R.string.title_buy_bid_credits);
                } else if (fragment instanceof TvShowFragment) {
                    toolbarTitle.setText(R.string.title_tv_show);
                } else if (fragment instanceof ReferFriend) {
                    toolbarTitle.setText(R.string.title_refer_friend);
                } else if (fragment instanceof MakeWish) {
                    toolbarTitle.setText(R.string.title_make_wish);
                } else if (fragment instanceof DeleteAccount) {
                    toolbarTitle.setText(R.string.title_del_account);
                } else if (fragment instanceof ContactUs) {
                    toolbarTitle.setText(R.string.title_contact_us);
                } else if (fragment instanceof OnGoingAuction) {
                    toolbarTitle.setText(R.string.title_on_going_auc);
                } else if (fragment instanceof RecentClosedFrag) {
                    toolbarTitle.setText(R.string.title_closed_auc);
                } else if (fragment instanceof WonAuctions) {
                    toolbarTitle.setText(R.string.title_won_auc);
                } else if (fragment instanceof OrderedAuctions) {
                    toolbarTitle.setText(R.string.title_order_auc);
                } else if (fragment instanceof Favourites) {
                    toolbarTitle.setText(R.string.title_fav);
                } else if (fragment instanceof PurchaseHistory) {
                    toolbarTitle.setText(R.string.title_purchase_history);
                } else if (fragment instanceof RedeemBonusPoint) {
                    toolbarTitle.setText(R.string.title_redeem_bonus_point);
                } else if (fragment instanceof AboutApp) {
                    Bundle bundle = fragment.getArguments();
                    if (bundle != null) {
                        if (bundle.getBoolean("aboutUs")) {
                            toolbarTitle.setText(R.string.title_about_us);
                        } else if (bundle.getBoolean("faq")) {
                            toolbarTitle.setText(R.string.title_faq);
                        } else if (bundle.getBoolean("works")) {
                            toolbarTitle.setText(R.string.title_how_works);
                        } else if (bundle.getBoolean("tnc")) {
                            toolbarTitle.setText(R.string.title_term_condn);
                        } else if (bundle.getBoolean("privacy")) {
                            toolbarTitle.setText(R.string.title_privacy_policy);
                        } else {
                            toolbarTitle.setText(R.string.app_name);
                        }
                    }
                } else {
                    toolbarTitle.setText(R.string.app_name);
                }
            }
        }, 200);
    }


    public void setHeaderData() {
        RelativeLayout loginView = (RelativeLayout) views.findViewById(R.id.login_view);
        RelativeLayout logoutView = (RelativeLayout) views.findViewById(R.id.logout_view);

        if (isLogin) {
            loginView.setVisibility(View.VISIBLE);
            logoutView.setVisibility(View.GONE);
            TextView navUserName = (TextView) views.findViewById(R.id.tv_user_name);
            TextView navUserEmail = (TextView) views.findViewById(R.id.tv_user_email);
            TextView navTotalBidCredit = (TextView) views.findViewById(R.id.tv_total_bid_credit);
            TextView navTotalBonusPoint = (TextView) views.findViewById(R.id.tv_total_bonus_points);
            profileImage = (ImageView) views.findViewById(R.id.profile_image);
            profileImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    fm.beginTransaction().replace(R.id.content_frame, new Profile(), " Profile")
                            .addToBackStack(null)
                            .commit();
                    toolbarTitle.setText(getString(R.string.title_edit_profile));
                    drawer.closeDrawer(GravityCompat.START);
                }
            });
            String imageUrl = prefsHelper.getString(PreferenceHelper.APP_USER_PROFILE_URL, "");
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(getApplicationContext()).load(imageUrl)
                        .into(profileImage);
            } else {
                Picasso.with(getApplicationContext()).load(ImageHelper.getDefaultUserImage(MainActivity.this,
                        prefsHelper.getString(PreferenceHelper.APP_USER_GENDER, "M"))).into(profileImage);
            }

            navUserName.setText(prefsHelper.getString(PreferenceHelper.APP_USER_NAME, ""));
            navUserEmail.setText(prefsHelper.getString(PreferenceHelper.APP_USER_EMAIL, ""));
            navTotalBidCredit.setText(getString(R.string.your_bid_credits) + " " + prefsHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            navTotalBonusPoint.setText(getString(R.string.your_bonus_point) + " " + prefsHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));


            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + prefsHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + prefsHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            logoutView.setVisibility(View.VISIBLE);
            loginView.setVisibility(View.GONE);

            LinearLayout btnLogin = (LinearLayout) views.findViewById(R.id.header_login);
            LinearLayout btnRegister = (LinearLayout) views.findViewById(R.id.header_register);
            btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, LoginActivity.class));
                }
            });
            btnRegister.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(MainActivity.this, RegisterActivity.class));
                }
            });

            holderUserPoints.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        // Handle navigation view item clicks here.
        //to make item checkable
        item.setCheckable(true);
        item.setChecked(true);
        if (mPreviousMenuItem != null) {
            mPreviousMenuItem.setChecked(false);
        }
        mPreviousMenuItem = item;

        int id = item.getItemId();
        Fragment fragment = null;
        Bundle bundle = new Bundle();

        switch (id) {
            case R.id.nav_home:
                fragment = new Home();
//                toolbarTitle.setText(getString(R.string.title_home));
                break;
            case R.id.nav_live_auction:
                Intent intent = new Intent(getApplicationContext(), SearchProductListingActivity.class);
                intent.putExtra("moreLive", true);
                startActivity(intent);
//                toolbarTitle.setText(R.string.title_live_auc);
                break;
            case R.id.nav_purchase_bid_credits:
                if (isLogin) {
                    fragment = new BuyBidCredits();
//                    toolbarTitle.setText(R.string.title_buy_bid_credits);
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
                break;

            case R.id.nav_tv_show:
                fragment = new TvShowFragment();
//                    toolbarTitle.setText(R.string.title_tv_show);
                break;

            case R.id.nav_refer_friend:
                if (isLogin) {
                    fragment = new ReferFriend();
//                    toolbarTitle.setText(R.string.title_refer_friend);
                } else {
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                }
                break;
            case R.id.account_wish:
                fragment = new MakeWish();
//                toolbarTitle.setText(R.string.title_make_wish);
                break;
            case R.id.account_delete_account:
                fragment = new DeleteAccount();
//                toolbarTitle.setText(R.string.title_del_account);
                break;

            case R.id.nav_about_us:
                fragment = new AboutApp();
                Bundle bundle30 = new Bundle();
                bundle30.putBoolean("aboutUs", true);
                fragment.setArguments(bundle30);
//                toolbarTitle.setText(R.string.title_about_us);

                break;
            case R.id.nav_contact_us:
                fragment = new ContactUs();
//                toolbarTitle.setText(R.string.title_contact_us);
                break;
            case R.id.nav_faq:
                fragment = new AboutApp();
                Bundle bundle1 = new Bundle();
                bundle1.putBoolean("faq", true);
                fragment.setArguments(bundle1);
//                toolbarTitle.setText(R.string.title_faq);
                break;
            case R.id.nav_how_it_works:
                fragment = new AboutApp();
                Bundle bundle3 = new Bundle();
                bundle3.putBoolean("works", true);
                fragment.setArguments(bundle3);
//                toolbarTitle.setText(R.string.title_how_works);
                break;
            case R.id.nav_term_condition:
                fragment = new AboutApp();
                Bundle bundle2 = new Bundle();
                bundle2.putBoolean("tnc", true);
                fragment.setArguments(bundle2);
//                toolbarTitle.setText(R.string.title_term_condn);
                break;
            case R.id.nav_privacy_policy:
                fragment = new AboutApp();
                Bundle bundle4 = new Bundle();
                bundle4.putBoolean("privacy", true);
                fragment.setArguments(bundle4);
//                toolbarTitle.setText(R.string.title_privacy_policy);
                break;

            case R.id.account_myprofile:
                fragment = new Profile();
//                toolbarTitle.setText(R.string.title_my_profile);
                break;

            case R.id.account_ongoing:
                fragment = new OnGoingAuction();
                bundle.putInt("Ongoing", Config.ONGOING_AUCTION);
                fragment.setArguments(bundle);
                toolbarTitle.setText(R.string.title_on_going_auc);
                break;
            case R.id.account_closed:
                fragment = new RecentClosedFrag();
//                toolbarTitle.setText(R.string.title_closed_auc);
                break;
            case R.id.account_won:
                fragment = new WonAuctions();
//                toolbarTitle.setText(R.string.title_won_auc);
                break;
            case R.id.account_ordered:
                fragment = new OrderedAuctions();
//                toolbarTitle.setText(R.string.title_order_auc);
                break;
            case R.id.account_fav:
                fragment = new Favourites();
//                toolbarTitle.setText(R.string.title_fav);
                break;
            case R.id.user_purchase:
                fragment = new PurchaseHistory();
//                toolbarTitle.setText(R.string.title_purchase_history);
                break;
            case R.id.account_coupon:
                fragment = new RedeemBonusPoint();
//                toolbarTitle.setText(R.string.title_redeem_bonus_point);
                break;

            case R.id.account_log_out:
//                boolean firstTimeProfile = prefsHelper.getBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true);
                prefsHelper.edit().clear().commit();
//                prefsHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, firstTimeProfile).commit();
                Intent intent1 = new Intent(getApplicationContext(), MainActivity.class);
                intent1.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent1);
                break;
        }
        if (fragment != null) {
            fm.beginTransaction().replace(R.id.content_frame, fragment)
                    .addToBackStack(null)
                    .commit();
            drawer.closeDrawer(GravityCompat.START);
        }
        setTitle();

        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        setHeaderData();
        getUserData();
        fm.addOnBackStackChangedListener(this);
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (drawer.isDrawerOpen(GravityCompat.START)) {
                    AlertUtils.hideInputMethod(MainActivity.this, toolbar);
                }
            }
        }, 100);


    }

    @Override
    protected void onPause() {
        fm.removeOnBackStackChangedListener(this);
        super.onPause();

    }

    @Override
    public void onBackStackChanged() {
        Logger.e("onBackStackChanged", "back stack changed !!!");
        Fragment fragment = fm.findFragmentById(R.id.content_frame);
        if (fragment instanceof Home) {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        } else {
            setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_UNSPECIFIED);
        }
        setTitle();
    }

    public void getUserData() {
        if (isLogin) {
            Api.getInstance().getUserDetailTask(this, new UserDetailGet() {
                @Override
                public void onUserDetailResponse(boolean isSuccess) {
                    if (isSuccess) {
                        setHeaderData();
                        if (!isProfileLoaded) {
                            loadProfileImage();
                        }
                    }
                }
            });
        }
    }

    private void loadProfileImage() {
        try {
            String imageUrl = prefsHelper.getString(PreferenceHelper.APP_USER_PROFILE_URL, "");
            if (!TextUtils.isEmpty(imageUrl)) {
                Picasso.with(getApplicationContext()).load(imageUrl).into(profileImage);
                isProfileLoaded = true;
            }
        } catch (Exception e) {
            Logger.e("profile image load ex", e.getMessage() + "");
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        Logger.e("onActivity result", resultCode + " : REQ " + requestCode);
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == WonAuctions.PAYMENT_CONFIRM_REQ_CODE) {
                if (data.getBooleanExtra("paid", false)) {
                    Fragment fragment = fm.findFragmentById(R.id.content_frame);
                    if (fragment instanceof WonAuctions) {
                        fragment.onActivityResult(requestCode, resultCode, data);
                    }
                }
            }

            if (requestCode == OngoingAuctionAdapter.PRODUCT_DETAIL_REQUEST_CODE_FROM_AUCTION) {
                setResult(RESULT_OK, data);
            }

            if (requestCode == FavListAdapter.PRODUCT_DETAIL_REQUEST_CODE) {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof Favourites) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

            if (requestCode == Home.PRODUCT_DETAIL_REQUEST_CODE) {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof Home) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

            //profile image result
            if (requestCode == Profile.RESULT_LOAD_IMAGE || requestCode == Profile.CAMERA_CAPTURE_IMAGE_REQUEST_CODE
                    || requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE || requestCode == Profile.REQ_CUSTOM_CROP_ACT) {
                Fragment fragment = fm.findFragmentById(R.id.content_frame);
                if (fragment instanceof Profile) {
                    fragment.onActivityResult(requestCode, resultCode, data);
                }
            }

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case Profile.REQ_PERMISSIONS_CAMERA:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the

                    Fragment fragment = fm.findFragmentById(R.id.content_frame);
                    if (fragment instanceof Profile) {
                        ((Profile) fragment).takePhoto();
                    }

                } else {
                    AlertUtils.showToast(getApplicationContext(), getString(R.string.denied_permission));
                }
                return;

            case Profile.REQ_PERMISSIONS_GALLERY:
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // permission was granted, yay! Do the

                    Fragment fragment = fm.findFragmentById(R.id.content_frame);
                    if (fragment instanceof Profile) {
                        ((Profile) fragment).chooseFrmGallery();
                    }

                } else {
                    AlertUtils.showToast(getApplicationContext(), getString(R.string.denied_permission));
                }
                return;
        }

    }

    public void openPurchaseHistory() {
        fm.beginTransaction().replace(R.id.content_frame, new PurchaseHistory(), " PURCHASEHISTORY")
                .addToBackStack(null)
                .commit();
//        toolbarTitle.setText(getString(R.string.title_purchase_history));
        setTitle();
    }

    public void openHomeFragment() {
        fm.beginTransaction().add(R.id.content_frame, new Home(), " Home")
                .addToBackStack(null)
                .commit();
        setTitle();
    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("getProfileImageTask");
        super.onDestroy();
    }


    public void runMarketingScript(String script) {
        if (script == null) {
            return;
        }
        if (script.equals("") || script.equalsIgnoreCase("null")) {
            return;
        }

        WebView webView = new WebView(MainActivity.this);

        WebSettings webSettings = webView.getSettings();
        webSettings.setJavaScriptCanOpenWindowsAutomatically(true);
        webSettings.setLoadWithOverviewMode(true);
        webSettings.setJavaScriptEnabled(true);

        // Register a new JavaScript interface called HTMLOUT /
//        webView.addJavascriptInterface(, "HTMLOUT");

        webView.setWebViewClient(new WebViewClient() {
            @Override
            public boolean shouldOverrideUrlLoading(WebView view, WebResourceRequest request) {
                return super.shouldOverrideUrlLoading(view, request);
            }

            @Override
            public void onPageStarted(WebView view, String url, Bitmap favicon) {
                super.onPageStarted(view, url, favicon);
                Logger.e("marketing script start", "URL :" + url + "\nview:" + view);
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                super.onPageFinished(view, url);
                Logger.e("marketing script ended", "URL :" + url + "\nview:" + view);
            }
        });
        webView.setWebChromeClient(new WebChromeClient());

        CookieManager.getInstance().removeAllCookie();
        webView.clearCache(true);

        webView.loadUrl(script);
        Logger.e("loaded marketing script", "DONE LOADED (******");
    }
}
