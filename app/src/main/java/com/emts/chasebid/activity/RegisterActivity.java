package com.emts.chasebid.activity;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.telephony.SmsMessage;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.appsflyer.AFInAppEventParameterName;
import com.appsflyer.AFInAppEventType;
import com.appsflyer.AppsFlyerLib;
import com.emts.chasebid.R;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Config;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;
import com.google.firebase.iid.FirebaseInstanceId;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by User on 24/02/2017.
 */
public class RegisterActivity extends AppCompatActivity {
    boolean isValid = true;
    TextView tvDob;
    public static final String DATEPICKER_TAG = "datepicker";
    public static final int REQUEST_ACCEPT_CONDITION = 890;
    CheckBox cbTnC;
    PreferenceHelper prefHelper;
    Spinner spState; //country,
    //    LinearLayout holderStates;
//    ArrayList<String> countries = new ArrayList<>();
//    ArrayList<Integer> countryIdList = new ArrayList<>();
//    ArrayList<String> countryCodeList = new ArrayList<>();
    ArrayList<String> indiaStateArray = new ArrayList<>();

    EditText verificationCode;

    boolean isFbRegister;
    Intent intent;

    SmsReceiver smsReceiver = new SmsReceiver();

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_register);

        prefHelper = PreferenceHelper.getInstance(this);

        intent = getIntent();
        if (intent != null) {
            if (intent.hasExtra("fb_login")) {
                isFbRegister = intent.getBooleanExtra("fb_login", false);
            }
        }
        final Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_register);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });

        LinearLayout holderUserPoints = findViewById(R.id.register_toolbar).findViewById(R.id.holder_user_points);
        holderUserPoints.setVisibility(View.GONE);

        tvDob = (TextView) findViewById(R.id.et_dob);
        tvDob.setOnClickListener(dob_clickListener);
        // tvDob.setOnTouchListener(dob_touchListener);

        cbTnC = (CheckBox) findViewById(R.id.cb_term_condition);
        TextView termandCondition = (TextView) findViewById(R.id.tv_term_condition);
        termandCondition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                Intent intent = new Intent(RegisterActivity.this, TermAndConditionActivity.class);
//                startActivityForResult(intent, REQUEST_ACCEPT_CONDITION);
            }
        });

        final EditText mobileNo = (EditText) findViewById(R.id.et_contact_mobile);
        verificationCode = (EditText) findViewById(R.id.et_verification_code);

        TextView btnVerify = (TextView) findViewById(R.id.verify);
        btnVerify.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (Patterns.PHONE.matcher(mobileNo.getText().toString().trim()).matches() &&
                            mobileNo.getText().toString().trim().length() > 9 &&
                            mobileNo.getText().toString().trim().length() < 15) {
                        mobileVerifyTask(mobileNo.getText().toString().trim());
                    } else {
                        AlertUtils.showSnack(getApplicationContext(), toolbar, getString(R.string.error_mobile_length));
                    }
                } else {
                    AlertUtils.showSnack(RegisterActivity.this, toolbar, getString(R.string.error_no_internet));
                }
            }
        });

//        country = (Spinner) findViewById(R.id.sp_country);
//        File countryList = new File(getFilesDir() + "/" + "countrylist.txt");
//        if (countryList.exists()) {
//
//            String response = readFromFile(RegisterActivity.this);
//
//            try {
//                JSONObject jsonObject = new JSONObject(response);
//                JSONArray countryArray = jsonObject.getJSONArray("countries");
//                for (int i = 0; i < countryArray.length(); i++) {
//                    JSONObject eachObj = countryArray.getJSONObject(i);
//                    String countryName = eachObj.getString("country");
//                    Integer countryId = Integer.valueOf(eachObj.getString("id"));
//                    String countryCode = eachObj.getString("short_code");
//                    countryIdList.add(countryId);
//                    countryCodeList.add(countryCode);
//                    countries.add(countryName);
//                    countryIdList.add(countryId);
//
//                }
//                setSpinnerAdapter(0);
//            } catch (JSONException e) {
//                Logger.e("country ex 1234", e.getMessage() + "");
//            }
//
//        } else {
//            if (NetworkUtils.isInNetwork(getApplicationContext())) {
//                getCountryTask();
//            }
//        }

        spState = (Spinner) findViewById(R.id.sp_state);
//        holderStates = (LinearLayout) findViewById(R.id.holder_state);
//        country.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
//            @Override
//            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                if (countries.get(i).contains("india")) {
//                    if (indiaStateArray.size() < 1) {
//                        getStatesTask();
//                    }
//                } else {
//                    holderStates.setVisibility(View.GONE);
//                }
//            }
//
//            @Override
//            public void onNothingSelected(AdapterView<?> adapterView) {
//
//            }
//        });
        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            getStatesTask();
        }

        final EditText userName = (EditText) findViewById(R.id.et_username);
        final EditText email = (EditText) findViewById(R.id.et_email);
        final EditText password = (EditText) findViewById(R.id.et_password);
        final EditText cPassword = (EditText) findViewById(R.id.et_c_password);
        final EditText firstName = (EditText) findViewById(R.id.et_f_name);
        final EditText lastName = (EditText) findViewById(R.id.et_l_name);


//        final EditText address1 = (EditText) findViewById(R.id.et_address1);
//        final EditText address2 = (EditText) findViewById(R.id.et_address2);
        final EditText city = (EditText) findViewById(R.id.et_city);
//        final EditText postalCode = (EditText) findViewById(R.id.et_postal_code);
        final TextView btn_sign_up = (TextView) findViewById(R.id.sign_up);
        assert btn_sign_up != null;

        final RadioGroup rgGender = (RadioGroup) findViewById(R.id.rg_gender);
        RadioButton male = (RadioButton) findViewById(R.id.rb_male);
        RadioButton female = (RadioButton) findViewById(R.id.rb_female);
        RadioButton gOther = (RadioButton) findViewById(R.id.rb_other);

        Typeface font = Typeface.createFromAsset(getAssets(), "fonts/yanonekaffeesatz_regular.ttf");
        male.setTypeface(font);
        female.setTypeface(font);
        gOther.setTypeface(font);

        //set data if isFbRegister
        if (isFbRegister) {
            userName.setText(intent.getStringExtra("fb_username"));
            email.setText(intent.getStringExtra("fb_email"));
            firstName.setText(intent.getStringExtra("fb_fname"));
            lastName.setText(intent.getStringExtra("fb_lname"));
        }

        btn_sign_up.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!NetworkUtils.isInNetwork(RegisterActivity.this)) {
                    AlertUtils.showSnack(RegisterActivity.this, btn_sign_up, getString(R.string.error_no_internet));
                    return;
                }
                isValid = true;
                //validate start
                if (TextUtils.isEmpty(userName.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_username), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_username));
                }
                if (TextUtils.isEmpty(email.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_email), getString(R.string.empty_error_msg));
                } else {
                    if (!Patterns.EMAIL_ADDRESS.matcher(email.getText().toString().trim()).matches()) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_email), getString(R.string.incorrect_email));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_email));
                    }
                }
                if (TextUtils.isEmpty(password.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_passeord), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_passeord));
                }
                if (TextUtils.isEmpty(cPassword.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_cpassword), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_cpassword));
                }
                if (!TextUtils.isEmpty(password.getText().toString().trim()) && !TextUtils.isEmpty(cPassword.getText().toString().trim())) {
                    if (password.getText().toString().trim().length() < 6) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_passeord), getString(R.string.error_password_length));
                    } else {
                        if (!password.getText().toString().trim().equals(cPassword.getText().toString().trim())) {
                            isValid = false;
                            showErrorView((TextView) findViewById(R.id.error_cpassword), getString(R.string.error_password_not_match));
                        } else {
                            hideErrorView((TextView) findViewById(R.id.error_cpassword));
                        }
                    }
                }

                int selectedId = rgGender.getCheckedRadioButtonId();
                String gender_m_f = "";
                if (selectedId == R.id.rb_male) {
                    gender_m_f = "M";
                } else if (selectedId == R.id.rb_female) {
                    gender_m_f = "F";
                } else if (selectedId == R.id.rb_other) {
                    gender_m_f = "O";
                }
                if (TextUtils.isEmpty(gender_m_f)) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_choose_gender), getString(R.string.choose_gender));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_choose_gender));
                }

                if (TextUtils.isEmpty(firstName.getText().toString().trim()) && TextUtils.isEmpty(lastName.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_name), getString(R.string.empty_both));
                } else {
                    if (TextUtils.isEmpty(lastName.getText().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_name), getString(R.string.empty_error_msg));
                    } else if (TextUtils.isEmpty(firstName.getText().toString().trim())) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_name), getString(R.string.empty_error_msg));
                    } else {
                        if (firstName.getText().toString().trim().length() < 2 || lastName.getText().toString().trim().length() < 2) {
                            isValid = false;
                            showErrorView((TextView) findViewById(R.id.error_name), getString(R.string.error_first_n_last_name_lenght));
                        } else {
                            hideErrorView((TextView) findViewById(R.id.error_name));
                        }
                    }
                }
                if (TextUtils.isEmpty(mobileNo.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_mob_no), getString(R.string.empty_error_msg));
                } else {
                    if (mobileNo.getText().toString().trim().length() > 9 && mobileNo.getText().toString().trim().length() < 15) {
                        hideErrorView((TextView) findViewById(R.id.error_mob_no));
                    } else {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_mob_no), getString(R.string.mobile_number));
                    }
                }
                if (TextUtils.isEmpty(verificationCode.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_verification_code), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_verification_code));
                }
                if (TextUtils.isEmpty(tvDob.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_dob), getString(R.string.empty_error_msg));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_dob));
                }

//                if (TextUtils.isEmpty(address1.getText().toString().trim())) {
//                    isValid = false;
//                    showErrorView((TextView) findViewById(R.id.error_address1), getString(R.string.empty_error_msg));
//                } else {
//                    hideErrorView((TextView) findViewById(R.id.error_address1));
//                }
//                if (TextUtils.isEmpty(city.getText().toString().trim()) && TextUtils.isEmpty(postalCode.getText().toString().trim())) {
//                    isValid = false;
//                    showErrorView((TextView) findViewById(R.id.error_citypostal), getString(R.string.empty_both));
//                } else {
//                    if (TextUtils.isEmpty(postalCode.getText().toString().trim())) {
//                        isValid = false;
//                        showErrorView((TextView) findViewById(R.id.error_citypostal), getString(R.string.error_postal_code));
//                    } else if (TextUtils.isEmpty(city.getText().toString().trim())) {
//                        showErrorView((TextView) findViewById(R.id.error_citypostal), getString(R.string.error_city_req));
//                    } else {
//                        hideErrorView((TextView) findViewById(R.id.error_citypostal));
//                    }
//                }
                if (TextUtils.isEmpty(city.getText().toString().trim())) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_citypostal), getString(R.string.error_city_req));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_citypostal));
                }
//                if (country.getSelectedItem() != null) {
//                    if (TextUtils.isEmpty(country.getSelectedItem().toString().trim())) {
//                        isValid = false;
//                        showErrorView((TextView) findViewById(R.id.error_country), getString(R.string.empty_error_msg));
//                    } else {
//                        if (country.getSelectedItem().toString().trim().contains("india")) {
//                            //state is compulsory
//                            if (spState.getAdapter() != null) {
//                                if (spState.getSelectedItemPosition() == -1) {
//                                    isValid = false;
//                                    showErrorView((TextView) findViewById(R.id.error_state), getString(R.string.empty_error_msg));
//                                } else {
//                                    hideErrorView((TextView) findViewById(R.id.error_state));
//                                }
//                            } else {
//                                isValid = false;
//                                getStatesTask();
//                                showErrorView((TextView) findViewById(R.id.error_state), getString(R.string.empty_error_msg));
//                                return;
//                            }
//                        } else {
//                            hideErrorView((TextView) findViewById(R.id.error_country));
//                        }
//                    }
//                } else {
//                    isValid = false;
//                    AlertUtils.showSnack(RegisterActivity.this, btn_sign_up, getString(R.string.reload_country));
//                    return;
//                }
//                //state is compulsory
                if (spState.getAdapter() != null) {
                    if (spState.getSelectedItemPosition() < 1) {
                        isValid = false;
                        showErrorView((TextView) findViewById(R.id.error_state), getString(R.string.empty_error_msg));
                    } else {
                        hideErrorView((TextView) findViewById(R.id.error_state));
                    }
                } else {
                    isValid = false;
                    getStatesTask();
                    showErrorView((TextView) findViewById(R.id.error_state), getString(R.string.empty_error_msg));
                    return;
                }
                if (!cbTnC.isChecked()) {
                    isValid = false;
                    showErrorView((TextView) findViewById(R.id.error_terms), getString(R.string.accept_terms));
                } else {
                    hideErrorView((TextView) findViewById(R.id.error_terms));
                }

                if (isValid) {
                    view.clearAnimation();
                    if (NetworkUtils.isInNetwork(getApplicationContext())) {
//                        String stateSel = "";
//                        if (country.getSelectedItem().toString().trim().contains("india")) {
//                            stateSel = spState.getSelectedItem().toString().trim();
//                        }

                        registrationTask(userName.getText().toString().trim(), email.getText().toString().trim(),
                                password.getText().toString().trim(), cPassword.getText().toString().trim(), gender_m_f,
                                firstName.getText().toString().trim(), lastName.getText().toString().trim(),
                                mobileNo.getText().toString().trim(), verificationCode.getText().toString().trim(),
                                tvDob.getText().toString().trim(),
                                ""/*address1.getText().toString().trim()*/, ""/*address2.getText().toString().trim()*/,
                                city.getText().toString().trim(), ""/*postalCode.getText().toString().trim()*/,
                                ""/*String.valueOf(countryIdList.get(country.getSelectedItemPosition()))*/,
                                spState.getSelectedItem().toString().trim());
                    } else {
                        AlertUtils.showSnack(RegisterActivity.this, btn_sign_up, getString(R.string.error_no_internet));
                    }
                } else {
                    view.clearAnimation();
                    Animation anim = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.shake_animation);
                    view.setAnimation(anim);
                    AlertUtils.showSnack(RegisterActivity.this, btn_sign_up, getString(R.string.form_validate_message));
                }
            }
        });

    }

    private void getStatesTask() {
        final com.emts.chasebid.customviews.ProgressDialog pDialog = new com.emts.chasebid.customviews.ProgressDialog(RegisterActivity.this);
        pDialog.setMessage(getString(R.string.please_wait));
        pDialog.show();
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().indiaStateListApi, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        try {
                            JSONObject res = new JSONObject(response);
                            if (res.getBoolean("status")) {
                                JSONArray dataArray = res.getJSONArray("data");
                                if (dataArray.length() > 0) {
                                    indiaStateArray.add(getString(R.string.select_state));
                                }
                                for (int i = 0; i < dataArray.length(); i++) {
                                    JSONObject eachState = dataArray.getJSONObject(i);
                                    indiaStateArray.add(eachState.getString("city_state"));
                                }
                                setStateSpinnerAdapter();
                            } else {

                            }

                            setStateSpinnerAdapter();

                        } catch (Exception e) {
                            Logger.e("getStatesTask  Ex", e.getMessage() + " ");
                        }
                        pDialog.dismiss();
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError volleyError) {
                        try {
                            final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setTitle(R.string.title_error_mesg);
                            builder.setMessage(Html.fromHtml(getString(R.string.reload_state)));
                            builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    getStatesTask();
                                }
                            });
                            builder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    dialogInterface.dismiss();
                                    onBackPressed();
                                }
                            });
                            builder.show();


                        } catch (Exception e) {

                        }
                        pDialog.dismiss();
                    }
                }, "getStatesTask");
    }

//    private String readFromFile(Context context) {
//        String ret = "";
//
//        try {
//            InputStream inputStream = context.openFileInput("countrylist.txt");
//
//            if (inputStream != null) {
//                InputStreamReader inputStreamReader = new InputStreamReader(inputStream);
//                BufferedReader bufferedReader = new BufferedReader(inputStreamReader);
//                String receiveString = "";
//                StringBuilder stringBuilder = new StringBuilder();
//
//                while ((receiveString = bufferedReader.readLine()) != null) {
//                    stringBuilder.append(receiveString);
//                }
//                inputStream.close();
//                ret = stringBuilder.toString();
//            }
//        } catch (FileNotFoundException e) {
//            Log.e("login activity", "File not found: " + e.toString());
//        } catch (IOException e) {
//            Log.e("login activity", "Can not read file: " + e.toString());
//        }
//
//        return ret;
//    }

//    private void getCountryTask() {
//
////        final ProgressDialog pDailog = AlertUtils.showProgressDialog(RegisterActivity.this, "Please wait .....");
//        final com.emts.chasebid.customviews.ProgressDialog pDialog = new com.emts.chasebid.customviews.ProgressDialog(RegisterActivity.this);
//        pDialog.setMessage(getString(R.string.please_wait));
//        pDialog.show();
//        VolleyHelper vHelper = VolleyHelper.getInstance(this);
//        HashMap<String, String> postParams = Api.getInstance().getPostParams();
//        String lastUpdate = prefHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");
//        postParams.put("last_updated_date", lastUpdate);
//
//        vHelper.addVolleyRequestListeners(Api.getInstance().countryListUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
//            @Override
//            public void onSuccess(String response) {
//                try {
//                    JSONObject res = new JSONObject(response);
//                    Date date2 = null;
//                    if (res.getBoolean("status")) {
////                        new InsertCountryAsync().execute(res.getJSONArray("message").toString());
//                        JSONArray countryarray = res.getJSONArray("countries");
//                        for (int i = 0; i < countryarray.length(); i++) {
//                            JSONObject countryObj = countryarray.getJSONObject(i);
//                            String lastupdate = countryObj.getString("last_update");
//                            String countryName = countryObj.getString("country");
//                            Integer countryId = Integer.valueOf(countryObj.getString("id"));
//                            String countryCode = countryObj.getString("short_code");
//                            countries.add(countryName);
//                            countryIdList.add(countryId);
//                            countryCodeList.add(countryCode);
//
//
//                            if (countryObj.getString("default_country").equalsIgnoreCase("Yes")) {
//                                prefHelper.edit().putString(PreferenceHelper.SITE_CURR_CODE, countryObj.getString("currency_code")).commit();
//                                prefHelper.edit().putString(PreferenceHelper.SITE_CURR_SIGN, countryObj.getString("currency_sign")).apply();
//                                prefHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, countryObj.getString("short_code")).apply();
//                            }
//                            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-DD");
//                            Date date1 = formatter.parse(lastupdate);
//                            String preflastUpdate = prefHelper.getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");
//
//                            if (TextUtils.isEmpty(preflastUpdate)) {
//                                prefHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastupdate).commit();
//                            } else {
//                                date2 = formatter.parse(preflastUpdate);
//                                if (date2.after(date1)) {
//                                    prefHelper.edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastupdate).commit();
//                                }
//                            }
//
//                        }
//                        setSpinnerAdapter(0);
//                        writeTofile(response, RegisterActivity.this);
//
//                    }
//
//                } catch (Exception e) {//
//                    Logger.e("getCountryTask  Ex", e.getMessage() + " ");
//                }
//                pDialog.dismiss();
//            }
//
//            @Override
//            public void onError(String errorResponse, VolleyError volleyError) {
//                try {
//                    final AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
//                    builder.setTitle(R.string.title_error_mesg);
//                    builder.setMessage(Html.fromHtml(getString(R.string.reload_countries)));
//                    builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            getCountryTask();
//                        }
//                    });
//                    builder.setNegativeButton(R.string.alert_cancel, new DialogInterface.OnClickListener() {
//                        @Override
//                        public void onClick(DialogInterface dialogInterface, int i) {
//                            dialogInterface.dismiss();
//                            onBackPressed();
//                        }
//                    });
//                    builder.show();
//
//
//                } catch (Exception e) {
//
//                }
//                pDialog.dismiss();
//            }
//        }, "getCountryTask");
//    }

//    private void writeTofile(String response, Context context) {
//        try {
//            OutputStreamWriter outputStreamWriter = new OutputStreamWriter(context.openFileOutput("countrylist.txt", Context.MODE_PRIVATE));
//            outputStreamWriter.write(response);
//            outputStreamWriter.close();
//        } catch (IOException e) {
//            Log.e("Exception", "File write failed: " + e.toString());
//        }
//
//    }

    private void mobileVerifyTask(final String mobileNo) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("mobile", mobileNo);
        Logger.e("post mobile", mobileNo);

        final ProgressDialog progressDialog = new ProgressDialog(RegisterActivity.this);
        progressDialog.setMessage(getString(R.string.verifying));
        progressDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().mobileVerification, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        showAlert(res.getString("message"));
                    } else {
                        showAlert(res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("mobileVerifyTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    showAlert(errorObj.getString("message"));
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "mobileVerifyTask");
    }

    View.OnClickListener dob_clickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            showDatePickerDialog();
        }
    };
    String year, month, day;
    com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener dateSetListener = new com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog, int year, int month, int day) {
            RegisterActivity.this.year = String.valueOf(year);
            RegisterActivity.this.month = String.format("%02d", month + 1); // (month < 10) ? Integer.valueOf("0" + month) : month;
            RegisterActivity.this.day = String.format("%02d", day);//(day < 10) ? Integer.valueOf("0" + day) : day;
            tvDob.setText(year + "-" + (RegisterActivity.this.month) + "-" + day);

        }
    };

    private void showDatePickerDialog() {
        final Calendar calendar = Calendar.getInstance();

        final com.fourmob.datetimepicker.date.DatePickerDialog datePickerDialog =
                com.fourmob.datetimepicker.date.DatePickerDialog.newInstance(dateSetListener,
                        // calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), true);
                        1997, 00, 01, true);

        datePickerDialog.setVibrate(true);
        datePickerDialog.setYearRange(1902, 1997);
        datePickerDialog.setCloseOnSingleTapDay(false);
        if (!datePickerDialog.isVisible()) {
            datePickerDialog.show(getSupportFragmentManager(), DATEPICKER_TAG);
        }
    }

    private void registrationTask(final String userName, final String email, final String password, final String cPassword,
                                  final String mGender, final String firstName,
                                  final String lastName, final String mobNo, final String verificationCode, final String dob,
                                  final String address1, final String address2, final String city,
                                  final String postalCode, final String country, final String state) {
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("user_name", userName);
        postMap.put("email", email);
        postMap.put("password", password);
        postMap.put("re_password", cPassword);
        postMap.put("gender", mGender);
        postMap.put("fname", firstName);
        postMap.put("lname", lastName);
        postMap.put("mobile", mobNo);
        postMap.put("verification_code", verificationCode);
        postMap.put("dobday", day);
        postMap.put("dobyear", year);
        postMap.put("dobmonth", month);
        postMap.put("address", address1);
        postMap.put("address2", address2);
        postMap.put("city", city);
        postMap.put("zip", postalCode);
        postMap.put("country", country);
        postMap.put("state", state);
        postMap.put("t_c", "yes");
        postMap.put("selector", "yes");
        String pushToken = FirebaseInstanceId.getInstance().getToken();
        if (!TextUtils.isEmpty(pushToken)) {
            postMap.put("push_id", pushToken);
            Logger.e("push token", pushToken);
        }

        final ProgressDialog pDialog = AlertUtils.showProgressDialog(RegisterActivity.this, getString(R.string.registering_user));
        pDialog.show();

        vHelper.addVolleyRequestListeners(Api.getInstance().registerUrl, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {

                        if (res.getString("send_email_verification").equalsIgnoreCase("0")) {
                            prefHelper.edit().putBoolean(PreferenceHelper.IS_LOGIN, true).commit();
                            prefHelper.edit().putString(PreferenceHelper.APP_USER_EMAIL, res.getString("email")).commit();
                            prefHelper.edit().putString(PreferenceHelper.APP_USER_NAME, res.getString("username")).commit();
                            prefHelper.edit().putString(PreferenceHelper.TOKEN, res.getString("token")).commit();
                            prefHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, res.getString("normal_balance")).commit();
                            prefHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, res.getString("normal_balance")).commit();
                            prefHelper.edit().putInt(PreferenceHelper.APP_USER_ID, Integer.parseInt(res.getString("user_id"))).commit();
                            prefHelper.edit().putString(PreferenceHelper.APP_USER_COUNTRY_CODE, res.getString("country_shotd_code")).commit();
                            prefHelper.edit().putString(PreferenceHelper.APP_USER_GENDER, mGender).commit();

                            prefHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true).commit();

                            Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                            intent.putExtra("marketingScript", res.optString("marketing_script"));
                            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            startActivity(intent);
                        } else {

                            prefHelper.edit().putBoolean(PreferenceHelper.FIRST_TIME_PROFILE, true).commit();

                            try {
                                MainActivity act = new MainActivity();
                                act.runMarketingScript(res.optString("marketing_script"));
                            } catch (Exception e) {
                            }
                            AlertDialog.Builder builder = new AlertDialog.Builder(RegisterActivity.this);
                            builder.setTitle(R.string.title_registration);
                            builder.setMessage(res.getString("message"));
//                        builder.setMessage(R.string.reg_success_mesg);
                            builder.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK
                                            | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                                    startActivity(intent);
                                }
                            });
                            builder.setCancelable(false);
                            builder.show();
                        }
                        ChaseBidApp.getInstance().afTrackRegisterEvent(res.optString("user_id"), "normal_registration");
                    } else {
                        AlertUtils.showSnack(RegisterActivity.this, tvDob, res.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("registerTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(new String(error.networkResponse.data));
                    showAlert(errorObj.getString("message"));
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ServerError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_server), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof AuthFailureError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_authFailureError), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof ParseError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_parse_error), Toast.LENGTH_SHORT).show();
                    } else if (error instanceof TimeoutError) {
                        Toast.makeText(getApplicationContext(), getString(R.string.error_time_out), Toast.LENGTH_SHORT).show();
                    }
                }
            }
        }, "registrationTask");
    }

    public void showErrorView(TextView errorView, String message) {
        errorView.setText(message);
        errorView.setVisibility(View.VISIBLE);
    }

    public void hideErrorView(TextView errorView) {
        errorView.setVisibility(View.GONE);
    }

    @Override

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_ACCEPT_CONDITION && resultCode == RESULT_OK)
            cbTnC.setChecked(data.getBooleanExtra("Accept", false));
    }

    protected void onResume() {
        IntentFilter filter = new IntentFilter("android.provider.Telephony.SMS_RECEIVED");
        registerReceiver(smsReceiver, filter);
        super.onResume();
    }

    @Override
    protected void onPause() {
        unregisterReceiver(smsReceiver);
        super.onPause();
    }


    public void showAlert(String message) {
        AlertDialog.Builder alert = new AlertDialog.Builder(RegisterActivity.this);
        alert.setMessage(message);
        alert.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        alert.show();
    }

    public void setVerificationCode(String code) {
        try {
            verificationCode.setText(code);
        } catch (Exception e) {
            Logger.e("set broadcast code set", e.getMessage() + " ");
        }
    }

    public class SmsReceiver extends BroadcastReceiver {
        private final String TAG = SmsReceiver.class.getSimpleName();

        @Override
        public void onReceive(Context context, Intent intent) {
            Logger.e("TAG", "onReceive");

//            setVerificationCode("123456789");

            final Bundle bundle = intent.getExtras();
            try {
                if (bundle != null) {
                    Object[] pdusObj = (Object[]) bundle.get("pdus");
                    for (Object aPdusObj : pdusObj) {
                        SmsMessage currentMessage = SmsMessage.createFromPdu((byte[]) aPdusObj);
                        String senderAddress = currentMessage.getDisplayOriginatingAddress();
                        String message = currentMessage.getDisplayMessageBody();

                        Logger.e(TAG, "Received SMS: " + message + ", Sender: " + senderAddress);

                        // if the SMS is not from our gateway, ignore the message
//                    if (!senderAddress.toLowerCase().contains(Config.SMS_ORIGIN.toLowerCase())) {
//                        return;
//                    }
//                    if (message.contains("ubidubuy")){
//
//                    }

//                    setVerificationCode("123456789");

                        // verification code from sms
                        String verificationCode = getVerificationCode(message);

                        Logger.e(TAG, "OTP received: " + verificationCode);

//                    Intent hhtpIntent = new Intent(context, HttpService.class);
//                    hhtpIntent.putExtra("otp", verificationCode);
//                    context.startService(hhtpIntent);
                    }
                }
            } catch (Exception e) {
                Logger.e(TAG, "Exception: " + e.getMessage());
            }
        }

        /**
         * Getting the OTP from sms message body
         * ':' is the separator of OTP from the message
         *
         * @param message
         * @return
         */
        private String getVerificationCode(String message) {
            String code = null;
            int index = message.indexOf(Config.OTP_DELIMITER);

            if (index != -1) {
                int start = index + 2;
                int length = 6;
                code = message.substring(start, start + length);
                return code;
            }

            return code;
        }
    }

//    private void getCountryTask() {
//        String url = Api.countryListUrl;
//        Logger.e("getCountryTask Url", url);
//
//        RetryPolicy policy = new DefaultRetryPolicy(30000, DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT);
//        StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Logger.e("getCountryTask response", response);
//                try {
//                    JSONObject res = new JSONObject(response);
//                    if (res.getBoolean("status")) {
//
//                        new InsertCountryAsync().execute(res.getJSONArray("country_list").toString(),res.getString("last_updated_date"));
//
////                        JSONArray countryList = res.getJSONArray("country_list");
////                        for(int i=0; i<countryList.length(); i++){
////                            dbHelper.storeCountryToLocalDb(countryList.getString(i));
////                        }
////                        ArrayList<String> countries = new ArrayList<>(countryList.length());
////                        for (int i = 0; i < countryList.length(); i++) {
////                            countries.add(countryList.getString(i));
////                        }
////                        dbHelper.storeCountryToLocalDb(countries);
////
////
////                        countries =  dbHelper.getCountryList();
////                        setSpinnerAdapter();
//
//                    } else {
//                        Logger.e("Message", res.getString("message"));
//
//                    }
//                } catch (JSONException e) {
//                    Logger.e("getCountryTask  json ex", e.getMessage());
//                }
//            }
//        }, new Response.ErrorListener() {
//
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                Logger.e("getCountryTask  error", error.getMessage() + "");
//
//                try {
//                    Logger.printLongLog("getCountryTask error response", new String(error.networkResponse.data));
//                } catch (Exception e) {
//
//                }
//            }
//        }) {
//            @Override
//            protected Map<String, String> getParams() throws AuthFailureError {
//                Map<String, String> map = new HashMap<>();
//                map.put("api_key", Api.apiKey);
//                String lastUpdate = PreferenceHelper.getInstance(RegisterActivity.this).getString(PreferenceHelper.COUNTRY_LAST_UPDATE, "");
//                map.put("last_updated_date", lastUpdate);
//                return map;
//            }
//        };
//        request.setRetryPolicy(policy);
//        ChaseBidApp.getInstance().addToRequestQueue(request, "getCountrysTask");
//        ChaseBidApp.getInstance().getRequestQueue().addRequestFinishedListener(new RequestQueue.RequestFinishedListener<Object>() {
//            @Override
//            public void onRequestFinished(Request<Object> request) {
//
//            }
//        });
//
//    }

//    private void setSpinnerAdapter(int countryIndex) {
//        // Creating adapter for spinner
//        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(RegisterActivity.this, android.R.layout.simple_spinner_item, countries) {
//
//        };
//
//        // Drop down layout style - list view with radio button
//        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
//
//        // attaching data adapter to spinner
//        country.setAdapter(dataAdapter);
//        try {
//            country.setSelection(countryIndex);
//        } catch (Exception e) {
//
//        }
//    }

    private void setStateSpinnerAdapter() {
        // Creating adapter for spinner
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(RegisterActivity.this,
                android.R.layout.simple_spinner_item, indiaStateArray) {
        };

        // Drop down layout style - list view with radio button
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // attaching data adapter to spinner
        spState.setAdapter(dataAdapter);
//        holderStates.setVisibility(View.VISIBLE);
    }

//    class InsertCountryAsync extends AsyncTask<String, Void, Void> {
//
//        @Override
//        protected Void doInBackground(String... strings) {
//            String arrayJson = strings[0];
//            String lastUpdateDate = strings[1];
//            int countryIndex = 0;
//            try {
//                JSONArray countryList = new JSONArray(arrayJson);
//                ArrayList<String> countriess = new ArrayList<>(countryList.length());
//                for (int i = 0; i < countryList.length(); i++) {
//                    countriess.add(countryList.getString(i));
//                    if (countryList.getString(i).equalsIgnoreCase("singapore")) {
//                        countryIndex = i;
//                    }
//                }
//                countries = countriess;
//                setSpinnerAdapter(countryIndex);
//                dbHelper.storeCountryToLocalDb(countries);
//                PreferenceHelper.getInstance(RegisterActivity.this).edit().putString(PreferenceHelper.COUNTRY_LAST_UPDATE, lastUpdateDate).commit();
//            } catch (Exception e) {
//                Logger.e("async ex", e.getMessage() + "");
//            }
//
//            return null;
//        }
//    }

    @Override
    protected void onDestroy() {
//        VolleyHelper.getInstance(this).cancelRequest("getCountryTask");
        VolleyHelper.getInstance(this).cancelRequest("registerTask");
        VolleyHelper.getInstance(this).cancelRequest("mobileVerifyTask");
//        ChaseBidApp.getInstance().getRequestQueue().cancelAll("getCountrysTask");
//        ChaseBidApp.getInstance().getRequestQueue().cancelAll("registerTask");
//        ChaseBidApp.getInstance().getRequestQueue().cancelAll("mobileVerifyTask");
        super.onDestroy();
    }


}

