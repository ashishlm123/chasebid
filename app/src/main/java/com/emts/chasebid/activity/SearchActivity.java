package com.emts.chasebid.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.searchCategory.CategoryItem;
import com.emts.chasebid.adapters.searchCategory.SearchCategoryAdapter;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.VolleyHelper;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by User on 2016-06-30.
 */
public class SearchActivity extends AppCompatActivity {
    ArrayList<CategoryItem> headers = new ArrayList<>();
    ArrayList<List<CategoryItem>> child = new ArrayList<>();
    ExpandableListView expandableListView;
    FrameLayout progressBar;
//    ProgressBar progressBar;
    SearchCategoryAdapter adapter;
    LinearLayout errorView;
    TextView errorMessage;
    //  String searchKeyWord = "";

    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_search);
        expandableListView = (ExpandableListView) findViewById(R.id.search_category);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(R.string.title_category);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });


        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        PreferenceHelper preferenceHelper = PreferenceHelper.getInstance(this);

        if (preferenceHelper.isLogin()) {
            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            holderUserPoints.setVisibility(View.GONE);
        }

        errorView = (LinearLayout) findViewById(R.id.error_view);
        errorMessage = (TextView) errorView.findViewById(R.id.tv_error_message);

        Button errorBtn = (Button) findViewById(R.id.btn_error_view);
        assert errorBtn != null;
        errorBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(SearchActivity.this)) {
                    getCategoryTask();
                }
            }
        });

        final EditText search = (EditText) findViewById(R.id.search);
        assert search != null;
        search.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView textView, int i, KeyEvent keyEvent) {

                if (i == EditorInfo.IME_ACTION_SEARCH || i == EditorInfo.IME_ACTION_GO) {
                    String searchKeyWord = search.getText().toString().trim();
                    if (!TextUtils.isEmpty(searchKeyWord)) {
                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            Intent intent = new Intent(SearchActivity.this, SearchProductListingActivity.class);
                            intent.putExtra("searchKeyWord", searchKeyWord);
                            intent.putExtra("fromSearchBar", true);
                            startActivity(intent);

                        } else {
                            Toast.makeText(SearchActivity.this, getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                        }
                    }
                }
                return true;
            }

        });

        expandableListView.setOnChildClickListener(new ExpandableListView.OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView expandableListView, View view, int groupPosition, int childPosition, long childId) {
                String childName = child.get(groupPosition).get(childPosition).getTitle();
                int categoryId = headers.get(groupPosition).getCategoryId();
                Intent intent = new Intent(SearchActivity.this, SearchProductListingActivity.class);
                intent.putExtra("ParentId", categoryId);
                intent.putExtra("childName", childName);
                intent.putExtra("childId", (int) childId);
                intent.putExtra("fromCategory", true);
                Logger.e("CategoryList", childName + "\t" + childId + "\t" + categoryId);
                startActivity(intent);
                return false;

            }
        });

        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView expandableListView, View view, int groupPosition, long parentId) {
//                if (child.get(groupPosition).size() == 0) {
                Intent intent = new Intent(SearchActivity.this, SearchProductListingActivity.class);
                intent.putExtra("ParentId", headers.get(groupPosition).getCategoryId());
                intent.putExtra("childName", headers.get(groupPosition).getTitle());
                intent.putExtra("fromCategory", true);
                Logger.e("CategoryList1", parentId + "\t" + headers.get(groupPosition).getTitle());

                startActivity(intent);
//                }
                return false;
            }
        });

        if (NetworkUtils.isInNetwork(SearchActivity.this)) {
            getCategoryTask();
        } else {
            errorMessage.setText(getString(R.string.error_no_internet));
            errorView.setVisibility(View.VISIBLE);
            expandableListView.setVisibility(View.GONE);
        }

    }


    private void getCategoryTask() {
        progressBar = (FrameLayout) findViewById(R.id.progress);
//        progressBar = (ProgressBar) findViewById(R.id.progress);
//        final LinearLayout errorView = (LinearLayout) findViewById(R.id.error_view);
//        final TextView errorMessage = (TextView) errorView.findViewById(R.id.tv_error_message);
        errorView.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();

        vHelper.addVolleyRequestListeners(Api.getInstance().searchUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject res = new JSONObject(response);
                    if (res.getBoolean("status")) {
                        //parsing starts
                        //  String lastUpdate = response.getString("update_date");

                        headers.clear();
                        child.clear();

                        JSONArray datas = res.getJSONArray("categories");
                        for (int i = 0; i < datas.length(); i++) {
                            JSONObject eachObj = datas.getJSONObject(i);

                            String cat_id = eachObj.getString("id");
                            String cat_name = eachObj.getString("name");
                            CategoryItem headerItem = new CategoryItem(cat_name, "");
                            headerItem.setCategoryId(Integer.parseInt(cat_id));
                            //  headerItem.setImageUrl(response.getString("product_image_dir") + eachObj.getString("image"));
                            headers.add(headerItem);

                            //check for sub category
//                            JSONArray subArray = eachObj.getJSONArray("subcategories");
//
                            List<CategoryItem> childs = new ArrayList<>();
//
//                            for (int j = 0; j < subArray.length(); j++) {
//                                JSONObject eachSub = subArray.getJSONObject(j);
//                                String subCatId = eachSub.getString("sub_id");
//                                String subCatName = eachSub.getString("sub_name");
//
//                                CategoryItem childItem = new CategoryItem(subCatName, "");
//                                childItem.setCategoryId(Integer.parseInt(cat_id));
//                                childItem.setSubCategoryId(Integer.parseInt(subCatId));
//                                //   childItem.setImageUrl(response.getString("product_image_dir") + eachSub.getString("image"));
//                                childs.add(childItem);
//
//                            }
                            child.add(childs);
                        }
                        SearchCategoryAdapter adapter = new SearchCategoryAdapter(SearchActivity.this, headers, child);
                        expandableListView.setAdapter(adapter);
                        expandableListView.setVisibility(View.VISIBLE);

                    } else {
                        errorMessage.setText(res.getString("message"));
                        errorView.setVisibility(View.VISIBLE);
                        expandableListView.setVisibility(View.GONE);

                    }

                } catch (Exception e) {
                    Logger.e("getCategoryTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                expandableListView.setVisibility(View.GONE);
                errorView.setVisibility(View.VISIBLE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMessage.setText(errorObj.getString("message"));
                } catch (Exception e) {

                    if (error instanceof NetworkError) {
                        errorMessage.setText(getString(R.string.error_no_internet));
                    } else if (error instanceof ServerError) {
                        errorMessage.setText(getString(R.string.error_server));
                    } else if (error instanceof AuthFailureError) {
                        errorMessage.setText(getString(R.string.error_authFailureError));
                    } else if (error instanceof ParseError) {
                        errorMessage.setText(getString(R.string.error_parse_error));
                    } else if (error instanceof TimeoutError) {
                        errorMessage.setText(getString(R.string.error_time_out));
                    }
                }
            }
        }, "getCategoryTask");


    }

    @Override
    protected void onDestroy() {
        VolleyHelper.getInstance(this).cancelRequest("getCategoryTask");
        super.onDestroy();
    }

}
