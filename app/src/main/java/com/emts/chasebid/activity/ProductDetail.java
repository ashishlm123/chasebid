package com.emts.chasebid.activity;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.animation.AnimatorSet;
import android.animation.ObjectAnimator;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.support.annotation.Nullable;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.daimajia.slider.library.Indicators.PagerIndicator;
import com.daimajia.slider.library.SliderLayout;
import com.daimajia.slider.library.SliderTypes.BaseSliderView;
import com.daimajia.slider.library.SliderTypes.DefaultSliderView;
import com.emts.chasebid.R;
import com.emts.chasebid.adapters.LiveProductAdapter;
import com.emts.chasebid.app.ChaseBidApp;
import com.emts.chasebid.customviews.ProgressDialog;
import com.emts.chasebid.helper.AlertUtils;
import com.emts.chasebid.helper.Api;
import com.emts.chasebid.helper.ImageHelper;
import com.emts.chasebid.helper.Logger;
import com.emts.chasebid.helper.NetworkUtils;
import com.emts.chasebid.helper.PreferenceHelper;
import com.emts.chasebid.helper.Utils;
import com.emts.chasebid.helper.VolleyHelper;
import com.emts.chasebid.model.Product;
import com.emts.chasebid.model.WonAuction;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.facebook.share.Sharer;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;
import com.google.android.gms.plus.PlusShare;
import com.squareup.picasso.Picasso;
import com.twitter.sdk.android.Twitter;
import com.twitter.sdk.android.core.Callback;
import com.twitter.sdk.android.core.Result;
import com.twitter.sdk.android.core.TwitterAuthConfig;
import com.twitter.sdk.android.core.TwitterException;
import com.twitter.sdk.android.core.TwitterSession;
import com.twitter.sdk.android.core.models.Tweet;
import com.twitter.sdk.android.core.services.StatusesService;
import com.twitter.sdk.android.tweetcomposer.TweetComposer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import io.fabric.sdk.android.Fabric;

/**
 * Created by User on 2016-06-30.
 */
public class ProductDetail extends AppCompatActivity {
    private static final int REQUEST_BUY_NOW_CONFIRM = 935;
    private static final int TWEET_COMPOSER_REQUEST_CODE = 1;
    NestedScrollView contentView;
    ProgressDialog pDialog;
    CallbackManager callbackManager;
    ShareDialog shareDialog;

    //    FrameLayout progressDesc, progressBidHistory,progressBar;
    FrameLayout progressBar;
    ProgressBar progressDesc, progressBidHistory;
    long productId;
    static Product product;
    static WonAuction won, current;
    PreferenceHelper preferenceHelper;

    ArrayList<Product> moreList = new ArrayList<>();
    LiveProductAdapter moreAuctionAdapter;
    RecyclerView moreAuctions;
    //    ProgressBar progressBar;
    TextView errorText;

    CardView cardBidHistory, cardLiveProduct, cardClosedProduct, cardCurrentWinning;
    LinearLayout holderBidHistory;
    static LinearLayout placeBidLayout;
    EditText etSinglePrice, etRangeStart, etRangeEnd;
    ImageView favIcon;
    static long serverTime;
    int position;
    int limit = 6;
    int offset = 0;

    static boolean last5minute = false;
    public static TextView pTimer;
    //    public static long elapsedTime;
    static Timer timer;
//    static boolean auctionCommenced = false;

    Handler mainHandler;
    LinearLayoutManager layoutManager;
    ScheduledExecutorService service;

    boolean isClosed = false;
    boolean isCurrent = false;

    //    Socket socketJs;
    ImageView winnerImg;
    TextView winnerName;
    TextView winnerAddress;
    String profileImageDir = "";

    TextView tvNormalCredit, tvBonusPoints;
    LinearLayout holderUserPoints;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        preferenceHelper = PreferenceHelper.getInstance(getApplicationContext());
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);


        //twitter
        String twitterConsumerKey = preferenceHelper.getString(PreferenceHelper.TWITTER_APP_KEY, getString(R.string.twitter_consumer_key));
        String twitterConsumerSecret = preferenceHelper.getString(PreferenceHelper.TWITTER_APP_SECRET, getString(R.string.twitter_consumer_secret));

        TwitterAuthConfig authConfig = new TwitterAuthConfig(
                twitterConsumerKey, twitterConsumerSecret);
        Fabric.with(this, new Twitter(authConfig));

        setContentView(R.layout.activity_product_detail);


        Intent intent = getIntent();
        if (intent == null) return;
        productId = intent.getLongExtra("product_id", 0);
        serverTime = intent.getLongExtra("serverTime", 0);
        position = intent.getIntExtra("position", -1);
        product = (Product) intent.getSerializableExtra("product");

        won = (WonAuction) intent.getSerializableExtra("won");
        isClosed = intent.getBooleanExtra("is_closed", false);

        contentView = (NestedScrollView) findViewById(R.id.nested_scroll);
        cardClosedProduct = (CardView) findViewById(R.id.closed_auc_view);
        cardLiveProduct = (CardView) findViewById(R.id.live_auc_view);
        cardCurrentWinning = (CardView) findViewById(R.id.current_winning_view);

        current = (WonAuction) intent.getSerializableExtra("current");
        isCurrent = intent.getBooleanExtra("is_current", false);
        profileImageDir = intent.getStringExtra("profile_dir");

        winnerImg = cardCurrentWinning.findViewById(R.id.current_winner_image);
        winnerName = cardCurrentWinning.findViewById(R.id.tv_current_winner_name);
        winnerAddress = cardCurrentWinning.findViewById(R.id.address);
        if (!isClosed) {
            setCurrentWinnerData();
        }

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.setMessage(getString(R.string.loading));

        if (product != null) {
            initViews(product);
        } else {
            contentView.setVisibility(View.GONE);
            pDialog.show();
        }

        if (NetworkUtils.isInNetwork(getApplicationContext())) {
            if (isClosed) {
                pDialog.show();
                closedProductDetailTask(productId);
            } else {
                productDetailTask(productId);
            }
        } else {
            AlertUtils.showSnack(ProductDetail.this, errorText, getString(R.string.error_no_internet));
        }
    }

    private void setCurrentWinnerData() {
        if (current == null) {
            current = new WonAuction();

            current.setWinnerName("N/A");
            current.setWinnerUrl("");
            current.setAddress("N/A");
        }

        if (!TextUtils.isEmpty(current.getWinnerUrl())) {
            Picasso.with(ProductDetail.this).load(current.getWinnerUrl()).into(winnerImg);
        } else {
            Picasso.with(ProductDetail.this).load(ImageHelper.getDefaultUserImage(getApplicationContext(),
                    current.getWinnerGender())).into(winnerImg);
        }
        winnerName.setText(current.getWinnerName());
        winnerAddress.setText(current.getAddress());

        cardCurrentWinning.setVisibility(View.VISIBLE);
    }

    private void closedProductDetailTask(long productId) {

        if (progressDesc != null) {
            progressDesc.setVisibility(View.VISIBLE);
        }
        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        if (preferenceHelper.isLogin()) {
            postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postParams.put("token", preferenceHelper.getToken());
        }
        postParams.put("product_id", String.valueOf(productId));
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().closedProductDetailApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                cardClosedProduct.setVisibility(View.VISIBLE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {

                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

                        JSONObject eachObj = resObj.getJSONObject("auc_data");
                        Product productGet = new Product();

                        productGet.setpId(Long.parseLong(eachObj.getString("product_id")));

                        productGet.setpPrice(Double.parseDouble(eachObj.getString("price")));
                        productGet.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                        productGet.setShippingCost(eachObj.getString("shipping_cost"));
                        productGet.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                        productGet.setDesc(eachObj.getString("description"));
                        productGet.setpName(eachObj.getString("name"));

//                        if (TextUtils.isEmpty(eachObj.getString("current_winner_name").trim())) {
                        ((TextView) findViewById(R.id.tv_closed_winner_name)).setText(eachObj.getString("first_name") + " " + eachObj.getString("last_name"));
//                        } else {
//                            ((TextView) findViewById(R.id.tv_closed_winner_name)).setText(eachObj.getString("current_winner_name"));
//                        }

                        ((TextView) findViewById(R.id.closed_total_credit)).setText(eachObj.getString("total_bids"));
                        ((TextView) findViewById(R.id.closed_winning_bid)).setText(eachObj.getString("current_winner_amount"));

                        double totalSaving = ((Double.parseDouble(eachObj.getString("price")) - (Double.parseDouble(eachObj.getString("current_winner_amount")) + Double.parseDouble(eachObj.getString("shipping_cost")))) / (Double.parseDouble(eachObj.getString("price")))) * 100;
                        ((TextView) findViewById(R.id.closed_total_saving)).setText(Utils.formatAmount(totalSaving) + " %");


                        if (!TextUtils.isEmpty(eachObj.getString("image"))) {
                            Picasso.with(getApplicationContext()).load(eachObj.getString("image"))
                                    .into((ImageView) findViewById(R.id.closed_winner_image));
                        } else {
                            try {
                                Picasso.with(ProductDetail.this).load(ImageHelper.getDefaultUserImage(getApplicationContext(),
                                        eachObj.getString("gender"))).into((ImageView) findViewById(R.id.closed_winner_image));
                            } catch (Exception e) {
                                Logger.e("winner image ex", "here it is: " + e.getMessage());
                            }
                        }

                        if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                            productGet.setIs_buy_now(false);
                        } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                            productGet.setIs_buy_now(true);
                        }


                        Date startTime = sdf.parse(eachObj.getString("start_date"));
                        productGet.setStartTime(startTime.getTime());

                        Date endDate = sdf.parse(eachObj.getString("end_date"));
                        productGet.setEndTime(endDate.getTime());

                        ArrayList<String> images = new ArrayList<String>();
                        if (!TextUtils.isEmpty(eachObj.getString("image1"))) {
                            String images1 = resObj.getString("img_dir") + eachObj.getString("image1");
                            images.add(images1);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image2"))) {
                            String images2 = resObj.getString("img_dir") + eachObj.getString("image2");
                            images.add(images2);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image3"))) {
                            String images3 = resObj.getString("img_dir") + eachObj.getString("image3");
                            images.add(images3);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image4"))) {
                            String images4 = resObj.getString("img_dir") + eachObj.getString("image4");
                            images.add(images4);
                        }
                        if (product == null) {
                            product = productGet;
                            initViews(product);
                        }

                        setResponseData(productGet, images);
                        contentView.setVisibility(View.VISIBLE);
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }

                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            getMoreAuctionTask(limit, offset);
                        }
                    }
                    progressDesc.setVisibility(View.GONE);
                } catch (JSONException e) {
                    Logger.e("closedProductDetailTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("closedProductDetailTask parse ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                String errorMsg = getString(R.string.error_loading_product);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("onError Exception", e.getMessage() + "");
                }

                if (pDialog != null) {
                    pDialog.dismiss();
                    AlertUtils.showAlertMessage(ProductDetail.this, getString(R.string.title_error), errorMsg);
                }
            }
        }, "closedProductDetailTask");

    }


    public void initViews(final Product product) {
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        ((TextView) toolbar.findViewById(R.id.custom_toolbar_title)).setText(product.getpName());
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onBackPressed();
            }
        });
//        toolbar.inflateMenu(R.menu.menu_share);
//        toolbar.setOnMenuItemClickListener(new Toolbar.OnMenuItemClickListener() {
//            @Override
//            public boolean onMenuItemClick(MenuItem item) {
//                if (item.getItemId() == R.id.fb_share) {
//                    fbShareTask();
//                } else if (item.getItemId() == R.id.google_share) {
//                    googleshareTask();
//                } else if (item.getItemId() == R.id.twitter_share) {
//                    twitterShareTask();
//                }
//                return false;
//            }
//        });
        tvNormalCredit = (TextView) findViewById(R.id.normal_bid_credit);
        tvBonusPoints = (TextView) findViewById(R.id.bonus_point);
        holderUserPoints = (LinearLayout) findViewById(R.id.holder_user_points);

        if (preferenceHelper.isLogin()) {
            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
            holderUserPoints.setVisibility(View.VISIBLE);
        } else {
            holderUserPoints.setVisibility(View.GONE);
        }

        placeBidLayout = (LinearLayout) findViewById(R.id.place_bid_layout);
        placeBidLayout.setVisibility(View.VISIBLE);
        //bid history
        cardBidHistory = (CardView) findViewById(R.id.bidding_cartView);
        holderBidHistory = (LinearLayout) findViewById(R.id.holder_bid_history);
        if (preferenceHelper.isLogin()) {
            cardBidHistory.setVisibility(View.VISIBLE);
        } else {
            cardBidHistory.setVisibility(View.GONE);
        }


        if (isClosed) {
            cardBidHistory.setVisibility(View.GONE);
            cardLiveProduct.setVisibility(View.GONE);
            placeBidLayout.setVisibility(View.GONE);
//            cardCurrentWinning.setVisibility(View.GONE);
            ((TextView) findViewById(R.id.tv_closed_product_name)).setText(product.getpName());
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            String enddate = sdf.format(product.getEndTime());
            ((TextView) findViewById(R.id.closed_auc_ended)).setText(enddate);
            ((TextView) findViewById(R.id.closed_retial_price)).setText(preferenceHelper.getCurrency() + " " + Utils.formatAmount(product.getpPrice()));

        } else {
            cardClosedProduct.setVisibility(View.GONE);
            cardLiveProduct.setVisibility(View.VISIBLE);
            placeBidLayout.setVisibility(View.VISIBLE);
//            if (isCurrent) {
//                cardCurrentWinning.setVisibility(View.VISIBLE);
//            } else {
//                cardCurrentWinning.setVisibility(View.GONE);
//            }


        }

//
        progressDesc = (ProgressBar) findViewById(R.id.progress_desc);
        progressBidHistory = (ProgressBar) findViewById(R.id.progress_bid_history);
//        progressDesc = (FrameLayout) findViewById(R.id.progress_desc);
//        progressBidHistory = (FrameLayout) findViewById(R.id.progress_bid_history);

        ((TextView) findViewById(R.id.tv_product_name)).setText(product.getpName());
        ((TextView) findViewById(R.id.tv_summary_desc)).setText(product.getShortDesc());

        if (product.getBidFee() > 1) {
            ((TextView) findViewById(R.id.normal_bid_credit_det)).setText(product.getBidFee() +
                    " " + getString(R.string.credits));
        } else {
            ((TextView) findViewById(R.id.normal_bid_credit_det)).setText(product.getBidFee() +
                    " " + getString(R.string.title_credit));
        }

        ((TextView) findViewById(R.id.retial_price)).setText(preferenceHelper.getCurrency() +
                " " + Utils.formatAmount(product.getpPrice()));

        favIcon = (ImageView) findViewById(R.id.set_fav);
        assert favIcon != null;

        if (product.isFavourite()) {
            favIcon.setImageResource(R.drawable.ic_favorite_selected);
        } else {
            favIcon.setImageResource(R.drawable.ic_favorite);
        }
        favIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    if (PreferenceHelper.getInstance(ProductDetail.this).isLogin()) {
                        animatePhotoLike(favIcon, findViewById(R.id.like_animator_bg));

                        setFavouriteTask(product.isFavourite(),
                                productId, view);

                        if (product.isFavourite()) {
                            ((ImageView) view).setImageResource(R.drawable.ic_favorite);
                        } else {
                            ((ImageView) view).setImageResource(R.drawable.ic_favorite_selected);
                        }
                    } else {
                        AlertUtils.showSnack(ProductDetail.this, view, getString(R.string.login_to_add_fav));
                    }
                } else {
                    AlertUtils.showSnack(ProductDetail.this, view, getString(R.string.check_internet));
                }
            }
        });

        //timer type
        pTimer = (TextView) findViewById(R.id.item_timer);

        if (serverTime < product.getEndTime()) {
            getRemainingTime(product.getEndTime());
            pTimer.setVisibility(View.VISIBLE);
        } else {
            pTimer.setTextColor(Color.RED);
            pTimer.setText(R.string.auc_closed);
            pTimer.setVisibility(View.VISIBLE);
        }

        //init click listener
        //place bid
        etSinglePrice = (EditText) findViewById(R.id.single_bid_price);
        TextView btnSingleBidPlace = (TextView) findViewById(R.id.single_bid);

        assert btnSingleBidPlace != null;
        btnSingleBidPlace.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PreferenceHelper.getInstance(ProductDetail.this).isLogin()) {

                    if (!NetworkUtils.isInNetwork(ProductDetail.this)) {
                        showAlert(getString(R.string.error_no_internet));
                        return;
                    }

                    if (!TextUtils.isEmpty(etSinglePrice.getText().toString().trim())) {
                        if (Double.parseDouble(etSinglePrice.getText().toString().trim()) > product.getMinBid()) {
                            singlePlaceBidTask(etSinglePrice.getText().toString().trim());
                        } else {
                            showAlert(getString(R.string.minimum_bid_amount) + product.getMinBid());
                        }
                    } else {
                        showAlert(getString(R.string.enter_bid_amt));
                    }
                } else {
                    startActivity(new Intent(ProductDetail.this, LoginActivity.class));
                }
            }
        });

        etRangeStart = (EditText) findViewById(R.id.et_range_start);
        etRangeEnd = (EditText) findViewById(R.id.et_range_end);
        TextView btnPlaceBidRange = (TextView) findViewById(R.id.btn_bid_range);
        assert btnPlaceBidRange != null;
        btnPlaceBidRange.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (PreferenceHelper.getInstance(ProductDetail.this).isLogin()) {

                    if (!NetworkUtils.isInNetwork(ProductDetail.this)) {
                        showAlert(getString(R.string.error_no_internet));
                        return;
                    }

                    if (!TextUtils.isEmpty(etRangeStart.getText().toString().trim()) &&
                            !TextUtils.isEmpty(etRangeEnd.getText().toString().trim())) {
                        if (Double.parseDouble(etRangeStart.getText().toString().trim()) > product.getMinBid()) {
                            if (Double.parseDouble(etRangeStart.getText().toString().trim()) <
                                    Double.parseDouble(etRangeEnd.getText().toString().trim())) {
                                multiplePlaceBidRange(etRangeStart.getText().toString().trim(), etRangeEnd.getText().toString().trim());
                            } else {
                                showAlert(getString(R.string.enter_proper_bid));
                            }
                        } else {
                            showAlert(getString(R.string.minimum_bid_amount) + product.getMinBid());
                        }
                    } else {
                        showAlert(getString(R.string.enter_bid_range));
                    }
                } else {
                    startActivity(new Intent(ProductDetail.this, LoginActivity.class));
                }
            }
        });

        //refresh bid credit
        ImageView refreshBidHistory = (ImageView) findViewById(R.id.bid_history_refresh);
        assert refreshBidHistory != null;
        refreshBidHistory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                    getBidHistoryTask(productId, String.valueOf(preferenceHelper.getUserId()), preferenceHelper.getToken());
                } else {
                    Toast.makeText(ProductDetail.this, getString(R.string.error_no_internet), Toast.LENGTH_SHORT).show();
                }
            }
        });

        progressBar = (FrameLayout) findViewById(R.id.live_auction_progress);
//        progressBar = (ProgressBar) findViewById(R.id.live_auction_progress);
        errorText = (TextView) findViewById(R.id.error_tv_live_auction);
        //get past moreList task
        moreAuctions = (RecyclerView) findViewById(R.id.recycler_more_auctions);
        layoutManager = new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.HORIZONTAL, false);
        moreAuctions.setLayoutManager(layoutManager);
        moreAuctions.setNestedScrollingEnabled(false);
        moreAuctions.setItemAnimator(null);
        moreAuctionAdapter = new LiveProductAdapter(ProductDetail.this, moreList);
        moreAuctions.setAdapter(moreAuctionAdapter);

        moreAuctionAdapter.setOnRecyclerViewItemClickListener(new LiveProductAdapter.OnRecyclerViewItemClickListener() {
            @Override
            public void onRecyclerViewItemClicked(View view, int position, long id) {
                Intent intent = new Intent(ProductDetail.this, ProductDetail.class);
                intent.putExtra("product_id", id);
                intent.putExtra("serverTime", serverTime);
                intent.putExtra("position", position);
                intent.putExtra("product", moreList.get(position));
                startActivity(intent);
            }
        });

        mainHandler = new Handler(Looper.getMainLooper());
        service = Executors.newSingleThreadScheduledExecutor();

//        if (current != null) {
//            if (!TextUtils.isEmpty(current.getWinnerUrl())) {
//                Picasso.with(ProductDetail.this).load(current.getWinnerUrl()).into(winnerImg);
//            }
//            winnerName.setText(current.getWinnerName());
//            winnerAddress.setText(current.getAddress());
//        }

        //buy now
        CardView cardBuyNow = (CardView) findViewById(R.id.card_buy_now);
        if (product.is_buy_now() && !isClosed) {
            cardBuyNow.setVisibility(View.VISIBLE);
            TextView buyNowPrice = (TextView) findViewById(R.id.tv_buy_now_price);
            buyNowPrice.setText(preferenceHelper.getCurrency() + " " + Utils.formatAmount(product.getpPrice()));
            TextView btnBuyNow = (TextView) findViewById(R.id.btn_buy_now);
            btnBuyNow.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    if (preferenceHelper.isLogin()) {
                        Intent intent = new Intent(getApplicationContext(), PaymentConfirmActivity.class);
                        intent.putExtra("product_id", product.getpId());
                        intent.putExtra("buyNow", true);
                        startActivityForResult(intent, REQUEST_BUY_NOW_CONFIRM);
                    } else {
                        Intent intent1 = new Intent(getApplicationContext(), LoginActivity.class);
                        startActivity(intent1);
                    }
                }
            });
        } else {
            cardBuyNow.setVisibility(View.GONE);
        }

        //more on more auctions
        TextView tvMoreLiveAuction = (TextView) findViewById(R.id.more_live_auctions);
        tvMoreLiveAuction.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getApplicationContext(), SearchProductListingActivity.class);
                intent.putExtra("moreLive", true);
                startActivity(intent);
            }
        });
    }

    private void twitterShareTask() {

        String tweetUrl = "https://twitter.com/intent/tweet?text=WRITE YOUR MESSAGE HERE &url="
                + "https://www.google.com";
        Uri uri = Uri.parse(tweetUrl);
        startActivity(new Intent(Intent.ACTION_VIEW, uri));
//        postTweet();
//        postTweetManually();


    }

    private void postTweet() {
       /* TweetComposer.Builder builder = new TweetComposer.Builder(this)
                .text("just setting up my Fabric.");
        Intent twitterIntent = builder.createIntent();
        startActivityForResult(twitterIntent, REQUEST_TWEET_POST);*/

        Intent intent = null;
        try {
            intent = new TweetComposer.Builder(this)
                    .text("Tweet from Fabric!")
                    .url(new URL("http://www.twitter.com"))
                    .createIntent();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        }
        startActivityForResult(intent, TWEET_COMPOSER_REQUEST_CODE);
    }

    private void postTweetManually() {
        TwitterSession twitterSession = Twitter.getSessionManager().getActiveSession();
        StatusesService statusesService = Twitter.getApiClient(twitterSession).getStatusesService();
        String username = Twitter.getSessionManager().getActiveSession().getUserName();
        statusesService.update("@" + username + "Manually update on twitter1", 1L, true, 0.0d, 0.0d, "", true, true, new Callback<Tweet>() {
            @Override
            public void success(Result<Tweet> result) {
                Toast.makeText(ProductDetail.this, "Tweet Updated", Toast.LENGTH_LONG).show();
                Log.e("Tweet Updated", result.data.user.name);
            }

            @Override
            public void failure(TwitterException e) {
                Log.e("Tweet Update Failed", e.getMessage());
            }
        });
    }

    private void fbShareTask() {
        if (ShareDialog.canShow(ShareLinkContent.class)) {
            ShareLinkContent linkContent = new ShareLinkContent.Builder()
                    .setContentUrl(Uri.parse("http://developers.facebook.com/android"))
                    .build();
            shareDialog.show(linkContent);
            shareDialog.registerCallback(callbackManager, new FacebookCallback<Sharer.Result>() {
                @Override
                public void onSuccess(Sharer.Result result) {
                    Logger.e("post", "success");
                }

                @Override
                public void onCancel() {

                }

                @Override
                public void onError(FacebookException error) {
                    Logger.e("error", "not success");
                }
            });

        }


    }

//        ShareButton shareButton = (ShareButton) findViewById(R.id.fb_share_button);
//        ShareLinkContent content = new ShareLinkContent.Builder()
//                .setContentUrl(Uri.parse("https://developers.facebook.com"))
//                .build();
//
//        shareButton.setShareContent(content);

    private void googleshareTask() {
//
////        // Launch the Google+ share dialog with attribution to your app.
        Intent shareIntent = new PlusShare.Builder(this)
                .setText("Check out: http://example.com/cheesecake/lemon")
                .setType("text/plain")
                .setContentUrl(Uri.parse("http://example.com/cheesecake/lemon"))
                .setContentDeepLinkId(String.valueOf(Uri.parse("http://example.com/cheesecake/lemon")))
                .getIntent();


        startActivityForResult(shareIntent, 0);


//        PlusShare.Builder share = new PlusShare.Builder(this);
//        share.setText("hello everyone!");
//        share.setContentUrl(Uri.parse("http://stackoverflow.com"));
//        share.setType("text/plain");
//        startActivityForResult(share.getIntent(), 0);

    }

    public void productDetailTask(final double pId) {
        if (progressDesc != null) {
            progressDesc.setVisibility(View.VISIBLE);
        }

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        if (preferenceHelper.isLogin()) {
            postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postParams.put("token", preferenceHelper.getToken());
        }
        postParams.put("product_id", String.valueOf(pId));
        postParams.put("limit", String.valueOf(limit));
        postParams.put("offset", String.valueOf(offset));

        vHelper.addVolleyRequestListeners(Api.getInstance().productDetailApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getString("status").equalsIgnoreCase("success")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(resObj.getString("server_time"));
                        serverTime = serverDate.getTime();
//                        if (resObj.getJSONArray("auc_data").length() == 0) {
//                            errorText.setText("No product");
//                        }
                        JSONObject eachObj = resObj.getJSONObject("auc_data");
                        Product productGet = new Product();

                        productGet.setpId(Long.parseLong(eachObj.getString("product_id")));
                        productGet.setpPrice(Double.parseDouble(eachObj.getString("price")));
                        productGet.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                        productGet.setShippingCost(eachObj.getString("shipping_cost"));
                        productGet.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                        productGet.setDesc(eachObj.getString("description"));
                        productGet.setpName(eachObj.getString("name"));
                        if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                            productGet.setIs_buy_now(false);
                        } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                            productGet.setIs_buy_now(true);
                        }
                        productGet.setFavourite(eachObj.getBoolean("watch"));

                        Date startTime = sdf.parse(eachObj.getString("start_date"));
                        productGet.setStartTime(startTime.getTime());

                        Date endDate = sdf.parse(eachObj.getString("end_date"));
                        productGet.setEndTime(endDate.getTime());

                        ArrayList<String> images = new ArrayList<String>();
                        if (!TextUtils.isEmpty(eachObj.getString("image1"))) {
                            String images1 = eachObj.getString("image1");
                            images.add(images1);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image2"))) {
                            String images2 = eachObj.getString("image2");
                            images.add(images2);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image3"))) {
                            String images3 = eachObj.getString("image3");
                            images.add(images3);
                        }
                        if (!TextUtils.isEmpty(eachObj.getString("image4"))) {
                            String images4 = eachObj.getString("image4");
                            images.add(images4);
                        }
                        try {
                            JSONObject winnerObject = resObj.getJSONObject("winner_data");
                            if (current != null) {
                                current.setWinnerName(winnerObject.getString("first_name") + " "
                                        + winnerObject.getString("last_name"));
                                if (!TextUtils.isEmpty(winnerObject.getString("image"))) {
                                    current.setWinnerUrl(resObj.getString("img_dir"));
                                }
                                current.setWinnerGender(winnerObject.getString("gender"));
                                current.setAddress(winnerObject.getString("city"));
                            }
                        } catch (JSONException e) {
                            Logger.e("productDetail winner ex", e.getMessage() + " ");
                            current.setWinnerName("N/A");
                            current.setWinnerUrl("");
                            current.setAddress("N/A");
                        }
                        if (product == null) {
                            product = productGet;
                            initViews(product);
                        }
                        setResponseData(productGet, images);
                        contentView.setVisibility(View.VISIBLE);
                        if (pDialog != null) {
                            pDialog.dismiss();
                        }
//                        try {
//                            connectToServerJs();
//                        } catch (URISyntaxException e) {
//                            Logger.e("nodejs server url ex", e.getMessage() + "");
//                        }
//                        //setCurrentWinner task
                        setCurrentWinnerData();

                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            if (preferenceHelper.isLogin()) {
                                getBidHistoryTask(product.getpId(),
                                        String.valueOf(preferenceHelper.getUserId()),
                                        preferenceHelper.getToken());
                            }
                            getMoreAuctionTask(6, 0);
                        }
                    }
                    progressDesc.setVisibility(View.GONE);
                } catch (JSONException e) {
                    Logger.e("productDetailTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("productDetailTask parse ex", e.getMessage() + " ");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                String errorMsg = "Error loading product detail. Please try again !!!";
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    errorMsg = errorObj.getString("message");
                } catch (Exception e) {
                    Logger.e("onError Exception", e.getMessage() + "");
                }

                if (pDialog != null) {
                    pDialog.dismiss();
                    AlertUtils.showAlertMessage(ProductDetail.this, "Error !!!", errorMsg);
                }
            }
        }, "productDetailTask");

    }
//
//    private void connectToServerJs() throws URISyntaxException {
//        if (socketJs != null) {
//            if (socketJs.connected()) {
//                socketJs.off();
//                socketJs.disconnect();
//            }
//        }
////        String nodeServerHostUrl = "http://202.166.198.151";
////        String nodePortNo = "3030";
//
//        String nodeServerHostUrl = preferenceHelper.getString(PreferenceHelper.NODE_SEVER, "http://www.chasebid.com");
////        String nodeServerHostUrl = "http://202.166.198.151";
//        String nodePortNo = preferenceHelper.getString(PreferenceHelper.NODE_PORT, "8000");
////        socketJs = IO.socket(Api.nodeServer);
//        socketJs = IO.socket(nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        Logger.e("nodeJs url", nodeServerHostUrl + ":" + nodePortNo + "/bidding_process");
//        socketJs.on(io.socket.client.Socket.EVENT_CONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def connect call", "Objent : ");
//
//                if (socketJs.connected()) {
//                    Logger.e("nodeJs connected", socketJs.toString() + "");
//                    socketJs.off();
//
//                    emitConnectAndJoinAuctionRoom(product.getpId());
//                    listenForAuctionWinner();
//                } else {
//                    try {
//                        connectToServerJs();
//                    } catch (URISyntaxException e) {
//                        //catch
//                    }
//                }
//
//            }
//
//        }).on(io.socket.client.Socket.EVENT_DISCONNECT, new Emitter.Listener() {
//            @Override
//            public void call(Object... args) {
//                Logger.e("nodeJs def. disconnect", "Object " + args.length);
//                socketJs.off();
//            }
//        });
//        socketJs.connect();
//    }
//
//    //    START NODE JS LISTENERS
//
//    public void emitBidPlaceSuccess() {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs emit for bid place success", "emitter for socket bid place success");
//            //for auction_room
//            try {
//                JSONObject roomObj = new JSONObject();
//                roomObj.put("auc_id", productId);
//                Logger.e("nodeJs bid_placed emitter data", roomObj.toString() + " **");
//
//                socketJs.emit("bid_placed", roomObj, new Ack() {
//                    @Override
//                    public void call(Object... args) {
//                        Logger.e("nodeJs auction_room emit ack", "Ack received for bid_placed :" +
//                                (args.length > 0 ? args[0].toString() : "0"));
//                    }
//                });
//            } catch (JSONException e) {
//                Logger.e("nodeJs ex23", e.getMessage() + "");
//            }
//        }
//    }
//
//    public void emitConnectAndJoinAuctionRoom(long hostId) {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs emit for connected and join auction room", "emitter for socket connected and join to auction room");
//            //for auction_room
//            try {
//                JSONObject roomObj = new JSONObject();
//                roomObj.put("aid", hostId);
//                Logger.e("nodeJs auction_room emitter data", roomObj.toString() + " **");
//
//                socketJs.emit("auction_room", roomObj, new Ack() {
//                    @Override
//                    public void call(Object... args) {
//                        Logger.e("nodeJs auction_room emit ack", "Ack received for auction_room :" +
//                                (args.length > 0 ? args[0].toString() : "0"));
//                    }
//                });
//            } catch (JSONException e) {
//                Logger.e("nodeJs ex15", e.getMessage() + "");
//            }
//        }
//    }
//
//    public void listenForAuctionWinner() {
//        if (socketJs.connected()) {
//            Logger.e("nodeJs listener for bidding_success", "listener set for winner");
//
//            socketJs.off("bidding_success");
//            socketJs.on("bidding_success", new Emitter.Listener() {
//                @Override
//                public void call(Object... args) {
//                    Logger.e("nodeJs response for listener -->", "bidding_success : " + args.length);
//
//                    if (args.length > 0) {
//                        Logger.printLongLog("nodeJs bidding_success", args[0].toString() + " **");
//                        try {
//                            final JSONObject bidPlacedObj = new JSONObject(args[0].toString());
//                            int aId = bidPlacedObj.getInt("auc_id");
//                            final String name = bidPlacedObj.getString("name");
//                            final String address = bidPlacedObj.getString("address");
//                            final String imageUrl = bidPlacedObj.getString("image");
//                            String message = bidPlacedObj.getString("message");
//                            final String gender = bidPlacedObj.optString("gender");
//
//                            try {
//                                runOnUiThread(new Runnable() {
//                                    @Override
//                                    public void run() {
//                                        winnerName.setText(name);
//                                        winnerAddress.setText(address);
//                                        if (!TextUtils.isEmpty(imageUrl)) {
//                                            Picasso.with(ProductDetail.this).load(profileImageDir + imageUrl).into(winnerImg);
//                                        } else {
//                                            Picasso.with(ProductDetail.this)
//                                                    .load(ImageHelper.getDefaultUserImage(ProductDetail.this, gender))
//                                                    .into(winnerImg);
//                                        }
//                                        if (preferenceHelper.isLogin()) {
//                                            getBidHistoryTask(product.getpId(),
//                                                    String.valueOf(preferenceHelper.getUserId()),
//                                                    preferenceHelper.getToken());
//                                        }
//                                        Logger.e("nodeJs notify auction_item_finished",
//                                                "Product sold pos:" + position);
//                                    }
//                                });
//                            } catch (Exception e) {
//                                Logger.e("nodeJs el18672", "ex :" + e.getMessage());
//                            }
//
//                        } catch (JSONException e) {
//                            Logger.e("nodeJs itemFinished listen ex", e.getMessage() + "");
//                        }
//                    }
//                }
//            });
//        } else {
//            Logger.e("nodeJs lbl 1", "not connected !!!");
//        }
//    }

    private void getBidHistoryTask(final long productId, final String userId, final String token) {
        progressBidHistory.setVisibility(View.VISIBLE);
        holderBidHistory.removeAllViews();

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("product_id", String.valueOf(productId));
        postParams.put("user_id", userId);
        postParams.put("token", token);

        vHelper.addVolleyRequestListeners(Api.getInstance().bidHistoryApi, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBidHistory.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        //render history
                        JSONArray historyArray = resObj.getJSONArray("bid_history");
                        if (historyArray.length() > 0) {
                            for (int i = 0; i < historyArray.length(); i++) {
                                JSONObject eachHistory = historyArray.getJSONObject(i);

                                LayoutInflater inflater = LayoutInflater.from(ProductDetail.this);
                                View view = inflater.inflate(R.layout.dynamic_bid_history, null, false);
                                TextView bidDate = (TextView) view.findViewById(R.id.bid_date);
                                TextView bidAmount = (TextView) view.findViewById(R.id.bid_amount);
                                TextView bidStatus = (TextView) view.findViewById(R.id.bid_status);

                                bidDate.setText(eachHistory.getString("bid_date"));
                                bidAmount.setText(preferenceHelper.getCurrency() + eachHistory.getString("userbid_bid_amt"));
                                String statusMsg = eachHistory.getString("bidstatus");
                                bidStatus.setText(statusMsg);

                                holderBidHistory.addView(view);
                            }
                        } else {
//                            showBidHistoryError(resObj.getString("message"));
                            showBidHistoryError(getString(R.string.no_bid_placed));
                        }
                    } else {
                        showBidHistoryError(resObj.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("getBidHistoryTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBidHistory.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showBidHistoryError(errorObj.getString("message"));
                } catch (Exception e) {
                    showBidHistoryError(getString(R.string.error_loading_bid_history));
                }
            }
        }, "getBidHistoryTask");

    }

    public void showBidHistoryError(String message) {
        TextView tvNoBid = new TextView(ProductDetail.this);
        LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        tvNoBid.setLayoutParams(lp);
        tvNoBid.setPadding(0, 30, 0, 30);
        tvNoBid.setText(message);
        tvNoBid.setGravity(Gravity.CENTER);
        tvNoBid.setTextColor(getResources().getColor(R.color.red));

        holderBidHistory.addView(tvNoBid);
    }

    private void multiplePlaceBidRange(final String start, final String end) {
        final ProgressDialog pDialog = new ProgressDialog(ProductDetail.this);
        pDialog.setMessage(getString(R.string.placing_bid));
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("auc_id", productId + "");
        postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        postParams.put("token", preferenceHelper.getToken());
        postParams.put("amount_f", start);
        postParams.put("amount_t", end);

        vHelper.addVolleyRequestListeners(Api.getInstance().multipleBidUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                pDialog.dismiss();
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        String c_u_t = resObj.getString("c_u_t");
                        String newbalance = resObj.getString("new_balance");
                        String auc_id = resObj.getString("auc_id");

                        if (c_u_t.equalsIgnoreCase("normal_credit")) {
                            preferenceHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, newbalance).apply();
                        } else if (c_u_t.equalsIgnoreCase("bonus_credit")) {
                            preferenceHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, newbalance).apply();
                        }
//                                else if (c_u_t.equalsIgnoreCase("free_credit")) {
//                                  preferenceHelper.edit().putString(PreferenceHelper.FREE_BAL, newbalance).apply();
//                                }

                        //update app bar
                        if (preferenceHelper.isLogin()) {
                            tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
                            tvBonusPoints.setText(getString(R.string.bonus_points) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
                            holderUserPoints.setVisibility(View.VISIBLE);
                        } else {
                            holderUserPoints.setVisibility(View.GONE);
                        }

//                        emitBidPlaceSuccess();

                        if (NetworkUtils.isInNetwork(getApplicationContext())) {
                            getBidHistoryTask(productId, String.valueOf(preferenceHelper.getUserId()),
                                    preferenceHelper.getString(PreferenceHelper.TOKEN, "null"));
                        }
                        etRangeStart.setText("");
                        etRangeEnd.setText("");

                        showAlert(resObj.getString("message"));

                        ChaseBidApp.getInstance().afTrackBidding(String.valueOf(productId),
                                start + " to " + end, new Date().toString(), "multiple_bid");
                    } else {
                        showAlert(resObj.getString("message"));
                    }
                } catch (JSONException e) {
                    Logger.e("multiplePlaceBidTask json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                pDialog.dismiss();
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    showAlert(errorObj.getString("message"));
                } catch (Exception e) {
                    if (error instanceof NetworkError) {
                        showAlert(getString(R.string.error_no_internet));
                    } else if (error instanceof ServerError) {
                        showAlert(getString(R.string.error_server));
                    } else if (error instanceof AuthFailureError) {
                        showAlert(getString(R.string.error_authFailureError));
                    } else if (error instanceof ParseError) {
                        showAlert(getString(R.string.error_parse_error));
                    } else if (error instanceof TimeoutError) {
                        showAlert(getString(R.string.error_time_out));
                    }
                }
            }
        }, "multiplePlaceBidTask");

    }

    private void singlePlaceBidTask(final String amount) {
        final ProgressDialog pDialog = new ProgressDialog(ProductDetail.this);
        pDialog.setMessage(getString(R.string.placing_bid));
        pDialog.setCancelable(true);
        pDialog.setCanceledOnTouchOutside(false);
        pDialog.show();

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        postParams.put("auc_id", productId + "");
        postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
        postParams.put("token", preferenceHelper.getToken());
        postParams.put("amount", amount);

        vHelper.addVolleyRequestListeners(Api.getInstance().singleBidUrl, Request.Method.POST,
                postParams, new VolleyHelper.VolleyHelperInterface() {
                    @Override
                    public void onSuccess(String response) {
                        pDialog.dismiss();
                        try {
                            JSONObject resObj = new JSONObject(response);
                            if (resObj.getBoolean("status")) {
                                String c_u_t = resObj.getString("c_u_t");
                                String newbalance = resObj.getString("new_balance");
                                String auc_id = resObj.getString("auc_id");

                                showAlert(resObj.getString("message"));

                                if (c_u_t.equalsIgnoreCase("normal_credit")) {
                                    preferenceHelper.edit().putString(PreferenceHelper.App_USER_BID_CREDITS, newbalance).apply();
                                } else if (c_u_t.equalsIgnoreCase("bonus_credit")) {
                                    preferenceHelper.edit().putString(PreferenceHelper.App_USER_BONUS_POINTS, newbalance).apply();
                                }
//                                else if (c_u_t.equalsIgnoreCase("free_credit")) {
//                                  preferenceHelper.edit().putString(PreferenceHelper.FREE_BAL, newbalance).apply();
//                                }

                                //update app bar
                                if (preferenceHelper.isLogin()) {
                                    tvNormalCredit.setText(getString(R.string.bid_credits) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BID_CREDITS, ""));
                                    tvBonusPoints.setText(getString(R.string.bonus_points) + " " + preferenceHelper.getString(PreferenceHelper.App_USER_BONUS_POINTS, ""));
                                    holderUserPoints.setVisibility(View.VISIBLE);
                                } else {
                                    holderUserPoints.setVisibility(View.GONE);
                                }

//                                emitBidPlaceSuccess();

                                if (NetworkUtils.isInNetwork(getApplicationContext())) {
                                    getBidHistoryTask(productId, String.valueOf(preferenceHelper.getUserId()),
                                            preferenceHelper.getString(PreferenceHelper.TOKEN, "null"));
                                }
                                etSinglePrice.setText("");

                                ChaseBidApp.getInstance().afTrackBidding(String.valueOf(productId),
                                        amount, new Date().toString(), "single_bid");

                            } else {
                                showAlert(resObj.getString("message"));
                            }
                        } catch (JSONException e) {
                            Logger.e("json ex", e.getMessage() + "");
                        }
                    }

                    @Override
                    public void onError(String errorResponse, VolleyError error) {
                        pDialog.dismiss();
                        try {
                            JSONObject errorObj = new JSONObject(errorResponse);
                            showAlert(errorObj.getString("message"));
                        } catch (Exception e) {
                            if (error instanceof NetworkError) {
                                showAlert(getString(R.string.error_no_internet));
                            } else if (error instanceof ServerError) {
                                showAlert(getString(R.string.error_server));
                            } else if (error instanceof AuthFailureError) {
                                showAlert(getString(R.string.error_authFailureError));
                            } else if (error instanceof ParseError) {
                                showAlert(getString(R.string.error_parse_error));
                            } else if (error instanceof TimeoutError) {
                                showAlert(getString(R.string.error_time_out));
                            }
                        }
                    }

                }, "singlePlaceBidTask");

    }

    private void animatePhotoLike(final View ivLike, View vBgLike) {
        vBgLike.setVisibility(View.VISIBLE);
        ivLike.setVisibility(View.VISIBLE);

        vBgLike.setScaleY(0.1f);
        vBgLike.setScaleX(0.1f);
        vBgLike.setAlpha(1f);
        ivLike.setScaleY(0.1f);
        ivLike.setScaleX(0.1f);

        final AnimatorSet animatorSet = new AnimatorSet();

        ObjectAnimator bgScaleYAnim = ObjectAnimator.ofFloat(vBgLike, "scaleY", 0.1f, 1f);
        bgScaleYAnim.setDuration(200);
        bgScaleYAnim.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator bgScaleXAnim = ObjectAnimator.ofFloat(vBgLike, "scaleX", 0.1f, 1f);
        bgScaleXAnim.setDuration(200);
        bgScaleXAnim.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator bgAlphaAnim = ObjectAnimator.ofFloat(vBgLike, "alpha", 1f, 0f);
        bgAlphaAnim.setDuration(200);
        bgAlphaAnim.setStartDelay(150);
        bgAlphaAnim.setInterpolator(new DecelerateInterpolator());

        ObjectAnimator imgScaleUpYAnim = ObjectAnimator.ofFloat(ivLike, "scaleY", 0.1f, 1f);
        imgScaleUpYAnim.setDuration(300);
        imgScaleUpYAnim.setInterpolator(new DecelerateInterpolator());
        ObjectAnimator imgScaleUpXAnim = ObjectAnimator.ofFloat(ivLike, "scaleX", 0.1f, 1f);
        imgScaleUpXAnim.setDuration(300);
        imgScaleUpXAnim.setInterpolator(new DecelerateInterpolator());

//        ObjectAnimator imgScaleDownYAnim = ObjectAnimator.ofFloat(ivLike, "scaleY", 1f, 0f);
//        imgScaleDownYAnim.setDuration(300);
//        imgScaleDownYAnim.setInterpolator(new AccelerateInterpolator());
//        ObjectAnimator imgScaleDownXAnim = ObjectAnimator.ofFloat(ivLike, "scaleX", 1f, 0f);
//        imgScaleDownXAnim.setDuration(300);
//        imgScaleDownXAnim.setInterpolator(new AccelerateInterpolator());

        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim);
//        animatorSet.playTogether(bgScaleYAnim, bgScaleXAnim, bgAlphaAnim, imgScaleUpYAnim, imgScaleUpXAnim);
        ((ImageView) ivLike).setImageResource(R.drawable.ic_favorite_selected);
        animatorSet.playTogether(imgScaleUpYAnim, imgScaleUpXAnim);
//        animatorSet.play(imgScaleDownYAnim).with(imgScaleDownXAnim).after(imgScaleUpYAnim);

        animatorSet.addListener(new AnimatorListenerAdapter() {
            @Override
            public void onAnimationEnd(Animator animation) {

//                AnimatorSet originalImg = new AnimatorSet();
//                ObjectAnimator imgYToOrg = ObjectAnimator.ofFloat(ivLike, "scaleY", 0f, 1f);
//                imgYToOrg.setDuration(300);
//                imgYToOrg.setInterpolator(new AccelerateInterpolator());
//                ObjectAnimator imgXToOrg = ObjectAnimator.ofFloat(ivLike, "scaleX", 0f, 1f);
//                imgXToOrg.setDuration(300);
//                imgXToOrg.setInterpolator(new AccelerateInterpolator());
//                originalImg.playTogether(imgYToOrg, imgXToOrg);
//                Logger.e("animation finidhed", "and reset started");

            }
        });
        animatorSet.start();
    }

    private void setImageTask(final ArrayList<String> images) {
        final SliderLayout imageSlider = (SliderLayout) findViewById(R.id.slider);
        final PagerIndicator pagerIndicator = (PagerIndicator) findViewById(R.id.custom_indicator);

        imageSlider.stopAutoCycle();
        imageSlider.setVisibility(View.GONE);
        for (int i = 0; i < images.size(); i++) {
            DefaultSliderView defaultSlider = new DefaultSliderView(ProductDetail.this);
            // initialize a SliderLayout
            defaultSlider
                    .image(images.get(i))
                    .setScaleType(BaseSliderView.ScaleType.CenterInside);

            imageSlider.addSlider(defaultSlider);
        }

        if (images.size() > 1) {
            // play from first image, when all images loaded
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        imageSlider.setPresetTransformer(SliderLayout.Transformer.Default);
                        imageSlider.setCustomIndicator(pagerIndicator);
//                        imageSlider.setCurrentPosition(0, false);
//                        imageSlider.setCurrentPosition(0, false); //this hangs and gives ANR
                        //so instead of going to first go to last and move to next
                        imageSlider.setCurrentPosition(images.size() - 1, false);
                        imageSlider.moveNextPosition();
                        pagerIndicator.onPageSelected(0);

                        imageSlider.setDuration(4000);
                        imageSlider.setVisibility(View.VISIBLE);

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                try {
                                    imageSlider.startAutoCycle();
                                } catch (Exception e) {
                                }
                            }
                        }, 4400);
                    } catch (Exception e) {
                        Logger.e("reflection ex", e.getMessage() + " ");
                    }
                }
            }, 900);

        } else {
            imageSlider.setVisibility(View.VISIBLE);
            pagerIndicator.setVisibility(View.GONE);
            imageSlider.setIndicatorVisibility(PagerIndicator.IndicatorVisibility.Invisible);
            imageSlider.stopAutoCycle();
        }
    }

    private void setResponseData(Product product, ArrayList<String> images) {
        setImageTask(images);

        try {
            ((TextView) findViewById(R.id.p_desc)).setText(Html.fromHtml(product.getDesc()));
        } catch (Exception e) {
            Logger.e("ex in case of error", e.getMessage() + "");
        }
    }

    public void showAlert(String message) {
        AlertDialog.Builder errorAlert = new AlertDialog.Builder(ProductDetail.this);
        errorAlert.setTitle(R.string.title_alert);
        errorAlert.setMessage(Html.fromHtml(message));
        errorAlert.setPositiveButton(getString(R.string.alert_ok), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        });
        errorAlert.show();
    }

    public static void getRemainingTime(long endDate) {
//        auctionCommenced = false;
        if (serverTime < endDate) {
//            auctionCommenced = true;
            startTimer();
        } else {
            pTimer.setText(R.string.auc_closed);
            pTimer.setTextColor(Color.RED);
        }
    }

    public static void startTimer() {
        timer = new java.util.Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                serverTime += 1000;
                mHandler.obtainMessage(1).sendToTarget();
            }
        }, 0, 1000);
    }

    public static final Handler mHandler = new Handler() {
        public void handleMessage(Message msg) {
            String timeLefts = null;
            try {
                timeLefts = ddHHMMSS(product.getEndTime() - serverTime);
                if (!timeLefts.equals("")) {
//                if (auctionCommenced) {
//                    placeBidLayout.setVisibility(View.GONE);
//                    pTimer.setText("Auction Commences:\n" + timeLefts);
//                    pTimer.setTextColor(Color.parseColor("#8a000000"));
//                } else {
                    if (last5minute) {
                        pTimer.setTextColor(Color.RED);
                    } else {
                        pTimer.setTextColor(Color.parseColor("#8a000000"));
                    }
                    pTimer.setText(timeLefts);
//                }
                } else {
                    pTimer.setText(R.string.auc_closed);
                    pTimer.setTextColor(Color.RED);
                    timer.cancel();
                }
            } catch (ParseException e) {
                Logger.e("timer ex", e.getMessage() + " **");
            }
        }
    };

    public static String ddHHMMSS(long elapsedTimes) throws ParseException {
//        Logger.e("Check serverTime", elapsedTimes + "");
//        if (!auctionCommenced && elapsedTimes <= 0) {
//            return "";
//        }

        long days, hours, minute, second;
        days = TimeUnit.MILLISECONDS.toDays(elapsedTimes);
        elapsedTimes -= TimeUnit.DAYS.toMillis(days);
        hours = TimeUnit.MILLISECONDS.toHours(elapsedTimes);
        elapsedTimes -= TimeUnit.HOURS.toMillis(hours);
        minute = TimeUnit.MILLISECONDS.toMinutes(elapsedTimes);
        elapsedTimes -= TimeUnit.MINUTES.toMillis(minute);
        second = TimeUnit.MILLISECONDS.toSeconds(elapsedTimes);
//        Logger.e("HMS", String.valueOf(second));
        String remainingTime = "";
//        if (days != 0) {
        if (days == 1) {
            remainingTime = days + "day : ";
        } else {
            remainingTime = days + "days : ";
        }
//        }
//        if (hours != 0) {
        remainingTime = remainingTime + hours + "h : ";
//        }
//        if (minute != 0) {
        remainingTime = remainingTime + minute + "m : ";
//        }
//        if (second != 0){
        remainingTime = remainingTime + second + "s";
//    }
        if (days == 0 && hours == 0 && minute < 5) {
            last5minute = true;
        } else {
            last5minute = false;
        }
        if (days <= 0 && hours <= 0 && minute <= 0 && second <= 0) {//auctionCommenced &&
//            checkHideShow();
            remainingTime = "";
            placeBidLayout.setVisibility(View.GONE);
        }

        return remainingTime;
    }

    @Override
    public void onBackPressed() {
        try {
            timer.cancel();
            service.shutdown();
        } catch (Exception e) {

        }

        Intent returnIntent = new Intent();
        returnIntent.putExtra("product", product);
        returnIntent.putExtra("position", position);
        setResult(RESULT_OK, returnIntent);
        super.onBackPressed();
    }

    private void setFavouriteTask(final boolean favourite, final long productId, final View view) {

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        HashMap<String, String> postParams = Api.getInstance().getPostParams();
        if (preferenceHelper.isLogin()) {
            postParams.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postParams.put("token", String.valueOf(preferenceHelper.getToken()));
        }
        postParams.put("product_id", String.valueOf(productId));

        vHelper.addVolleyRequestListeners(Api.getInstance().favListUrl, Request.Method.POST, postParams, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        if (favourite) {
                            product.setFavourite(false);
                        } else {
                            product.setFavourite(true);
                        }
                    } else {
                    }
                } catch (JSONException e) {
                    Logger.e("json ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                product.setFavourite(!product.isFavourite());
                if (product.isFavourite()) {
                    ((ImageView) view).setImageResource(R.drawable.ic_favorite);
                    product.setFavourite(false);
                } else {
                    ((ImageView) view).setImageResource(R.drawable.ic_favorite_selected);
                    product.setFavourite(true);
                }
            }
        }, "setFavouriteTask");
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == REQUEST_BUY_NOW_CONFIRM) {
                if (data.getBooleanExtra("buy_now_error", false)) {
                    showAlert(data.getStringExtra("buy_now_error_msg"));
                }
            } else if (requestCode == TWEET_COMPOSER_REQUEST_CODE) {
                Toast.makeText(ProductDetail.this, "Updated tweet using composer", Toast.LENGTH_SHORT).show();
                Logger.e("u r here", "u rhere");
            }

        }
    }

    @Override
    protected void onDestroy() {
        try {
            timer.cancel();
        } catch (Exception e) {

        }
        VolleyHelper.getInstance(this).cancelRequest("closedProductDetailTask");
        VolleyHelper.getInstance(this).cancelRequest("productDetailTask");
        VolleyHelper.getInstance(this).cancelRequest("getBidHistoryTask");
        VolleyHelper.getInstance(this).cancelRequest("singlePlaceBidTask");
        VolleyHelper.getInstance(this).cancelRequest("multiplePlaceBidTask");
        VolleyHelper.getInstance(this).cancelRequest("setFavouriteTask");
        VolleyHelper.getInstance(this).cancelRequest("getMoreAuctionTask");
        super.onDestroy();
    }


    public void getMoreAuctionTask(final int limit, final int offset) {
        HashMap<String, String> postMap = Api.getInstance().getPostParams();
        postMap.put("offset", String.valueOf(offset));
        postMap.put("limit", String.valueOf(limit));
        if (preferenceHelper.isLogin()) {
            postMap.put("user_id", String.valueOf(preferenceHelper.getUserId()));
            postMap.put("token", preferenceHelper.getToken());

        }
        errorText.setVisibility(View.GONE);
        progressBar.setVisibility(View.VISIBLE);

        VolleyHelper vHelper = VolleyHelper.getInstance(this);
        vHelper.addVolleyRequestListeners(Api.getInstance().getLiveAuction, Request.Method.POST, postMap, new VolleyHelper.VolleyHelperInterface() {
            @Override
            public void onSuccess(String response) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject resObj = new JSONObject(response);
                    if (resObj.getBoolean("status")) {
                        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                        Date serverDate = sdf.parse(resObj.getString("server_time"));
                        serverTime = serverDate.getTime();

                        JSONArray pArray = resObj.getJSONArray("live_auctions");
                        if (pArray.length() > 0) {
                            moreList.clear();
                            for (int i = 0; i < pArray.length(); i++) {
                                JSONObject eachObj = pArray.getJSONObject(i);

                                Product product = new Product();
                                product.setpId(Long.parseLong(eachObj.getString("product_id")));
                                product.setpName(eachObj.getString("name"));
                                product.setpPrice(Double.parseDouble(eachObj.getString("price")));
                                product.setMinBid(Float.parseFloat((eachObj.getString("min_bids_value"))));
                                product.setShippingCost(eachObj.getString("shipping_cost"));
                                product.setImageUrl(eachObj.getString("image1"));
                                product.setBidFee(Integer.parseInt(eachObj.getString("bid_fee")));
                                if (eachObj.getString("is_buy_now").equalsIgnoreCase("No")) {
                                    product.setIs_buy_now(false);
                                } else if (eachObj.getString("is_buy_now").equalsIgnoreCase("Yes")) {
                                    product.setIs_buy_now(true);
                                }

                                Date startTime = sdf.parse(eachObj.getString("start_date"));
                                product.setStartTime(startTime.getTime());

                                Date endTime = sdf.parse(eachObj.getString("end_date"));
                                product.setEndTime(endTime.getTime());

                                product.setFavourite(eachObj.getBoolean("watch"));
                                product.setMinBid(Float.parseFloat(eachObj.getString("min_bids_value")));

                                moreList.add(product);
                            }
                            moreAuctions.setVisibility(View.VISIBLE);
                            moreAuctionAdapter.setServerTime(serverTime);
                            if (service != null && !service.isShutdown()) {
                                service.scheduleAtFixedRate(runnable,
                                        0, 1000, TimeUnit.MILLISECONDS);
                            }
                            moreAuctionAdapter.notifyDataSetChanged();
                        } else {
                            errorText.setVisibility(View.VISIBLE);
                            moreAuctions.setVisibility(View.GONE);
                            errorText.setText(R.string.no_product);
                        }

                    } else {
                        moreAuctions.setVisibility(View.GONE);
                        progressBar.setVisibility(View.GONE);
                        errorText.setText(resObj.getString("error"));
                        errorText.setVisibility(View.VISIBLE);
                    }

                } catch (JSONException e) {
                    Logger.e("getMoreAuctionTask json ex", e.getMessage() + "");
                } catch (ParseException e) {
                    Logger.e("getMoreAuctionTask ParseException ex", e.getMessage() + "");
                }
            }

            @Override
            public void onError(String errorResponse, VolleyError error) {
                progressBar.setVisibility(View.GONE);
                try {
                    JSONObject errorObj = new JSONObject(errorResponse);
                    if (offset == 0) {
                        errorText.setText(errorObj.getString("message"));
                        errorText.setVisibility(View.VISIBLE);
                        moreAuctions.setVisibility(View.GONE);
                    }
                } catch (Exception e) {
                    try {
                        if (error instanceof NetworkError) {
                            errorText.setText(ProductDetail.this.getString(R.string.error_no_internet));
                        } else if (error instanceof ServerError) {
                            errorText.setText(ProductDetail.this.getString(R.string.error_server));
                        } else if (error instanceof AuthFailureError) {
                            errorText.setText(ProductDetail.this.getString(R.string.error_authFailureError));
                        } else if (error instanceof ParseError) {
                            errorText.setText(ProductDetail.this.getString(R.string.error_parse_error));
                        } else if (error instanceof TimeoutError) {
                            errorText.setText(ProductDetail.this.getString(R.string.error_time_out));
                        }
                        if (offset == 0) {
                            errorText.setVisibility(View.VISIBLE);
                            moreAuctions.setVisibility(View.GONE);
                        }
                    } catch (Exception ee) {
                    }
                }
            }
        }, "getMoreAuctionTask");
    }

    Runnable runnable = new Runnable() {
        @Override
        public void run() {
            final int firstVisible = layoutManager.findFirstVisibleItemPosition();
            final int lastVisible = layoutManager.findLastVisibleItemPosition();

            mainHandler.post(new Runnable() {
                @Override
                public void run() {
//                    serverTime = serverTime + 1000;
                    moreAuctionAdapter.setServerTime(serverTime);

                    for (int i = firstVisible; i <= lastVisible; i++) {
                        try {
                            moreAuctionAdapter.notifyItemChanged(i);
                        } catch (Exception e) {
                        }
                    }
                }
            });
        }
    };

}
