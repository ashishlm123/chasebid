package com.emts.chasebid.model;

import java.io.Serializable;

/**
 * Created by User on 2016-07-01.
 */
public class BidCredit implements Serializable{

    private int id;
    private String creditPrice;
    private int normalCredit;
    private int bonusCredit;
    private int freeCredit;

    private int requiredBid;
    private int remainingBid;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCreditPrice() {
        return creditPrice;
    }

    public void setCreditPrice(String creditPrice) {
        this.creditPrice = creditPrice;
    }

    public int getNormalCredit() {
        return normalCredit;
    }

    public void setNormalCredit(int normalCredit) {
        this.normalCredit = normalCredit;
    }

    public int getBonusCredit() {
        return bonusCredit;
    }

    public void setBonusCredit(int bonusCredit) {
        this.bonusCredit = bonusCredit;
    }

    public int getFreeCredit() {
        return freeCredit;
    }

    public void setFreeCredit(int freeCredit) {
        this.freeCredit = freeCredit;
    }

    public int getRequiredBid() {
        return requiredBid;
    }

    public void setRequiredBid(int requiredBid) {
        this.requiredBid = requiredBid;
    }

    public int getRemainingBid() {
        return remainingBid;
    }

    public void setRemainingBid(int remainingBid) {
        this.remainingBid = remainingBid;
    }
}
