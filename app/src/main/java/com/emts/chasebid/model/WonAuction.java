package com.emts.chasebid.model;

import java.io.Serializable;

/**
 * Created by Srijana on 3/29/2017.
 */

public class WonAuction extends Product implements Serializable {
    private String winnerUrl;
    private String winnerName;
    private String winnerGender;
    private String winningBidAmount;
    private String totalCreditUsed;
    private String payment_status;
    private String shippingStatus;
    private String address;

    public String getWinnerUrl() {
        return winnerUrl;
    }

    public void setWinnerUrl(String winnerUrl) {
        this.winnerUrl = winnerUrl;
    }

    public String getWinnerName() {
        return winnerName;
    }

    public void setWinnerName(String winnerName) {
        this.winnerName = winnerName;
    }

    public String getWinningBidAmount() {
        return winningBidAmount;
    }

    public void setWinningBidAmount(String winningBidAmount) {
        this.winningBidAmount = winningBidAmount;
    }

    public String getTotalCreditUsed() {
        return totalCreditUsed;
    }

    public void setTotalCreditUsed(String totalCreditUsed) {
        this.totalCreditUsed = totalCreditUsed;
    }

    public String getShippingStatus() {
        return shippingStatus;
    }

    public void setShippingStatus(String shippingStatus) {
        this.shippingStatus = shippingStatus;
    }

    public String getPayment_status() {
        return payment_status;
    }

    public void setPayment_status(String payment_status) {
        this.payment_status = payment_status;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getWinnerGender() {
        return winnerGender;
    }

    public void setWinnerGender(String winnerGender) {
        this.winnerGender = winnerGender;
    }
}
