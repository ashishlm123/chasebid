package com.emts.chasebid.model;

/**
 * Created by User on 2016-07-22.
 */
public class BuyCredit extends BidCredit {
    private String packageId;
    private String packageName;

    private String[] paymentId;
    private String[] paymentLogo;
    private String[] paymentGateway;

    public String getPackageId() {
        return packageId;
    }

    public void setPackageId(String packageId) {
        this.packageId = packageId;
    }

    public String[] getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String[] paymentId) {
        this.paymentId = paymentId;
    }

    public String[] getPaymentLogo() {
        return paymentLogo;
    }

    public void setPaymentLogo(String[] paymentLogo) {
        this.paymentLogo = paymentLogo;
    }

    public String[] getPaymentGateway() {
        return paymentGateway;
    }

    public void setPaymentGateway(String[] paymentGateway) {
        this.paymentGateway = paymentGateway;
    }

    public String getPackageName() {
        return packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }
}
