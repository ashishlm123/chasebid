package com.emts.chasebid.model;

import java.io.Serializable;

/**
 * Created by User on 2016-07-01.
 */
public class Transaction implements Serializable{

    private String invoiceId;
    private String auctionId;
    private String transName;
    private String transType;
    private String date;
    private String amount;
    private String receivedNormalCredit;
    private String receivedBonusCredit;
    private String receivedFreeCredit;
    private String creditUsed;
    private String creditTypeUsed;
    private String status;
    private String bidPoint;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getAuctionId() {
        return auctionId;
    }

    public void setAuctionId(String auctionId) {
        this.auctionId = auctionId;
    }

    public String getTransName() {
        return transName;
    }

    public void setTransName(String transName) {
        this.transName = transName;
    }

    public String getTransType() {
        return transType;
    }

    public void setTransType(String transType) {
        this.transType = transType;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public String getCreditUsed() {
        return creditUsed;
    }

    public void setCreditUsed(String creditUsed) {
        this.creditUsed = creditUsed;
    }

    public String getCreditTypeUsed() {
        return creditTypeUsed;
    }

    public void setCreditTypeUsed(String creditTypeUsed) {
        this.creditTypeUsed = creditTypeUsed;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getReceivedNormalCredit() {
        return receivedNormalCredit;
    }

    public void setReceivedNormalCredit(String receivedNormalCredit) {
        this.receivedNormalCredit = receivedNormalCredit;
    }

    public String getReceivedBonusCredit() {
        return receivedBonusCredit;
    }

    public void setReceivedBonusCredit(String receivedBonusCredit) {
        this.receivedBonusCredit = receivedBonusCredit;
    }

    public String getReceivedFreeCredit() {
        return receivedFreeCredit;
    }

    public void setReceivedFreeCredit(String receivedFreeCredit) {
        this.receivedFreeCredit = receivedFreeCredit;
    }

    public String getBidPoint() {
        return bidPoint;
    }

    public void setBidPoint(String bidPoint) {
        this.bidPoint = bidPoint;
    }
}
