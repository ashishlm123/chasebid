package com.emts.chasebid.model;

import java.io.Serializable;

/**
 * Created by Srijana on 3/29/2017.
 */

public class Redeem implements Serializable {
    private String bonuspoint;
    private String bid;
    private boolean isChecked;
    private String id;


    public String getBonuspoint() {
        return bonuspoint;
    }

    public void setBonuspoint(String bonuspoint) {
        this.bonuspoint = bonuspoint;
    }

    public String getBid() {
        return bid;
    }

    public void setBid(String bid) {
        this.bid = bid;
    }

    public boolean isChecked() {
        return isChecked;
    }

    public void setChecked(boolean checked) {
        isChecked = checked;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }


}
