package com.emts.chasebid.model;

import com.emts.chasebid.helper.PreferenceHelper;

/**
 * Created by Srijana on 3/28/2017.
 */

public class VoteProduct extends Product {
    public static final int VOTE_STATUS_NEGATIVE=0;
    public static final int VOTE_STATUS_POSITIVE=1;
    public static final int VOTE_STATUS_NOT_RATE=2;

    private int voteStatus=2;

    public int getVoteStatus() {
        return voteStatus;
    }

    public void setVoteStatus(int voteStatus) {
        this.voteStatus = voteStatus;
    }
}
