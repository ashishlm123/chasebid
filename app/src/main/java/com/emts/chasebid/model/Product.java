package com.emts.chasebid.model;

import java.io.Serializable;

/**
 * Created by User on 2016-03-09.
 */
public class Product extends BidCredit implements Serializable {
    private String pName;
    private double pPrice;
    private long pId;
    private long winnerId;
    private String imageUrl;
    private String itemType;
    private int noOfBids;
    private int bidFee;
    private String currentTime;
    private long endTime;
    private long startTime;
    private String shortDesc;
    private String desc;
    private String productCode;
    private String feature;
    private String status;
    private int expireTime;
    private boolean isFavourite;
    private String favTime;
    private boolean is_buy_now;


    private String posiviteRating;
    private String negativeRating;
    private boolean is_liked;

    private float minBid = 0;
    private int maxBid;

    //order detail
    private String shippingCost;
    private String totalCost;

    public String getCurrentTime() {
        return currentTime;
    }

    public void setCurrentTime(String currentTime) {
        this.currentTime = currentTime;
    }

    public int getExpireTime() {
        return expireTime;
    }

    public void setExpireTime(int expireTime) {
        this.expireTime = expireTime;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getpName() {
        return pName;
    }

    public void setpName(String pName) {
        this.pName = pName;
    }

    public double getpPrice() {
        return pPrice;
    }

    public void setpPrice(double pPrice) {
        this.pPrice = pPrice;
    }

    public long getpId() {
        return pId;
    }

    public void setpId(long pId) {
        this.pId = pId;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public String getItemType() {
        return itemType;
    }

    public void setItemType(String itemType) {
        this.itemType = itemType;
    }

    public int getNoOfBids() {
        return noOfBids;
    }

    public void setNoOfBids(int noOfBids) {
        this.noOfBids = noOfBids;
    }

    public long getEndTime() {
        return endTime;
    }

    public void setEndTime(long endTime) {
        this.endTime = endTime;
    }





    public int getMaxBid() {
        return maxBid;
    }

    public void setMaxBid(int maxBid) {
        this.maxBid = maxBid;
    }

    public long getStartTime() {
        return startTime;
    }

    public void setStartTime(long startTime) {
        this.startTime = startTime;
    }

    public String getShortDesc() {
        return shortDesc;
    }

    public void setShortDesc(String shortDesc) {
        this.shortDesc = shortDesc;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getFeature() {
        return feature;
    }

    public void setFeature(String feature) {
        this.feature = feature;
    }

    public boolean isFavourite() {
        return isFavourite;
    }

    public void setFavourite(boolean favourite) {
        isFavourite = favourite;
    }

    public String getShippingCost() {
        return shippingCost;
    }

    public void setShippingCost(String shippingCost) {
        this.shippingCost = shippingCost;
    }

    public String getTotalCost() {
        return totalCost;
    }

    public void setTotalCost(String totalCost) {
        this.totalCost = totalCost;
    }

    public long getWinnerId() {
        return winnerId;
    }

    public void setWinnerId(long winnerId) {
        this.winnerId = winnerId;
    }

    public String getFavTime() {
        return favTime;
    }

    public void setFavTime(String favTime) {
        this.favTime = favTime;
    }

    public int getBidFee() {
        return bidFee;
    }

    public void setBidFee(int bidFee) {
        this.bidFee = bidFee;
    }

    public boolean is_buy_now() {
        return is_buy_now;
    }

    public void setIs_buy_now(boolean is_buy_now) {
        this.is_buy_now = is_buy_now;
    }

    public float getMinBid() {
        return minBid;
    }

    public void setMinBid(float minBid) {
        this.minBid = minBid;
    }

    public String getPosiviteRating() {
        return posiviteRating;
    }

    public void setPosiviteRating(String posiviteRating) {
        this.posiviteRating = posiviteRating;
    }

    public String getNegativeRating() {
        return negativeRating;
    }

    public void setNegativeRating(String negativeRating) {
        this.negativeRating = negativeRating;
    }


    public boolean is_liked() {
        return is_liked;
    }

    public void setIs_liked(boolean is_liked) {
        this.is_liked = is_liked;
    }
}
